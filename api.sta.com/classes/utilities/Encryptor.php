<?php
/****************************************************
 * Collection of functions for encrypting passwords *
 ****************************************************/
class Encryptor {
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Encryptor class: method '.$name.'() does not exist');
	} //call()
	
	/***
	 * Create a SHA256 hash for a password
	 * @params: a string of password (plain), a string of salt (plain)
	 * @return: a string in the format of "ALGORITHM:ITERATIONS:SALT:HASH".
	 * Note: the return salt and hash are base64-encoded.
	 * Note: to hash a time-sensitive code, use the start time as salt.
	 ***/
	static function getHash($password, $salt=NULL) {
		// Settings
		$hash_algo = 'sha256';
		$salt_bytes = 24;
		$iterations = 1000;
		$hash_bytes = 24;
		
		// Generate Salt
		if (is_null($salt)) {
			$salt = base64_encode (mcrypt_create_iv ($salt_bytes, MCRYPT_DEV_URANDOM));
		}
		else {
			$salt = str_pad($salt, $salt_bytes, '0', STR_PAD_LEFT);
			$salt = base64_encode (substr($salt, 0, $salt_bytes));
		} // custom salt: make sure it's raw 24bytes.

		// Generate Key
		$hash = base64_encode (Encryptor::pbkdf2 (
			$hash_algo,
			$iterations,
			$salt,
			$password,
			$hash_bytes
		));

		return $hash_algo.':'.$iterations.':'.$salt.':'.$hash;
	} //getHash()
	
	/***
	 * Checks that a time-sensitive code is correct by comparing its hash with a known good hash
	 * - the UNIX timestamp of the good_hash is stored as the salt, so we can compare the time to know if this code has expired.
	 * - Compares the hashes in length-constant time.
	 * @param: string of code, e.g. "96818637123456"
	 * @param: string of hash, e.g. "sha256:1000:MRmvW6PJzU1flQwwbQTpBncwdPaL1hS2:UZOErhJE4sRYqgbTaPCEadcnJUNo2m8d"
	 * @param: int of milliseconds, e.g. 300000 (for 5mins)
	 * @return: bool
	 ***/
	static function checkTimeSensitiveCode($code, $good_hash, $time_allowed) {
		//Check params for errors
		$params = explode(':', $good_hash);
		if(count($params) < 4) return FALSE;
		
		$good_salt =  base64_decode($params[2]);
		$good_pbkdf2 = base64_decode($params[3]);
		
		//Check for expiry
		if (getTimeInMs() > ($good_salt + $time_allowed)) {
			Log::warning('' . getTimeInMs() . ' is > ' . $good_salt . ' + ' . $time_allowed . ' (' . ($good_salt + $time_allowed) .  ')');
			return FALSE;
		}

		//Check hash
		$unkn_pbkdf2 = Encryptor::pbkdf2 (
			$params[0],			//algo
			(int)$params[1],	//count
			$params[2],			//salt
			$code,			//password
			strlen($good_pbkdf2) //length
		);
		
		//Run time-constant equal check (slow equals)
		$diff = strlen($good_pbkdf2) ^ strlen($unkn_pbkdf2);
		for($i = 0; $i < strlen($good_pbkdf2) && $i < strlen($unkn_pbkdf2); $i++) {
			$diff |= ord($good_pbkdf2[$i]) ^ ord($unkn_pbkdf2[$i]);
		}
		return $diff === 0;
	} //checkTimeSensitiveCode()
	
	/***
	 * Checks that a password is correct by comparing its hash with a known good hash
	 * - Compares the hashes in length-constant time.
	 * @param: string of ASCII password (plain), string of good_hash
	 * @return: TRUE on correct, FALSE otherwise
	 ***/
	static function checkPassword($password, $good_hash) {
		//Check params for errors
		$params = explode(':', $good_hash);
		if(count($params) < 4) return FALSE;
			
		$good_pbkdf2 = base64_decode($params[3]);
		$unkn_pbkdf2 = Encryptor::pbkdf2 (
			$params[0],			//algo
			(int)$params[1],	//count
			$params[2],			//salt
			$password,			//password
			strlen($good_pbkdf2) //length
		);
		
		//Run time-constant equal check (slow equals)
		$diff = strlen($good_pbkdf2) ^ strlen($unkn_pbkdf2);
		for($i = 0; $i < strlen($good_pbkdf2) && $i < strlen($unkn_pbkdf2); $i++) {
			$diff |= ord($good_pbkdf2[$i]) ^ ord($unkn_pbkdf2[$i]);
		}
		return $diff === 0;
	} //checkPassword()
	
	/***
	 * PBKDF2 (Password-Based Key Derivation Function 2) function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
	 * Test vectors can be found at: https://www.ietf.org/rfc/rfc6070.txt
	 *
	 * This implementation of PBKDF2 was originally created by https://defuse.ca
	 * With improvements by http://www.variations-of-shadow.com
	 *
	 * Params:
	 *     $algorithm - The hash algorithm to use. Recommended: SHA256
	 *     $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
	 *     $salt - A salt that is unique to the password (base64 encoded)
	 *     $password - The password.
	 *     $key_length - The length of the derived key in bytes.
	 * Returns: A $key_length-byte key derived from the password and salt.
	 ***/
	private static function pbkdf2($algorithm, $count, $salt, $password, $key_length) {
		//Check params for errors
		if(!in_array($algorithm, hash_algos(), true)) {
			throw new Exception('Error in Encryptor pbkdf2(): Invalid hash algorithm '.$algorithm);
		}
		if($count <= 0 || $key_length <= 0) {
			throw new Exception('Error in Encryptor pbkdf2(): Invalid parameters:'.$algorithm.' , '.$count.' , '.$salt.' , '.$password.', '.$key_length.' , '.$raw_output);
		}
		
		$hash_length = strlen(hash($algorithm, '', true));
		$block_count = ceil($key_length / $hash_length);
			
		$output = "";
		for($i = 1; $i <= $block_count; $i++) {
			//$i encoded as 4 bytes, big endian.
			$last = $salt . pack("N", $i);
			
			//first iteration
			$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
			
			//perform the other iterations
			for ($j = 1; $j < $count; $j++) {
				$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
			}
			$output .= $xorsum;
		}
			
		return substr($output, 0, $key_length);
	} //pbkdf2()
	
	/***
	 * Generates a random numeric code, e.g. '481067'
	 * @param: (optional) int of length, default: 6
	 * @return: string
	 ***/
	static function generateRandomNumericCode($length=6){
		$alphabet = "0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomNumericCode()

	static function generateRandomNumericOperatorCode($length=6){
		$alphabet = "0-1234-56789-";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
			$n = rand(0, $alphaLength);
			$code[] = $alphabet[$n];
		}

		return implode($code); // convert to array first
	} //generateRandomNumericCode()
	
	/***
	 * Generates a random alphanumeric code, e.g. "Fgh4Yu9Q"
	 * @param: (optional) int of length, default: 8
	 * @return: string
	 ***/
	static function generateRandomAlphanumericCode($length=8){
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomAlphanumericCode()
	
	/***
	 * Generates a random uppercase alphanumeric code, e.g. "FG3TY74Q"
	 * @param: (optional) int of length, default: 8
	 * @return: string
	 ***/
	static function generateRandomUpperCaseAlphanumericCode($length=8){
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomUpperCaseAlphanumericCode()
	
	/***
	 * Generates a random password using non-capital alphabetical letters and numbers
	 * Doesn't use digits 0 and 1 to prevent confusion with I and O.
	 * @param: (optional) int of length, default: 8
	 * @return: string, e.g. 'j0de8zqr'
	 ***/
	static function generateRandomPassword($length=8) {
		$alphabet = "abcdefghijklmnopqrstuwxyz23456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		
		return implode($pass); // convert to array first
	} //generateRandomPassword()
	
	/***
	 * Generates a random UPPERCASE code, e.g. 'SRQSPGAS'
	 * @param: (optional) int of length, default: 8
	 * @return: string
	 ***/
	static function generateRandomUpperCaseCode($length=8) {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomUpperCaseCode()
	
	/***
	 * Generates a random lowercase code, e.g. "bhwcdqas"
	 * @param: (optional) int of length, default: 8
	 * @return: string
	 ***/
	static function generateRandomLowerCaseCode($length=6) {
		$alphabet = "abcdefghijklmnopqrstuwxyz";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomLowerCaseCode()
	
	/***
	 * Generates a random mix-case (alphabetic) code, e.g. "FghTYuaQ"
	 * @param: (optional) int of length, default: 8
	 * @return: string
	 ***/
	static function generateRandomMixCaseCode($length=8) {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomMixCaseCode()
} // class Encryptor