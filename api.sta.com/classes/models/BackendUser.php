<?php
class BackendUser {
    // fields
    private $ID;
    private $Email;
    private $FirstName;
    private $LastName;
    private $Phone;
    private $PasswordHash;		// string: PBKDF2 hash of password and random salt. (DEFAULT: NULL)
    private $PersistentLoginHashes; // string: PBKDF2 hashes of "persistent login cookies" (DEFAULT: '')
    private $VerificationCode; // string: PBKDF2 hashes of "persistent login cookies" (DEFAULT: '')
    private $LastUpdatedDateTime; 		// int timestamp (UNIX Timestamp) (DEFAULT: now)
    private $RoleID;
    private $Status;



    // Default function: checks if this class has a property
    static function hasProperty($a) {
        return property_exists('BackendUser', $a);
    } //hasProperty()

    // Default function: make invalid method calls throw Exceptions
    function __call($name, $arguments) {
        throw new Exception ('Error in User BackendUser: method '.$name.'() does not exist');
    } //call()

    // Default function: Converts all data members into array
    function toArray() {
        return get_object_vars($this);
    } //toArray()

    // Converts pre-selected data members into array
    function toOutputArray() {
        // Take everything except sensitive stuff (password)
        $array = get_object_vars($this);
        unset($array['passwordHash']);
        unset($array['persistentLoginHashes']);

        // Add user type
        // $array['userType'] = 'Backend User';
        return $array;
    } //toOutputArray()

    /***********
     * Constructor takes in an array of args, and maps each arg into a data member
     * Only data members defined by the User class will be processed; unrecognised args will be ignored.
     ***********/
    function __construct($arg) {

        if (!is_array($arg)) throw new Exception('Error creating BackendUser: arg is not array');
        foreach ($arg as $key => $value) {
            if (property_exists('BackendUser', $key)) {
                $this->$key = $value;
            } //property exists
        } //parse each arg in the array

        $this->checkBackendUserData();
    } //construct()

    /***********
     * Updates User with an array of new data (e.g. from $_POST)
     * Note: only data members defined by the User class will be processed; unrecognised args will be ignored.
     *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
     * Params: an array of new data (key => value)
     * Return: TRUE on success update, FALSE if no changes were made
     ***********/
    function update($arg) {
        if (!is_array($arg)) throw new Exception('Error updating Staff: arg is not array');

        //$hasChanged = parent::update($arg);
        foreach ($arg as $key => $value) {
            if (property_exists('BackendUser', $key)) {
                if ((string) $this->$key != $value) {
                    //Log::test($key.' changed from '.$this->$key.' to '.$value);
                    $this->$key = $value;
                    $hasChanged = true;
                } //update only if different
            } //property exists
        } //parse each arg in the array

        $this->checkBackendUserData();

        return $hasChanged;
    } //update()

    // Table that this model should be stored
    static function getTableName() {
        return 'sta_BackendUser';
    } //getTableName()

    /*
     * Checks that each data member is valid:
     * - throws Exceptions when critical data is invalid
     * - sets other compulsory fields with invalid data to their default values
     * - sets optional fields with invalid data to NULL
     */
    private function checkBackendUserData() {
        if (!isUnsignedInt($this->ID)){
            $this->ID = 0;
        }
        else {
            $this->ID = (int) $this->ID;
        }

//        if (!isValidEmail($this->Email)) {
//            throw new Exception('Error in Staff data: invalid email: '.$this->Email);
//        }

        if (!isset($this->FirstName) || strlen(trim($this->FirstName)) == 0){
            $this->FirstName = NULL;
        }
        if (!isset($this->LastName) || strlen(trim($this->LastName)) == 0){
            $this->LastName = NULL;
        }

        if (!isset($this->PersistentLoginHashes)){
            $this->PersistentLoginHashes = '';
        }
        if (!isUnsignedInt($this->LastUpdatedDateTime)) {
            $this->LastUpdatedDateTime = getTimeInMs();
        }
        else {
            $this->LastUpdatedDateTime = (int) $this->LastUpdatedDateTime;
        }

        if (!isset($this->RoleID)){
            $this->RoleID = 0;
        }
        else {
            $this->RoleID = (int) $this->RoleID;
        }


        if ((isset($this->Status) && $this->Status != 'Delete' && $this->Status != 'Disabled')){
                $this->Status = 'Active';
        }

    } //checkBackendUserData()

    //For DB to set ID after creation
    function setID($input) {
        if (!isUnsignedInt($input)){
            throw new Exception ('Error in Backend User setID: invalid id');
        }
        $this->ID = (int) $input;
    } //setID()

    function IsDeleted(){
        if($this->Status =='Delete')
            return true;
        else
            return false;
    }

    /*******************
     *   MODEL LOGIC   *
     *******************/
    /***
     * Compares an input password with the one stored in PasswordHash
     * @return: TRUE on same, FALSE otherwise
     ***/
    function checkPassword($password) {
        return Encryptor::checkPassword($password, $this->PasswordHash);
    }

    /***
     * Tries to find and replace a persistent login cookie
     * - will also purge old persistent login cookies that are expired.
     * @param: string of cookie
     * @return: string of new cookie on success, FALSE on failure
     ***/
    function renewPersistentLoginCookie($code) {
        //Log::debug('User::renewPersistentLoginCookie() - checking '.$code);

        if (strlen($this->PersistentLoginHashes) == 0) {
            return FALSE;
        }
        $hashes = str_getcsv($this->PersistentLoginHashes);
        if (!is_array($hashes)) {
            return FALSE;
        }

        // Check each hash
        foreach ($hashes as $idx => $hash) {
            // Check hash consistency
            $params = explode(':', $hash);
            if(count($params) < 3) {
                unset($hashes[$idx]);
                $this->PersistentLoginHashes = array2csv($hashes);
                continue;
            }

            // Check expiry
            $salt = base64_decode($params[2]);
            $time = getTimeInMs();
            if ($time > ($salt + PERSISTENT_LOGIN_PERIOD)) {
                unset($hashes[$idx]);
                $this->PersistentLoginHashes = array2csv($hashes);
                continue;
            }

            // Check correctness
            if (Encryptor::checkTimeSensitiveCode($code, $hash, PERSISTENT_LOGIN_PERIOD)) {
                // Generate new cookie
                $new_cookie = Encryptor::generateRandomAlphanumericCode(PERSISTENT_LOGIN_COOKIE_LENGTH);
                $new_hash = Encryptor::getHash($new_cookie, $time);
                $this->PersistentLoginHashes = str_replace($hash, $new_hash, $this->PersistentLoginHashes);
                return $new_cookie;
            }
        } //for each hash

        return FALSE;
    } //renewPersistentLoginCookie()

    /***
     * Generates a new persistent login cookie and store it in the user's list of cookies.
     * - will kick out the first cookie hash if it exceeds DB schema size limit
     * @return: string of cookie
     ***/
    function generatePersistentLoginCookie() {
        $time = getTimeInMs();
        $new_cookie = Encryptor::generateRandomAlphanumericCode(PERSISTENT_LOGIN_COOKIE_LENGTH);
        $new_hash = Encryptor::getHash($new_cookie, $time);

        if (strlen($this->PersistentLoginHashes) == 0) {
            $this->PersistentLoginHashes = $new_hash;
        }
        else if ((strlen($this->PersistentLoginHashes) + strlen($new_hash)) > 2000) { // DB schema is for 2000 char
            $first_comma_idx = strpos($this->PersistentLoginHashes, ',');
            $this->PersistentLoginHashes = substr($this->PersistentLoginHashes, $first_comma_idx+1);
            $this->PersistentLoginHashes .= ','.$new_hash;
        }
        else {
            $this->PersistentLoginHashes .= ','.$new_hash;
        }

        return $new_cookie;
    } //generatePersistentLoginCookie()

    /***************
     *   GETTERS   *
     ***************/
    function getID() {
        return $this->ID;
    }
    function getPasswordHash() {
        return $this->PasswordHash;
    }

    function getPersistentLoginHashes() {
        return $this->PersistentLoginHashes;
    }
    function getLastUpdatedDateTime() {
        return $this->LastUpdatedDateTime;
    }
    function getRoleID() {
        return $this->RoleID;
    }
    function isActive() {
        return $this->Status;
    }
} //class User
