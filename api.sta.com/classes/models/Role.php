<?php
class Role {
    // fields
    private $ID;
    private $Name;
    private $LastUpdatedDateTime;
    private $Status;

    // Default function: checks if this class has a property
    static function hasProperty($a) {
        return property_exists('Role', $a);
    } //hasProperty()

    // Default function: make invalid method calls throw Exceptions
    function __call($name, $arg) {
        throw new Exception ('Error in Role class: method '.$name.'() does not exist');
    } //call()

    // Default function: make this a printable string
    function __toString() {
        return get_class($this).' '.$this->ID;
    } //toString()

    // Default function: converts ALL data members into array
    function toArray() {
        return get_object_vars($this);
    } //toArray()

    // Converts pre-selected data members into array
    function toOutputArray() {
        return get_object_vars($this);
    }

    // Table that this model should be stored
    static function getTableName() {
        return 'sta_Role';
    } //getTableName()

    /***********
     * Constructor takes an array of args, and maps each arg into a data member
     * Only data members defined by this class will be processed; unrecognised args will be ignored.
     ***********/
    function __construct($arg) {
        if (!is_array($arg)) throw new Exception('Error creating Role: arg is not array');

        foreach ($arg as $key => $value) {
            if (property_exists('Role', $key)) {
                $this->$key = $value;
            } //property exists
        } //parse each arg in the array

        $this->checkRoleData();
    } //construct()

    /***********
     * Updates Role with an array of new data (e.g. from $_POST)
     * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
     *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
     * Params: an array of new data (key => value)
     * Return: TRUE on success update, FALSE if no changes were made
     ***********/
    function update($arg) {
        if (!is_array($arg)) throw new Exception('Error updating Role: arg is not array');

        foreach ($arg as $key => $value) {
            if (property_exists('Role', $key)) {
                if ((string) $this->$key != $value) {
                    //Log::test($key.' changed from '.$this->$key.' to '.$value);
                    $this->$key = $value;
                    $hasChanged = true;
                } //update only if different
            } //property exists
        } //parse each arg in the array

        $this->checkRoleData();

        return $hasChanged;
    } //update()

    /*
     * Checks that each data member is valid:
     * - throws Exceptions when critical data is invalid
     * - sets other compulsory fields with invalid data to their default values
     * - sets optional fields with invalid data to NULL
     */
    private function checkRoleData() {
        if (!isUnsignedInt($this->ID)){
            $this->ID = 0;
        }
        else {
            $this->ID = (int) $this->ID;
        }

        if (!isset($this->Name) || strlen($this->Name)==0) {
            throw new Exception('Error in Role data: Name not set');
        }
        if (!isset($this->Status)) {
            $this->Status ='Active';
        }
        if (!isUnsignedInt($this->LastUpdatedDateTime)) {
            $this->LastUpdatedDateTime = getTimeInMs();
        }
        else {
            $this->LastUpdatedDateTime = (int) $this->LastUpdatedDateTime;
        }


    } //checkRoleData()
    function IsDeleted(){
        if($this->Status =='Delete')
            return true;
        else
            return false;
    }


    /*******************
     *   MODEL LOGIC   *
     *******************/
    //For DB to set ID after creation
    function setID($input) {
        if (!isUnsignedInt($input)){
            throw new Exception ('Error in Role setID: invalid id');
        }
        $this->ID = (int) $input;
    } //setID()

    /***************
     *   GETTERS   *
     ***************/
    function getID() {
        return $this->ID;
    }

    function getName() {
        return $this->Name;
    }

    function getStatus() {
        return $this->Status;
    }
    function getLastUpdatedDateTime() {
        return $this->LastUpdatedDateTime;
    }
} //class Role