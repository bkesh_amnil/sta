<?php
// Record of things done on the system, e.g. Staff login, logout, product added, category edited
class Activity {
	// Compulsory fields
	private $ID;					// int (DEFAULT: 0)
	private $UserID;			// int timestamp (UNIX Timestamp) (DEFAULT: now)
	private $UserType;			// int timestamp (UNIX Timestamp) (DEFAULT: now)
	private $Action;				// int
	private $ModelType;				// string (class name)
	private $CreatedDateTime;				// string (class name)

	// Optional fields (DEFAULT: NULL)
	private $OldValue;				// string (class name)
	private $NewValue;              // string
	private $ModelID;				// int

	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Activity', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Activity class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->ID;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		// Include everything
		return get_object_vars($this);
	} //toOutputArray()
	
	// Table that this model should be stored
	static function getTableName() {
		return 'sta_ActivityLog';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Activity: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Activity', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkActivityData();
	} //construct()
	
	/***********
	 * Updates model with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Activity: arg is not array');
		
		//$hasChanged = parent::update($arg);
		foreach ($arg as $key => $value) {
			if (property_exists('Activity', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkActivityData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkActivityData() {	
		if (!isUnsignedInt($this->ID)){
			$this->ID = 0;
		}
		else {
			$this->ID = (int) $this->ID;
		}
		
		if (!isUnsignedInt($this->CreatedDateTime)) {
			$this->CreatedDateTime = getTimeInMs();
		}
		else {
			$this->CreatedDateTime = $this->CreatedDateTime;   //#bkesh : remmoved (int)
		}
		
		if (!isUnsignedInt($this->UserID)) {
		 	throw new Exception('Error in Activity data: userID not set');
		}
		else {
			$this->UserID = (int) $this->UserID;
		}

		
		if (!isset($this->Action) || strlen(trim($this->Action)) == 0) {
		 	throw new Exception('Error in Activity data: action not set');
		}
		
		if (!isUnsignedInt($this->ModelID)){
			$this->ModelID = NULL;
		}
		else {
			$this->ModelID = (int) $this->ModelID;
		}
		
		if (!isset($this->ModelType) || strlen(trim($this->ModelType)) == 0) {
		 	$this->ModelType = NULL;
		}

		if (!isset($this->UserType) || strlen(trim($this->UserType)) == 0) {
			$this->UserType = NULL;
		}

		if (!isset($this->OldValue)) {
			$this->OldValue = NULL;
		}

		if (!isset($this->NewValue)) {
			$this->NewValue = NULL;
		}
	} //checkActivityData()
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Activity setID: invalid id');
		}
		$this->ID = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->ID;
	}

	
	
	function getCreatedDateTime() {
		return $this->CreatedDateTime;
	}
	
	function getBackendUserID() {
		return $this->UserID;
	}

	function getOldValue() {
		return $this->OldValue;
	}

	function getNewValue() {
		return $this->NewValue;
	}

	function getAction() {
		return $this->Action;
	}
	
	function getModelID() {
		return $this->ModelID;
	}
	
	function getModelType() {
		return $this->ModelType;
	}
	function getUserType() {
		return $this->UserType;
	}
} // class Activity
