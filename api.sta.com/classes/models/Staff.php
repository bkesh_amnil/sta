<?php
class Staff extends User {
	// Compulsory fields
	private $ID;					// int (DEFAULT: 0)
	private $Email;					//Estring
	
	// Optionsl fields
	private $FirstName;
        private $Phone;				// string
	private $LastName;				// string
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Staff', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Staff class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->ID;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		// Take everything except sensitive stuff (password)
		$array = get_object_vars($this);
		unset($array['passwordHash']);
		unset($array['persistentLoginHashes']);
		
		
		// Add user type
		// $array['userType'] = 'Backend User';
		return $array;
	} //toOutputArray()
	
	// Table that this model should be stored
	static function getTableName() {
		return 'sta_BackendUser';
	} //getTableName()
	
	//Gets the default access level of this class of User (for use during parent object creation)
	static function getDefaultAccessLevel() {
		return GENERAL_STAFF_ACCESS_LEVEL;
	} //getDefaultAccessLevel()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Staff: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Staff', $key)) {
				$this->$key = $value;

				

			} //property exists
		} //parse each arg in the array

		$this->checkUserData();
		$this->checkStaffData();
	} //construct()
	
	/***********
	 * Updates model with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Staff: arg is not array');
		
		//$hasChanged = parent::update($arg);
		foreach ($arg as $key => $value) {
			if (property_exists('Staff', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkUserData();
		$this->checkStaffData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkStaffData() {		
		if (!isUnsignedInt($this->ID)){ 
			$this->ID = 0;
		}
		else {
			$this->ID = (int) $this->ID;
		}
		
		if (!isValidEmail($this->Email)) {
			throw new Exception('Error in Staff data: invalid email: '.$this->Email);
		}
		
		if (!isset($this->FirstName) || strlen(trim($this->FirstName)) == 0){
		 	$this->FirstName = NULL;
		}
		if (!isset($this->LastName) || strlen(trim($this->LastName)) == 0){
		 	$this->LastName = NULL;
		}

		
	} //checkStaffData()
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Staff setID: invalid id');
		}
		$this->ID = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->ID;
	}
	function getFirstName() {
		return $this->FirstName;
	}
	function getLastName() {
		return $this->LastName;
	}
	function getName() {
		if (is_null($this->FirstName) && is_null($this->LastName)) {
			return NULL;
		}
		return $this->FirstName . ' ' . $this->LastName;
	}
	function getEmail() {
		return $this->Email;
	}

	
	function getUserType() {
		return 'Staff';
	}
} //class Staff