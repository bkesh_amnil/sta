<?php
class RoleDetail {
    // fields
    private $ID;
    private $RoleID;
    private $ModuleID;

    // Default function: checks if this class has a property
    static function hasProperty($a) {
        return property_exists('RoleDetail', $a);
    } //hasProperty()

    // Default function: make invalid method calls throw Exceptions
    function __call($name, $arg) {
        throw new Exception ('Error in RoleDetail class: method '.$name.'() does not exist');
    } //call()

    // Default function: make this a printable string
    function __toString() {
        return get_class($this).' '.$this->ID;
    } //toString()

    // Default function: converts ALL data members into array
    function toArray() {
        return get_object_vars($this);
    } //toArray()

    // Converts pre-selected data members into array
    function toOutputArray() {
        return get_object_vars($this);
    }

    // Table that this model should be stored
    static function getTableName() {
        return 'sta_RoleDetails';
    } //getTableName()

    /***********
     * Constructor takes an array of args, and maps each arg into a data member
     * Only data members defined by this class will be processed; unrecognised args will be ignored.
     ***********/
    function __construct($arg) {
        if (!is_array($arg)) throw new Exception('Error creating RoleDetail: arg is not array');

        foreach ($arg as $key => $value) {
            if (property_exists('RoleDetail', $key)) {
                $this->$key = $value;
            } //property exists
        } //parse each arg in the array

        $this->checkRoleDetailData();
    } //construct()

    /***********
     * Updates RoleDetail with an array of new data (e.g. from $_POST)
     * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
     *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
     * Params: an array of new data (key => value)
     * Return: TRUE on success update, FALSE if no changes were made
     ***********/
    function update($arg) {
        if (!is_array($arg)) throw new Exception('Error updating RoleDetail: arg is not array');

        foreach ($arg as $key => $value) {
            if (property_exists('RoleDetail', $key)) {
                if ((string) $this->$key != $value) {
                    //Log::test($key.' changed from '.$this->$key.' to '.$value);
                    $this->$key = $value;
                    $hasChanged = true;
                } //update only if different
            } //property exists
        } //parse each arg in the array

        $this->checkRoleDetailData();

        return $hasChanged;
    } //update()

    /*
     * Checks that each data member is valid:
     * - throws Exceptions when critical data is invalid
     * - sets other compulsory fields with invalid data to their default values
     * - sets optional fields with invalid data to NULL
     */
    private function checkRoleDetailData() {
        if (!isUnsignedInt($this->ID)){
            $this->ID = 0;
        }
        else {
            $this->ID = (int) $this->ID;
        }

    } //checkRoleDetailData()

    /*******************
     *   MODEL LOGIC   *
     *******************/
    //For DB to set ID after creation
    function setID($input) {
        if (!isUnsignedInt($input)){
            throw new Exception ('Error in RoleDetail setID: invalid id');
        }
        $this->ID = (int) $input;
    } //setID()

    /***************
     *   GETTERS   *
     ***************/
    function getID() {
        return $this->ID;
    }

    function getRoleID() {
        return $this->RoleID;
    }

    function getModuleID() {
        return $this->ModuleID;
    }

} //class RoleDetail