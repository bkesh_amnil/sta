<?php
/***
 * Some PHP versions don't allow multiple inheritance
 * and interfaces are really just "contracts" that document the methods
 * therefore actual implementation is optional
 * as long as all models have these methods
 ***/
interface iModel {
	static function hasProperty();
	static function getTableName();
	function __call($name, $arg);
	function __toString();
	function toArray();
	function toOutputArray();
	
	// Create / update
	function __construct($arg);
	function update($arg);
	
	// Mutators
	function setID($input);
	
	// Accessors (also add one for each data member)
	function getID();
}