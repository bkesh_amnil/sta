<?php
class Mail {
	private $from;				//email address: can be user@example.com or User <user@example.com>
	private $to;				//array of email addresses: can be user@example.com or User <user@example.com>
	private $cc;				//array of email addresses: can be user@example.com or User <user@example.com>
	private $bcc;				//array of email addresses: can be user@example.com or User <user@example.com>
	private $reply_to;			//email address that mail recipient can reply to
	private $return_path;		//email address for bounces
	private $subject;			//string
	private $priority;			//int (1 = HIGH, 3 = Normal, 5 = low)
	private $content;			/* multi-dimensional array of contents, e.g.
								 * content['type'] => 'multipart/mixed'
								 * content[0]['type'] => 'multipart/alternative'
 								 * content[0][0]['type'] => 'text/plain'			//Note: always attach plain-text first
								 * content[0][0]['file'] => 'sample_mail.txt'
								 * content[0][1]['type'] => 'text/html'
								 * content[0][1]['file'] => 'sample_mail.html'
								 * content[1]['type'] => 'application/octet-stream'
								 * content[1]['file'] => 'attachment.csv'
								 *
			 					 * content-type can be:
								 * 	text/plain				: plain text portions
								 *	text/html				: HTML portions
								 *	multipart/alternative	: let client choose (between plain/HTML)
								 *	multipart/mixed			: mail with attachments
								 *	application/octet-stream: attachments
								 */
	private $mime_boundary;		//required for multipart mails
	
	private $variable;			//array of variables used by email templates.
	
	//Checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Mail', $a);
	} //hasProperty()
	
	//Default function: make invalid method calls throw Exceptions
	function __call($name, $arguments) {
		throw new Exception ('Error in Mail class: method '.$name.'() does not exist');
	} //call()
	
	function __construct() {
		$this->mime_boundary = 'MULTIPART_BOUNDARY_'.md5(time());
		$this->content = array();
		
		//Defaults
		$this->to= array();
		$this->cc = array();
		$this->bcc = array();
		$this->from = WEBMASTER_EMAIL;
		$this->reply_to = WEBMASTER_EMAIL;
		$this->return_path = WEBMASTER_EMAIL;
		$this->priority = 3;
	} //construct()
	
	/*
	 * Adds a plaintext portion to the email
	 * - Note: Should be called before adding any attachments.
	 * - Params: String containing absolute path to file
	 * - Return: none
	 */
	function addTextMail($a) {
		$plain_part['type'] = 'text/plain';
		$plain_part['file'] = $a;
		
		if (empty($this->content)) {
			$this->content = $plain_part;
		} //No content yet
		else if ($this->content['type'] == 'text/html') {
			$html_part = $this->content;
			$this->content = array(
				'type' => 'multipart/alternative',
				0 => $plain_part,
				1 => $html_part
			);
		} //Already has an HTML mail
		else {
			throw new Exception ('Error in Mail addTextMail: already has an existing '. $this->content['type'].' mail');
		}
	} //addTextMail()
	
	/*
	 * Adds a html portion to the email
	 * - Note: Should be called before adding any attachments.
	 * - Params: String containing absolute path to file
	 * - Return: none
	 */
	function addHTMLMail($a) {
		$html_part['type'] = 'text/html';
		$html_part['file'] = $a;
		
		if (empty($this->content)) {
			$this->content = $html_part;
		} //No content yet
		else if ($this->content['type'] == 'text/plain') {
			$plain_part = $this->content;
			$this->content = array(
				'type' => 'multipart/alternative',
				0 => $plain_part,
				1 => $html_part
			);
		} //Already has an plaintext mail
		else {
			throw new Exception ('Error in Mail addHTMLMail: already has an existing '. $this->content['type'].' mail');
		}
	} //addHTMLMail()
	
	/*
	 * Adds an attachment to the email
	 * - Note: Should be called after setting the plaintext/html parts of the mail
	 * - Params: String containing absolute path to file
	 * - Return: none
	 */
	function addAttachment($a) {
		$attach_part['type'] = 'application/octet-stream';
		$attach_part['file'] = $a;
		
		if (empty($this->content)) throw new Exception ('Error in Mail addAttachment: content is empty!');
		if ($this->content['type'] == 'multipart/mixed') {
			//Append this one
			$this->content[] = $attach_part;
		} //Already has attachments
		else if ($this->content['type'] == 'text/plain' || $this->content['type'] == 'text/html' || $this->content['type'] == 'multipart/alternative') {
			$text_part = $this->content;
			$this->content = array(
				'type' => 'multipart/mixed',
			 	0 => $text_part,
				1 => $attach_part
			);
		} //Already has plain/html/alternative part
	} //addAttachment()
	
	/*
	 * Sends out the mail
	 * - constructs the headers
	 * - constructs the message
	 * - Note: does NOT mean the mail will actually reach the intended destination
	 * Return: TRUE if mail was accepted for delivery, FALSE otherwise
	 */
	function send() {
		//Construct headers
		$headers = $this->makeHeaders();
		$message = $this->makeMessage($this->content);
		$addressee = implode(',',$this->to);
		
		//send out the mail
		return mail($addressee, $this->subject, $message, $headers);
	} //send()
	
	/*
	 * To be called by the send() function
	 * Constructs headers for the mail
	 * - will set MIME boundaries accordingly
	 * - will set Date, From, To, Cc, Bcc, Reply-To, Return-Path headers
	 * Return: String containing the complete header
	 */
	private function makeHeaders() {
		$headers  = 'MIME-Version: 1.0' . "\n";
		if ($this->content['type'] == 'multipart/alternative') {
			$this->mime_boundary = 'ALT_'.$this->mime_boundary;
			$headers .= 'Content-Type: '.$this->content['type'].'; boundary="'.$this->mime_boundary.'"' . "\n";
		}
		else if ($this->content['type'] == 'multipart/mixed') {
			$this->mime_boundary = 'MIXED_'.$this->mime_boundary;
			$headers .= 'Content-Type: '.$this->content['type'].'; boundary="'.$this->mime_boundary.'"' . "\n";
		}
		else {
			$headers .= 'Content-Type: '.$this->content['type'].'; charset=utf-8' . "\n";
		}
		
		$headers .= 'Date: '.date('r', $_SERVER['REQUEST_TIME']) . "\n";
		$headers .= 'From: '.SITE_NAME.'<'.$this->from.'>' . "\n";
		//$headers .= 'To: '.implode(',',$this->to) . "\n";
		if (isset($this->cc)) {
			$headers .= 'Cc: '.implode(',',$this->cc) . "\n";
		}
		if (isset($this->bcc)) {
			$headers .= 'Bcc: '.implode(',',$this->bcc) . "\n";
		}
		if (isset($this->reply_to)) {
			$headers .= 'Reply-To: '.$this->reply_to . "\n";
		}
		if (isset($this->return_path)) {
			$headers .= 'Return-Path: '.$this->return_path . "\n";
		}
		//$headers .= 'Subject: '.$this->subject . "\n";
		$headers .= 'X-Mailer: PHP/'.phpversion() . "\n";
		$headers .= 'X-Priority: '.$this->priority . "\n";
		
		return $headers;
	} //makeHeaders()
	
	/*
	 * To be called by the send() function
	 * Constructs the message portion of the mail
	 * - will recursively add multipart mails
	 * Return: String containing the entire message
	 */
	private function makeMessage($content, $boundary = NULL) {
		if (is_null($boundary))
			$boundary = $this->mime_boundary;
		$msg ='';
		
		//remove the content_type from the array of contents
		$content_type = $content['type'];
		unset($content['type']);
		
		if ($content_type == 'multipart/alternative') {
			foreach ($content as $index => $part) {
				$msg .= "\n\n--" . $boundary . "\n";
				
				if ($part['type'] == 'text/html' || $part['type'] == 'text/plain') {
					$msg .= 'Content-Type:'. $part['type'] . ';charset=utf-8' . "\n";
					$msg .= 'Content-Transfer-Encoding: 7bit' . "\n\n";
					$msg .= $this->makeMessage($part) . "\n";
				}
				else throw new Exception ('Error in makeMessage(): unknown type in multipart/alternative '.$part['type']);
			} //add each message part
			$msg .= "\n\n--" . $boundary . "--\n";
		} //Multipart/Alternative
		
		else if ($content_type == 'multipart/mixed') {
			foreach ($content as $index => $part) {
				$msg .= "\n\n--". $boundary . "\n";
				
				if ($part['type'] == 'text/html' || $part['type'] == 'text/plain') {					
					$msg .= 'Content-Type: '. $part['type'] .';charset=utf-8' . "\n";
					$msg .= 'Content-Transfer-Encoding: 7bit' . "\n\n";
					$msg .= $this->makeMessage($part) . "\n";
				}
				else if ($part['type'] == 'multipart/alternative') {
					$part_boundary = 'ALT'.$index.'_'.$boundary;
					$msg .= 'Content-Type: multipart/alternative;boundary="'.$part_boundary.'"' . "\n\n";
					$msg .= $this->makeMessage($part, $part_boundary) . "\n\n";
				}
				else if ($part['type'] == 'application/octet-stream') {
					$msg .= 'Content-Type: '. $part['type'] .'; name="'.$part['file'].'"' . "\n";
					$msg .= 'Content-Transfer-Encoding: base64' ."\n";
					$msg .= 'Content-Disposition: attachment; filename="'.$part['file'].'"' . "\n\n";
					$msg .= $this->makeMessage($part) . "\n";
				}
				else throw new Exception ('Error in makeMessage(): no type set while parsing multipart/mixed ');
			} //add each message part
			$msg .= "\n\n--". $boundary . "--\n";
		} //Multipart/Mixed
		else if ($content_type == 'text/html') {
			//Read HTML file
			//return file_get_contents($content['file']);
			ob_start();
			include $content['file'];
			$content = ob_get_contents();
			ob_end_clean();
			
			return $content;
		} //HTML
		else if ($content_type == 'text/plain') {
			//Read text file
			//return file_get_contents($content['file']);
			ob_start();
			include $content['file'];
			$content = ob_get_contents();
			ob_end_clean();
			
			return $content;
		} 
		else if ($content_type == 'image/jpeg') {
			return $content['file'].' to be added as img';
		}
		else if ($content_type == 'application/octet-stream') {
			//Read attachment file
			$f_contents=file_get_contents ($content['file']);
			$f_contents=chunk_split (base64_encode ($f_contents)); //encode The Data For Transition using base64_encode();
			
			return $f_contents;
		} //attachment
		else throw new Exception ('Error in Mail class makeMessage(): content type "'.$content_type.'" unknown.');
		
		return $msg;
	} //makeBody()
	
	/***************
	 *   SETTERS   *
	 ***************/
	function setTo ($a) {
		if (is_array($a)) {
			$this->to = $a;
		}
		else if (is_string($a)) {
			$this->to = array ($a);
		}
		else throw new Exception ('Error in Mail class setTo(): expect arg of type array/string, but received '. gettype($a));
	} //setTo()
	function setFrom ($a) {
		if (is_string($a)) {
			$this->from = $a;
		}
		else throw new Exception ('Error in Mail class setFrom(): expect arg of type string, but received '. gettype($a));
	} //setFrom()
	function setCc($a) {
		if (is_array($a)) {
			$this->cc = $a;
		}
		else if (is_string($a)) {
			$this->cc = array ($a);
		}
		else throw new Exception ('Error in Mail class setCc(): expect arg of type array/string, but received '. gettype($a));
	} //setCc()
	function setBcc($a) {
		if (is_array($a)) {
			$this->bcc = $a;
		}
		else if (is_string($a)) {
			$this->bcc = array ($a);
		}
		else throw new Exception ('Error in Mail class setBcc(): expect arg of type array/string, but received '. gettype($a));
	} //setBcc()
	function setReplyTo ($a) {
		if (is_string($a)) {
			$this->reply_to = $a;
		}
		else throw new Exception ('Error in Mail class setReplyTo(): expect arg of type string, but received '. gettype($a));
	} //setReplyTo()
	
	function setReturnPath ($a) {
		if (is_string($a)) {
			$this->return_path = $a;
		}
		else throw new Exception ('Error in Mail class setReturnPath(): expect arg of type string, but received '. gettype($a));
	} //setReplyTo()
	function setContent($c) {
		$this->content = $c;
	} //setContent()
	function setSubject($s) {
		$this->subject = $s;
	} //setSubject()
	function setVariable($a) {
		$this->variable = $a;
	}
	
}//class Mail

//*/
