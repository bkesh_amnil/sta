<?php
class Page extends HTTPResponse {
	protected $name;
	protected $layout;				//array of absolute paths to HTML templates (note: sequence matters)
	protected $meta;				//array: each meta name is key of array.
	protected $og_meta;				//array: each meta name is key of array. // Open Graph eg og:title, og:description, og:url
	protected $lang = 'en';
	protected $title = '';
	protected $slug = '';			//For purpose of clean URLs (permalinks)
	protected $css;					//optional: array of filenames of css files, if page has its own css
	protected $js;					//optional: array of filenames of js files, if page has its own js
	protected $access_level = 0;	//Access levels required: 0 - public, higher levels are more restricted
	
	protected $load_fn;			//array of function names to execute on page load
	
	//Default function: make invalid method calls throw Exceptions
	function __call($name, $arguments) {
		//return parent::__call($name, $arguments);
		throw new Exception ('Error in Page class: method '.$name.'() does not exist');
	} //call()
	
	function __construct($a) {
		parent::__construct();
		
		if (!is_string($a)) throw new Exception ('Error in Page class construct(): expect arg of type string, but received '. gettype($a));
		
		$this->name = $a;
		$this->meta = array();
		$this->og_meta = array();
		$this->load_fn = array();
		$this->css = array();
		$this->js = array();
		$this->slug = $a;
	} //construct()
	
	/*
	 * Outputs the page (will include each HTML sequentially as specifiec in $layout)
	 * Params: none
	 * Return: none
	 */	
	function deliver() {
		header('HTTP/1.1 '.$this->response_code.' '.self::$http_response [$this->response_code]);
 
		// Check for CORS
		if (in_array($_SERVER['HTTP_ORIGIN'], self::$cors_allowed_origins)) {
			header('Access-Control-Max-Age: 1728000');
			header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
			header('Access-Control-Allow-Methods: OPTIONS, GET, POST');
			header('Access-Control-Allow-Credentials: true');
		}
		
		// Get the page content
		ob_start();
		if (is_readable($this->layout['content'])) include $this->layout['content'];
		$content = ob_get_clean();
		
		// Check the format requested (MIME content-types)
		$format = 'text/html';	//default to HTML
		if (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) $format = 'application/json';
		else if (strpos($_SERVER['HTTP_ACCEPT'], 'application/javacsript') !== false) $format = 'application/javascript';
		else if (strpos($_SERVER['HTTP_ACCEPT'], 'text/xml') !== false) $format = 'text/xml';
		
		// Deliver according to requested return format. Note: skips layout files if not HTML
		if($format == 'application/json'){
			header('Content-Type: application/json; charset=utf-8');
			
 			echo json_encode(array (
				'name' => $this->name,
				'meta' => $this->meta,
				'og_meta' => $this->og_meta,
				'title' => $this->title,
				'slug' => $this->slug,
				'css' => $this->css,
				'js' => $this->js,
				'content' => $content
			));
	    } //JSON
		elseif ($format == 'application/javascript'){
			header('Content-Type: application/json; charset=utf-8');
			
	        echo $_GET['callback'].'('. json_encode(array (
				'name' => $this->name,
				'meta' => $this->meta,
				'og_meta' => $this->og_meta,
				'title' => $this->title,
				'slug' => $this->slug,
				'css' => $this->css,
				'js' => $this->js,
				'content' => $content
			)).');';
    	} //JSONP
		elseif($format == 'text/xml'){
			header('Content-Type: application/xml; charset=utf-8');
			
			$xml_response = 
				'<?xml version="1.0" encoding="UTF-8"?>'."\n".
				'<response>'.
				toXML(array (
					'name' => $this->name,
					'meta' => $this->meta,
					'og_meta' => $this->og_meta,
					'title' => $this->title,
					'slug' => $this->slug,
					'css' => $this->css,
					'js' => $this->js,
					'content' => $content
				)).
				'</response>';
		
			echo $xml_response;
		} //XML
		else{
			foreach ($this->layout as $file) {
				if (!is_readable($file)) {
					throw new Exception('Error printing page: file '.$file.' does not exist');
				}
				include $file;
			}
		} //HTML
	} //output()
	
	/*
	 * Runs the functions necessary for the page to load
	 * - all defined in load_fn
	 */
	function load() {
		if (count($this->load_fn) == 0) return;
		
		$api = new InternalAPI();
		foreach ($this->load_fn as $fn) {
			if (!is_callable(array($api, $fn))) throw new Exception ('Error in Page load(): internal function '.$fn.'() is not callable.');
			$this->response_code = $api->$fn($this); //remember to pass this page to load function, so that the function can store return data. Regretably, only last response code will be stored
		}
	} //load()
	
	function addCss($a) {
		if (is_string($a)) {
			$this->css[] = $a;
		}
		else throw new Exception ('Error in Page class addCss(): $a is not string');
	} //addCss()
	
	function addJs($a) {
		if (is_string($a)) {
			$this->js[] = $a;
		}
		else throw new Exception ('Error in Page class addJs(): $a is not string');
	} //addJs()
	
	/***************
	 *   SETTERS   *
	 ***************/
	function setLayout($layout) {
		if (is_array($layout)) {
			$this->layout = $layout;
		}
		else throw new Exception ('Error in Page class setLayout(): expect arg of type array, but received '. gettype($layout));
	} //setLayout()

	function setOgMeta($ogMetas){
		if(is_array($ogMetas)){
			$this->og_meta = $ogMetas;
		}else{
			throw new Exception ('Error in Page class setOgMeta(): expect arg of type array, but received '. gettype($ogMetas));
		}
	} //setOgMeta()

	function setTitle($a) {
		if (is_string($a)) {
			$this->title = $a;
		}
		else throw new Exception ('Error in Page class setTitle(): expect arg of type string, but received '. gettype($a));
	} //setTitle()
        
	function setMenuTitle($a) {
		if (is_string($a)) {
			$this->menu_title = $a;
		}
		else throw new Exception ('Error in Page class setMenuTitle(): expect arg of type string, but received '. gettype($a));
	} //setMenuTitle()
	function setPageTitle($a) {
		if (is_string($a)) {
			$this->page_title = $a;
		}
		else throw new Exception ('Error in Page class setPageTitle(): expect arg of type string, but received '. gettype($a));
	} //setMenuTitle()
	function setLang($a) {
		if (is_string($a)) {
			$this->lang = $a;
		}
		else throw new Exception ('Error in Page class setLang(): expect arg of type string, but received '. gettype($a));
	} //setLang()
	function setCss($a) {
		if (is_string($a)) {
			$this->css[] = $a;
		}
		else if (is_array($a)) {
			$this->css = $a;
		}
		else throw new Exception ('Error in Page class setCss(): expect args of type string, but received '. gettype($a));
	} ///addCSS()
	function setJs($a) {
		if (is_string($a)) {
			$this->js[] = $a;
		}
		else if (is_array($a)) {
			$this->js = $a;
		}
		else throw new Exception ('Error in Page class setJs(): expect args of type string, but received '. gettype($a));
	} ///addJS()
	function setAccessLevel($a) {
		if (is_int($a)) {
			$this->access_level = $a;
		}
		else throw new Exception ('Error in Page class setAccessLevel(): expect arg of type int, but received '. gettype($a));
	} //setAccessLevel()
	function setLoadFunction($a) {
		if (is_array($a)) {
			$this->load_fn = $a;
		}
		else throw new Exception ('Error in Page class setLoadFunction(): expect arg of type array, but received '. gettype($a));
	} //setLoadFunction()
	function setSlug($a) {
		if (is_string($a)) {
			$this->slug= $a;
		}
		else throw new Exception ('Error in Page class setSlug(): expect args of type string, but received '. gettype($a));
	} //setSlug()
	
	/***************
	 *   GETTERS   *
	 ***************/
	 function getAccessLevel() {
		 return $this->access_level;
	 }
	 
} //clas Page
