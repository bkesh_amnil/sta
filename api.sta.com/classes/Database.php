<?php
/***
 * Provides connectivity to persistence layer (MySQL)
 * Uses prepared statements (although slightly worse performance for simple direct queries, but easier for security).
 * Note: constants DB_HOST, DB_NAME, DB_USER, DB_PASS have to be defined in configuration file.
 *
 * Usage:
 * - To get connection:
 *   $db = Database::getConnection();
 *
 * - For CRUDs operations:
 *   $db->create($model);										// $model must implement methods of iModel
 *   $db->readSingle($class, $conditions, $update);				// $update is default FALSE. $conditions is 2D array of [$key, $operand, $value]
 *   $db->readMultiple($class, $conditions, $offset, $count, $sort, $order, $update);
 *   $db->update($model);
 *   $db->delete($model);
 *
 * - For transactions:
 *   $db->beginTransaction();
 *   $db->commit();
 *   $db->rollback();
 ***/
class Database {
	private static $connection;		// Singleton implementation
	private $dbh;					// PDO object
	private $statements;			// associative array of prepared statements, e.g. ['INSERT INTO transactions'] => PDOStatement,
	private $hasActiveTransaction = false;	// flag whether there is an active transaction

	private function __construct() {
		// Connection data (server_address, database, username, password).
		$this->dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
		$this->dbh->query("SET CHARACTER SET utf8");
		$this->dbh->query("SET NAMES utf8");
		$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} //construct()

	static function getConnection() {
		if (!isset(self::$connection)) self::$connection = new Database();
		return self::$connection;
	} //getConnection()

	//Default function: make invalid method calls throw Exceptions
	function __call($name, $arguments) {
		throw new Exception ('Error in Database class: method '.$name.'() does not exist');
	} //call()

	/***************************
	 *  Database TRANSACTIONS  *
	 ***************************/
	function beginTransaction() {
		if ($this->hasActiveTransaction) return FALSE;

		try {
			$this->hasActiveTransaction = $this->dbh->beginTransaction();
		} //try
		catch (Exception $e) {
			Log::warning('Database::beginTransaction() Exception: '.$e->getMessage());
			return FALSE;
		} //catch
		return $this->hasActiveTransaction;
	} //beginTransaction()
	function rollBack() {
		try {
			if ($this->dbh->rollback()) {
				$this->hasActiveTransaction = FALSE;
				return TRUE;
			}
			return FALSE;
		} //try
		catch (Exception $e) {
			Log::warning('Database::rollBack() fail: '.$e->getMessage());
			return FALSE;
		} //catch
	} //rollBack()
	function commit() {
		$committed = FALSE;
		try {
			if ($this->dbh->commit()) {
				$this->hasActiveTransaction = FALSE;
				return TRUE;
			}
			return FALSE;
		} //try
		catch (Exception $e) {
			Log::warning('Database::commit() fail: '.$e->getMessage());
			return FALSE;
		} //catch
	} //commit()

	// Use only when absolutely necessary: very very very unlikely
	function lockTables($arg) {
		$q = "LOCK TABLES $arg";
		$result = $this->dbh->query($q);
		if ($result === FALSE) return FALSE;
		else return TRUE;
	} //lockTable()
	function unlockTables() {
		$q = "UNLOCK TABLES";
		$result = $this->dbh->query($q);
		if ($result === FALSE) return FALSE;
		else return TRUE;
	} //unlockTable()

	/***
	 * Direct query (SQL) to database
	 * @param: string of SQL query
	 * @return: PDOStatement
	 ***/
	function query($q) {
		return $this->dbh->query($q);
	} //query()

	/************************************
	 *  Insert, Select, Update, Delete  *
	 ************************************/
	/***
	 * Performs an INSERT operation
	 * @param: SQL query of insert (a fully-binded prepared statement)
	 * @return: Success - returns last inserted ID of this query. (0 if not AUTO_INCREMENT key)
	 ***/
	private function performInsert($statement) {
		//Perfom query, and handle possible deadlocks
		$completed = FALSE;
		while (!$completed){
			try {
				$result = $statement->execute();
				$completed = TRUE;
			}
			catch (Exception $e) {
				//40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				if ($e->errorInfo[0] != 40001) {
					Log::warning('Database::performInsert(): '.$e->getMessage());
					return FALSE;
				}
			} //catch
		}//while not completed
		//Results: $result will be FALSE if there are no results, i.e. unsuccessful insert
		if (!$result) throw new Exception('Database::performInsert(): statement execute failure');

		return $this->dbh->lastInsertId();
	} //performInsert()

	/***
	 * Performs a SELECT operation
	 * @param: SQL query of select (a fully-binded prepared statement)
	 * @return: associative array of the row(s)
	 ***/
	private function performSelect($statement) {
		//Perfom query, and handle possible deadlocks
		$completed = FALSE;
		while (!$completed){
			try {
				$result = $statement->execute();
				$completed = TRUE;
			}
			catch (Exception $e) {
				//40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				if ($e->errorInfo[0] != 40001) {
					Log::warning('Database::performSelect(): '.$e->getMessage());
					Log::debug('Statement: '.$statement->queryString);
					return NULL;
				}
			} //catch
		} //while not completed

		//Results:
		if (!$result) throw new Exception('Database::performSelect(): statement execute failure');

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	} //performSelect()

	/***
	 * Performs an UPDATE operation
	 * @parm: SQL query of update (a fully-binded prepared statement)
	 * @return: number of rows updated
	 ***/
	private function performUpdate($statement) {
		//Perfom query, and handle possible deadlocks
		$completed = FALSE;
		while (!$completed){
			try {
				$result = $statement->execute();
				$completed = TRUE;
			}
			catch (Exception $e) {
				//40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				if ($e->errorInfo[0] != 40001) {
					Log::warning('Database::performUpdate(): '.$e->getMessage());
					return NULL;
				}
			} //catch
		} //while not completed

		//Results
		if (!$result) throw new Exception('Database::performUpdate(): statement execute failure ');

		return $statement->rowCount(); //rowCount will be 0 if no rows were updated
	} //performUpdate()

	/***
	 * Performs a DELETE operation
	 * @parm: SQL query of delete (a fully-binded prepared statement)
	 * @return: number of rows deleted
	 ***/
	private function performDelete($statement) {
		//Perfom query, and handle possible deadlocks
		$completed = FALSE;
		while (!$completed){
			try {
				$result = $statement->execute();
				$completed = TRUE;
			}
			catch (Exception $e) {
				//40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				if ($e->errorInfo[0] != 40001) {
					Log::warning('Database::performDelete(): '.$e->getMessage());
					return NULL;
				}
			} //catch
		} //while not completed

		//Results: rowCount will be 0 if no rows were updated
		if (!$result) throw new Exception('Database::performDelete(): statement execute failure ');

		return $statement->rowCount();
	} //performDelete()

	private function performCount($table) {
		$sql = "SELECT COUNT(*) as count FROM $table";
		$committed = FALSE;
		while (!$committed){
			try {
				$result = $this->dbh->query($sql);
				$committed = TRUE;
			}
			catch (Exception $e) {
				//40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				if ($e->errorInfo[0] != 40001) {
					Log::warning('Database::performCount(): '.$e->getMessage());
					return NULL;
				}
			} //catch
		} //while not completed
		if ($result === FALSE) throw new Exception('Database::performCount(): query failure');

		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['count'];
	} //performCount()

	/*************************
	 *  Prepared Statements  *
	 *************************/
	/****
	 * Prepares an INSERT query.
	 * If already prepared, returns the cached prepared statement
	 * @parm: $model - Object to be inserted
	 * @return: a prepared statement (PDOStatement)
	 ****/
	private function prepareInsertQuery($model) {
		//Return cached prepared statement if it has already been prepared
		$sql_insert = 'INSERT INTO '.$model->getTableName();
		$q = $this->statements[$sql_insert];
		if (isset($q)) {
			return $q;
		}

		//Prepare the statement
		$data = $model->toArray();

		$sql_params = ' (';
		$sql_values = 'VALUES (';
		foreach ($data as $key => $value) {
			if ($key != 'ID') {
				$sql_params .= $key.',';
				$sql_values .= ':'.$key.',';
			}
		} //foreach data member
		$sql_params = trim($sql_params, ',') . ') ';
		$sql_values = trim($sql_values, ',') . ')';
		$q = $this->dbh->prepare($sql_insert.$sql_params.$sql_values);


		//Cache this statement
		$this->statements[$sql_insert] = $q;
		return $q;

	} //prepareInsertQuery()

	/****
	 * Binds an INSERT query.
	 * @param: $q - prepared statement
	 * @param: $model - Object to be inserted
	 * @return: the binded query (PDOStatement)
	 ****/
	private function bindInsertQuery($q, $model) {
		//Bind params to query
		$data = $model->toArray();
		foreach ($data as $key => $value) {
			if ($key != 'ID') {
				if ($value === FALSE) $value = 0;	//cast FALSE to an int of value 0, else it will be auto-casted into an empty string
				else if (is_array($value )) $value = array2csv($value);	//stringify arrays
				$q->bindValue(':'.$key, $value);
			}
		} //foreach data value
		return $q;
	} //bindInsertQuery()

	/****
	 * Prepares an INSERT query.
	 * If already prepared, returns the cached prepared statement
	  @param: $table - name of table to select from (string)
	 * @param: $conditions - a non-associative 2D array of conditions
	 *			- each condition should be an associative array of [key, operand, value].
	 *			- e.g. array('key' => 'id', 'operand' => '=', 'value' => '17']
	 * @param: $offset - which row to start the search from
	 * @param: $count - how many rows to return. 0 means unlimited
	 * @param: $sort - which field to sort the results by
	 * @param: $order - ASC or DESC
	 * @param: $update - whether this read is for update (will lock row)
	 * @return: a prepared statement (PDOStatement)
	 ****/
	private function prepareSelectQuery($table, $conditions, $offset=0, $count=0, $sort=NULL, $order='ASC', $update=FALSE) {
		//Construct query
		$sql = "SELECT * FROM $table";
		foreach ($conditions as $index => $condition) {
			if ($index == 0) {
				$sql .= " WHERE ".$condition['key'].' '.$condition['operand'].' :'.$condition['key'].'0';
		} //first condition
			else {
				$sql .= " AND ".$condition['key'].' '.$condition['operand'].' :'.$condition['key'].$index;
			} //subsequent conditions
		} //foreach condition
		if (isset($sort)) {
			$sql .= " ORDER BY `$sort` $order";
		}
		if ($count > 0) {
			$sql .= " LIMIT $count";
		}
		if ($offset > 0) {
			$sql .= " OFFSET $offset";
		}
		if ($update) {
			$sql .= " FOR UPDATE";
		}
		else {
			$sql .= " LOCK IN SHARE MODE";
		}

		//Return cached prepared statement if it has already been prepared
		$q = $this->statements[$sql];


		if (isset($q)) return $q;


		//Prepare and cache this statement
		$q = $this->dbh->prepare($sql);


		$this->statements[$sql] = $q;
		 //print_r($q);


		return $q;



	} //prepareSelectQuery()

	/****
	 * Binds a SELECT query.
	 * @param: $q - prepared statement
	 * @param: $conditions - a non-associative 2D array of conditions
	 * @return: the binded query (PDOStatement)
	 ****/
	private function bindSelectQuery($q, $conditions) {
		//Data checks
		if (!isset($q)) throw new Exception('Database::bindSelectQuery(): $q is null');
		if (!is_array($conditions)) throw new Exception('Database::bindSelectQuery(): $conditions is not array');

		//Bind params to query
		foreach ($conditions as $index => $condition) {
			$q->bindValue(':'.$condition['key'].$index, $condition['value']);
		}
		return $q;
	} //bindSelectQuery()

	/****
	 * Prepares an UPDATE query.
	 * If already prepared, returns the cached prepared statement
	 * @parm: $model - Object to be inserted
	 * @return: a prepared statement (PDOStatement)
	 ****/
	private function prepareUpdateQuery($model) {
		//Return cached prepared statement if it has already been prepared
		$sql_update = 'UPDATE '.$model->getTableName();
		$q = $this->statements[$sql_update];
		if (isset($q)) {
			return $q;
		}

		//Prepare the statement
		$sql = $sql_update.' SET ';
		$data = $model->toArray();
		foreach ($data as $key => $value) {
			if ($key != 'ID') {
				$sql .= $key.'=:'.$key.',';
			}
		} //foreach data member
		$sql = trim($sql, ',') . ' WHERE ID=:ID LIMIT 1';
		$q = $this->dbh->prepare($sql);

		//Cache this statement
		$this->statements[$sql_update] = $q;


		return $q;
	} //prepareUpdateQuery()

	/****
	 * Binds an UPDATE query.
	 * @param: $q - prepared statement
	 * @param: $model - Object to be inserted
	 * @return: the binded query (PDOStatement)
	 ****/
	private function bindUpdateQuery($q, $model) {
		$data = $model->toArray();
		foreach ($data as $key => $value) {
			if ($value === FALSE) $value = 0;	//cast FALSE to an int of value 0, else it will be auto-casted into an empty string
			else if (is_array($value )) $value = array2csv($value);	//stringify arrays
			$q->bindValue(':'.$key, $value);
		} //foreach data value
		return $q;
	} //bindUpdateQuery()

	/****
	 * Prepares an DELETE query.
	 * If already prepared, returns the cached prepared statement
	 * @parm: $model - Object to be inserted
	 * @return: a prepared statement (PDOStatement)
	 ****/
	private function prepareDeleteQuery($model) {
		//Return cached prepared statement if it has already been prepared
		$sql_delete = 'DELETE FROM '.$model->getTableName();
		$q = $this->statements[$sql_delete];
		if (isset($q)) {
			return $q;
		}

		//Prepare the statement
		$sql = $sql_delete.' WHERE ID=:ID LIMIT 1';
		$q = $this->dbh->prepare($sql);

		//Cache this statement
		$this->statements[$sql_delete] = $q;

		return $q;
	} //prepareDeleteQuery()

	/****
	 * Prepares and binds a DELETE query.
	 * @param: $model - Object to be inserted
	 * @return: the binded query (PDOStatement)
	 ****/
	private function bindDeleteQuery($q, $model) {
		$q->bindValue(':ID', $model->getID(), PDO::PARAM_INT);

		return $q;
	} //bindDeleteQuery()

	/**********************
	 *   CRUD FUNCTIONS   *
	 **********************/
	/***
	 * Inserts this model into the database
	 * @param: model to be inserted
	 * @return: TRUE on success
	 * - Will also set the model's global ID upon success
	 ***/
	function create($model) {

		//Data checks
		if (!isset($model)) throw new Exception('Database::create(): model not set');

		//Prepare, bind and perform INSERT query
		$q = $this->prepareInsertQuery($model);
		$q = $this->bindInsertQuery($q, $model);
		$id = $this->performInsert($q);

		//Process result
		if ($id > 0) {
			$model->setID($id);
			return TRUE;
		}
		else {
			return FALSE;
		}
	} //create()

	/***
	 * Finds a model from the database based on some condition(s)
	 * @param: a non-associative 2D array of conditions
	 *			- each condition should be an associative array of [key, operand, value].
	 *			- e.g. array('key' => 'id', 'operand' => '=', 'value' => '17']
	 * @param: $update - whether this read is for update (will lock row)
	 * @return: the model object if success, NULL otherwise
	 ***/
	function readSingle($class, $conditions, $update=FALSE) {
		$models = $this->readMultiple($class, $conditions, 0, 1, NULL, 'ASC', $update);
		if (!is_array($models) || count($models) == 0) return NULL;
		else return $models[0];
	} //readSingle()

	// Synonym for readSingle
	function readOne($class, $conditions, $update=FALSE) {
		return readSingle($class, $conditions, $update);
	} //readOne

	/***
	 * Finds models from the database based on some condition(s)
	 * @param: $conditions - a non-associative 2D array of conditions
	 *			- each condition should be an associative array of [key, operand, value].
	 *			- non-associative arrays will be converted (but no guarantee of correctness).
	 *			- e.g. array('key' => 'id', 'operand' => '=', 'value' => '17']
	 * @param: $offset - which row to start the search from
	 * @param: $count - how many rows to return. 0 means unlimited
	 * @param: $sort - which field to sort the results by
	 * @param: $order - ASC or DESC
	 * @param: $update - whether this read is for update (will lock row)
	 * @return: the model object if success, NULL otherwise
	 ***/
	function readMultiple($class, $conditions, $offset=0, $count=0, $sort=NULL, $order='ASC', $update=FALSE) {
		//Data checks
		if (!is_array($conditions)) throw new Exception('Database::readMultiple(): $conditions is not array');

		// Make each condition associative if not associative
		foreach ($conditions as $index => $condition) {
			if (!isset($condition['key'])) {
				$conditions[$index]['key'] = $condition[0];
				$conditions[$index]['operand'] = $condition[1];
				$conditions[$index]['value'] = $condition[2];
			}
		}

		//Prepare, bind and perform SELECT query
		$table = call_user_func($class.'::getTableName');
// echo $table;exit;
		$q = $this->prepareSelectQuery($table, $conditions, $offset, $count, $sort, $order, $update);
		$q = $this->bindSelectQuery($q, $conditions);
		$rows = $this->performSelect($q);

		//Process result
		if (count($rows) == 0) {
			return NULL;
		}
		$models = array();
		foreach ($rows as $row) {
			$models[] = new $class($row);
		}
		return $models;
	} //readMultiple()

	// Synonym for readMultiple
	function readMany($class, $conditions, $offset=0, $count=0, $sort=NULL, $order='ASC', $update=FALSE) {
		return readSingle($class, $conditions, $offset, $count, $sort, $order, $update);
	} //readOne

	/***
	 * Updates a model in the database
	 * @param: model to be updated
	 * @return: TRUE on success
	 ***/
	function update($model) {
		//Data checks
		if (!isset($model)) throw new Exception('Database::update(): model not set');

		//Prepare, bind and perform UPDATE query
		$q = $this->prepareUpdateQuery($model);
		$q = $this->bindUpdateQuery($q, $model);
		$rowsAffected = $this->performUpdate($q);

		//Process result
		if ($rowsAffected > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	} //update()

	/***
	 * Deletes a model in the database
	 * @param: model to be updated
	 * @return: TRUE on success
	 ***/
	function delete($model) {
		//Data checks
		if (!isset($model)) throw new Exception('Database::delete(): model not set');

		//Prepare, bind and perform DELETE query
		$q = $this->prepareDeleteQuery($model);
		$q = $this->bindDeleteQuery($q, $model);
		$rowsAffected = $this->performDelete($q);

		//Process result
		if ($rowsAffected > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	} //delete()

	/***
	 * Counts the number of this model in the database
	 ***/
	function getCount($class) {
		$table = call_user_func($class.'::getTableName');
		return $this->performCount($table);
	} //getCount()
} //class Database
