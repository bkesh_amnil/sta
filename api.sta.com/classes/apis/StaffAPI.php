<?php

require APPROOT . '/vendor/firebase/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

class StaffAPI {

    function generateToken($id) {
        $time = getTimeInMs();
        $token = array(
            "userid" => $id,
            "time" => $time,
            "exp" => $time + TOKEN_KEY_EXPIRY,
        );
        $jwt = JWT::encode($token, TOKEN_KEY);
        return $jwt;
    }

    function checkTokenExpiry($token) {
        try {
            return JWT::decode($token, TOKEN_KEY, array('HS256'));
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Attempts to set the staff as the session user.
     * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
     * @input: $_POST['email', 'password', 'persistent']
     * @output: Staff {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdatedTime, lastActivityTime}
     * 'persistentLoginCookie' (optional)
     */
    function login($response) {
        if (!isValidEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email address.' . $_POST['email']);
            return 400;
        } else {
            $_POST['email'] = strtolower($_POST['email']);
        }
        if (strlen($_POST['password']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Invalid password.');
            return 400;
        }

        $time = getTimeInMs();
        // Transaction for ACID-ity

        $db = Database::getConnection();

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('Staff', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);

            if (!isset($user)) {
                $response->addData('error', 'Invalid email address.');
                return 401;
            } // No such user

            if (($user->isActive() !== "Active")) {
                $response->addData('error', "Account is Disabled.");
                return 403;
            }

            // Check password
            if (!$user->checkPassword($_POST['password'])) {
                $response->addData('error', 'Wrong password.');
                return 401;
            } // Wrong password
            // Generate persistent cookie if requested
            if ($_POST['persistent']) {
                $cookie = $user->generatePersistentLoginCookie();
                $response->addData('persistentLoginCookie', $cookie);
            }
            if ($user->getRoleID() != '1') {
                $roledetail = $db->readMultiple('RoleDetail', array(
                    array("RoleID", "=", $user->getRoleID())
                ));
                if (!is_array($roledetail)) {
                    $response->addData('modules', array());
                } else {
                    foreach ($roledetail as $role) {
                        $module = $db->readSingle('Module', array(
                            array("ID", "=", $role->getModuleID())
                        ));
                        $modules[] = $module->toOutputArray();
                    }
                }
                $response->addData('modules', $modules);
            } else {
                $modules = $db->readMultiple('Module', array());
                if (is_array($modules)) {
                    foreach ($modules as $mod) {
                        $module[] = $mod->toOutputArray();
                    }
                } else {
                    $module = array();
                }
                $response->addData('modules', $module);
            }
            //Update user
            $user->update(array(
                'LastUpdatedDateTime' => $time,
            ));

            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB Unable to update login status for ' . $user . '. Persistent: ' . $_POST['persistent'] . '. Cookie: ' . $cookie);
                //$response->addData('error', 'Unable to store login status.');
                //return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => 'BackendUser',
                'Action' => 'Logged in',
                'CreatedDateTime' => $time,
            ));

            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' login"');
            } else {
                Log::error(__METHOD__ . '() - DB created to create activity "' . $user . ' login"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Set session user

        $response->addData('Staff', $user->toOutputArray());
        $response->addData('Token', $this->generateToken($user->getID()));
        return 200;
    }

    /**
     * Attempts to set the staff as the session user using a persistent cookie.
     * A new persistent cookie will be issued.
     * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
     * @input: $_POST['email', 'cookie']
     * @output: Staff {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdatedTime, lastActivityTime}
     * 'persistentLoginCookie'
     */
    function persistentLogin($response) {
        // Data checks
        if (!isValidEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email ' . $_POST['email']);
            return 400;
        } else {
            $_POST['email'] = strtolower($_POST['email']);
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('Staff', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);


            if (!isset($user)) {
                $response->addData('error', 'Invalid email address.');
                return 401;
            } // No such user
            // Check and renew cookie (which will check cookie validity)
            $new_cookie = $user->renewPersistentLoginCookie($_POST['persistentLoginCookie']);
            if (!$new_cookie) {
                $response->addData('error', 'Invalid cookie.');
                return 401;
            }
            $response->addData('persistentLoginCookie', $new_cookie);

            // Update user
            $user->update(array(
                'LastUpdatedDateTime' => $time
                    // 'lastActivityTime' => $time
            ));
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB Unable to update login status for ' . $user . '. Persistent: ' . $_POST['persistent'] . '. Cookie: ');
                $response->addData('error', 'Unable to process persistent login.');
                return 500;
            }

            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => 'BackendUser',
                'Action' => 'Login',
                'CreatedDateTime' => $time,
            ));


            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' login (persistent)"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Set session user
        $_SESSION['user'] = $user;
        Session::regenerateID(); //remember to regenerate ID to prevent session fixation
        // Yay!
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

    /**
     * Attempts to set the staff as the session user using a persistent cookie.
     * A new persistent cookie will be issued.
     * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
     * @input: $_POST['email', 'cookie']
     * @output: Staff {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdatedTime, lastActivityTime}
     * 'persistentLoginCookie'
     */
    function validateCookie($response) {
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        $_POST = json_decode(file_get_contents('php://input'), true);
        // Data checks
        if (!isValidEmail($_POST['Email'])) {
            $response->addData('error', 'Invalid email ' . $_POST['Email']);
            return 400;
        } else {
            $_POST['Email'] = strtolower($_POST['Email']);
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('BackendUser', array(
                array('Email', '=', $_POST['Email']),
                array('PersistentLoginHashes', '=', $_POST['PersistentCookie'])
                    ), TRUE);

            if (!isset($user)) {
                $response->addData('message', 'false');
                return 200;
            } else {
                $response->addData('staff', $user->toOutputArray());
                $response->addData('message', 'true');
                return 200;
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        }
    }

    /**
     * Logout a staff
     * Any existing Session ID is cleared and a new session will be started
     * @input: none
     * @output: message
     * @param type $response
     * @return int
     */
    function logout($response) {
        //user validation here
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create and store this activity

            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Logged out',
                'CreatedDateTime' => $time,
            ));

            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user']->getID() . ' logout"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Restart this session with new session ID
        // YAY!
        return 200;
    }

    /**
     * Changes password of session staff
     * - must be logged in
     * @input: $_POST['oldPassword, newPassword']
     * @output: Staff {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdatedTime, lastActivityTime...}
     */
    function changePassword($response) {
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
//        $_POST = json_decode(file_get_contents('php://input'), true);
        // Data checks
        if (strlen($_POST['OldPassword']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Wrong password.');
            return 401;
        }
        if (strlen($_POST['NewPassword']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Password must be at least ' . MIN_PASSWORD_LENGTH . ' characters.');
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $userID = $users->userid;

        // Find this user in DB
        $user = $db->readSingle('BackendUser', array(
            array('ID', '=', $userID)
                ), TRUE);
        if (!isset($user)) {
            $response->addData('error', 'No such staff.');
            return 404;
        }

        // Check password
        if (!$user->checkPassword($_POST['OldPassword'])) {
            $response->addData('error', 'Wrong password.');
            return 401;
        }

        $time = getTimeInMs();

        // Cache variables
        $hash = Encryptor::getHash($_POST['NewPassword']);

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            if (!isset($_POST['NewPassword']) || strlen($_POST['NewPassword']) < MIN_PASSWORD_LENGTH) {
                $response->addData('error', 'Password NOT changed: your password must be at least ' . MIN_PASSWORD_LENGTH . ' characters long.');
                return 400;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'PasswordHash' => $hash,
                    'LastUpdatedDateTime' => $time
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }

            // Update him in DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update new profile.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $userID,
                'UserType' => $users->usertype,
                'Action' => 'Change Password',
                'ModelID' => $userID,
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' change password"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed

        return 200;
    }

    /*     * * ******************** *** Customers *** ******************** ** */
    /*     * *
     * Gets a list of ApplicationUser
     * @output: array of ApplicationUser {}
     */

    function addCustomer($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $time = getTimeInMs();
        if ($_FILES) {
            $target_dir = APPROOT . "/uploads/customer/";
            $target_dir = $target_dir . basename($_FILES['file']['name']);
            $fileName = "uploads/customer/" . basename($_FILES['file']['name']);
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_dir)) {
                $response->addData('error', "Image could not be uploaded");
                return 400;
            }
        }
        // Generate hash ahead of transaction
        $passwordHash = Encryptor::getHash($_POST['Password']);

        // Create backend user - can be done outside transaction as we don't need persistence (yet)
        try {
            $applicationUser = new ApplicationUser(array(
                'UserType' => 'Customer',
                'Name' => ($_POST['Name']) ? $_POST['Name'] : '',
                'Mobile' => ($_POST['Mobile']) ? $_POST['Mobile'] : '',
                'Email' => ($_POST['Email']) ? $_POST['Email'] : '',
                'Photo' => isset($fileName) ? $fileName : '',
                'Password' => $passwordHash,
                'CompanyName' => ($_POST['CompanyName']) ? $_POST['CompanyName'] : '',
                'CompanyAddress' => ($_POST['CompanyAddress']) ? $_POST['CompanyAddress'] : '',
                'CompanyWebsite' => ($_POST['CompanyWebsite']) ? $_POST['CompanyWebsite'] : '',
                'BusNumber' => ($_POST['BusNumber']) ? $_POST['BusNumber'] : '',
                'BusCapacity' => ($_POST['BusCapacity']) ? $_POST['BusCapacity'] : '',
                'DoesRepresentCompany' => ($_POST['DoesRepresentCompany']) ? $_POST['DoesRepresentCompany'] : '',
                'Status' => 'Active',
                'LanguageID' => ($_POST['LanguageID']) ? $_POST['LanguageID'] : '',
                'LastUpdatedDateTime' => $time,
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check that this email is not in use
            $existing_backendUser = $db->readSingle('ApplicationUser', array(
                array('Email', '=', $_POST['Email'])
                    ), TRUE);
            if (isset($existing_backendUser)) {
                $response->addData('error', 'This email is already in use.');
                return 400;
            }
            $existing_backendUser = $db->readSingle('ApplicationUser', array(
                array('Mobile', '=', $_POST['Mobile'])
                    ), TRUE);
            if (isset($existing_backendUser)) {
                $response->addData('error', 'This Mobile Number is already in use.');
                return 400;
            }

            // Create user in DB
            if (!$db->create($applicationUser)) {
                Log::error(__METHOD__ . '() - DB unable to create backend user ' . $_POST['Email']);
                $response->addData('error', 'DB unable to create backend user.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $applicationUser->getID(),
                'ModelType' => 'ApplicationUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('ApplicationUser', $applicationUser->toOutputArray());
        return 200;
    }

    /*     * *
     * Gets a list of Customers that have been updated since a certain time
     * - Note: must have a session staff
     * @input : $_GET['fromTime']
     * @output: array of Customers
     */

    function getCustomers($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Read customers from DB
        $db = Database::getConnection();
        $customers = $db->readMultiple('ApplicationUser', array(
            array('UserType', '=', 'Customer')
        ));

        // Handle empty cases
        if (!is_array($customers)) {
            $response->addData('customers', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        foreach ($customers as $user) {
            $data[] = $user->toOutputArray();
        }

        // YAY
        $response->addData('ApplicationUser', $data);
        return 200;
    }

    /**
     * @param type $response
     * @return intEdits a Bus type
     * @input: $_POST['ID, BusType, MinCapacity, MaxCapacity, MinPrice,MaxPrice,Status']
     * @output: busType
     */
    function editCustomers($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        if ($_FILES) {
            $target_dir = APPROOT . "/uploads/customer/";
            $target_dir = $target_dir . basename($_FILES['file']['name']);
            $fileName = "uploads/customer/" . basename($_FILES['file']['name']);
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_dir)) {
                $response->addData('error', "Image could not be uploaded");
                return 400;
            }
        }
        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
            // Find from DB
            $customer = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($customer)) {
                $response->addData('error', 'No such serviceProvider ' . $_POST['ID']);
                return 400;
            }
            $existing_User = $db->readSingle('ApplicationUser', array(
                array('Email', '=', $_POST['Email']),
                array('ID', '!=', $_POST['ID'])
                    ), TRUE);
            if (isset($existing_User)) {
                $response->addData('error', 'This email is already in use.');
                return 400;
            }

            // Update it
            $updated = $customer->update(array(
                'UserType' => $_POST['UserType'],
                'Name' => $_POST['Name'],
                'Mobile' => $_POST['Mobile'],
                'Email' => $_POST['Email'],
                'CompanyName' => $_POST['CompanyName'],
                'CompanyAddress' => $_POST['CompanyAddress'],
                'CompanyWebsite' => $_POST['CompanyWebsite'],
                'DoesRepresentCompany' => $_POST['DoesRepresentCompany'],
                'Status' => $_POST['Status'],
                'LastUpdatedDateTime' => $time
            ));
            // Update it in DB
            if (!$db->update($customer)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $customer);
                $response->addData('error', 'DB unable to update bus type data.');
                return 500;
            }

            $oldvalue = json_encode($customer->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $customer->getID(),
                'ModelType' => 'ApplicationUser',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edited bus type"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('ApplicationUser', $customer->toOutputArray());
        return 200;
    }

    /**
     * Gets the advertisement listss
     * @param type $response
     * @return int
     */
    function getAdvertisements($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Read customers from DB
        $db = Database::getConnection();
//        $advertisements = $db->readMultiple('Advertisement', array());
        $advertisements = $db->readMultiple('Advertisement', array(
                ), 0, 0, 'DisplayPriority', 'DESC', FALSE);

        // Handle empty cases
        if (!is_array($advertisements)) {
            $response->addData('advertisements', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        foreach ($advertisements as $advertisement) {
            $data[] = $advertisement->toOutputArray();
        }

        // YAY
        $response->addData('advertisements', $data);
        return 200;
    }

    /**
     * adds advertisement info to the database
     * @param type $response
     * @return int
     */
    function addAdvertisement($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $time = getTimeInMs();

        //Check for file errors
        $post_image = $_FILES['img'];
        if (!isset($post_image['tmp_name'])) {
            $response->addData('error', 'No image file uploaded.');
            return 400;
        } else if ($post_image['error'] > 0) {
            $response->addData('error', $post_image['error']);
            return 400;
        } else {
            $folder_path = 'img/advertisements';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path) : NULL;
        }

        // Create advertisement - can be done outside transaction as we don't need persistence (yet)
        try {
            $advertisement = new Advertisement(array(
                'Name' => $_POST['Name'],
                'URL' => $_POST['URL'],
                'Title' => $_POST['Title'],
                'Image' => $image,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'CreatedTime' => $time,
                'LastUpdatedTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create in DB
            if (!$db->create($advertisement)) {
                Log::error(__METHOD__ . '() - DB unable to create advertisement');
                $response->addData('error', 'DB unable to create advertisement.');
                return 500;
            }

            //Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $advertisement->getID(),
                'ModelType' => 'Advertisment',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('Advertisement', $advertisement->toOutputArray());
        return 200;
    }

    /**
     * updates the advertisement data to database
     * @param type $response
     * @return int
     */
    function editAdvertisement($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Check if img file was uploaded
        $post_image = $_FILES['img'];
        $has_img_file_upload = TRUE;
        if (!isset($post_image['tmp_name'])) {
            $has_img_file_upload = FALSE;
        } else if ($post_image['error'] > 0) {
            if ($post_image['error'] !== 4) { // 4: no file uploaded
                Log::warning(__METHOD__ . '() - img appears uploaded but there were errors: ' . $post_image['error']);
            }
            $has_img_file_upload = FALSE;
        }
        //upload the image
        $post_image_prev = $_POST['img_prev'];
        $image = $post_image_prev;
        if ($has_img_file_upload) {
            $folder_path = 'img/advertisements';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path, $post_image_prev) : NULL;
        }

        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
            // Find from DB
            $advertisement = $db->readSingle('Advertisement', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($advertisement)) {
                $response->addData('error', 'No such advertisement ' . $_POST['ID']);
                return 400;
            }
            // Update it
            $advertisement->update(array(
                'Name' => $_POST['Name'],
                'URL' => $_POST['URL'],
                'Title' => $_POST['Title'],
                'Image' => $image,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'LastUpdatedTime' => $time
            ));


            // $response->addData('error', json_encode($advertisement->toOutputArray()));
            //                return 400;
            // Update it in DB
            if (!$db->update($advertisement)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $advertisement);
                $response->addData('error', 'DB unable to update edit data.');
                return 500;
            }

            $oldvalue = json_encode($advertisement->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $advertisement->getID(),
                'ModelType' => 'Advertisment',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edit advertisement"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!
        $response->addData('Advertisement', $advertisement->toOutputArray());
        return 200;
    }

    /**
     * Gets the company listss
     * @param type $response
     * @return int
     */
    function getCompany($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Read customers from DB
        $db = Database::getConnection();
//        $company = $db->readMultiple('Company', array());
        $company = $db->readMultiple('Company', array(
                ), 0, 0, 'DisplayPriority', 'DESC', FALSE);

        // Handle empty cases
        if (!is_array($company)) {
            $response->addData('company', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        $i = 0;
        foreach ($company as $comp) {
            //# for representative
            $r_data = array();
            $representatives = $db->readMultiple('Representative', array(
                array("CompanyID", "=", $comp->getID())
            ));
            if (is_array($representatives)) {
                foreach ($representatives as $representative) {
                    $r_data[] = $representative->toOutputArray();
                }
            }
            //# for Associatecompany
            $a_data = array();
            $associatecompanys = $db->readMultiple('Associatecompany', array(
                array("CompanyID", "=", $comp->getID())
            ));
            if (is_array($associatecompanys)) {
                foreach ($associatecompanys as $associatecompany) {
                    $a_data[] = $associatecompany->toOutputArray();
                }
            }
            $data[] = $comp->toOutputArray();
            $data[$i]['Representatives'] = $r_data;
            $data[$i]['Associates'] = $a_data;
            $i++;
        }
        // YAY
        $response->addData('company', $data);
        return 200;
    }

    /**
     * adds company info to the database
     * @param type $response
     * @return int
     */
    function addCompany($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $time = getTimeInMs();

        // Create company - can be done outside transaction as we don't need persistence (yet)
        try {
            $company = new Company(array(
                'CompanyNameEnglish' => $_POST['CompanyNameEnglish'],
                'CompanyNameChinese' => $_POST['CompanyNameChinese'],
                'Address' => $_POST['Address'],
                'Phone' => $_POST['Phone'],
                'Fax' => $_POST['Fax'],
                'Email' => $_POST['Email'],
                'Website' => $_POST['Website'],
                'Registration' => $_POST['Registration'],
                'ProductsServicesEnglish' => $_POST['ProductsServicesEnglish'],
                'ProductsServicesChinese' => 'na',
                'DisplayPriority' => $_POST['DisplayPriority'],
                'CreatedTime' => $time,
                'LastUpdatedTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }
        // $response->addData('bkesh',$company->toOutputArray() );
        // return 400;
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create in DB
            if (!$db->create($company)) {
                Log::error(__METHOD__ . '() - DB unable to create company');
                $response->addData('error', 'DB unable to create company.');
                return 500;
            }

            //# for Representative
            $representatives = json_decode($_POST['Representatives'], true);
            foreach ($representatives as $v) {
                try {
                    $representative = new Representative(array(
                        'CompanyID' => $company->getID(),
                        'NameEnglish' => $v['NameEnglish'],
                        'NameChinese' => 'cc',
                        'Phone' => isset($v['Phone']) ? $v['Phone'] : '',
                        'DisplayPriority' => $v['DisplayPriority'],
                    ));
                } catch (Exception $e) {
                    Log::error(__METHOD__ . '() - DB unable to create Representatives: ' . $_POST['CompanyNameEnglish']);
                    $response->addData('error', $e->getMessage());
                    return 400;
                }


                if (!$db->create($representative)) {
                    Log::error(__METHOD__ . '() - DB unable to create Representative for: ' . $_POST['CompanyNameEnglish']);
                    $response->addData('error', 'DB unable to create Representative.');
                    return 500;
                }
            }

            //# for Associatecompany
            $associates = json_decode($_POST['Associates'], true);
            foreach ($associates as $k => $v) {
                $associatecompany = new Associatecompany(array(
                    'CompanyID' => $company->getID(),
                    'AssociateCompanyNameEnglish' => $v['AssociateCompanyNameEnglish'],
                    'AssociateCompanyNameChinese' => isset($v['AssociateCompanyNameChinese']) ? $v['AssociateCompanyNameChinese'] : '',
                    'DisplayPriority' => $v['DisplayPriority'],
                ));
                if (!$db->create($associatecompany)) {
                    Log::error(__METHOD__ . '() - DB unable to create Representative for: ' . $_POST['CompanyNameEnglish']);
                    $response->addData('error', 'DB unable to create Representative.');
                    return 500;
                }
            }

            //Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $company->getID(),
                'ModelType' => 'Company',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('Company', $company->toOutputArray());
        return 200;
    }

    /**
     * updates the company data to database
     * @param type $response
     * @return int
     */
    function editCompany($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
            // Find from DB
            $company = $db->readSingle('Company', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($company)) {
                $response->addData('error', 'No such company ' . $_POST['ID']);
                return 400;
            }
            // Update it
            $company->update(array(
                'CompanyNameEnglish' => $_POST['CompanyNameEnglish'],
                'CompanyNameChinese' => $_POST['CompanyNameChinese'],
                'Address' => $_POST['Address'],
                'Phone' => $_POST['Phone'],
                'Fax' => $_POST['Fax'],
                'Email' => $_POST['Email'],
                'Website' => $_POST['Website'],
                'Registration' => $_POST['Registration'],
                'ProductsServicesEnglish' => $_POST['ProductsServicesEnglish'],
                'ProductsServicesChinese' => 'na',
//                'ProductsServicesChinese' => $_POST['ProductsServicesChinese'],
                'DisplayPriority' => $_POST['DisplayPriority'],
                'LastUpdatedTime' => $time
            ));

            // Update it in DB
            if (!$db->update($company)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . ($company));
                $response->addData('error', 'DB unable to update company data.');
                return 500;
            }
            //# for representatives
            $representatives = $db->readMultiple('Representative', array(
                array("CompanyID", "=", $_POST['ID'])
            ));

            if ($representatives) {
                foreach ($representatives as $representative) {
                    if (!$db->delete($representative)) {
                        Log::error(__METHOD__ . '() - DB unable to edit Representative for: ' . $_POST['CompanyNameEnglish']);
                        $response->addData('error', 'DB unable to edit Representative.');
                        return 500;
                    }
                }
            }


            $representativesPost = json_decode($_POST['Representatives'], true);
            if ($representativesPost) {
                foreach ($representativesPost as $v) {
                    try {
                        $representative = new Representative(array(
                            'CompanyID' => $company->getID(),
                            'NameEnglish' => $v['NameEnglish'],
                            'NameChinese' => '-',
                            'Phone' => isset($v['Phone']) ? $v['Phone'] : '',
                            'DisplayPriority' => $v['DisplayPriority'],
                        ));

                        if (!$db->create($representative)) {
                            Log::error(__METHOD__ . '() - DB unable to create Representative for: ' . $_POST['CompanyNameEnglish']);
                            $response->addData('error', 'DB unable to create Representative.');
                            return 500;
                        }
                    } catch (Exception $e) {
                        Log::error(__METHOD__ . '() - DB unable to update: ' . $_POST['CompanyNameEnglish']);
                        $response->addData('error', $e->getMessage());
                        return 400;
                    }
                }
            }

            //# for Associatecompany
            $associatecompanys = $db->readMultiple('Associatecompany', array(
                array("CompanyID", "=", $_POST['ID'])
            ));
            if ($associatecompanys) {
                foreach ($associatecompanys as $associatecompany) {
                    if (!$db->delete($associatecompany)) {
                        Log::error(__METHOD__ . '() - DB unable to edit Associatecompany for: ' . $_POST['CompanyNameEnglish']);
                        $response->addData('error', 'DB unable to edit Associatecompany.');
                        return 500;
                    }
                }
            }

            $associates = json_decode($_POST['Associates'], true);
            if ($associates) {
                foreach ($associates as $k => $v) {
                    $associatecompany = new Associatecompany(array(
                        'CompanyID' => $company->getID(),
                        'AssociateCompanyNameEnglish' => $v['AssociateCompanyNameEnglish'],
                        'AssociateCompanyNameChinese' => isset($v['AssociateCompanyNameChinese']) ? $v['AssociateCompanyNameChinese'] : '',
                        'DisplayPriority' => $v['DisplayPriority'],
                    ));
                    if (!$db->create($associatecompany)) {
                        Log::error(__METHOD__ . '() - DB unable to create Representative for: ' . $_POST['CompanyNameEnglish']);
                        $response->addData('error', 'DB unable to create Representative.');
                        return 500;
                    }
                }
            }

            $oldvalue = json_encode($company->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $company->getID(),
                'ModelType' => 'Company',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edit company"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!
        $response->addData('Company', $company->toOutputArray());
        return 200;
    }

    /**
     * Gets the boc listss
     * @param type $response
     * @return int
     */
    function getbocs($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Read customers from DB
        $db = Database::getConnection();
//        $bocs = $db->readMultiple('Boc', array());
        $bocs = $db->readMultiple('Boc', array(
                ), 0, 0, 'DisplayPriority', 'DESC', FALSE);

        // Handle empty cases
        if (!is_array($bocs)) {
            $response->addData('bocs', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        foreach ($bocs as $boc) {
            $data[] = $boc->toOutputArray();
        }

        // YAY
        $response->addData('boc', $data);
        return 200;
    }

    /**
     * adds boc info to the database
     * @param type $response
     * @return int
     */
    function addBoc($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $time = getTimeInMs();

        //Check for file errors
        $post_image = $_FILES['img'];
        if (!isset($post_image['tmp_name'])) {
            $response->addData('error', 'No image file uploaded.');
            return 400;
        } else if ($post_image['error'] > 0) {
            $response->addData('error', $post_image['error']);
            return 400;
        } else {
            $folder_path = 'img/bocs';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path) : NULL;
        }


        // Create boc - can be done outside transaction as we don't need persistence (yet)
        try {
            $boc = new Boc(array(
                'Name' => $_POST['Name'],
                'Designation' => $_POST['Designation'],
                'Company' => $_POST['Company'],
                'Image' => $image,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'CreatedTime' => $time,
                'LastUpdatedTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create in DB
            if (!$db->create($boc)) {
                Log::error(__METHOD__ . '() - DB unable to create Boc');
                $response->addData('error', 'DB unable to create Boc.');
                return 500;
            }

            //Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $boc->getID(),
                'ModelType' => 'Board of Committee',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('Boc', $boc->toOutputArray());
        return 200;
    }

    /**
     * updates the boc data to database
     * @param type $response
     * @return int
     */
    function editBoc($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        // Check if img file was uploaded
        $post_image = $_FILES['img'];
        $has_img_file_upload = TRUE;
        if (!isset($post_image['tmp_name'])) {
            $has_img_file_upload = FALSE;
        } else if ($post_image['error'] > 0) {
            if ($post_image['error'] !== 4) { // 4: no file uploaded
                Log::warning(__METHOD__ . '() - img appears uploaded but there were errors: ' . $post_image['error']);
            }
            $has_img_file_upload = FALSE;
        }
        //upload the image
        $post_image_prev = $_POST['img_prev'];
        $image = $post_image_prev;
        if ($has_img_file_upload) {
            $folder_path = 'img/bocs';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path, $post_image_prev) : NULL;
        }

        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
            // Find from DB
            $boc = $db->readSingle('Boc', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($boc)) {
                $response->addData('error', 'No such boc ' . $_POST['ID']);
                return 400;
            }
            // Update it
            $boc->update(array(
                'Name' => $_POST['Name'],
                'Designation' => $_POST['Designation'],
                'Company' => $_POST['Company'],
                'Image' => $image,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'LastUpdatedTime' => $time
            ));
            // Update it in DB
            if (!$db->update($boc)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $boc);
                $response->addData('error', 'DB unable to update bus type data.');
                return 500;
            }

            $oldvalue = json_encode($boc->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $boc->getID(),
                'ModelType' => 'Board of Committee',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edit boc"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!
        $response->addData('Boc', $boc->toOutputArray());
        return 200;
    }

    /**
     * Gets the eventd listss
     * @param type $response
     * @return int
     */
    function getEvents($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Read customers from DB
        $db = Database::getConnection();
//        $events = $db->readMultiple('Event', array());
        $events = $db->readMultiple('Event', array(
                ), 0, 0, 'DisplayPriority', 'DESC', FALSE);

        // Handle empty cases
        if (!is_array($events)) {
            $response->addData('events', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        foreach ($events as $event) {
            $data[] = $event->toOutputArray();
        }

        // YAY
        $response->addData('events', $data);
        return 200;
    }

    /**
     * adds event info to the database
     * @param type $response
     * @return int
     */
    function addEvent($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $time = getTimeInMs();

        //Check for image file errors
        $post_image = $_FILES['img'];
        if (!isset($post_image['tmp_name'])) {
            $response->addData('error', 'No image file uploaded.');
            return 400;
        } else if ($post_image['error'] > 0) {
            $response->addData('error', $post_image['error']);
            return 400;
        } else {
            $folder_path = 'img/events';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path) : NULL;
        }

        // Check that doc file was uploaded
        $has_doc_file_upload = TRUE;
        if (!isset($_FILES['doc']['tmp_name'])) {
            $has_doc_file_upload = FALSE;
        } else if ($_FILES['doc']['error'] > 0) {
            if ($_FILES['doc']['error'] !== 4) { // 4: no file uploaded
                Log::warning(__METHOD__ . '() - doc appears uploaded but there were errors: ' . $_FILES['doc']['error']);
            }
            $has_doc_file_upload = FALSE;
        }
        if ($has_doc_file_upload) {
            $post_doc = $_FILES['doc'];
            $folder_path = 'docs';
            $doc = isset($post_doc) ? $this->simply_upload_file($post_doc['tmp_name'], $post_doc['name'], $folder_path, '', array('pdf')) : NULL;
        } else {
            $doc = '';
        }

        // Create event - can be done outside transaction as we don't need persistence (yet)
        try {
            $event = new Event(array(
                'EventName' => $_POST['EventName'],
                'EventDate' => str_replace('/', '-', $_POST['EventDate']),
                'EventFrom' => $_POST['EventFrom'],
                'EventTo' => $_POST['EventTo'],
                'Description' => $_POST['Description'],
                'Venue' => $_POST['Venue'],
                'Latitude' => $_POST['Latitude'],
                'Longitude' => $_POST['Longitude'],
                'CourseFee' => $_POST['CourseFee'],
                'Remarks' => $_POST['Remarks'],
                'Image' => $image,
                'Pdf' => $doc,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'Status' => 'Active',
                'CreatedTime' => $time,
                'LastUpdatedTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // $response->addData('bkesh', $_FILES);
            // return 400;
            // Create in DB
            if (!$db->create($event)) {
                Log::error(__METHOD__ . '() - DB unable to create event');
                $response->addData('error', 'DB unable to create event.');
                return 500;
            }

            //Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
//                'UserID' => 1,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $event->getID(),
                'ModelType' => 'Event',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        //#send email notification if set to send
//        if (isset($_POST['sendNotification'])) {
        $companyMemberLists = array();
//        $companyMemberLists = $this->getMemberEmailList(); //#activate only in live site
        $memberEmailList = array_merge($companyMemberLists, explode(',', CONTACT_EMAIL));

        // Create email
        $mail = new Mail();
        $mail->setSubject('STA Website - Event Notification');
        $mail->setTo($memberEmailList);
        $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Event Notification');
        $mail->setReplyTo(WEBMASTER_EMAIL);
        $mail->addHTMLMail(APPROOT . '/mails/event-notification-message.html');
        $mail->addTextMail(APPROOT . '/mails/event-notification-message.txt');
        $formattedDate = str_replace('/', '-', $_POST['EventDate']);
        $formattedDate = date('d M Y', strtotime($formattedDate));
        $pdfFile = WWW_ROOT . '/docs/' . $event->getPdf();
        if ($event->getPdf() != '' && file_exists($pdfFile)) {
            $pdfLink = WWW_URL . '/docs/' . $event->getPdf();
        } else {
            $pdfLink = "";
        }
        $MapLink = WWW_URL . '/map?latt=' . $_POST['Longitude'] . '&long=' . $_POST['Longitude'];

        $mail->setVariable(array(
            'EventName' => $_POST['EventName'],
            'EventDate' => $formattedDate,
            'EventFrom' => $_POST['EventFrom'],
            'EventTo' => $_POST['EventTo'],
            'EventLink' => WWW_URL . '/event#' . $this->slugify($_POST['EventName']),
            'Description' => $_POST['Description'],
            'Venue' => $_POST['Venue'],
            'Latitude' => $_POST['Latitude'],
            'Longitude' => $_POST['Longitude'],
            'CourseFee' => $_POST['CourseFee'],
            'Remarks' => $_POST['Remarks'],
            'PdfLink' => $pdfLink,
            'MapLink' => $MapLink,
            'API_URL' => API_URL,
            'WWW_URL' => WWW_URL,
        ));

        // Send email
        if (!$mail->send()) {
            Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
            $response->addData('error', 'Server email error!');
            return 500;
        }
//        }//# for triggering the email notification
        // YAY
        $response->addData('Event', $event->toOutputArray());
        return 200;
    }

    /**
     * lists the email of member companies
     * @return type
     */
    function getMemberEmailList() {
        $db = Database::getConnection();
        $companies = $db->readMultiple('Company', array(
            array('DisplayPriority', '>', 0)
                ), 0, 0, 'LastUpdatedTime', 'ASC', FALSE);
        // Make each companies into array
        $companyEmails = array();
        if (is_array($companies)) {
            foreach ($companies as $company) {
                if ($company->getEmail() != '') {
                    $companyEmails[] = $company->getEmail();
                }
            }
        }
        return $companyEmails;
    }

    /**
     * updates the event data to database
     * @param type $response
     * @return int
     */
    function editEvent($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        // Check if image file was uploaded
        $post_image = $_FILES['img'];
        $has_img_file_upload = TRUE;
        if (!isset($post_image['tmp_name'])) {
            $has_img_file_upload = FALSE;
        } else if ($post_image['error'] > 0) {
            if ($post_image['error'] !== 4) { // 4: no file uploaded
                Log::warning(__METHOD__ . '() - img appears uploaded but there were errors: ' . $post_image['error']);
            }
            $has_img_file_upload = FALSE;
        }
        //upload the image
        $post_image_prev = $_POST['img_prev'];
        $image = $post_image_prev;
        if ($has_img_file_upload) {
            $folder_path = 'img/events';
            $image = isset($post_image) ? $this->simply_upload_file($post_image['tmp_name'], $post_image['name'], $folder_path, $post_image_prev) : NULL;
        }

        // Check that doc file was uploaded
        $post_doc = $_FILES['doc'];
        $has_doc_file_upload = TRUE;
        if (!isset($post_doc['tmp_name'])) {
            $has_doc_file_upload = FALSE;
        } else if ($post_doc['error'] > 0) {
            if ($post_doc['error'] !== 4) { // 4: no file uploaded
                Log::warning(__METHOD__ . '() - doc appears uploaded but there were errors: ' . $post_doc['error']);
            }
            $has_doc_file_upload = FALSE;
        }

        //upload the doc
        $post_doc_prev = $_POST['doc_prev'];
        $doc = $post_doc_prev;
        $folder_path = 'docs';
        if ($has_doc_file_upload) {
            $doc = isset($post_doc) ? $this->simply_upload_file($post_doc['tmp_name'], $post_doc['name'], $folder_path, $post_doc_prev, array('pdf')) : NULL;
        }

        if ($_POST["PdfDeletedOnEdit"] == "true") {
            $doc = '';
            if ($post_doc_prev != '') {
                $path = WEBROOT . '/' . $folder_path . '/' . $post_doc_prev;
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }

        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
            // Find from DB
            $event = $db->readSingle('Event', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($event)) {
                $response->addData('error', 'No such event ' . $_POST['ID']);
                return 400;
            }

            // Update it
            $event->update(array(
                'EventName' => $_POST['EventName'],
                'EventDate' => str_replace('/', '-', $_POST['EventDate']),
                'EventFrom' => $_POST['EventFrom'],
                'EventTo' => $_POST['EventTo'],
                'Description' => $_POST['Description'],
                'Venue' => $_POST['Venue'],
                'Latitude' => $_POST['Latitude'],
                'Longitude' => $_POST['Longitude'],
                'CourseFee' => $_POST['CourseFee'],
                'Remarks' => $_POST['Remarks'],
                'Image' => $image,
                'Pdf' => $doc,
                'DisplayPriority' => $_POST['DisplayPriority'],
                'LastUpdatedTime' => $time
            ));
            Log::warning(__METHOD__ . '() - event arrary edit: ' . json_encode($event->toOutputArray()));

            // Update it in DB
            if (!$db->update($event)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $event);
                $response->addData('error', 'DB unable to update bus type data.');
                return 500;
            }

            $oldvalue = json_encode($event->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $event->getID(),
                'ModelType' => 'Event',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edit event"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        //#send email notification if set to send
        if (isset($_POST['sendNotification'])) {
            $companyMemberLists = array();
//        $companyMemberLists = $this->getMemberEmailList(); //#activate only in live site
            $memberEmailList = array_merge($companyMemberLists, explode(',', CONTACT_EMAIL));

            // Create email
            $mail = new Mail();
            $mail->setSubject('STA Website - Event Notification Edit');
            $mail->setTo($memberEmailList);
            $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Event Notification Edit');
            $mail->setReplyTo(WEBMASTER_EMAIL);
            $mail->addHTMLMail(APPROOT . '/mails/event-notification-message.html');
            $mail->addTextMail(APPROOT . '/mails/event-notification-message.txt');
            $formattedDate = str_replace('/', '-', $_POST['EventDate']);
            $formattedDate = date('d M Y', strtotime($formattedDate));
            $pdfFile = WWW_ROOT . '/docs/' . $event->getPdf();
            if ($event->getPdf() != '' && file_exists($pdfFile)) {
                $pdfLink = WWW_URL . '/docs/' . $event->getPdf();
            } else {
                $pdfLink = "";
            }
            $MapLink = WWW_URL . '/map?latt=' . $_POST['Longitude'] . '&long=' . $_POST['Longitude'];

            $mail->setVariable(array(
                'EventName' => $_POST['EventName'],
                'EventDate' => $formattedDate,
                'EventFrom' => $_POST['EventFrom'],
                'EventTo' => $_POST['EventTo'],
                'EventLink' => WWW_URL . '/event#' . $this->slugify($_POST['EventName']),
                'Description' => $_POST['Description'],
                'Venue' => $_POST['Venue'],
                'Latitude' => $_POST['Latitude'],
                'Longitude' => $_POST['Longitude'],
                'CourseFee' => $_POST['CourseFee'],
                'Remarks' => $_POST['Remarks'],
                'PdfLink' => $pdfLink,
                'MapLink' => $MapLink,
                'API_URL' => API_URL,
                'WWW_URL' => WWW_URL,
            ));

            // Send email
            if (!$mail->send()) {
                Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
                $response->addData('error', 'Server email error!');
                return 500;
            }
        }//# for triggering the email notification
        // YAY!
        $response->addData('Event', $event->toOutputArray());
        return 200;
    }

    /**
     * Adds Backend User to system
     * - must be logged in as Admin
     * @input: $_POST['Email, Password, FirstName, LastName, Status']
     * @output: Backend User
     */
    function addBackendUser($response) {
        // Ensure action is legal - check that the user is indeed a staff with admin rights
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        $time = getTimeInMs();
        $_POST = json_decode(file_get_contents('php://input'), true);
        // Generate hash ahead of transaction
        $passwordHash = Encryptor::getHash($_POST['Password']);

        // Create backend user - can be done outside transaction as we don't need persistence (yet)
        try {
            $backendUser = new BackendUser(array(
                'FirstName' => $_POST['FirstName'],
                'LastName' => $_POST['LastName'],
                'Email' => $_POST['Email'],
                'PasswordHash' => $passwordHash,
                'LastUpdatedDateTime' => $time,
                'RoleID' => $_POST['RoleID'],
                'Status' => $_POST['Status']
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check that this email is not in use
            $existing_backendUser = $db->readSingle('BackendUser', array(
                array('Email', '=', $_POST['Email'])
                    ), TRUE);
            if (isset($existing_backendUser)) {
                $response->addData('error', 'This email is already in use.');
                return 400;
            }

            // Create user in DB
            if (!$db->create($backendUser)) {
                Log::error(__METHOD__ . '() - DB unable to create backend user ' . $_POST['Email']);
                $response->addData('error', 'DB unable to create backend user.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Add',
                'ModelID' => $backendUser->getID(),
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added backendUser"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('backendUser', $backendUser->toOutputArray());
        return 200;
    }

    /**
     * Edits a backend user
     * - must be logged in as Admin
     * @input: $_POST['ID, Email, Password, FirstName, LastName']
     * @output: Staff
     */
    function editBackendUser($response) {
        // Ensure action is legal - check that the user is indeed a staff with admin rights
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $BackendUser = $db->readSingle('BackendUser', array(
                array('ID', '=', $_POST['ID'])
                    ), TRUE);
            if (!isset($BackendUser)) {
                $response->addData('error', 'No such BackendUser ' . $_POST['ID']);
                return 400;
            }

            // Update it
            $updated = $BackendUser->update(array(
                'ID' => $_POST['ID'],
                'FirstName' => $_POST['FirstName'],
                'LastName' => $_POST['LastName'],
                'Email' => $_POST['Email'],
                'RoleID' => $_POST['RoleID'],
                'LastUpdatedDateTime' => $time,
                'Status' => $_POST['Status']
            ));

            // Update it in DB
            if (!$db->update($BackendUser)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $BackendUser);
                $response->addData('error', 'DB unable to update bus type data.');
                return 500;
            }

            $oldvalue = json_encode($BackendUser->toOutputArray());
            $newvalue = json_encode($_POST);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'BackendUser',
                'Action' => 'Update',
                'ModelID' => $BackendUser->getID(),
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time,
                'OldValue' => $oldvalue,
                'NewValue' => $newvalue
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' edited bus type"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('BackendUser', $BackendUser->toOutputArray());
        return 200;
    }

    /**
     * Gets a list of Backend User
     * @output: array of Backend User {ID, Email, Password, FirstName, LastName'}
     */
    function getBackendUser($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $BackendUser = $db->readMultiple('BackendUser', array());

        // Handle empty cases
        if (!is_array($BackendUser)) {
            $response->addData('BackendUser', array());
            return 202;
        }
        // Make each Language into array
        $count = 0;
        foreach ($BackendUser as $key => $BackendUser) {
            if (!$BackendUser->IsDeleted()) {
                $data[$count] = $BackendUser->toOutputArray();
                $roleId = $db->readSingle('Role', array(
                    array('ID', '=', $data[$count]['RoleID'])
                ));
                $name = $roleId->toOutputArray();
                $data[$count]['RoleName'] = $name['Name'];
                $count++;
            }
        }
        // YAY
        $response->addData('BackendUser', $data);
        return 200;
    }

    /*     * * ******************** *** Module *** ******************** ** */
    /*     * *
     * Gets a list of Module
     * @output: array of Module
     */

    function getModule($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        // Init return data
        $data = array();


        //Read Surcharge from DB
        $db = Database::getConnection();
        $module = $db->readMultiple('Module', array());

        // Handle empty cases
        if (!is_array($module)) {
            $response->addData('Module', array());
            return 202;
        }

        // Make each $role into array
        foreach ($module as $module) {
            $data[] = $module->toOutputArray();
        }
        // YAY
        $response->addData('Module', $data);
        return 200;
    }

//getModule()

    /*     * * ******************** *** Activity *** ******************** ** */

    /*     * *
     * Gets a list of Module
     * @output: array of Module
     */
    function getActivity($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }

        //$_POST = json_decode(file_get_contents('php://input'), true);
        $start = ($_POST['pageNumber'] - 1) * 10;

        // Init return data
        $data = array();
        //Read Surcharge from DB
        $db = Database::getConnection();
        $activity = $db->readMultiple('Activity', array(array('UserType', '=', 'BackendUser')), $start, 10, 'ID', 'DESC');
        // Handle empty cases
        if (!is_array($activity)) {
            $response->addData('Activity', array());
            return 202;
        }

        // Make each $role into array
        $count = 0;
        foreach ($activity as $activity) {
            $data[$count] = $activity->toOutputArray();
            if ($data[$count]['UserType'] == "BackendUser") {
                $userID = $db->readSingle('BackendUser', array(
                    array("ID", "=", $data[$count]['UserID'])
                ));
                if ($userID) {
                    $activityData[$count] = $data[$count];
                    $activityData[$count]['UserEmail'] = $userID->toOutputArray()['Email'];
                    $count++;
                }
            }
        }
        $activityList = $db->readMultiple('Activity', array(
            array('UserType', '=', 'BackendUser')
        ));
        // Handle empty cases
        if (is_array($activityList)) {
            $number = explode('.', count($activityList) / 10);
            if ($number[1])
                $number = $number[0] + 1;
            else
                $number = $number[0];
            $response->addData('TotalPage', $number);
        }
        // YAY
        $response->addData('Activity', $activityData);
        return 200;
    }

    /**
     * simply uploads the image to server
     * @param type $temp_name
     * @param type $name
     * @param type $folder
     * @param type $prev_image
     * @return string
     */
    function simply_upload_file($temp_name, $name, $folder, $prev_image = NULL, $file_types = NULL) {
        if ($name != '') {
            $file = time();
            $tempFile = $temp_name;
            $targetPath = WWW_ROOT . '/' . $folder;
            if (!file_exists($targetPath)) {
                Log::error(__METHOD__ . '() - Target path is not found' . $targetPath);
                return $prev_image;
            }
            $new_name = $file . '_' . $name;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_name;

            // Validate the file type
            if ($file_types != '') {
                $fileTypes = $file_types;
            } else {
                $fileTypes = array('png', 'jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'svg'); // File extensions
            }
            $fileParts = pathinfo($name);

            if (in_array($fileParts['extension'], $fileTypes)) {

                if (!move_uploaded_file($tempFile, $targetFile)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $targetFile);
                    return $prev_image;
                } else {
                    //delete previous image
                    if ($prev_image != '') {
                        $path = WEBROOT . '/' . $folder . '/' . $prev_image;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    return $new_name;
                }
            } else {
                return $prev_image;
            }
        } else {
            return $prev_image;
        }
    }

    /**
     * gets the url friendly string
     * @param type $text
     * @return string
     */
    function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Edits the logged in staff profile.
     * - nust be logged in
     * @input: $_POST['firstName, lastName, email']
     * @output: Staff {id, firstName, lastName, email, createdTime, lastUpdateTime, lastActivtyTime, accessLevel, banned , language}
     */
    function editAccountProfile($response) {
        if (!isset(getallheaders()['Authorization'])) {
            $response->setData(array('error' => 'No Token'));
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if (!$users) {
            $response->setData(array('error' => 'Token Expired'));
            return 401;
        }
//        $_POST = json_decode(file_get_contents('php://input'), true);

        $time = getTimeInMs();

        // Cache variables
        $userID = $users->userid;

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('BackendUser', array(
                array('ID', '=', $userID)
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'Unable to find user in DB.');
                return 500;
            } // No such user
            // Update staff (may throw exceptions if input data invalid)
            try {
                $updated = $user->update(array(
                    'FirstName' => $_POST['FirstName'],
                    'Phone' => $_POST['Phone'],
                    'LastUpdatedDateTime' => $time
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }
            // Store session staff to DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' in DB');
                $response->addData('error', 'DB Unable to update new profile');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $userID,
                'UserType' => $users->usertype,
                'Action' => 'Edit Profile',
                'ModelID' => $userID,
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' edited profile');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!             
        $response->addData('BackendUser', $user->toOutputArray());
        return 200;
    }

    function forgotPassword($response) {
        // Data check: email
        if (!isset($_POST['email'])) {
            $response->addData('error', 'Email is required.');
            return 400;
        }
        if (!Validator::isEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email: ' . $_POST['email']);
            return 400;
        } else {
            $email = strtolower($_POST['email']);
        }
//        >>bkesh
        // Find this user in DB
        $db = Database::getConnection();
        $user = $db->readSingle('BackendUser', array(
            array('Email', '=', $email)
                ), TRUE);
        if (!isset($user)) {
            $response->addData('error', 'No such staff.');
            return 404;
        }
        $userID = $user->getID();
        $time = getTimeInMs();
        $verification_code = Encryptor::generateRandomNumericCode();

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'VerificationCode' => $verification_code,
                    'LastUpdatedDateTime' => $time
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }

            // Update him in DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update new profile.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $userID,
                'UserType' => 'BackendUser',
                'Action' => 'Forgot Password',
                'ModelID' => $userID,
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' change password"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
//        >>bkesh
        // Create email
        $mail = new Mail();
        $mail->setSubject('STA Website - Password Reset Email');
        $mail->setTo($email);
        $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Password Reset Email');
        $mail->setReplyTo(WEBMASTER_EMAIL);
        $mail->addHTMLMail(APPROOT . '/mails/password-reset-message.html');
        $mail->addTextMail(APPROOT . '/mails/password-reset-message.txt');
        $mail->setVariable(array(
            'resetLink' => STAFF_URL . '/resetPassword/' . $verification_code,
        ));

        // Send email
        if (!$mail->send()) {
            Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
            $response->addData('error', 'Server email error!');
            return 500;
        }
        // YAY
        return 200;
    }

    function resetPassword($response) {
        // Data check: code
        if (!isset($_GET['code'])) {
            $response->addData('error', 'Verification code is missing.');
            return 400;
        }
        $verification_code = $_GET['code'];
        // Find this user in DB
        $db = Database::getConnection();
        $user = $db->readSingle('BackendUser', array(
            array('VerificationCode', '=', $verification_code)
                ), TRUE);
        if (!isset($user)) {
            $response->addData('error', 'Invalid verification code.');
            return 404;
        }
        $userID = $user->getID();
        $time = getTimeInMs();
        if (strlen($_POST['NewPassword']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Password must be at least ' . MIN_PASSWORD_LENGTH . ' characters.');
            return 400;
        }

        // Cache variables
        $hash = Encryptor::getHash($_POST['NewPassword']);

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            if (!isset($_POST['NewPassword']) || strlen($_POST['NewPassword']) < MIN_PASSWORD_LENGTH) {
                $response->addData('error', 'Password NOT changed: your password must be at least ' . MIN_PASSWORD_LENGTH . ' characters long.');
                return 400;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'PasswordHash' => $hash,
                    'VerificationCode' => '',
                    'LastUpdatedDateTime' => $time
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }

            // Update him in DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update new profile.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $userID,
                'UserType' => 'BackendUser',
                'Action' => 'Reset Password',
                'ModelID' => $userID,
                'ModelType' => 'BackendUser',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' change password"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        return 200;
    }

//editAccountProfile()
}
