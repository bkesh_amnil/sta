<?php

class PublicAPI {

    function test($response) {
        $price = InternalAPI::calculateShirtDesignPrice($_GET['id']);
        $response->addData('price', $price);

        $db = Database::getConnection();
        $order_items = $db->readMultiple('TailorKitOrderItem', array(
            array('orderID', '=', $_GET['id'])
                ), 0, 0, NULL, 'ASC', FALSE);
        $items_data = InternalAPI::denormalizeTaiorKitOrderItems($order_items);

        $response->addData('items_data', $items_data);

        return 200;
    }

    /*     * *
     * Gets the current session user
     * @input: none
     * @output: User {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime}
     * * */

    function getSessionUser($response) {
        $response->addData('user', $_SESSION['user']->toOutputArray());
        return 200;
    }

    /**
     * Gets a list of advertisements. The highest priority ones will be returned.
     * @output: array of advertisements
     */
    function getAdvertisements($response) {
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for count: ' . $_GET['fromTime']);
            return 400;
        }

        // Read testimonials from DB
        $db = Database::getConnection();
        $advertisements = $db->readMultiple('Advertisement', array(
            array('LastUpdatedTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'LastUpdatedTime', 'ASC', FALSE);
        // Handle empty cases
        if (!is_array($advertisements)) {
            $response->addData('advertisements', array());
            return 202;
        }

        // Make each advertisement into array
        $data = array();

        $i = 0;
        foreach ($advertisements as $advertisement) {
            $data[] = $advertisement->toOutputArray();
            $data[$i]['id'] = $advertisement->getID();
            $imageFile = WWW_ROOT . '/img/advertisements/' . $advertisement->getAdvertisementImage();
            if (file_exists($imageFile)) {
                $data[$i]['hasImage'] = 1;
            } else {
                $data[$i]['hasImage'] = 0;
            }
            $i++;
        }
        // YAY
        $response->addData('advertisements', $data);
        return 200;
    }

    /**
     * Gets a list of events that have been updated since a certain time
     * @output: array of events
     */
    function getUpdatedEvents($response) {
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for count: ' . $_GET['fromTime']);
            return 400;
        }

        // Read events from DB
        $db = Database::getConnection();
        $events = $db->readMultiple('Event', array(
            array('LastUpdatedTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'LastUpdatedTime', 'ASC', FALSE);

        // Handle empty cases
        if (!is_array($events)) {
            $response->addData('events', array());
            return 202;
        }

        // Make each events into array
        $data = array();
        $i = 0;
        foreach ($events as $event) {
            $timestamp = strtotime($event->getEventDate());
            if ($timestamp < time()) {
                continue;
            }
            $data[] = $event->toOutputArray();
            $data[$i]['id'] = $event->getID();
            //for date
            $data[$i]['EventTimestamp'] = $timestamp;
            $data[$i]['EventYear'] = date('Y', $timestamp);
            $data[$i]['EventMonth'] = date('M', $timestamp);
            $data[$i]['EventDay'] = date('d', $timestamp);

            //# check if image exists
            $imageFile = WWW_ROOT . '/img/events/' . $event->getEventImage();
            if ($event->getEventImage() != '' && file_exists($imageFile)) {
                $data[$i]['hasImage'] = 1;
            } else {
                $data[$i]['hasImage'] = 0;
            }
            //# check if pdf exists
            $pdfFile = WWW_ROOT . '/docs/' . $event->getPdf();
            if ($event->getPdf() != '' && file_exists($pdfFile)) {
                $data[$i]['hasPdf'] = 1;
            } else {
                $data[$i]['hasPdf'] = 0;
            }
            $i++;
        }
        // YAY
        $response->addData('events', $data);
        return 200;
    }

    /**
     * Gets a list of bocs that have been updated since a certain time
     * @output: array of bocs
     */
    function getUpdatedBocs($response) {
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }

        // Read events from DB
        $db = Database::getConnection();
        $bocs = $db->readMultiple('Boc', array(
            array('LastUpdatedTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'LastUpdatedTime', 'ASC', FALSE);

        // Handle empty cases
        if (!is_array($bocs)) {
            $response->addData('bocs', array());
            return 202;
        }

        // Make each bocs into array
        $data = array();
        $i = 0;
        foreach ($bocs as $boc) {
            $data[] = $boc->toOutputArray();
            $data[$i]['id'] = $boc->getID();
            $imageFile = WWW_ROOT . '/img/bocs/' . $boc->getBocImage();
            if (file_exists($imageFile)) {
                $data[$i]['hasImage'] = 1;
            } else {
                $data[$i]['hasImage'] = 0;
            }
            $i++;
        }
        // YAY
        $response->addData('bocs', $data);
        return 200;
    }

    /**
     * Gets a list of companies that have been updated since a certain time
     * @output: array of companies
     */
    function getUpdatedCompanies($response) {
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }

        // Read companies from DB
        $db = Database::getConnection();
        $companies = $db->readMultiple('Company', array(
            array('LastUpdatedTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'LastUpdatedTime', 'ASC', FALSE);

        // Handle empty cases
        if (!is_array($companies)) {
            $response->addData('companies', array());
            return 202;
        }

        // Make each companies into array
        $data = array();
        $i = 0;
        foreach ($companies as $company) {
            $data[] = $company->toOutputArray();
            $data[$i]['id'] = $company->getID();
            //# for representative
            $r_data = array();
            $representatives = $db->readMultiple('Representative', array(
                array("CompanyID", "=", $company->getID())
            ));
            if (is_array($representatives)) {
                foreach ($representatives as $representative) {
                    $r_data[] = $representative->toOutputArray();
                }
            }
            //# for Associatecompany
            $a_data = array();
            $associatecompanys = $db->readMultiple('Associatecompany', array(
                array("CompanyID", "=", $company->getID())
            ));
            if (is_array($associatecompanys)) {
                foreach ($associatecompanys as $associatecompany) {
                    $a_data[] = $associatecompany->toOutputArray();
                }
            }
            $data[$i]['Representatives'] = $r_data;
            $data[$i]['Associates'] = $a_data;
            $i++;
        }
        // YAY
        $response->addData('companies', $data);
        return 200;
    }

    /**
     * send email for event registration
     * @param type $response
     * @return int
     */
    function sendEventRegistrationMessage($response) {
        // Data check: name
        if (!isset($_POST['contact_name']) || !Validator::isName($_POST['contact_name'])) {
            $response->addData('error', 'Invalid Contact name: ' . $_POST['contact_name']);
            return 400;
        } else {
            $contact_name = trim($_POST['contact_name']);
        }

        // Data check: email
        if (!isset($_POST['email'])) {
            $response->addData('error', 'Email is required.');
            return 400;
        }
        if (!Validator::isEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email: ' . $_POST['email']);
            return 400;
        } else {
            $email = strtolower($_POST['email']);
        }

        // Data check: mobile
//		if (!isset($_POST['mobile']) || !Validator::isMobile($_POST['mobile'])) {
//			$response->addData('error', 'Illegal Mobile.');
//			return 400;
//		}
//		else {
//			$mobile = trim($_POST['mobile']);
//		}
        // Create email
        $mail = new Mail();
        $mail->setSubject('STA Website - Event Registration');
        $mail->setTo(explode(',', CONTACT_EMAIL));
//            $mail->setBcc(CONTACT_EMAIL);
        $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Event Registration');
        $mail->setReplyTo($email);
        $mail->addHTMLMail(APPROOT . '/mails/event-reservation-message.html');
        $mail->addTextMail(APPROOT . '/mails/event-reservation-message.txt');
        $mail->setVariable(array(
            'contact_name' => $contact_name,
            'company_name' => $_POST['company_name'],
            'email' => $email,
            'mobile' => $_POST['mobile'],
            'no_of_participants' => $_POST['participants'],
            'event_name' => $_POST['event_name'],
        ));

        // Send email
        if (!$mail->send()) {
            Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
            $response->addData('error', 'Server email error!');
            return 500;
        }
        // YAY
        return 200;
    }

    /**
     * Sends email to website contact person
     * @output: message
     */
    function sendContactMessage($response) {
        // Data check: name
        if (!isset($_POST['contact_name']) || !Validator::isName($_POST['contact_name'])) {
            $response->addData('error', 'Invalid Contact name: ' . $_POST['contact_name']);
            return 400;
        } else {
            $contact_name = trim($_POST['contact_name']);
        }

        // Data check: email
        if (!isset($_POST['email'])) {
            $response->addData('error', 'Email is required.');
            return 400;
        }
        if (!Validator::isEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email: ' . $_POST['email']);
            return 400;
        } else {
            $email = strtolower($_POST['email']);
        }

        // Data check: message
        if (!isset($_POST['message']) || strlen(trim($_POST['message'])) == 0) {
            $response->addData('error', 'Message cannot be empty.');
            return 400;
        }
        // Create email
        $mail = new Mail();
        $mail->setSubject('STA Website - Contact Us Enquiry');
        $mail->setTo(explode(',', CONTACT_EMAIL));
//            $mail->setBcc(CONTACT_EMAIL);
        $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Contact Us Enquiry');
        $mail->setReplyTo($email);
        $mail->addHTMLMail(APPROOT . '/mails/contact-message.html');
        $mail->addTextMail(APPROOT . '/mails/contact-message.txt');
        $mail->setVariable(array(
            'contact_name' => $contact_name,
            'company_name' => $_POST['company_name'],
            'email' => $email,
            'mobile' => $_POST['mobile'],
            'message' => $_POST['message'],
        ));

        // Send email
        if (!$mail->send()) {
            Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
            $response->addData('error', 'Server email error!');
            return 500;
        }
        // YAY
        return 200;
    }

    /**
     * Sends email to website contact person
     * @output: message
     */
    function sendBizMatchMessage($response) {
        // Data check: name
        if (!isset($_POST['contact_name']) || !Validator::isName($_POST['contact_name'])) {
            $response->addData('error', 'Invalid Contact name: ' . $_POST['contact_name']);
            return 400;
        } else {
            $contact_name = trim($_POST['contact_name']);
        }

        // Data check: email
        if (!isset($_POST['email'])) {
            $response->addData('error', 'Email is required.');
            return 400;
        }
        if (!Validator::isEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email: ' . $_POST['email']);
            return 400;
        } else {
            $email = strtolower($_POST['email']);
        }

        if (isset($_POST['business_type'])) {
            $business_type = '<ul>';
            foreach ($_POST['business_type'] as $bt) {
                $business_type .= '<li> - ' . $bt . '</li>';
            }
            $business_type .= '</ul>';
        } else {
            $business_type = 'Not selected';
        }
        if (isset($_POST['product_interest'])) {
            $product_interest = '<ul>';
            foreach ($_POST['product_interest'] as $pi) {
                $product_interest .= '<li> - ' . $pi . '</li>';
            }
            $product_interest .= '</ul>';
        } else {
            $product_interest = 'Not selected';
        }
        // Create email
        $mail = new Mail();
        $mail->setSubject('STA Website - Business Matching');
        $mail->setTo(explode(',', CONTACT_EMAIL));
//            $mail->setBcc(CONTACT_EMAIL);
        $mail->setFrom(NO_REPLY_EMAIL, 'STA Website - Business Matching');
        $mail->setReplyTo($email);
        $mail->addHTMLMail(APPROOT . '/mails/biz-match-message.html');
        $mail->addTextMail(APPROOT . '/mails/biz-match-message.txt');
        $mail->setVariable(array(
            'contact_name' => $contact_name,
            'company_name' => $_POST['company_name'],
            'company_address' => $_POST['company_address'],
            'email' => $email,
            'mobile' => $_POST['mobile'],
            'designation' => $_POST['designation'],
            'business_type' => $business_type,
            'product_interest' => $product_interest,
            'source_country' => $_POST['source_country'],
            'products' => $_POST['products'],
            'company_details' => $_POST['company_details'],
        ));

        // Send email
        if (!$mail->send()) {
            Log::fatal(__METHOD__ . '() - unable to send out email to ' . $email);
            $response->addData('error', 'Server email error!');
            return 500;
        }
        // YAY
        return 200;
    }

}

//class PublicAPI
