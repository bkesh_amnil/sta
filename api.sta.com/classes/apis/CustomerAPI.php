<?php
require APPROOT .'/vendor/firebase/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;
define('ENCRYPTION_KEY', 'd0a7e7997b6d5fcd55f4b5c32611b87cd923e88837b63bf2941ef819dc8ca282');

class CustomerAPI {

    function generateToken($id, $usertype){
        $time=getTimeInMs();
        $token = array(
            "userid" => $id,
            "usertype" => $usertype,
            "time" => $time,
            "exp" => $time+TOKEN_KEY_EXPIRY,
        );
        $jwt = JWT::encode($token, TOKEN_KEY);
        return $jwt;
    }

    function generateCode($id,$reason,$type){
        $time = getTimeInMs();
        // Generate registration verification code
        $registrationCode = Encryptor::generateRandomNumericCode(REGISTRATION_CODE_LENGTH);
        $db = Database::getConnection();
        $setting = $db->readSingle('Setting', array(
            array('SettingName', '=', 'CodeExpixyInMinutes')
        ));

        $code = new VerificationCodeLog (array(
            'VerificationType' => $reason,
            'ApplicationUserID' => $id,
            'Code' => $registrationCode,
            'CodeSentDateTime'=>$time,
            'CodeExpireDateTime'=>$time+($setting->toOutputArray()['SettingValue']*60*1000),
            'NotificationMediumType'=>$type
        ));
        return $code;
    }

    function checkTokenExpiry($token){
        try {
            return JWT::decode($token, TOKEN_KEY, array('HS256'));
        } catch (Exception $e) {
            return false;
        }
    }

    function checkRegistrationCode($code,$reason,$userid){
        $time=getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $verificationCode = $db->readSingle('VerificationCodeLog', array(
            array('ApplicationUserID', '=', $userid),
            array('Code','=',$code),
            array('VerificationType','=',$reason)
        ));
        if (isset($verificationCode)) {
            $verificationCode = $verificationCode->toOutputArray();
            if($verificationCode['CodeExpireDateTime'] > $time){
                return array('message'=>'','status'=>'success');
            }else{
                return array('message'=>'Code has been expired','status'=>'error', 'errorType'=>'CodeExpired');
            }
        }else{
            return array('message'=>'Code Did not match','status'=>'error', 'errorType'=>'InvalidCode');
        }

    }

    function returnData($response,$success,$msg,$data){
        $response->addData('success', $success);
        $response->addData('msg', $msg);
        if($data){
            $response->addData('data', $data);
        }
    }

    function getServerTime($response){

        $time=(new DateTime())->format('d/m/Y H:i:s');
        $this->returnData($response,1,'Date',$time);

        return 200;

    }

    function getServerDateTimeForBookingNumber(){
        $time=(new DateTime())->format('dmy');
        return $time;
    }

    function calculateDisputeNumber() {
        // Get timestamps
        $month =(new DateTime())->format('n');
        $day = (new DateTime())->format('j');
        $year = (new DateTime())->format('Y');

        $time_from = mktime(0, 0, 0, $month, $day, $year) * 1000;
        $time_to = mktime(23, 59, 59, $month, $day, $year) * 1000;

        $db = Database::getConnection();
        $disputes = $db->readMultiple('BookingDispute', array(
            array('RaisedDateTime', '>', $time_from),
            array('RaisedDateTime', '<', $time_to)
        ), FALSE);

        $next_num = count($disputes) + 1;
        return 'DT-'.date('ymd'). str_pad($next_num, DISPUTE_NUMBER_PADDING_LENGTH, '0', STR_PAD_LEFT);
    } //calculateDisputeNumber()

    function calculateBookingNumber() {
        $month =(new DateTime())->format('n');
        $day = (new DateTime())->format('j');
        $year = (new DateTime())->format('Y');
        $time_from = mktime(0, 0, 0, $month, $day, $year) * 1000;
        $db = Database::getConnection();
        $number = $db->readMultiple('Booking', array(
            array('CreatedDateTime', '>=',$time_from)
        ));
        if($number){
            $number=end($number);
            $bookingNumber=str_pad(((explode('-',($number->getBookingNumber()))[2])+1), BOOKING_NUMBER_PADDING_LENGTH, '0', STR_PAD_LEFT);
        }else{
            $bookingNumber=str_pad(1, BOOKING_NUMBER_PADDING_LENGTH, '0', STR_PAD_LEFT);
        }
        return 'WB-'.date('ymd').'-'.$bookingNumber;
    } //calculateBookingNumber()

    /*** ******************** *** User *** ******************** ***/

    function register($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }

        if (!isValidEmail($_POST['Email'])) {
            $response->addData("error_code", 1);
            $this->returnData($response,0,'Invalid email '.$_POST['Email'],'');
            return 400;
        }
        else {
            $_POST['Email'] = strtolower($_POST['Email']);
        }
        $passwordHash = Encryptor::getHash($_POST['Password']);
        $time = getTimeInMs();
        // Create user
        $user = new ApplicationUser(array(
            'Email' => $_POST['Email'],
            'Name' => $_POST['Name'],
            'Mobile' => $_POST['Mobile'],
            'Password' => $passwordHash,
            'UserType'=>'Customer',
            'LanguageID'=>'1',
            'Status'=> 'Verification Pending'
        ));

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            // Check that this email is not in use
            $existing_user = $db->readSingle('ApplicationUser', array(
                array('Email', '=', $_POST['Email']),
                array('Status','!=','Delete')
            ), TRUE);
            $existing_user1 = $db->readSingle('ApplicationUser', array(
                array('Mobile', '=', $_POST['Mobile']),
                array('Status','!=','Delete')
            ), TRUE);
            if (isset($existing_user) || isset($existing_user1)){
                if(isset($existing_user) && $existing_user->isEmailVerified()){
                    $response->addData("error_code", 2);
                    $this->returnData($response,0,'This email is already in use.','');
                    return 405;
                }elseif(isset($existing_user1) && $existing_user1->isEmailVerified()) {
                    $response->addData("error_code", 3);
                    $this->returnData($response,0,'This mobile is already in use.','');
                    return 406;
                }else{
                    if(isset($existing_user)){
                        $user_id= $existing_user->getID();
                    }else{
                        $user_id = $existing_user1->getID();
                    }

                    // Create and store this activity
                    $activity = new Activity(array(
                        'UserID' =>  $user_id,
                        'UserType' => 'Customer',
                        'Action' => 'Registered ',
                        'ModelID' => $user->getID(),
                        'ModelType' => 'Application User',
                        'CreatedDateTime' => $time
                    ));

                    if (!$db->create($activity)) {
                        Log::error(__METHOD__.'() - DB Unable to create activity "' . $existing_user . ' register (retry)"');
                    }
                }
            }else{
                // Create user
                if (!$db->create($user)) {
                    Log::error(__METHOD__.'() - DB unable to create user.');
                    $this->returnData($response,0,'DB unable to create user.','');
                    return 500;
                }

                // Create and store this activity
                $activity = new Activity(array(
                    'UserID' => $user->getID(),
                    'UserType' => 'Customer',
                    'Action' => 'Register',
                    'ModelID' => $user->getID(),
                    'ModelType' => 'Application User',
                    'CreatedDateTime' => $time
                ));
                $user_id=$user->getID();
                if (!$db->create($activity)) {
                    Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' register"');
                }
                $notificationSetting = new ApplicationUserNotificationSetting(array(
                    'ApplicationUserID' =>  $user_id,
                    'Email' => '1',
                    'SMS' => '0',
                    'LastUpdatedDateTime' => $time
                ));
                if (!$db->create($notificationSetting)) {
                    Log::error(__METHOD__.'() - DB Unable to create notification Setting "' . $existing_user . ' register "');
                    $this->returnData($response,0,'Unable to create notification setting','');
                    return 500;
                }// Customer already exists (but not verified)
            }// Customer does not exist

            //generate code
            $code = $this->generateCode($user_id,'Registration','Email');
            if (!$db->create($code)) {
                Log::error(__METHOD__.'() - DB Unable to create code "' . $user . ' register"');
                $this->returnData($response,0,'DB unable to create code.','');
                return 500;
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction.','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        }
        $message="Thank you for signing up with <span style='color:#006ab4'>WantBus</span>! To complete your registration, simply copy and paste this verification code in the app - <b>".$code->toOutputArray()['Code']."</b>.";
        if($_POST['Email'] !=''){
            $mailDetails = array(
                'subject'=>'Registration Code',
                'email'=>$_POST['Email'],
                'message'=> $message,
                'file'=>'registration-code'
            );
            $sendEmail=$this->sendEmail($mailDetails);
            if(!$sendEmail){
                $this->returnData($response,0,'Unable to send Email','');
                return 500;
            }
        }
        if($_POST['Mobile'] !=''){
            /* $sendSMS=$this->sendSMS($_POST['Mobile'],$message);
             if(!$sendSMS){
                 $this->returnData($response,0,'Unable to send SMS','');
                 return 500;
             }
             $data['sms']=$sendSMS;*/
        }

        $data['token']=$this->generateToken($user_id,'Customer');
        $this->returnData($response,1,'Registration Successful',$data);
        return 200;
    }

    function login($response) {
        // Data checks
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if (!isValidEmail($_POST['UserName'])) {
            $username['name']= "Mobile";
            $username['value']= $_POST['UserName'];
        }else{
            $username['name']= "Email";
            $username['value']= strtolower($_POST['UserName']);
        }
        if (strlen($_POST['Password']) < MIN_PASSWORD_LENGTH) {
            $this->returnData($response,0,'Wrong Password','');
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('ApplicationUser', array(
                array($username['name'], '=', $username['value'])
            ));

            if (!isset($user)) {
                $response->addData("error_code", 1);
                $this->returnData($response,0,'User not registered','');
                return 400;
            } // No such user

            // Check if account is banned
            if ($user->isBanned()) {
                $response->addData("error_code", 2);
                $this->returnData($response,0,'Account banned.','');
                return 403;
            }

            // Check if account is email verified
            if (!$user->isEmailVerified()) {
                $response->addData("error_code", 3);
                $this->returnData($response,0,'Account not Verified.','');
                return 402;
            }

            // Check password
            if (!$user->checkPassword($_POST['Password'])) {
                $response->addData("error_code", 4);
                $this->returnData($response,0,'Wrong Pasword','');
                return 401;
            } // Wrong password

            // Generate persistent cookie if requested
            if ($_POST['persistent']) {
                $cookie = $user->generatePersistentLoginCookie();
                $response->addData('persistentLoginCookie', $cookie);
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => $user->getUserType(),
                'Action' => 'Login',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' login"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $user=$user->toOutputArray();
        $user=array(
            'ID'=>$user['ID'],
            'UserType'=>$user['UserType'],
            'Name'=>$user['Name'],
            'Photo'=>$user['Photo'],
            'Mobile'=>$user['Mobile'],
            'Email'=>$user['Email'],
            'CompanyName'=>$user['CompanyName'],
            'LanguageID'=>(int)$user['LanguageID']
        );
        $data['customer']=$user;
        $data['token']=$this->generateToken($user['ID'],$user['UserType']);
        $this->returnData($response,1,'Login Successful',$data);

        return 200;
    }

    function updateProfileResendCode($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=',$users->userid),
            ));
            if (!isset($user)) {
                $this->returnData($response,0,'No Such user','');
                return 401;
            }

            $oldcode = $db->readMultiple('VerificationCodeLog', array(
                array('ApplicationUserID', '=', $users->userid),
                array('VerificationType','=','Update Profile'),
                array('NotificationMediumType','=','Email')
            ));
            if($oldcode){
                foreach($oldcode as $oldcode){
                    $code = $db->readSingle('VerificationCodeLog', array(
                        array('ID', '=', $oldcode->getID())
                    ),TRUE);
                    if($code){
                        $code->update(array(
                            'CodeExpireDateTime'=> getTimeInMs()-(50*60*1000),
                        ));
                        $db->update($code);
                    }
                }
            }

            $code = $this->generateCode($users->userid,'Update Profile','Email');
            if (!$db->create($code)) {
                Log::error(__METHOD__.'() - DB Unable to create code "' . $users->userid . ' register"');
                $this->returnData($response,0,'DB unable to create code','');
                return 500;
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

            }
        } // while not committed
        $user = $db->readSingle('ApplicationUser', array(
            array('ID', '=', $users->userid)
        ), TRUE);
        if (!isset($user)) {
            $this->returnData($response,0,'No such customer','');
            return 404;
        }
        $message="Thank you for signing up with <span style='color:#006ab4'>WantBus</span>! To complete your registration, simply copy and paste this verification code in the app - <b>".$code->toOutputArray()['Code']."</b>.";
        if($_POST['Email'] !=''){
            $mailDetails = array(
                'subject'=>'Update Profile',
                'email'=>$_POST['Email'],
                'message'=> $message,
                'file'=>'notification'
            );
            if ($user->getEmail() != $_POST['Email'] && $user->getMobile() != $_POST['Mobile']) {
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
                if($_POST['Mobile'] !=''){
                    //$sendSMS=$this->sendSMS($_POST['Mobile'],$message);
                    /*if(!$sendEmail){
                        $this->returnData($response,0,'Unable to send Email','');
                        return 500;
                    }*/
                }
            }elseif($user->getEmail() != $_POST['Email']){
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
            }else{
                $mailDetails = array(
                    'subject'=>'Update Profile',
                    'email'=>$user->getEmail(),
                    'message'=> $message,
                    'file'=>'notification'
                );
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
                //$sendSMS=$this->sendSMS($_POST['Mobile'],$message);
                /*if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }*/
            }
        }

        $this->returnData($response,1,'Code send in the email','');
        return 200;
    }

    function resendCode($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=',$users->userid),
            ));
            if (!isset($user)) {
                $this->returnData($response,0,'No Such user','');
                return 401;
            }
            //generate code
            if($_POST['Type'] == 0){
                $reason = "Registration";
            }elseif($_POST['Type'] == 1){
                $reason = "Forgot Password";
            }else if($_POST['Type'] == 2){
                $reason = "Update Profile";
            } else {
                $this->returnData($response,0,'Invalid Type','');
                return 400;
            }
            $oldcode = $db->readMultiple('VerificationCodeLog', array(
                array('ApplicationUserID', '=', $users->userid),
                array('VerificationType','=',$reason),
                array('NotificationMediumType','=','Email')
            ));
            if($oldcode){
                foreach($oldcode as $oldcode){
                    $code = $db->readSingle('VerificationCodeLog', array(
                        array('ID', '=', $oldcode->getID())
                    ),TRUE);
                    if($code){
                        $code->update(array(
                            'CodeExpireDateTime'=> getTimeInMs()-(50*60*1000),
                        ));
                        $db->update($code);
                    }
                }
            }

            $code = $this->generateCode($users->userid,$reason,'Email');
            if (!$db->create($code)) {
                Log::error(__METHOD__.'() - DB Unable to create code "' . $users->userid . ' register"');
                $this->returnData($response,0,'DB unable to create code','');
                return 500;
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

            }
        } // while not committed
        $message="Thank you for signing up with <span style='color:#006ab4'>WantBus</span>! To complete your registration, simply copy and paste this verification code in the app - <b>".$code->toOutputArray()['Code']."</b>.";
        if($user->getEmail() !=''){
            $mailDetails = array(
                'subject'=>'Registration Code',
                'email'=>$user->getEmail(),
                'message'=> $message,
                'file'=>'notification'
            );
            $sendEmail=$this->sendEmail($mailDetails);
            if(!$sendEmail){
                $this->returnData($response,0,'Unable to send Email','');
                return 500;
            }
        }
        if($user->getMobile() !=''){
            //$sendSMS=$this->sendSMS($_POST['Mobile'],$message);
            /*if(!$sendEmail){
                $this->returnData($response,0,'Unable to send Email','');
                return 500;
            }*/
        }
        $this->returnData($response,1,'Code send in the email','');
        return 200;
    }

    function verifyCode($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }

        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        if($_POST['Type'] == 0){
            $reason = "Registration";
        }elseif($_POST['Type'] == 1){
            $reason = "Forgot Password";
        }else{
            $reason = "Update Profile";
        }
        $statusForCode=$this->checkRegistrationCode($_POST['Code'],$reason,$users->userid);
        if($statusForCode['status']== 'success'){
            $db = Database::getConnection();
            $committed = FALSE;
            while (!$committed) {
                // Begin DB transaction
                if (!$db->beginTransaction()) {
                    Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                    $this->returnData($response,0,'DB unable to begin transaction.','');
                    return 500;
                }

                // Check that this email is not in use
                $existing_user = $db->readSingle('ApplicationUser', array(
                    array('ID', '=', $users->userid)
                ), TRUE);

                if (isset($existing_user)){
                    if($existing_user->getStatus() == 'Verification Pending'){
                        $updated = $existing_user->update(array(
                            'Status'=> 'Active',
                        ));
                        // Update it in DB
                        if (!$db->update($existing_user)) {
                            Log::error(__METHOD__ . '() - DB unable to update ' . $existing_user);
                            $this->returnData($response,0,'DB unable to update user data.','');
                            return 500;
                        }
                    }

                    // Create and store this activity
                    $activity = new Activity(array(
                        'UserID' =>  $users->userid,
                        'UserType' => $users->usertype,
                        'Action' => 'Registered (status updated and loggedin)',
                        'ModelID' =>  $users->userid,
                        'ModelType' => 'Application User',
                        'CreatedDateTime' => $time
                    ));

                    if (!$db->create($activity)) {
                        Log::error(__METHOD__.'() - DB Unable to create activity "' . $existing_user . ' register "');
                        $this->returnData($response,0,'Unable to log activity','');
                        return 500;
                    }
                }
                // Commit transaction
                $committed = $db->commit();
                if (!$committed) {
                    Log::warning(__METHOD__.'() - DB unable to commit transaction');
                    if (!$db->rollback()) {
                        Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                        $this->returnData($response,0,'DB unable to commit transaction','');
                        return 500;
                    }
                }
            }
        }else{
            if($statusForCode['errorType'] == 'CodeExpired') {
                $response->addData("error_code", 1);
            } else if($statusForCode['errorType'] == 'InvalidCode') {
                $response->addData("error_code", 2);
            }
            $this->returnData($response,0, $statusForCode['message'],'');
            return 400;
        }
        if($_POST['Type'] == 0){
            $message = "Hello there! You have successfully registered a new account with <span style='color:#006ab4'>WantBus</span>. We hope you enjoy using our app for all your bus requirements. Kindly drop us a feedback should you encounter any difficulties using the app. (From the Menu, select Others > Support.)";
            if($existing_user->getEmail() !=''){
                $mailDetails = array(
                    'subject'=>'Sign Up Success',
                    'email'=>$existing_user->getEmail(),
                    'message'=> $message,
                    'file'=>'registration-code'
                );
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
            }
        }
        $this->returnData($response,1, 'Verification Successful','');
        return 200;
    }

    function forgetPassword($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        $time = getTimeInMs();
        if (!isValidEmail($_POST['UserName'])) {
            $username['name']= "Mobile";
            $username['value']= $_POST['UserName'];
        }else{
            $username['name']= "Email";
            $username['value']= strtolower($_POST['UserName']);
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0, 'DB unable to begin transaction.','');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array($username['name'], '=', $username['value'])
            ), TRUE);
            if (!isset($user)) {
                $response->addData("error_code", 2);
                $this->returnData($response,0, 'User does not exist.','');
                return 404;
            }
            // Check if account is banned
            if ($user->isBanned()) {
                $response->addData("error_code", 1);
                $this->returnData($response,0, 'Account banned.','');
                return 403;
            }
            $code = $this->generateCode($user->getID(),'Forgot Password','Email');

            if (!$db->create($code)) {
                Log::error(__METHOD__.'() - DB Unable to create code "' . $user . ' forget password"');
                $this->returnData($response,0, 'DB unable to create code.','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => $user->usertype,
                'Action' => 'Forget Password',
                'CreatedDateTime' => $time,
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Forget password"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0, 'DB unable to commit transaction','');
                    return 500;
                }
            }
        } // while not committed
        if($username['name'] == 'Email'){
            $message="We have received a request from you to reset your WantBus password. To proceed, simply copy and paste this verification code in the app - ".$code->toOutputArray()['Code'].".";
            $mailDetails = array(
                'subject'=>'Forgot Password Code',
                'email'=>$username['value'],
                'message'=> $message,
                'file'=>'registration-code'
            );
            $sendEmail=$this->sendEmail($mailDetails);
        }
        $data['token']=$this->generateToken($user->getID(),$user->getUserType());
        $this->returnData($response,1, 'Code Sent Sucessfully',$data);
        return 200;
    }

    function changePassword($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }

        // Data checks
        if (strlen($_POST['Password']) < MIN_PASSWORD_LENGTH) {
            $response->addData("error_code", 1);
            $this->returnData($response,0,'Password must be at least '.MIN_PASSWORD_LENGTH.' characters.','');
            return 400;
        }
        $hash = Encryptor::getHash($_POST['Password']);
        $time = getTimeInMs();

        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response, 0, 'DB unable to begin transaction.', '');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $this->returnData($response, 0, 'No such customer.', '');
                return 404;
            }

            // Update user
            $updated = $user->update(array(
                'Password' => $hash,
                'LastUpdatedDateTime' => $time
            ));

            // Update him in DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' passwordHash.');
                $this->returnData($response, 0, 'DB Unable to update new password', '');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $user->usertype,
                'Action' => 'Change Password',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' change password"');
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $this->returnData($response, 0, 'DB unable to commit transaction', '');
                    return 500;
                }
            }
        }// while not committed
        // YAY!
        $this->returnData($response,1,'Password Change Sucessful','');
        return 200;
    }

    function checkProfile($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }
            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $this->returnData($response,0,'No such customer','');
                return 404;
            }
            // Update user
            if($_POST['Password']){
                $pass=Encryptor::getHash($_POST['Password']);
            }else{
                $pass=$user->getPassword();
            }
            if(($user->getEmail() == $_POST['Email'] && $user->getMobile() == $_POST['Mobile']) || !isset($_POST['Email']) ){
                try {
                    $updated = $user->update(array(
                        'Name' => ($_POST['Name'])?$_POST['Name']:$user->getName(),
                        'Mobile' => ($_POST['Mobile'])?$_POST['Mobile']:$user->getMobile(),
                        'Email' => ($_POST['Email'])?$_POST['Email']:$user->getEmail(),
                        'CompanyName' => ($_POST['CompanyName'])?$_POST['CompanyName']:$user->getCompanyName(),
                        'CompanyAddress' => ($_POST['CompanyAddress'])?$_POST['CompanyAddress']:$user->getCompanyAddress(),
                        'CompanyWebsite' => ($_POST['CompanyWebsite'])?$_POST['CompanyWebsite']:$user->getCompanyWebsite(),
                        'Password' => $pass,
                        'BusNumber' => ($_POST['BusNumber'])?$_POST['BusNumber']:$user->getBusNumber(),
                        'BusCapacity' => ($_POST['BusCapacity'])?$_POST['BusCapacity']:$user->getBusCapacity(),
                        'DoesRepresentCompany' => (isset($_POST['DoesRepresentCompany']))?$_POST['DoesRepresentCompany']:$user->getDoesRepresentCompany(),
                        'Status' => 'Active',
                        'LastUpdatedDateTime' => $time,
                    ));
                }
                catch (Exception $e) {
                    $this->returnData($response,0,$e->getMessage(),'');
                    return 400;
                }
                if (!$db->update($user)) {
                    Log::error(__METHOD__.'() - DB unable to update '.$user.' profile.');
                    $this->returnData($response,0,'DB unable to update Profile','');
                    return 500;
                }
                $action = "Edit Profile";
                $data['customer']['ID']=$user->toOutputArray()['ID'];
                $data['customer']['Name']=$user->toOutputArray()['Name'];
                $data['customer']['Mobile']=$user->toOutputArray()['Mobile'];
                $data['customer']['Email']=$user->toOutputArray()['Email'];
                $data['customer']['CompanyName']=$user->toOutputArray()['CompanyName'];
                $data['customer']['CompanyAddress']=$user->toOutputArray()['CompanyAddress'];
                $data['customer']['CompanyWebsite']=$user->toOutputArray()['CompanyWebsite'];
                $data['customer']['BusNumber']=$user->toOutputArray()['BusNumber'];
                $data['customer']['BusCapacity']=$user->toOutputArray()['BusCapacity'];
                $data['customer']['DoesRepresentCompany']=$user->toOutputArray()['DoesRepresentCompany'];
                $this->returnData($response,1,'Profile Updated Sucessfully',$data);
            }else {
                if ($user->getEmail() != $_POST['Email'] && $user->getMobile() != $_POST['Mobile']) {
                    $existing_backendUser = $db->readSingle('BackendUser', array(
                        array('Email', '=', $_POST['Email']),
                        array('ID', '!=', $users->userid)
                    ), TRUE);
                    if (isset($existing_backendUser)) {
                        $this->returnData($response,0,'This email is already in use','');
                        return 400;
                    }
                    $existing_backendUser = $db->readSingle('BackendUser', array(
                        array('Mobile', '=', $_POST['Mobile']),
                        array('ID', '!=', $users->userid)
                    ), TRUE);
                    if (isset($existing_backendUser)) {
                        $this->returnData($response,0,'This Mobile is already in use','');
                        return 400;
                    }
                    //generate code
                    $code = $this->generateCode($users->userid,'Update Profile','Email');
                    if (!$db->create($code)) {
                        Log::error(__METHOD__.'() - DB Unable to create code "' . $user . ' register"');
                        $this->returnData($response,0,'DB unable to create Code','');
                        return 500;
                    }
                    if($_POST['Email']){
                        $message="We have received a request from you to update your email address. To proceed, simpy copy and paste this verification code in the app - XSMLXL. <br />Code : ".$code->toOutputArray()['Code'];
                        $mailDetails = array(
                            'subject'=>'Update Profile',
                            'email'=>$_POST['Email'],
                            'message'=> $message,
                            'file'=>'notification'
                        );
                        $sendEmail=$this->sendEmail($mailDetails);
                        if(!$sendEmail){
                            $this->returnData($response,0,'Unable to send Email','');
                            return 500;
                        }
                    }
                }elseif($user->getEmail() != $_POST['Email']){
                    $existing_backendUser = $db->readSingle('BackendUser', array(
                        array('Email', '=', $_POST['Email']),
                        array('ID', '!=', $users->userid)
                    ), TRUE);
                    if (isset($existing_backendUser)) {
                        $this->returnData($response,0,'This email is already in use','');
                        return 400;
                    }
                    $code = $this->generateCode($users->userid,'Update Profile','Email');
                    if (!$db->create($code)) {
                        Log::error(__METHOD__.'() - DB Unable to create code "' . $user . ' register"');
                        $this->returnData($response,0,'DB unable to create code','');
                        return 500;
                    }
                    if($_POST['Email']) {
                        $message="We have received a request from you to update your email address. To proceed, simpy copy and paste this verification code in the app - XSMLXL. <br />Code : ".$code->toOutputArray()['Code'];
                        $mailDetails = array(
                            'subject'=>'Update Profile',
                            'email'=>$_POST['Email'],
                            'message'=> $message,
                            'file'=>'notification'
                        );
                        $sendEmail=$this->sendEmail($mailDetails);
                        if(!$sendEmail){
                            $this->returnData($response,0,'Unable to send Email','');
                            return 500;
                        }
                    }
                }else{
                    $existing_backendUser = $db->readSingle('BackendUser', array(
                        array('Mobile', '=', $_POST['Mobile']),
                        array('ID', '!=', $users->userid)
                    ), TRUE);
                    if (isset($existing_backendUser)) {
                        $this->returnData($response,0,'This mobile is already in use','');
                        return 400;
                    }
                    $code = $this->generateCode($users->userid, 'Update Profile','Email');
                    if (!$db->create($code)) {
                        Log::error(__METHOD__ . '() - DB Unable to create code "' . $users->userid . ' register"');
                        $this->returnData($response,0,'DB unable to create code','');
                        return 500;
                    }
                    $message="We have received a request from you to update your email address. To proceed, simpy copy and paste this verification code in the app - XSMLXL. <br />Code : ".$code->toOutputArray()['Code'];
                    $mailDetails = array(
                        'subject'=>'Update Profile',
                        'email'=>$user->getEmail(),
                        'message'=> $message,
                        'file'=>'notification'
                    );
                    $sendEmail=$this->sendEmail($mailDetails);
                    if(!$sendEmail){
                        $this->returnData($response,0,'Unable to send Email','');
                        return 500;
                    }

                }

                $action = "Verification";
                $this->returnData($response,1,'Code Sent Successfully',"");
            }

            $oldvalue=json_encode($user->toOutputArray());
            $newvalue=json_encode($_POST);

            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => $action,
                'ModelID' => $user->getID(),
                'ModelType' => 'Application User',
                'OldValue' =>$oldvalue,
                'NewValue' => $newvalue,
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' .  $users->userid . ' Edit Profile"');
                $this->returnData($response,0,'DB unable to create Activity','');
                return 500;
            }
            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        return 200;
    }

    function updateProfile($response) {
        // Ensure action is legal - check that the user is indeed a customer
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ));
            if (!isset($user)) {
                $this->returnData($response,0,'No such Customer','');
                return 404;
            }
            $data = $db->readMultiple('Activity', array(
                array('UserID', '=', $users->userid),
                array('Action','=','Verification')
            ), 0, 0, NULL, 'DESC');
            if(!is_array(end($data)->toOutputArray())){
                $this->returnData($response,0,'Could not save data','');
                return 500;
            }
            $data=json_decode(end($data)->toOutputArray()['NewValue']);
            try {
                $updated = $user->update(array(
                    'UserType' => 'Customer',
                    'Name' => $data->Name,
                    'Mobile' => $data->Mobile,
                    'Email' =>  $data->Email,
                    'CompanyName' =>  $data->CompanyName,
                    'CompanyAddress' =>  $data->CompanyAddress,
                    'CompanyWebsite' =>  $data->CompanyWebsite,
                    'Password' => ($data->Password)? $data->Password:$user->getPassword(),
                    'BusNumber' => $data->BusNumber,
                    'BusCapacity' =>  $data->BusCapacity,
                    'DoesRepresentCompany' =>  $data->DoesRepresentCompany,
                    'Status' => 'Active',
                    'LastUpdatedDateTime' => $time,
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__.'() - DB unable to update '.$user.' profile.');
                $this->returnData($response,0,'DB unable to update Profile','');
                return 500;
            }

            $oldvalue=json_encode($user->toOutputArray());
            $newvalue=json_encode($data);

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Edit Profile',
                'ModelID' => $user->getID(),
                'ModelType' => 'Application User',
                'OldValue' =>$oldvalue,
                'NewValue' => $newvalue,
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' .  $users->userid . ' Edit Profile"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        // $data['customer']=$user->toOutputArray();
        $this->returnData($response,1,'Profile Updated Sucessfully',$data);
        return 200;
    }

    function editProfilePicture($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }
            $target_dir = APPROOT . "/uploads/customer/";
            $target_dir = $target_dir . $users->userid . '_' . basename($_FILES['file']['name']);
            $fileName = "uploads/customer/" . $users->userid . '_' . basename($_FILES['file']['name']);
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_dir)) {
                $this->returnData($response,0,'Image could not be uploaded','');
                return 400;
            }
            // Update user
            try {
                $updated = $user->update(array(
                    'Photo' => $fileName,
                    'LastUpdatedDateTime' => $time,
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__.'() - unable to update '.$user.' profile Picture.');
                $this->returnData($response,0,'Unable to update Profile picture','');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__.'() - DB unable to update '.$user.' profile Picture.');
                $this->returnData($response,0,'DB unable to update Profile Picture','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => $users->usertype,
                'Action' => 'Edit Profile Picture',
                'ModelID' => $user->getID(),
                'ModelType' => 'Application User',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Edit Profile"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['customer']= $user->toOutputArray();
        $this->returnData($response,1,'Sucessfully updated Profile Picture',$data);
        return 200;
    }

    function deleteProfilePicture($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $this->returnData($response,0,'No such Customer','');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'Photo' => '',
                    'LastUpdatedDateTime' => $time,
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__.'() - DB unable to update '.$user.' profile Picture.');
                $this->returnData($response,0,'DB unable to update profile picture','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $user->getID(),
                'UserType' => $users->usertype,
                'Action' => 'Delete Profile Picture',
                'ModelID' => $user->getID(),
                'ModelType' => 'Application User',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Edit Profile"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['customer']= $user->toOutputArray();
        $this->returnData($response,1,'Successfully deleted the Profile Picture',$data);
        return 200;
    }

    function getMyProfile($response) {
        // Ensure action is legal - check that the user is indeed a customer
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        // Begin DB transaction
        if (!$db->beginTransaction()) {
            Log::fatal(__METHOD__.'() - DB unable to begin transaction');
            $this->returnData($response,0,'DB unable to begin transaction','');
            return 500;
        }

        // Find this user in DB
        $user = $db->readSingle('ApplicationUser', array(
            array('ID', '=', $users->userid)
        ));
        if (!isset($user)) {
            $this->returnData($response,0,'No such customer','');
            return 404;
        }
        $ratings='';
        if($user->toOutputArray()['UserType'] !="Customer"){
            $rating = $db->readMultiple('BookingRating', array(
                array('ServiceProviderApplicationUserID', '=', $users->userid)
            ));
            if($rating){
                foreach($rating as $rate){
                    $count[]=$rate;
                    $ratings +=$rate->toOutputArray()['Rating'];
                }
                $avgRating = $ratings/(count($count));
                $data['customer']['AverageRating']=$avgRating;
            } else {
                $data['customer']['AverageRating']=0;
            }
        }
        $data['customer']['ID']=$user->toOutputArray()['ID'];
        $data['customer']['UserType']=$user->toOutputArray()['UserType'];
        $data['customer']['Name']=$user->toOutputArray()['Name'];
        $data['customer']['Mobile']=$user->toOutputArray()['Mobile'];
        $data['customer']['Email']=$user->toOutputArray()['Email'];
        $data['customer']['CompanyName']=$user->toOutputArray()['CompanyName'];
        $data['customer']['CompanyAddress']=$user->toOutputArray()['CompanyAddress'];
        $data['customer']['CompanyWebsite']=$user->toOutputArray()['CompanyWebsite'];
        $data['customer']['Photo']=$user->toOutputArray()['Photo'];
        $data['customer']['BusCapacity']=$user->toOutputArray()['BusCapacity'];
        $data['customer']['DoesRepresentCompany']=$user->toOutputArray()['DoesRepresentCompany'];
        $data['customer']['BusNumber']=$user->toOutputArray()['BusNumber'];
        $data['customer']['LanguageID']=$user->toOutputArray()['LanguageID'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    /*** ******************** *** User Card *** ******************** ***/

    function getApplicationUserCard($response){
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read ApplicationUserCard from DB
        $db = Database::getConnection();
        $userCard = $db->readMultiple('ApplicationUserCard', array(
            array('ApplicationUserID','=',$users->userid),
            array('Status','=','Active')
        ));
        // Handle empty cases
        if (!is_array($userCard)) {
            $data['ApplicationUserCard']=array();
            $this->returnData($response,1,'Empty', $data);
            return 202;
        }

        // Make each BusType into array
        foreach ($userCard as $key=>$userCard) {
            $data['ApplicationUserCard'][$key]['ID'] = $userCard->toOutputArray()['ID'];
            $data['ApplicationUserCard'][$key]['CardNumber'] = $this->mc_decrypt($userCard->toOutputArray()['CardNumber'], ENCRYPTION_KEY);
            $data['ApplicationUserCard'][$key]['CCV'] = $this->mc_decrypt($userCard->toOutputArray()['CCV'], ENCRYPTION_KEY);
            $data['ApplicationUserCard'][$key]['CardType'] = $this->cardType($this->mc_decrypt($userCard->toOutputArray()['CardNumber'],ENCRYPTION_KEY));
            $data['ApplicationUserCard'][$key]['ExpiryDate'] = $userCard->toOutputArray()['IsVerified'] == '1' ? $this->mc_decrypt($userCard->toOutputArray()['ExpiryDate'], ENCRYPTION_KEY) : '(Inactive)';
            $data['ApplicationUserCard'][$key]['IsDefault'] = ($userCard->toOutputArray()['IsDefault'] == 1)?true:false;

        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function addApplicationUserCard($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unabale to begin transaction','');
                return 500;
            }
            $userCard = $db->readMultiple('ApplicationUserCard', array(
                array('ApplicationUserID','=',$users->userid),
                array('Status','=','Active')
            ));

            if (!is_array($userCard))
                $isDefault = 1;
            else
                $isDefault = 0;

            $verifyCard=true;

            if(!$verifyCard){
                $this->returnData($response,0,'Card Not Verified','');
                return 400;
            }
            //Check card length
            if (strlen($_POST['CardNumber']) != MIN_CARD_LENGTH) {
                $this->returnData($response,0,'Card Number must be at least '.MIN_CARD_LENGTH.' digits.','');
                return 402;
            }
            //Check ccv length
            if (!(strlen($_POST['CCV']) <= MAX_CCV_LENGTH && strlen($_POST['CCV']) >= MIN_CCV_LENGTH)) {
                $this->returnData($response,0,'CCV must be at least '.MIN_CCV_LENGTH.' digits and at most '.MAX_CCV_LENGTH.' digits','');
                return 403;
            }

            //Check Duplicate Card Number
            $applicationUserCards = $db->readMultiple('ApplicationUserCard', array(
                array('ApplicationUserID','=',$users->userid)
            ));
            if($applicationUserCards){
                foreach ($applicationUserCards as $key=>$applicationUserCards) {
                    if($_POST['CardNumber'] == $this->mc_decrypt($applicationUserCards->getCardNumber(), ENCRYPTION_KEY)){
                        $this->returnData($response,0,'Duplicate card number.','');
                        return 401;
                    }
                }
            }

            // Create LanguageValue - can be done outside transaction as we don't need persistence (yet)
            try {
                $userCard = new ApplicationUserCard(array(
                    'ApplicationUserID' => $users->userid,
                    'CardNumber' =>$this->mc_encrypt($_POST['CardNumber'], ENCRYPTION_KEY),
                    'ExpiryDate' =>$this->mc_encrypt($_POST['ExpiryDate'], ENCRYPTION_KEY),
                    'CCV' => $this->mc_encrypt($_POST['CCV'], ENCRYPTION_KEY),
                    'IsDefault' => $isDefault,
                    'IsVerified' => 1,
                    'Status' => 'Active',
                    'LastUpdatedDateTime' =>$time,
                    'CCV' => $this->mc_encrypt($_POST['CCV'], ENCRYPTION_KEY),
                ));
            } catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }

            // Create in DB
            if (!$db->create($userCard)) {
                Log::error(__METHOD__ . '() - DB unable to create userCard for: ' . $_POST['LabelValue']);
                $this->returnData($response,0,'DB unable to create userCard','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Add',
                'ModelID' => $userCard->getID(),
                'ModelType' => 'Application User Card',
                'CreatedDateTime' => $time,
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added Role"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['ApplicationUserCard']['ID'] = $userCard->getID();
        $data['ApplicationUserCard']['CardNumber'] = $this->mc_decrypt($userCard->getCardNumber(), ENCRYPTION_KEY);
        $data['ApplicationUserCard']['ExpiryDate'] = $this->mc_decrypt($userCard->getExpiryDate(), ENCRYPTION_KEY);
        $data['ApplicationUserCard']['CCV'] = $this->mc_decrypt($userCard->getCCV(), ENCRYPTION_KEY);
        $data['ApplicationUserCard']['IsDefault'] =($userCard->getIsDefault() == 1)?true:false ;
        $this->returnData($response,1,'Successfully added data',$data);
        return 200;
    }

    function editApplicationUserCard($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unabale to begin transaction','');
                return 500;
            }

            $verifyCard=true;
            if(!$verifyCard){
                $this->returnData($response,0,'Card Not Verified','');
                return 400;
            }
            $userCard = $db->readSingle('ApplicationUserCard', array(
                array('ID','=',$_POST['ID'])
            ),TRUE);
            //Check card length
            if (strlen($_POST['CardNumber']) != MIN_CARD_LENGTH) {
                $this->returnData($response,0,'Card Number must be at least '.MIN_CARD_LENGTH.' digits.','');
                return 402;
            }
            //Check ccv length
            if (!(strlen($_POST['CCV']) <= MAX_CCV_LENGTH && strlen($_POST['CCV']) >= MIN_CCV_LENGTH)) {
                $this->returnData($response,0,'CCV must be at least '.MIN_CCV_LENGTH.' digits and at most '.MAX_CCV_LENGTH.' digits.','');
                return 403;
            }
            //Check Duplicate Card Number
            $applicationUserCards = $db->readMultiple('ApplicationUserCard', array(
                array('ApplicationUserID','=',$users->userid),
                array('Status','=','Active')
            ));
            foreach ($applicationUserCards as $key=>$applicationUserCards) {
                if($_POST['CardNumber'] == $this->mc_decrypt($applicationUserCards->getCardNumber(), ENCRYPTION_KEY)
                    && $applicationUserCards->getId() != $_POST['ID']){
                    $this->returnData($response,0,'Duplicate card number.','');
                    return 401;
                }
            }
            $userCard->update(array(
                'ApplicationUserID' => $users->userid,
                'CardNumber' => $this->mc_encrypt($_POST['CardNumber'], ENCRYPTION_KEY),
                'ExpiryDate' => $this->mc_encrypt($_POST['ExpiryDate'], ENCRYPTION_KEY),
                'LastUpdatedDateTime' =>$time,
                'CCV' => $this->mc_encrypt($_POST['CCV'], ENCRYPTION_KEY),
                'IsVerified' => '1' // only verified cards get updated
            ));
            // Update in DB
            if (!$db->update($userCard)) {
                Log::error(__METHOD__ . '() - DB unable to update userCard for: ' . $users->userid);
                $this->returnData($response,0,'DB unable to update userCard','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Edit',
                'ModelID' => $userCard->getID(),
                'ModelType' => 'Application User Card',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added Role"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['ApplicationUserCard']['ID'] = $userCard->toOutputArray()['ID'];
        $data['ApplicationUserCard']['CardNumber'] = $this->mc_decrypt($userCard->toOutputArray()['CardNumber'], ENCRYPTION_KEY);
        $data['ApplicationUserCard']['ExpiryDate'] = $this->mc_decrypt($userCard->toOutputArray()['ExpiryDate'], ENCRYPTION_KEY);
        $data['ApplicationUserCard']['CCV'] = $this->mc_decrypt($userCard->getCCV(), ENCRYPTION_KEY);
        $data['ApplicationUserCard']['IsDefault'] = ($userCard->toOutputArray()['IsDefault'] == 1)?true:false;
        $this->returnData($response,1,'Successfully updated data',$data);
        return 200;
    }

    function deleteApplicationUserCard($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unabale to begin transaction','');
                return 500;
            }
            $userCard = $db->readSingle('ApplicationUserCard', array(
                array('ID','=',$_POST['ID'])
            ),TRUE);

            $userCard->update(array(
                'Status' => 'Deleted',
                'LastUpdatedDateTime' =>$time
            ));
            // Update in DB
            if (!$db->update($userCard)) {
                Log::error(__METHOD__ . '() - DB unable to update userCard for: ' . $users->userid);
                $this->returnData($response,0,'DB unable to update userCard','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Delete',
                'ModelID' => $userCard->getID(),
                'ModelType' => 'Application User Card',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added Role"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $this->returnData($response,1,'Successfully deleted data','');
        return 200;
    }

    function setDefaultApplicaitonUserCard($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Generate timestamp
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }

            $userCards = $db->readMultiple('ApplicationUserCard', array(
                array('ApplicationUserID','=',$users->userid),
                array('ID','!=',$_POST['ID'])
            ));
            if(is_array($userCards)){
                foreach($userCards as $userCard){
                    $card = $db->readSingle('ApplicationUserCard', array(
                        array('ID', '=', $userCard->getID())
                    ), TRUE);
                    if (isset($card)) {
                        // Update in DB
                        $card->update(array(
                            'IsDefault' => '0',
                            'LastUpdatedDateTime' =>$time
                        ));
                        if (!$db->update($card)) {
                            Log::error(__METHOD__ . '() - DB unable to update Application user card ');
                            $this->returnData($response,0,'DB unable to update Applicaiton user card','');
                            return 500;
                        }
                    }
                }
            }
            $userCard = $db->readSingle('ApplicationUserCard', array(
                array('ID','=',$_POST['ID']),
                array('Status','=','Active')
            ),TRUE);

            if(!$userCard) {
                $this->returnData($response,0,'Card is not active.','');
                return 400;
            }

            $userCard->update(array(
                'IsDefault' => 1,
                'LastUpdatedDateTime' =>$time
            ));
            // Update in DB
            if (!$db->update($userCard)) {
                Log::error(__METHOD__ . '() - DB unable to update userCard for: ' . $users->userid);
                $this->returnData($response,0,'DB unable to update userCard','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Edit',
                'ModelID' => $userCard->getID(),
                'ModelType' => 'Application User Card',
                'CreatedDateTime' => $time,
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added Role"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['ApplicationUserCard']['ID'] = $userCard->toOutputArray()['ID'];
        $data['ApplicationUserCard']['CardNumber'] = $this->mc_decrypt($userCard->toOutputArray()['CardNumber'], ENCRYPTION_KEY);
        $data['ApplicationUserCard']['ExpiryDate'] = $this->mc_decrypt($userCard->toOutputArray()['ExpiryDate'], ENCRYPTION_KEY);
        $data['ApplicationUserCard']['IsDefault'] = ($userCard->toOutputArray()['IsDefault'] == 1)?true:false;
        //$data['ApplicationUserCard']=$userCard->toOutputArray();
        $this->returnData($response,1,'Successfully updated data',$data);
        return 200;
    }

    /***   ********************  *** Language *** ******************** ***/

    function getLanguageDictionary($response) {
        // Ensure action is legal - check that the user is indeed a staff
        /*if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }*/
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $languages = $db->readMultiple('LanguageValue', array());

        // Handle empty cases
        if (!is_array($languages)) {
            $data['LanguageValue']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        // Make each Language into array
        $count = 0;
        foreach ($languages as $language) {
            if (!$language->IsDeleted()) {
                $languageArray[$count] = $language->toOutputArray();
                $languagedata[$count] = $language->toOutputArray();
                $languageId = $db->readSingle('Language', array(
                    array('ID', '=', $languagedata[$count]['LanguageID'])
                ));
                $name = $languageId->toOutputArray();
                //$languageData[$name['ID']]= $name['Name'];
                $languageData[$name['ID']][$languagedata[$count]['LabelCode']]=$languagedata[$count]['LabelValue'];
                $count ++;
            }

        }
        $data['LanguageValue']=$languageData;
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function setDefaultLanguage($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();
        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable tobegin transaction','');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $this->returnData($response,0,'No such customer','');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'LanguageID' => $_POST['LanguageID'],
                    'LastUpdatedDateTime' => $time,
                ));
            }
            catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__.'() - DB unable to update '.$user.' profile.');
                $this->returnData($response,0,'DB unable to update profile','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => 'Customer',
                'Action' => 'Edit Language',
                'ModelID' => $user->getID(),
                'ModelType' => 'Application User',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity "' . $users->userid . ' Edit Profile"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $response->addData('languageID', $_POST['LanguageID']);
        $this->returnData($response,1,'Successful','');
        return 200;
    }

    function getLanguage($response) {
        // Ensure action is legal - check that the user is indeed a staff
        /*if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }*/
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $languages = $db->readMultiple('Language', array());

        // Handle empty cases
        if (!is_array($languages)) {
            $data['Language']=array();
            $this->returnData($response,0,'Empty data',$data);
            return 202;
        }

        // Make each Language into array
        foreach ($languages as $language) {
            if (!$language->IsDeleted()) {
                $data['Language'][] = $language->toOutputArray();
            }
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    /*** ******************** *** Enquiry *** ******************** ***/

    function enquiry($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }
            if(!$_POST['EnquiryText']){
                $this->returnData($response,0,'EnquiryText Empty','');
                return 500;
            }

            try {
                $enquiry = new Enquiry (array(
                    'ApplicationUserID' => $users->userid,
                    'EnquiryText' => $_POST['EnquiryText'],
                    'EnquiryDateTime' => $time,
                    'EnquiryStatus' => 'Unread',
                    'LastUpdatedBy'=> $users->userid,
                    'LastUpdatedDateTime'=> $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($enquiry)) {
                Log::error(__METHOD__.'() - DB Unable to create enquiry"');
                $this->returnData($response,0,'DB umable to create enguiry','');
                return 500;
            }
            $user = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $users->userid)
            ), TRUE);
            if (!isset($user)) {
                $this->returnData($response,0,'No such customer','');
                return 404;
            }
            $message='Type of Partnership: '.$users->usertype.'<br />Name: '.$user->getName().' Mobile Number: '.$user->getMobile().'<br />Email: '.$user->getEmail().'<br />Company Name: '.$user->getCompanyName().'<br />Website: '.$user->getCompanyWebsite();
            $message1=$_POST['EnquiryText'].'<br /> <br />Type of Partnership: '.$users->usertype.'<br />Name: '.$user->getName().' Mobile Number: '.$user->getMobile().'<br />Email: '.$user->getEmail().'<br />Company Name: '.$user->getCompanyName().'<br />Website: '.$user->getCompanyWebsite();
            $mailDetails = array(
                'subject'=>'Enquiry Recieved',
                'email'=>$user->getEmail(),
                'message'=> $message,
                'file'=>'customer-message'
            );
            $mailDetails1 = array(
                'subject'=>'Enquiry Recieved',
                'email'=>CONTACT_EMAIL,
                'message'=> $message1,
                'file'=>'site-message'
            );
            $sendEmail=$this->sendEmail($mailDetails);
            $sendEmail=$this->sendEmail($mailDetails1);
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Add',
                'ModelID' => $enquiry->getID(),
                'ModelType' => 'Enquiy',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        $data['Enquiry']= $enquiry->toOutputArray();
        $this->returnData($response,1,'Successfully added Enguiry',$data);
        return 200;
    }

    /***   ********************  *** Booking *** ******************** ***/
    function dateForMenu($timestamp){
        $match_date = date('d/m/Y',$timestamp);
        if(date('d/m/Y', time()-86400) == $match_date){
            return 'Yesterday';
        }elseif (date('d/m/Y') == $match_date) {
            return 'Today';
        }elseif(date('d/m/Y', time()+86400) == $match_date){
            return 'Tomorrow';
        }else{
            return $match_date;
        }

    }

    function checkSurcharge($diff,$offeringPrice,$surchargePrice){
        $value=($surchargePrice*100)/$offeringPrice;
        //$percentage = ($surchargePrice * 100) / $offeringPrice;
        $db = Database::getConnection();
        $settings = $db->readMultiple('Settings', array());
        // Handle empty cases
        if (is_array($settings)) {
            foreach($settings as $setting) {
                $setting = $setting->toOutputArray();
                if (explode(' ', $setting['SettingName'])[3] == '(Surcharge)') {
                    $setting['Operator'] = explode(' ', $setting['SettingName'])[0];
                    $setting['Value'] = explode(' ', $setting['SettingName'])[1];
                    $hour=$diff/3600;
                    if($setting['Operator'] == '>') {
                        if ($hour > $setting['Value']) {
                            $nowValue = $setting['SettingValue'] ;
                            break;
                        }
                    }
                    else {
                        if ($hour < $setting['Value']) {
                            $nowValue = $setting['SettingValue'] ;
                            break;
                        }
                    }

                }
            }
        }

        if($value == $nowValue)
            return true;
        else
            return false;

    }

    function getSeatPriceByBusType($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }

        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $bustype = $db->readSingle('BusType', array(
            array('BusType','=',$_POST['BusType'])
        ));
        // Handle empty cases
        if (!($bustype)) {
            $data['SeatPrice']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }
        $data['SeatPrice']['MinPrice']=$bustype->toOutputArray()['MinPrice'];
        $data['SeatPrice']['MaxPrice']=$bustype->toOutputArray()['MaxPrice'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getSurchargePrice($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $settings = $db->readMultiple('Settings', array());
        // Handle empty cases
        if (!is_array($settings)) {
            $this->returnData($response,0,'No charge Found ','');
            return 202;
        }
        $PickUpDateTime = StrToTime($_POST['PickUpDate'].' '.$_POST['PickUpTime']);
        $t2 =(new DateTime())->getTimestamp();
        $diff = $PickUpDateTime - $t2;
        foreach($settings as $setting) {
            $setting = $setting->toOutputArray();
            if (explode(' ', $setting['SettingName'])[3] == '(Surcharge)') {
                $setting['Operator'] = explode(' ', $setting['SettingName'])[0];
                $setting['Value'] = explode(' ', $setting['SettingName'])[1];
                $hour=(string)$diff/3600;
                if($setting['Operator'] == '>') {
                    if ($hour > $setting['Value']) {
                        $data['SurchargePrice'] = $_POST['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }
                else {
                    if ($hour < $setting['Value']) {
                        $data['SurchargePrice'] = $_POST['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }

            }
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getSurchargeSetting($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $settings = $db->readMultiple('Settings', array());
        // Handle empty cases
        if (!is_array($settings)) {
            $this->returnData($response,0,'No charge Found ','');
            return 202;
        }
        foreach($settings as $key=>$setting) {
            $setting = $setting->toOutputArray();
            if (explode(' ', $setting['SettingName'])[3] == '(Surcharge)') {
                $datas['Operator'] = explode(' ', $setting['SettingName'])[0];
                $datas['Value'] = explode(' ', $setting['SettingName'])[1];
                $datas['Percentage'] = $setting['SettingValue'];
                $data[]=$datas;
            }
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }
    function getShuttleIntervalSetting($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
//        $setting = $db->readSingle('Setting', array(
//            array('SettingName', '=', 'BookABusValidateInMinutes')
//        ));
//
//        $this->returnData($response,1,'', $setting->toOutputArray()['SettingValue']);
        $settingsMinValue = $db->readSingle('Settings', array(
            array('SettingName','=','ShuttleIntervalMinValueInMinutes')
        ));
        $settingsMaxValue = $db->readSingle('Settings', array(
            array('SettingName','=','ShuttleIntervalMaxValueInMinutes')
        ));
        // Handle empty cases
        if (!$settingsMinValue || !$settingsMaxValue) {
            $this->returnData($response,0,'No shuttle interval setting found.','');
            return 202;
        }
        $datas['MinValue'] = $settingsMinValue->toOutputArray()['SettingValue'];
        $datas['MaxValue'] = $settingsMaxValue->toOutputArray()['SettingValue'];
        $data[] = $datas;
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }
    function getSurchargeInfo($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $defaultLanguage=$db->readSingle('ApplicationUser', array(
            array('ID','=',$users->userid)
        ));
        if(!$defaultLanguage){
            $this->returnData($response,0,'User doesnot exist','');
            return 400;
        }
        $SurchargeInfo = $db->readSingle('LanguageValue', array(
            array('LabelCode','=','SurchargeInfo'),
            array('LanguageID','=',$defaultLanguage->toOutputArray()['LanguageID'])
        ));
        // Handle empty cases
        if (!($SurchargeInfo)) {
            $this->returnData($response,0,'No Info Found','');
            return 404;
        }
        $data['SurchargeInfo']=$SurchargeInfo->toOutputArray()['LabelValue'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getCancellationPrice($db,$response,$bookingID){
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$bookingID)
        ));
        if (!($bookings)) {
            $this->returnData($response,0,'Empty data ','');
            return 202;
        }
        $settings = $db->readMultiple('Settings', array());
        if (!is_array($settings)) {
            $this->returnData($response,0,'No charge Found ','');
            return 202;
        }
        $booking=$bookings->toOutputArray();
        $PickUpDateTime = StrToTime(date('Y/m/d',$booking['PickUpDate']).' '.date('h:i:s',$booking['PickUpTime']));
        $t2 =(new DateTime())->getTimestamp();
        $diff = $PickUpDateTime - $t2;
        foreach($settings as $setting) {
            $setting = $setting->toOutputArray();
            if (explode(' ', $setting['SettingName'])[3] == '(CancellationFee)') {
                $setting['Operator'] = explode(' ', $setting['SettingName'])[0];
                $setting['Value'] = explode(' ', $setting['SettingName'])[1];
                $hour=$diff/3600;
                if($setting['Operator'] == '>') {
                    if ($hour > $setting['Value']) {
                        $CancellationPrice = $booking['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }
                else {
                    if ($hour < $setting['Value']) {
                        $CancellationPrice = $booking['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }

            }
        }
        $data['cancellationPrice']=$CancellationPrice;
        $data['bookings']=$bookings;
        return $data;
    }

    function getCancellationSetting($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();

        $settings = $db->readMultiple('Settings', array());
        if (!is_array($settings)) {
            $this->returnData($response,0,'No charge Found ','');
            return 202;
        }

        foreach($settings as $setting) {
            $setting = $setting->toOutputArray();
            if (explode(' ', $setting['SettingName'])[3] == '(CancellationFee)') {
                $datas['Operator'] = explode(' ', $setting['SettingName'])[0];
                $datas['Value'] = explode(' ', $setting['SettingName'])[1];
                $datas['Percentage'] = $setting['SettingValue'];
                $data[]=$datas;

            }
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getCancellationInfo($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $defaultLanguage=$db->readSingle('ApplicationUser', array(
            array('ID','=',$users->userid)
        ));
        if(!$defaultLanguage){
            $this->returnData($response,0,'User does not exist','');
            return 400;
        }
        $CancellationInfo = $db->readSingle('LanguageValue', array(
            array('LabelCode','=','CancellationInfo'),
            array('LanguageID','=',$defaultLanguage->toOutputArray()['LanguageID'])
        ));
        // Handle empty cases
        if (!($CancellationInfo)) {
            $this->returnData($response,0,'No Info Found','');
            return 404;
        }
        //$details=$this->getCancellationPrice($bookings->toOutputArray(),$settings);
        $data=$this->getCancellationPrice($db,$response,$_POST['BookingID']);
        $data['CancellationInfo']=$CancellationInfo->toOutputArray();
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getBookingCancellationPrice($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID'])
        ));
        if (!($bookings)) {
            $this->returnData($response,0,'Empty data ','');
            return 202;
        }
        $settings = $db->readMultiple('Settings', array());
        if (!is_array($settings)) {
            $this->returnData($response,0,'No charge Found ','');
            return 202;
        }
        $booking=$bookings->toOutputArray();
        if($booking['PickUpDate'] < ((new DateTime())->getTimestamp())){
            $this->returnData($response,0,'Cancel not allowed','');
            return 202;
        }
        $PickUpDateTime = StrToTime(date('Y/m/d',$booking['PickUpDate']).' '.date('h:i:s',$booking['PickUpTime']));
        $t2 =(new DateTime())->getTimestamp();
        $diff = $PickUpDateTime - $t2;
        foreach($settings as $setting) {
            $setting = $setting->toOutputArray();
            if (explode(' ', $setting['SettingName'])[3] == '(CancellationFee)') {
                $setting['Operator'] = explode(' ', $setting['SettingName'])[0];
                $setting['Value'] = explode(' ', $setting['SettingName'])[1];
                $hour=$diff/3600;
                if($setting['Operator'] == '>') {
                    if ($hour > $setting['Value']) {
                        $CancellationPrice = $booking['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }
                else {
                    if ($hour < $setting['Value']) {
                        $CancellationPrice = $booking['OfferingPrice'] * ($setting['SettingValue'] / 100);
                        break;
                    }
                }

            }
        }
        $data['CancellationFee']=number_format($CancellationPrice,2);
        $this->returnData($response,1,'',$data);
        return 200;
    }

    function getServiceProviderContactNumber($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $driverDetails = $db->readSingle('BookingDriverDetails', array(
            array('BookingID','=',$_POST['BookingID']),
            array('IsActive','=','1')
        ));
        if (!($driverDetails)) {
            $this->returnData($response,0,'Booking Request has not been accepted','');
            return 202;
        }
        $driverDetail=$driverDetails->toOutputArray();
        $userDetails= $db->readSingle('ApplicationUser', array(
            array('ID','=',$driverDetail['ServiceProviderApplicationUserID']),
            array('Status','=','Active')
        ));
        if (!($userDetails)) {
            $this->returnData($response,0,'User is not active','');
            return 202;
        }
        $userDetail=$userDetails->toOutputArray();
        $data['PhoneNumber']=$userDetail['Mobile'];
        $this->returnData($response,1,'',$data);
        return 200;
    }

    function bookaBusPaymentVerify($response){
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $data = array();

        //Read ApplicationUserCard from DB
        $db = Database::getConnection();
        $userCard = $db->readMultiple('ApplicationUserCard', array(
            array('ApplicationUserID','=',$users->userid),
            array('Status', '=', 'Active')
        ));
        // Handle empty cases
        if (!is_array($userCard)) {
            $data['CardInfo']=array();
            $this->returnData($response,1,'Empty', $data);
            return 202;
        }

        // Make each BusType into array
        $count=1;
        foreach ($userCard as $key=>$userCard) {
            $number = $this->mc_decrypt($userCard->toOutputArray()['CardNumber'], ENCRYPTION_KEY);
            $verify=$this->verifyCard($number);
            if($userCard->toOutputArray()['IsVerified'] == 1) {
		$verify = true;
	    } else {
		$verify = false;
	    }
            if($verify == true){
                $cardType = $this->cardType($this->mc_decrypt($userCard->toOutputArray()['CardNumber'],ENCRYPTION_KEY));;
            }else{
                $cardType='Inactive';
            }
            if($userCard->toOutputArray()['IsDefault'] == 1){
                $card[0]['ID']=$userCard->toOutputArray()['ID'];
                $card[0]['CardNumber'] = '**** **** **** '.substr($number,-4)." (".$cardType.")"; //Format: **** **** **** 1234
                    //substr($number, 0, 4) . str_repeat('*', strlen($number) - 8) . substr($number, -4);
                    //str_repeat('*', strlen($number) - 4)." ". substr($number, -4)." (".$cardType.")";
                $card[0]['IsVerified'] = $cardType == 'Inactive' ? 0 : 1;
                $verifiedCards[0]['ID'] = $userCard->toOutputArray()['ID'];
                $verifiedCards[0]['IsVerified'] = $cardType == 'Inactive' ? 0 : 1;
            }else{
                $card[$count]['ID']=$userCard->toOutputArray()['ID'];
                $card[$count]['CardNumber'] = '**** **** **** '.substr($number,-4)." (".$cardType.")";//Format: **** **** **** 1234
                //substr($number, 0, 4) . str_repeat('*', strlen($number) - 8) . substr($number, -4);
                    //str_repeat('* ', strlen($number) - 4)." ". substr($number, -4)." (".$cardType.")";
                $card[$count]['IsVerified'] = $cardType == 'Inactive' ? 0 : 1;
                $verifiedCards[$count]['ID'] = $userCard->toOutputArray()['ID'];
                $verifiedCards[$count]['IsVerified'] = $cardType == 'Inactive' ? 0 : 1;
                $count=$count+1;
            }
        }
        $time = getTimeInMs();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction','');
                return 500;
            }
            foreach($verifiedCards as $verifiedCards) {
                $verifiedCard = $verifiedCards;
                $userCard = $db->readSingle("ApplicationUserCard", array(
                            array('ID','=',$verifiedCard['ID'])
                        ) ,TRUE);
                $userCard->update(array(
                    'LastUpdatedDateTime' => $time,
                    'IsVerified' => $verifiedCard['IsVerified']
                ));
                // Update in DB
               /* if (!$db->update($userCard)) {
                    Log::error(__METHOD__ . '() - DB unable to update userCard for: ' . $users->userid);
                    $this->returnData($response, 0, 'DB unable to update userCard', '');
                    return 500;
                }
*/

                // Create and store this activity
                $activity = new Activity(array(
                    'UserID' => $users->userid,
                    'UserType' => $users->usertype,
                    'Action' => 'Verify Card',
                    'ModelID' => $userCard->getID(),
                    'ModelType' => 'Application User Card',
                    'CreatedDateTime' => $time
                ));
                if (!$db->create($activity)) {
                    Log::error(__METHOD__ . '() - DB Unable to create activity "' . $users->userid . ' added Role"');
                }
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }
            }
        } // while not committed
        foreach($card as $card){
            $data['CardInfo'][]=$card;
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function addBookaBus($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a customer
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $db = Database::getConnection();
        $userCard = $db->readSingle('ApplicationUserCard', array(
            array('ApplicationUserID','=',$users->userid),
            array('ID', '=', $_POST['CardID']),
            array('Status', '=', 'Active')
        ));
        if(!$userCard) {
            $this->returnData($response,0,'Invalid Card','');
            return 400;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }
            $PickUpDateTime = strtotime($_POST['PickUpDate'].' '.$_POST['PickUpTime']);
            $now =(new DateTime()) -> getTimestamp();
            if($now > $PickUpDateTime){
                $this->returnData($response,0,'Invalid Booking','');
                return 400;
            }
            $check=$this->checkSurcharge($PickUpDateTime-$now, $_POST['OfferingPrice'], $_POST['Surcharge']);

            if(!$check){
                $this->returnData($response,0,'Invalid Surcharge','');
                return 406;
            }

            $busTypeID = $db->readSingle('BusType', array(
                array('BusType','=',$_POST['BusType'])
            ));
            if(!$busTypeID){
                $this->returnData($response,0,'BusType Did not match','');
                return 404;
            }
            //Min and Max Price valid for single and return trips only.
            if($_POST['TripType'] != "Shuttle"){
                if($_POST['OfferingPrice'] < $busTypeID->toOutputArray()['MinPrice'] || $_POST['OfferingPrice'] > $busTypeID->toOutputArray()['MaxPrice'] ){
                    $this->returnData($response,0,'Invalid Offering Price for bus type '.$_POST['BusType'],'');
                    return 405;
                }
            }
            //Check Payment method.
            if(!isset($_POST['CardID'])){
                $this->returnData($response,0,'Payment method not found.');
                return 406;
            }

            // Update user
            try {
                $booking = new Booking (array(
                    'BookingNumber' => $this->calculateBookingNumber(),
                    'CustomerApplicationUserID' =>$users->userid,
                    'TripType' => $_POST['TripType'],
                    'PickUpPoint' => $_POST['PickUpPoint'],
                    'PickUpPointCoordinate' => $_POST['PickUpPointLatitude'].','.$_POST['PickUpPointLongitude'],
                    'Destination' => $_POST['Destination'],
                    'DestinationCoordinate' => $_POST['DestinationLatitude'].','.$_POST['DestinationLongitude'],
                    'PickUpDate' => strtotime($_POST['PickUpDate']),
                    'PickUpTime' => strtotime($_POST['PickUpTime']),
                    'ReturnDate' => strtotime($_POST['ReturnDate']),
                    'ReturnTime' => strtotime($_POST['ReturnTime']),
                    'IntervalHour' => $_POST['IntervalHour'],
                    'BusTypeID' => $busTypeID->toOutputArray()['ID'],
                    'OfferingPrice' => $_POST['OfferingPrice'],
                    'NoteToDriver' => $_POST['NoteToDriver'],
                    'Surcharge' => $_POST['Surcharge'],
                    'CancellationFee' => ($_POST['CancellationFee'])?$_POST['CancellationFee']:0,
                    'Status' => 'Request Pending',
                    'CreatedDateTime' => getTimeInMs()
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($booking)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking"');
                $this->returnData($response,0,'DB unable to create Booking','');
                return 500;
            }
            try {
                $bookingPaymentMethod = new BookingPaymentMethod (array(
                    'BookingID' => $booking->getID(),
                    'CardID' =>$_POST['CardID'],
                    'Status' =>'Verified',
                    'CreatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingPaymentMethod)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to add Booking Payment Method','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $booking->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => 'Request Pending',
                    'Remarks' =>'',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Add',
                'ModelID' => $booking->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        }
        //$data['BookaBus']=$booking->toOutputArray();
        $this->returnData($response,1,'Booking Successful',$booking->getID());
        return 200;
    }

    /***   *** *** -- Customer -- *** **** ***/

    function getNumberOfCustomerTrips($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readMultiple('Booking', array(
            array('CustomerApplicationUserID','=',$users->userid),
        ));

        // Handle empty cases
        if (!is_array($bookings)) {
            $data['Count']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        // Make each BusType into array
        $bookingData=array();
        foreach ($bookings as $key=>$bookings) {
            $booking = $bookings->toOutputArray();
            if ($booking['Status'] == 'Request Accepted' || $booking['Status'] == 'Request Pending' || $booking['Status'] == 'On Dispute') {
                $unread = $db->readSingle('BookingNotification', array(
                    array('BookingID','=',$booking['ID']),
                    array('RecipientUserID','=',$users->userid),
                    array('IsRead','=',0)
                ));
                if($unread){
                    $bookingData[]=$booking;
                }
            }
        }
        $data['Count']=count($bookingData);
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getCustomerViewMyTrips($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readMultiple('Booking', array(
            array('CustomerApplicationUserID','=',$users->userid),
        ));

        // Handle empty cases
        if (!is_array($bookings)) {
            $data['ViewMyTrips']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }
        // Make each BusType into array
        foreach ($bookings as $key=>$bookings) {
            $booking = $bookings->toOutputArray();
            if ($booking['Status'] == 'Request Accepted' || $booking['Status'] == 'Request Pending' || $booking['Status'] == 'On Dispute') {
                $driverDetail=false;
                if ($booking['Status'] == 'Request Accepted') {
                    $displayStatus = 'REQUEST ACCEPTED';
                    $busdrivers= $db->readMultiple('BookingDriverDetails', array(
                        array('ID','=',$_POST['BookingID'])
                    ));
                    if($busdrivers){
                        foreach($busdrivers as $busdriver){
                            $driver[]=$busdriver;
                        }
                        if(count($driver)>1){
                            $driverDetail = true;
                            $olddrivername=prev($busdrivers)->getDriverName();
                            $newdrivername=end($busdrivers)->getDriverName();
                            $oldbusnumber=prev($busdrivers)->getBusNumber();
                            $newbusnumber=end($busdrivers)->getBusNumber();
                            $oldreturndrivername=prev($busdrivers)->getReturnDriverName();
                            $newreturndrivername=end($busdrivers)->getReturnDriverName();
                            $oldreturnbusnumber=prev($busdrivers)->getReturnBusNumber();
                            $newreturnbusnumber=end($busdrivers)->getReturnBusNumber();
                            if($olddrivername != $newdrivername || $oldbusnumber !=$newbusnumber){
                                $typeofcriverchanged='PickUp';
                            }elseif($oldreturndrivername != $newreturndrivername || $oldreturnbusnumber !=$newreturnbusnumber){
                                $typeofcriverchanged='Return';
                            }else{
                                $typeofcriverchanged='Both';
                            }
                        }

                    }
                } else if ($booking['Status'] == 'Request Pending') {
                    $displayStatus = 'LOOKING FOR A BUS DRIVER';
                } else {
                    $displayStatus = 'RESOLVING DISPUTES...';
                }
                $unreads = $db->readMultiple('BookingNotification', array(
                    array('BookingID','=',$booking['ID']),
                    array('RecipientUserID','=',$users->userid),
                    array('IsRead','=',0),
                    array('NotificationMediumType', '=', 'App Notification')
                ));
                $data['ID'] = $booking['ID'];
                $data['BookingNumber'] = $booking['BookingNumber'];
                $data['TripType'] = $booking['TripType'];
                $data['PickUpPoint'] = $booking['PickUpPoint'];
                $data['Destination'] = $booking['Destination'];
                $data['Date'] = ltrim($this->dateForMenu($booking['PickUpDate']),0);
                $data['Time'] = ltrim(date('h:i', $booking['PickUpTime']),0);
                $data['AM/PM'] = date('A', $booking['PickUpTime']);
                $data['TotalPrice'] = (string)number_format(($booking['OfferingPrice'] + $booking['Surcharge']), 2, '.', '');
                $data['Status'] = $booking['Status'];
                $data['DisplayStatus'] = $displayStatus;
                $data['DriverChanged'] = $driverDetail;
                $data['TypeOfDriverChanged'] = ($typeofcriverchanged)?$typeofcriverchanged:'';
                $data['datetime']=date('Y/m/d', $booking['PickUpDate']).' '.date('h:i', $booking['PickUpTime']);
                if(is_array($unreads)){
                    $isUnreadFound = 0;
                    foreach ($unreads as $key=>$unreads) {
                        $unread = $unreads->toOutputArray();
                        if($unread['NotificationType'] == 'Request Accepted'
                            || $unread['NotificationType'] == 'On Dispute') {
                            $data['IsRead'] = false;
                            $datas['ViewMyTrips'][] = $data;
                            $isUnreadFound = 1;
                            break;
                        }
                    }
                    if($isUnreadFound == 0) {
                        $data['IsRead'] = true;
                        $readData[]=$data;
                    }
                }else{
                    $data['IsRead'] = true;
                    $readData[]=$data;
                }
            }
        }
        if(!empty($datas['ViewMyTrips'])){
            $date_sort = array();
            foreach ($datas['ViewMyTrips'] as $key => $row)
            {
                $date_sort[$key] = strtotime($row['datetime']);
                unset($datas['ViewMyTrips'][$key]['datetime']);
            }
            array_multisort($date_sort, SORT_ASC, $datas['ViewMyTrips']);
        }
        if(!empty($readData)){
            $date_sort = array();
            foreach ($readData as $key => $row)
            {
                $date_sort[$key] = strtotime($row['datetime']);
                unset($readData[$key]['datetime']);
            }
            array_multisort($date_sort, SORT_ASC, $readData);
        }
        if(is_array($readData)) {
            foreach ($readData as $value) {
                $datas['ViewMyTrips'][] = $value;
            }
        }elseif(!empty($readData)){
            $datas['ViewMyTrips'][] = $readData;
        }
        if(!is_array($datas) || empty($datas)){
            $datas['ViewMyTrips'] = array();
        }
        $this->returnData($response,1,'Successful',$datas);
        return 200;
    }

    function getCustomerViewMyTripDetail($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID'])
        ));
        // Handle empty cases
        if (!($bookings)) {
            $data['ViewMyTrips']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }
        // Make each BusType into array
        $booking = $bookings->toOutputArray();
        if($booking['Status'] == 'Request Accepted' || $booking['Status'] == 'On Dispute'){
            $busdriver= $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$_POST['BookingID']),
                array('IsActive','=',1)
            ));
            if($busdriver){
                $drivername=$busdriver->toOutputArray()['DriverName'];
                $busnumber=$busdriver->toOutputArray()['BusNumber'];
                $returndrivername=$busdriver->toOutputArray()['ReturnDriverName'];
                $returnbusnumber=$busdriver->toOutputArray()['ReturnBusNumber'];
            }
            $applicationUser = $db->readSingle('ApplicationUser', array(
                array('ID','=',$busdriver->toOutputArray()['ServiceProviderApplicationUserID'])
            ));
            if (!($applicationUser)) {
                $this->returnData($response,0,'Invalid User',$data);
                return 404;
            }
            $applicationUser = $applicationUser->toOutputArray();
        }
        if ($booking['Status'] == 'Request Accepted' || $booking['Status'] == 'Request Pending' || $booking['Status'] == 'On Dispute') {
            if ($booking['Status'] == 'Request Accepted') {
                $displayStatus = 'REQUEST ACCEPTED';
            } else if ($booking['Status'] == 'Request Pending') {
                $displayStatus = ' ';
            } else {
                $displayStatus = 'In the midst of resolving disputes...';
            }
            $busCapacity= $db->readSingle('BusType', array(
                array('ID','=',$booking['BusTypeID'])
            ));
            $unreads = $db->readMultiple('BookingNotification', array(
                array('BookingID','=',$booking['ID']),
                array('RecipientUserID','=',$users->userid),
                array('IsRead','=',0),
                array('NotificationMediumType', '=', 'App Notification')
            ), 0, 0, NULL, 'ASC', TRUE);
            if(is_array($unreads)){
                foreach($unreads as $key=>$unreads) {
                    $unread = $unreads;
                    if($unread->getNotificationType() == 'Request Accepted'
                    || $unread->getNotificationType() == 'On Dispute') {
                        $unread->update(array(
                            'IsRead' => 1,
                            'ReadDateTime' => time()
                        ));

                        if (!$db->update($unread)) {
                            Log::error(__METHOD__ . '() - DB Unable to create Booking"');
                            $this->returnData($response, 0, 'DB unable to create Booking', '');
                            return 500;
                        }
                    }
                }
            }
            $setting = $db->readSingle('Setting', array(
                array('SettingName', '=', 'AllowCustomerCancellationInMinutes')
            ));
            if(!$setting){
                $this->returnData($response,0,'Setting not found AllowCustomerCancellationInMinutes',$data);
                return 404;
            }
            $name=($applicationUser['Name'])?$applicationUser['Name']:$applicationUser['CompanyName'];
            $data['ViewMyTripDetail']['ID'] = $booking['ID'];
            $data['ViewMyTripDetail']['Photo'] = $applicationUser['Photo'];
            $data['ViewMyTripDetail']['Name'] = $name;
            $data['ViewMyTripDetail']['Mobile'] = $applicationUser['Mobile'];

            $data['ViewMyTripDetail']['BookingNumber'] = $booking['BookingNumber'];
            $data['ViewMyTripDetail']['TripType'] = $booking['TripType'];
            $data['ViewMyTripDetail']['BusDriverName'] = $drivername;
            $data['ViewMyTripDetail']['BusNumber'] = $busnumber;
            $data['ViewMyTripDetail']['ReturnBusDriverName'] = ($returndrivername)?$returndrivername:'';
            $data['ViewMyTripDetail']['ReturnBusNumber'] = ($returnbusnumber)?$returnbusnumber:'';
            $data['ViewMyTripDetail']['PickUpPoint'] = $booking['PickUpPoint'];
            $data['ViewMyTripDetail']['Destination'] = $booking['Destination'];
            $data['ViewMyTripDetail']['DateTime'] =ltrim($this->dateForMenu($booking['PickUpDate']),0).' '.ltrim(date('h:ia', $booking['PickUpTime']),0);
            $data['ViewMyTripDetail']['Fare'] = (string)number_format(($booking['OfferingPrice'] + $booking['Surcharge']), 2, '.', '');
            $data['ViewMyTripDetail']['NoteToDriver'] = $booking['NoteToDriver'];
            $data['ViewMyTripDetail']['BusCapacity'] =$busCapacity->toOutputArray()['BusType'];
            $data['ViewMyTripDetail']['Status'] = $booking['Status'];
            $data['ViewMyTripDetail']['DisplayStatus'] = $displayStatus;
            $data['ViewMyTripDetail']['DriverChanged'] = false;
            if ($booking['Status'] == 'Request Accepted') {
                $date = $booking['PickUpDate'];
                $picktime=$booking['PickUpTime'];
                $PickUpDateTime = mktime(date('H',$picktime),date('i',$picktime),date('s',$picktime),date('m',$date),date('d',$date),date('Y',$date))+($setting->toOutputArray()['SettingValue'] * 60);
                $now =(new DateTime())->getTimestamp();
                if($PickUpDateTime > $now){
                    $data['ViewMyTripDetail']['AllowCancellation']=true;
                }else{
                    $data['ViewMyTripDetail']['AllowCancellation']=false;
                }
            }else{
                $data['ViewMyTripDetail']['AllowCancellation']=false;
            }
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function customerCancelBooking($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        $time=getTimeInMs();
        //Read from DB
        $db = Database::getConnection();

        $data=$this->getCancellationPrice($db,$response,$_POST['BookingID']);
        $bookings=$data['bookings'];
        $bustype = $db->readSingle('BusType', array(
            array('ID','=',$bookings->getBusTypeID())
        ));
        // Handle empty cases
        if (!($bustype)) {
            $this->returnData($response,0,'Data not found','');
            return 404;
        }

        if(!($bookings->getStatus() == "Request Pending" || $bookings->getStatus() == "Request Accepted")) {
            $this->returnData($response,0,'Request/Booking cannot be cancelled.','');
            return 400;
        }
        $usercard = $db->readSingle('BookingPaymentMethod', array(
            array('BookingID','=',$_POST['BookingID']),
            array('Status','=','Hold')
        ));
        // Handle empty cases
        if (!($usercard)) {
            $this->returnData($response,0,'Payment method not defined','');
            return 404;
        }
        $applicationUserCard = $db->readSingle('ApplicationUserCard', array(
            array('ID','=',$usercard->toOutputArray()['CardID'])
        ));
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            if($bookings->getStatus() == 'Request Pending')
                $status='Request Cancelled';
            else
                $status = 'Cancelled By Customer';

            if($bookings->getStatus() == 'Request Accepted') {
                $driverDetails = $db->readSingle('BookingDriverDetails', array(
                    array('BookingID','=',$bookings->getID()),
                    array('IsActive','=','1')
                ));

                $DriveruserType = $db->readSingle('ApplicationUser', array(
                    array('ID','=',$driverDetails->getServiceProviderApplicationUserID())
                ));
                $notification = $this->getNotificationSetting($users);

                if(!$notification){
                    $this->returnData($response,0,'No medium for notification is selected','');
                    return 500;
                }

                $sms = 'WantBus - Your booking request '. $bookings->getBookingNumber().' has been successfully cancelled. A cancellation fee of $'. $data['cancellationPrice'].' has been charged.';
                $email=' Thank you for using WantBus! Your booking request {bookingNumber} for a {bustype} Bus on {date} at {time} has been cancelled successfully.'.'<br/><br/>'.' A cancellation fee of ${fee} has been charged to your credit card ending in {cardNumber}';
                $number = $this->mc_decrypt($applicationUserCard->toOutputArray()["CardNumber"], ENCRYPTION_KEY);
                $cardNumber = substr($number,-4);
                $email=strtr($email, array(
                    '{bookingNumber}' => $bookings->getBookingNumber(),
                    '{bustype}' => $bustype->getBusType(),
                    '{date}' => date('d/m/Y',$bookings->getPickUpDate()),
                    '{time}' =>  date('h:ia',$bookings->getPickUpTime()),
                    '{fee}' => $data['CancellationFee'],
                    '{cardNumber}' => $cardNumber
                ));
                $notificationLogsms = new BookingNotification (array(
                    'BookingID' => $bookings->getID(),
                    'NotificationType' => 'Cancelled By Customer',
                    'NotificationMediumType' => 'SMS',
                    'Notification' => $sms,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $notificationLogemail = new BookingNotification (array(
                    'BookingID' => $bookings->getID(),
                    'NotificationType' => 'Cancelled By Customer',
                    'NotificationMediumType' => 'Email',
                    'Notification' => $email,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $mailDetails = array(
                    'subject'=>'Booking Cancelled By Customer',
                    'email'=>$DriveruserType->getEmail(),
                    'message'=>$email,
                    'file'=>'notification'
                );
                if($notification['UserNotificationSetting']['SMS'] == '1'){
                    if (!$db->create($notificationLogsms)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }

                }
                if($notification['UserNotificationSetting']['Email'] == '1'){
                    if (!$db->create($notificationLogemail)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }
                    $sendEmail=$this->sendEmail($mailDetails);
                    if(!$sendEmail){
                        $this->returnData($response,0,'Unable to send Email','');
                        return 500;
                    }
                }
                $card = $db->readSingle('BookingPaymentMethod', array(
                    array('BookingID','=',$bookings->getID()),
                    array('Status','=','Hold')
                ));
                if (!($card)) {
                    $this->returnData($response,0,'Data not correct. Not found Data in BookingPaymentMethod that is on hold','');
                    return 404;
                }
                try {
                    $bookingPaymentMethod = new BookingPaymentMethod (array(
                        'BookingID' => $bookings->getID(),
                        'CardID' =>$card->getCardID(),
                        'Status' =>'CancellationCharged',
                        'CreatedDateTime' => $time
                    ));
                }
                catch (Exception $e) {
                    $this->returnData($response,0,$e->getMessage(),'');
                    return 400;
                }
                if (!$db->create($bookingPaymentMethod)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                    $this->returnData($response,0,'DB unable to add Booking Payment Method','');
                    return 500;
                }
            }
            $bookings->update(array(
                'Status' => $status,
                'CancellationFee' => $data['cancellationPrice']
            ));

            if (!$db->update($bookings)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking"');
                $this->returnData($response,0,'DB unable to create Booking','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $bookings->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => $status,
                    'Remarks' =>'',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Cancel',
                'ModelID' => $bookings->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        }

        $this->returnData($response,1,'Successful cancelled Booking','');
        return 200;
    }


    /**
     * change the driver for the booked trip
     * makes changes in db and send notification to customer :bkesh
     * @param type $response
     * @return int
     */
    function changeDriver($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        //check authorised user type
        if ($users->usertype!='Corporate Service Provider') {
            $this->returnData($response,0,'Not authorized to update ','');
            return 202;
        }
        // Init return data
        $data = array();
        $time=getTimeInMs();
        $bookingID = $_POST['BookingID'];
        $driverName = $_POST['DriverName'];
        $busNumber = $_POST['BusNumber'];
        $returndriverName = $_POST['ReturnDriverName'];
        $returnbusNumber = $_POST['ReturnBusNumber'];
        $logText = 'Request Accepted';
        //Read from DB
        $db = Database::getConnection();

        $booking = $db->readSingle('Booking', array(
            array('ID','=',$bookingID)
        ));
        if (!($booking)) {
            $this->returnData($response,0,'Empty data ','');
            return 202;
        }
        if($booking->getStatus() !='Request Accepted'){
            $this->returnData($response,0,'Booking request must be accepted ','');
            return 202;
        }
        $bookingDriverDetail = $db->readSingle('BookingDriverDetails', array(
            array('BookingID','=',$bookingID),
            array('ServiceProviderApplicationUserID','=',$users->userid)
        ));
        if (!($bookingDriverDetail)) {
            $this->returnData($response,0,'Not authorized to update ','');
            return 202;
        }
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            $driverDetails = $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$booking->getID()),
                array('IsActive','=','1')
            ));
            $DriveruserType = $db->readSingle('ApplicationUser', array(
                array('ID','=',$driverDetails->getServiceProviderApplicationUserID())
            ));
            $_notification = $db->readSingle('ApplicationUserNotificationSetting', array(
                array('ApplicationUserID','=',$booking->getCustomerApplicationUserID())
            ));
            if ($_notification){
                $notification['UserNotificationSetting'] = array(
                    'SMS' => $_notification->toOutputArray()['SMS'],
                    'Email' => $_notification->toOutputArray()['Email']
                );
            }
            if(!$notification){
                $this->returnData($response,0,'No medium for notification is selected','');
                return 500;
            }
            //$notification = $notification->toOutputArray();

            $appNofitication = 'Your booking request '.$booking->getBookingNumber().' has a change in the driver and/or bus number. Please log in to the app to view updated details.';
            $sms = 'WantBus - Your booking request '. $booking->getBookingNumber().' has a change in the driver and/or bus number. Please login to the app to view updated details.';
            $email='We would like to inform you that there has been a change in the driver and/or bus number for your booking request '. $booking->getBookingNumber().'. Please login to the app to view the updated details.';
            $email=strtr($email, array());
            $userType = 'Customer';
            $notificationLogsms = new BookingNotification (array(
                'BookingID' => $booking->getID(),
                'NotificationType' => $logText,
                'NotificationMediumType' => 'SMS',
                'Notification' => $sms,
                'NotificationSentDateTime'=>time(),
                'RecipientUserID'=>$booking->getCustomerApplicationUserID(),
                'RecipientUserType'=>$userType,
                'IsRead'=>0
            ));
            $notificationLogemail = new BookingNotification (array(
                'BookingID' => $booking->getID(),
                'NotificationType' => $logText,
                'NotificationMediumType' => 'Email',
                'Notification' => $email,
                'NotificationSentDateTime'=>time(),
                'RecipientUserID'=>$booking->getCustomerApplicationUserID(),
                'RecipientUserType'=>$userType,
                'IsRead'=>0
            ));
            $notificationLogApp = new BookingNotification (array(
                'BookingID' => $booking->getID(),
                'NotificationType' => $logText,
                'NotificationMediumType' => 'App Notification',
                'Notification' => $appNofitication,
                'NotificationSentDateTime'=>time(),
                'RecipientUserID'=>$booking->getCustomerApplicationUserID(),
                'RecipientUserType'=>$userType,
                'IsRead'=>0
            ));
            $customer = $db->readSingle('ApplicationUser', array(
                array('ID','=',$booking->getCustomerApplicationUserID())
            ));
            $mailDetails = array(
                'subject'=>'Driver Changed',
                'email'=>$customer->toOutputArray()["Email"],
                'message'=>$email,
                'file'=>'notification'
            );

            if (!$db->create($notificationLogApp)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                $this->returnData($response,0,'DB unable to create Booking Notification','');
                return 500;
            }
            if($notification['UserNotificationSetting']['SMS'] == '1'){
                if (!$db->create($notificationLogsms)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }

            }
            if($notification['UserNotificationSetting']['Email'] == '1'){
                if (!$db->create($notificationLogemail)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
            }
            $driverDetails->update(array(
                'IsActive' => 0
            ));

            if (!$db->update($driverDetails)) {
                Log::error(__METHOD__.'() - DB Unable to update Driver Detail"');
                $this->returnData($response,0,'DB unable to update Driver Detail','');
                return 500;
            }
            //insert new driver details
            try {
                $newBookingDriverDetail = new BookingDriverDetails (array(
                    'BookingID' => $booking->getID(),
                    'ServiceProviderApplicationUserID' =>$users->userid,
                    'DriverName' => $driverName,
                    'BusNumber' => $busNumber,
                    'ReturnDriverName' => $returndriverName,
                    'ReturnBusNumber' => $returnbusNumber,
                    'IsActive' =>1,
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($newBookingDriverDetail)) {
                Log::error(__METHOD__.'() - DB Unable to create BookingDriverDetails Log"');
                $this->returnData($response,0,'DB unable to create BookingDriverDetails Log','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $booking->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => $logText,
                    'Remarks' =>'Driver Changed',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Driver Changed',
                'ModelID' => $booking->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }
                $time = getTimeInMs();
            }
        }

        $this->returnData($response,1,'Successful changed driver','');
        return 200;
    }

    /**
     * api call when the trip is completed,
     * changes the trip status in db and send notification to customer: bkesh
     * @param type $response
     * @return int
     */
    function completeTrip($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        //check authorised user type
        if ($users->usertype=='Customer') {
            $this->returnData($response,0,'Not authorized to update ','');
            return 202;
        }
        // Init return data
        $data = array();
        $time=getTimeInMs();
        $bookingID = $_POST['BookingID'];
        $logText = 'Trip Completed';
        $newStatus = 'Trip Completed';
        //Read from DB
        $db = Database::getConnection();

        $booking = $db->readSingle('Booking', array(
            array('ID','=',$bookingID)
        ));
        if (!($booking)) {
            $this->returnData($response,0,'Empty data ','');
            return 202;
        }
        if($booking->getStatus() !='Request Accepted'){
            $this->returnData($response,0,'Booking request must be accepted ','');
            return 202;
        }
        $bookingDriverDetail = $db->readSingle('BookingDriverDetails', array(
            array('BookingID','=',$bookingID),
            array('ServiceProviderApplicationUserID','=',$users->userid)
        ));
        if (!($bookingDriverDetail)) {
            $this->returnData($response,0,'Not authorized to update ','');
            return 202;
        }
        $bustype = $db->readSingle('BusType', array(
            array('ID','=',$booking->getBusTypeID())
        ));
        // Handle empty cases
        if (!($bustype)) {
            $this->returnData($response,0,'Data not found','');
            return 404;
        }

        $customer = $db->readSingle('ApplicationUser', array(
            array('ID', '=', $booking->getCustomerApplicationUserID())
        ));

        $usercard = $db->readSingle('BookingPaymentMethod', array(
            array('BookingID','=',$bookingID)
        ));
        // Handle empty cases
        if (!($usercard)) {
            $this->returnData($response,0,'Payment method not defined','');
            return 404;
        }
        $applicationUserCard = $db->readSingle('ApplicationUserCard', array(
            array('ID','=',$usercard->toOutputArray()['CardID']),
            array('Status','=','Hold')
        ));
        if (!($applicationUserCard)) {
            $this->returnData($response,0,'Application user card not found','');
            return 404;
        }
        $paymentAmount = $booking->getOfferingPrice() + $booking->getSurcharge();
        $paymentSuccess = $this->tripPaymentSuccess();

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            $driverDetails = $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$booking->getID()),
                array('IsActive','=','1')
            ));
            $DriveruserType = $db->readSingle('ApplicationUser', array(
                array('ID','=',$driverDetails->getServiceProviderApplicationUserID())
            ));
            $notification = $db->readSingle('ApplicationUserNotificationSetting', array(
                array('ApplicationUserID','=',$driverDetails->getServiceProviderApplicationUserID())
            ));

            if(!$notification){
                $this->returnData($response,0,'No medium for notification is selected','');
                return 500;
            }
            $notification = $notification->toOutputArray();
            if(!$paymentSuccess) {
                //insert new data
                try {
                    $bookingPaymentIncomplete = new BookingPaymentIncomplete(array(
                        'BookingID' => $booking->getID(),
                        'TotalAmount' => $paymentAmount
                    ));
                } catch (Exception $e) {
                    $this->returnData($response, 0, $e->getMessage(), '');
                    return 400;
                }
                if (!$db->create($bookingPaymentIncomplete)) {
                    Log::error(__METHOD__ . '() - DB Unable to create BookingPaymentIncomplete Log"');
                    $this->returnData($response, 0, 'DB unable to create BookingPaymentIncomplete Log', '');
                    return 500;
                }
                //need to send email to both customer and provider, as of now only sent to provider
                $sms = 'WantBus - Your booking request '. $booking->getBookingNumber().' payment has been Failed.';
                $email='Thank you for using WantBus! Your booking request {bookingNumber} payment has been Failed. ';
                $email=strtr($email, array(
                    '{bookingNumber}' => $booking->getBookingNumber(),
                    '{bustype}' => $bustype->getBusType(),
                    '{date}' => ($booking->getPickUpDate())?date('d/m/Y',$booking->getPickUpDate()):'',
                    '{time}' =>  date('h:ia',$booking->getPickUpTime()),
                    '{fee}' => $paymentAmount,
                    '{cardexpirydate}' => date('d/m/Y',$applicationUserCard->getExpiryDate())
                ));
                $notificationLogsms = new BookingNotification (array(
                    'BookingID' => $booking->getID(),
                    'NotificationType' => $logText,
                    'NotificationMediumType' => 'SMS',
                    'Notification' => $sms,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $notificationLogemail = new BookingNotification (array(
                    'BookingID' => $booking->getID(),
                    'NotificationType' => $logText,
                    'NotificationMediumType' => 'Email',
                    'Notification' => $email,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $mailDetails = array(
                    'subject'=>$logText,
                    'email'=>$DriveruserType->getEmail(),
                    'message'=>$email,
                    'file'=>'notification'
                );
                if($notification['UserNotificationSetting']['SMS'] == '1'){
                    if (!$db->create($notificationLogsms)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }

                }
                if($notification['UserNotificationSetting']['Email'] == '1'){
                    if (!$db->create($notificationLogemail)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }
                    $sendEmail=$this->sendEmail($mailDetails);
                    if(!$sendEmail){
                        $this->returnData($response,0,'Unable to send Email','');
                        return 500;
                    }
                }
            }else{
//                $card = $db->readSingle('BookingPaymentMethod', array(
//                    array('BookingID','=',$booking->getID()),
//                    array('Status','=','Hold')
//                ));
//                if (!($card)) {
//                    $this->returnData($response,0,'Data not correct. Not found Data in BookingPaymentMethod that is on hold','');
//                    return 404;
//                }
                try {
                    $bookingPaymentMethod = new BookingPaymentMethod (array(
                        'BookingID' => $booking->getID(),
                        'CardID' =>$applicationUserCard->getCardID(),
                        'Status' =>'TripCharged',
                        'CreatedDateTime' => $time
                    ));
                }
                catch (Exception $e) {
                    $this->returnData($response,0,$e->getMessage(),'');
                    return 400;
                }
                if (!$db->create($bookingPaymentMethod)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                    $this->returnData($response,0,'DB unable to add Booking Payment Method','');
                    return 500;
                }
                $sms = 'WantBus - Your booking request '. $booking->getBookingNumber().' has been successfully completed. The trip fare of $paymentAmount has been charged.';
                $email='Thank you for using WantBus! Your booking request {bookingNumber}  for a {bustype} Bus on {date} at {time} has been completed successfully.
                    The trip fare of ${fee} has been charged to your credit card (ending in {cardexpirydate}). ';
                $email=strtr($email, array(
                    '{bookingNumber}' => $booking->getBookingNumber(),
                    '{bustype}' => $bustype->getBusType(),
                    '{date}' => ($booking->getPickUpDate())?date('d/m/Y',$booking->getPickUpDate()):'',
                    '{time}' =>  date('h:ia',$booking->getPickUpTime()),
                    '{fee}' => $paymentAmount,
                    '{cardexpirydate}' => date('d/m/Y',$usercard->getExpiryDate())
                ));
                $notificationLogsms = new BookingNotification (array(
                    'BookingID' => $booking->getID(),
                    'NotificationType' => $logText,
                    'NotificationMediumType' => 'SMS',
                    'Notification' => $sms,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $notificationLogemail = new BookingNotification (array(
                    'BookingID' => $booking->getID(),
                    'NotificationType' => $logText,
                    'NotificationMediumType' => 'Email',
                    'Notification' => $email,
                    'NotificationSentDateTime'=>time(),
                    'RecipientUserID'=>$driverDetails->getServiceProviderApplicationUserID() ,
                    'RecipientUserType'=>$DriveruserType->getUserType() ,
                    'IsRead'=>0
                ));
                $mailDetails = array(
                    'subject'=>$logText,
                    'email'=>$DriveruserType->getEmail(),
                    'message'=>$email,
                    'file'=>'notification'
                );
                if($notification['UserNotificationSetting']['SMS'] == '1'){
                    if (!$db->create($notificationLogsms)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }

                }
                if($notification['UserNotificationSetting']['Email'] == '1'){
                    if (!$db->create($notificationLogemail)) {
                        Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                        $this->returnData($response,0,'DB unable to create Booking Notification','');
                        return 500;
                    }
                    $sendEmail=$this->sendEmail($mailDetails);
                    if(!$sendEmail){
                        $this->returnData($response,0,'Unable to send Email','');
                        return 500;
                    }
                }
            }


            $booking->update(array(
                'Status' => $newStatus
            ));

            if (!$db->update($booking)) {
                Log::error(__METHOD__.'() - DB Unable to update Booking"');
                $this->returnData($response,0,'DB unable to update Booking','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $booking->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => $logText,
                    'Remarks' =>'',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => $logText,
                'ModelID' => $booking->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }
                $time = getTimeInMs();
            }
        }

        $this->returnData($response,1,'Successful trip completed','');
        return 200;
    }


    function tripPaymentSuccess(){
        return TRUE;
    }

    function getUserNotificationSetting($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $usernotificationsettings = $db->readSingle('ApplicationUserNotificationSetting', array(
            array('ApplicationUserID','=',$users->userid)
        ));
        // Handle empty cases
        if (!($usernotificationsettings)) {
            $data['UserNotificationSetting']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }
        $data['UserNotificationSetting']=array(
            'ID'=>$usernotificationsettings->toOutputArray()['ID'],
            'SMS'=>($usernotificationsettings->toOutputArray()['SMS'] == 1)?true:false,
            'Email'=>($usernotificationsettings->toOutputArray()['Email'])?true:false
        );
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function emailNotification($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        // Data checks
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset($_POST['EmailNotificationSetting'])){
            $this->returnData($response,0,'Email notification setting value is required.','');
            return 402;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $usernotificationsettings = $db->readSingle('ApplicationUserNotificationSetting', array(
            array('ApplicationUserID','=',$users->userid)
        ),TRUE);
        // Handle empty cases
        if (!($usernotificationsettings)) {
            $data['UserNotificationSetting']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        if($_POST['EmailNotificationSetting'] == 0 && $usernotificationsettings->toOutputArray()['SMS'] == 0) {
            $this->returnData($response,0,'At least one notification medium must be active.',$data);
            return 400;
        }

        try {
            $usernotificationsettings->update(array(
                'SMS' => $usernotificationsettings->toOutputArray()['SMS'],
                'Email' => $_POST['EmailNotificationSetting'],
                'LastUpdatedDateTime' => time(),
            ));
        }
        catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }
        if (!$db->update($usernotificationsettings)) {
            Log::error(__METHOD__.'() - DB unable to update notification settings');
            $this->returnData($response,0,'DB unable to update profile','');
            return 500;
        }

        $data["Email"] = $_POST['EmailNotificationSetting'];
        $data["SMS"] = $usernotificationsettings->toOutputArray()['SMS'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function smsNotification($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        // Data checks
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        if(!isset($_POST['SMSNotificationSetting'])){
            $this->returnData($response,0,'SMS notification setting value is required.','');
            return 402;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read Languages from DB
        $db = Database::getConnection();
        $usernotificationsettings = $db->readSingle('ApplicationUserNotificationSetting', array(
            array('ApplicationUserID','=',$users->userid)
        ),TRUE);
        // Handle empty cases
        if (!($usernotificationsettings)) {
            $data['UserNotificationSetting']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        if($usernotificationsettings->toOutputArray()['Email'] == 0 && $_POST['SMSNotificationSetting'] == 0) {
            $this->returnData($response,0,'At least one notification medium must be active.',$data);
            return 400;
        }

        try {
            $usernotificationsettings->update(array(
                'SMS' => $_POST['SMSNotificationSetting'],
                'Email' =>$usernotificationsettings->toOutputArray()['Email'],
                'LastUpdatedDateTime' => time()
            ));
        }
        catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }
        if (!$db->update($usernotificationsettings)) {
            Log::error(__METHOD__.'() - DB unable to update notification settings');
            $this->returnData($response,0,'DB unable to update profile','');
            return 500;
        }
        $data["Email"] = $usernotificationsettings->toOutputArray()['Email'];
        $data["SMS"] = $_POST['SMSNotificationSetting'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getNumberCustomerHistory($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        $db = Database::getConnection();
        $history = $db->readMultiple('Booking', array(
            array('CustomerApplicationUserID','=',$users->userid),
        ));
        // Handle empty cases
        if (!is_array($history)) {
            $data['Count']=0;
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }
        $historyData=array();
        foreach ($history as $key => $history) {
            $history = $history->toOutputArray();
            if ($history['Status'] == 'Cancelled By Driver' || $history['Status'] == 'Trip Completed' || $history['Status'] == 'Cancelled By Customer') {
                $unread = $db->readSingle('BookingNotification', array(
                    array('BookingID','=',$history['ID']),
                    array('RecipientUserID','=',$users->userid),
                    array('IsRead','=',0)
                ));
                if($unread){
                    $historyData[]=$history;
                }
            }
        }
        $data['Count']=count($historyData);
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getCustomerHistory($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $history = $db->readMultiple('Booking', array(
            array('CustomerApplicationUserID','=',$users->userid),
        ));

        // Handle empty cases
        if (!is_array($history)) {
            $data['History']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        // Make each BusType into array
        foreach ($history as $key=>$history) {
            $history = $history->toOutputArray();
            if ($history['Status'] == 'Cancelled By Driver' || $history['Status'] == 'Trip Completed' || $history['Status'] == 'Cancelled By Customer') {
                $unread = $db->readSingle('BookingNotification', array(
                    array('BookingID','=',$history['ID']),
                    array('RecipientUserID','=',$users->userid),
                    array('IsRead','=',0)
                ));
                if ($history['Status'] == 'Cancelled By Driver') {
                    $displayStatus = 'Trip cancelled by driver.';
                } else if ($history['Status'] == 'Trip Completed') {
                    $displayStatus = 'Trip Completed';
                } else {
                    $displayStatus = 'Trip cancelled by you.';
                }
                $acceptedDriver = $db->readSingle('BookingDriverDetails', array(
                    array('BookingID','=',$history['ID']),
                    array('IsActive','=','1')
                ));
                $data['ID'] = $history['ID'];
                $data['BookingNumber'] = $history['BookingNumber'];
                $data['TripType'] = $history['TripType'];
                $data['PickUpPoint'] = $history['PickUpPoint'];
                $data['Destination'] = $history['Destination'];
                $data['Date'] = ltrim(date('d M Y', $history['PickUpDate']),0);
                $data['Time'] = ltrim(date('h:ia', $history['PickUpTime']),0);
                $data['Fare'] = number_format(($history['OfferingPrice'] + $history['Surcharge']),2);
                $data['Status'] = $history['Status'];
                $data['DisplayStatus'] = $displayStatus;
                $data['AcceptedDriverID'] = ($acceptedDriver)?$acceptedDriver->getServiceProviderApplicationUserID():'';
                if($unread){
                    $data['IsRead'] = false;
                    $datas['History'][]=$data;
                }else{
                    $data['IsRead'] = true;
                    $rdatas[]=$data;
                }
            }
        }

        if(!empty($datas['History'])){
            $date_sort = array();
            foreach ($datas['History'] as $key => $row)
            {
                $date_sort[$key] = strtotime($row['Date']);
            }
            array_multisort($date_sort, SORT_DESC, $datas['History']);
        }

        if(!empty($rdatas)){
            $date_sort = array();
            foreach ($rdatas as $key => $row)
            {
                $date_sort[$key] = strtotime($row['Date']);
            }
            array_multisort($date_sort, SORT_DESC, $rdatas);
        }

        if(is_array($rdatas)) {
            foreach ($rdatas as $value) {
                $datas['History'][] = $value;
            }
        }elseif(!empty($rdatas)){
            $datas['History'][] = $rdatas;
        }
        if(!is_array($datas) || empty($datas)){
            $datas['History'] = array();
        }
        $this->returnData($response,1,'Successful',$datas);
        return 200;
    }

    function invenDescSort($item1,$item2)
    {
        if ($item1['price'] == $item2['price']) return 0;
        return ($item1['price'] < $item2['price']) ? 1 : -1;
    }

    function getCustomerHistoryDetail($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        $db = Database::getConnection();
        $history = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID'])
        ));
        // Handle empty cases
        if (!($history)) {
            $data['History']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }


        $history = $history->toOutputArray();
        if ($history['Status'] == 'Cancelled By Driver' || $history['Status'] == 'Trip Completed' || $history['Status'] == 'Cancelled By Customer') {
            $unread = $db->readSingle('BookingNotification', array(
                array('BookingID','=',$history['ID']),
                array('RecipientUserID','=',$users->userid),
                array('IsRead','=',0)
            ),TRUE);
            if($unread){
                $unread->update(array(
                    'IsRead' => 1,
                    'ReadDateTime' => time()
                ));

                if (!$db->update($unread)) {
                    Log::error(__METHOD__.'() - DB Unable to create update Booking Notificationto read"');
                    $this->returnData($response,0,'DB Unable to create update Booking Notificationto read','');
                    return 500;
                }
            }
            if ($history['Status'] == 'Cancelled By Driver') {
                $displayStatus = 'Driver Cancelled this Trip';
            } else if ($history['Status'] == 'Trip Completed') {
                $displayStatus = 'Trip Completed';
            } else {
                $displayStatus = 'You cancelled this trip';
            }
            $busdriver= $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$history['ID']),
                array('IsActive', '=', 1)
            ));

            $applicationUser = $db->readSingle('ApplicationUser', array(
                array('ID','=',$busdriver->getServiceProviderApplicationUserID())
            ));
            $applicationUser = $applicationUser->toOutputArray();
            $rating = 0;
            if($busdriver){
                $drivername=$busdriver->toOutputArray()['DriverName'];
                $busnumber=$busdriver->toOutputArray()['BusNumber'];
                $returndrivername=$busdriver->toOutputArray()['ReturnDriverName'];
                $returnbusnumber=$busdriver->toOutputArray()['ReturnBusNumber'];
                $rating= $db->readSingle('BookingRating', array(
                    array('BookingID','=',$history['ID']),
                    array('CustomerApplicationUserID','=',$users->userid),
                    array('ServiceProviderApplicationUserID','=',$busdriver->toOutputArray()['ServiceProviderApplicationUserID'])
                ));
                if($rating){
                    $rating=number_format($rating->toOutputArray()['Rating'],1);
                    if($rating == null) {
                        $rating = 0;
                    }
                }
                else
                    $rating = 0;
            }else{
                $drivername=null;
                $busnumber=null;
                $returndrivername=null;
                $returnbusnumber=null;
                $rating = 0;
            }
            $busCapacity= $db->readSingle('BusType', array(
                array('ID','=',$history['BusTypeID'])
            ));
            $data['ViewMyHistoryDetail']['ID'] = $history['ID'];
            $data['ViewMyHistoryDetail']['Photo'] = $applicationUser['Photo'];
            $data['ViewMyHistoryDetail']['Name'] = ($applicationUser['Name'])?$applicationUser['Name']:$applicationUser['CompanyName'];
            $data['ViewMyHistoryDetail']['Mobile'] = $applicationUser['Mobile'];
            $data['ViewMyHistoryDetail']['BookingNumber'] = $history['BookingNumber'];
            $data['ViewMyHistoryDetail']['TripType'] = $history['TripType'];
            if($busdriver) {
                $data['ViewMyHistoryDetail']['ServiceProviderApplicationUserID'] = $busdriver->toOutputArray()['ServiceProviderApplicationUserID'];
            } else {
                $data['ViewMyHistoryDetail']['ServiceProviderApplicationUserID'] = null;
            }
            $data['ViewMyHistoryDetail']['BusDriverName'] = $drivername;
            $data['ViewMyHistoryDetail']['BusNumber'] = $busnumber;
            $data['ViewMyHistoryDetail']['ReturnBusDriverName'] = $returndrivername;
            $data['ViewMyHistoryDetail']['ReturnBusNumber'] = $returnbusnumber;
            $data['ViewMyHistoryDetail']['PickUpPoint'] = $history['PickUpPoint'];
            $data['ViewMyHistoryDetail']['Destination'] = $history['Destination'];
            $data['ViewMyHistoryDetail']['DateTime'] = ltrim($this->dateForMenu($history['PickUpDate']),0).' '.ltrim(date('h:ia', $history['PickUpTime']),0);
            $data['ViewMyHistoryDetail']['Fare'] = number_format(($history['OfferingPrice'] + $history['Surcharge']),2);
            $data['ViewMyHistoryDetail']['NoteToDriver'] = $history['NoteToDriver'];
            $data['ViewMyHistoryDetail']['BusCapacity'] =$busCapacity->toOutputArray()['BusType'];
            $data['ViewMyHistoryDetail']['Rating'] = $rating;
            $data['ViewMyHistoryDetail']['Status'] = $history['Status'];
            $data['ViewMyHistoryDetail']['DisplayStatus'] = $displayStatus;
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function rating($response){
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        $db = Database::getConnection();
        $bookingRating = $db->readSingle('BookingRating', array(
            array('BookingID','=',$_POST['BookingID']),
            array('CustomerApplicationUserID','=',$users->userid)
        ));
        if($bookingRating){
            $this->returnData($response,0,'This Booking is already rated','');
            return 404;
        }
        try {
            $rating = new BookingRating (array(
                'BookingID'=> $_POST['BookingID'],
                'CustomerApplicationUserID' =>$users->userid,
                'ServiceProviderApplicationUserID'=> $_POST['ServiceProviderApplicationUserID'],
                'Rating'=> $_POST['Rating'],
                'LastUpdatedDateTime'=> time()
            ));
        } catch (Exception $e) {
            $this->returnData($response,0,$e->getMessage(),'');
            return 400;
        }

        // Create in DB
        if (!$db->create($rating)) {
            Log::error(__METHOD__ . '() - DB unable to add rating for: ' . $_POST['BookingID']);
            $this->returnData($response,0,'DB unable to add rating','');
            return 500;
        }
        $data['Rating'] = $rating;
        $this->returnData($response,1,'Successful rated the trip','');
        return 200;
    }
    /***   *** *** -- Service Provider -- *** **** ***/
    function getServiceProviderViewRequest($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readMultiple('Booking', array(
            array('Status','=','Request Pending')
        ));

        // Handle empty cases
        if (!is_array($bookings)) {
            $data['ViewMyTrips']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        // Make each BusType into array
        foreach ($bookings as $key=>$bookings) {
            $booking = $bookings->toOutputArray();
            $busCapacity = $db->readSingle('BusType', array(
                array('ID', '=', $booking['BusTypeID'])
            ));
            $data['ViewMyTrips'][$key]['ID'] = $booking['ID'];
            $data['ViewMyTrips'][$key]['BookingNumber'] = $booking['BookingNumber'];
            $data['ViewMyTrips'][$key]['TripType'] = $booking['TripType'];
            $data['ViewMyTrips'][$key]['PickUpPoint'] = $booking['PickUpPoint'];
            $data['ViewMyTrips'][$key]['Destination'] = $booking['Destination'];
            $data['ViewMyTrips'][$key]['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
            $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
            $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
            if($booking['TripType'] == 'Shuttle') {
                $data['ViewMyTrips'][$key]['DateTime']  = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                $data['ViewMyTrips'][$key]['IntervalHour']=$this->IntervalHour($booking['IntervalHour']);
            }elseif($booking['TripType'] == 'Return') {
                $data['ViewMyTrips'][$key]['DateTime']  = $pickup .' & '.$return;
            }else{
                $data['ViewMyTrips'][$key]['DateTime']  = $pickup;
            }
            $data['ViewMyTrips'][$key]['IntervalHour'] =$this->IntervalHour($booking['IntervalHour']);
            $data['ViewMyTrips'][$key]['OfferedPrice'] = number_format($booking['OfferingPrice'], 2);
            $data['ViewMyTrips'][$key]['Status'] = $booking['Status'];
            $data['ViewMyTrips'][$key]['datetime'] = date('d M Y', $booking['PickUpDate']) .' '. ltrim(date('h:ia', $booking['PickUpTime']),0);
        }

        if(!empty($data['ViewMyTrips'])){
            $date_sort = array();
            foreach ($data['ViewMyTrips'] as $key => $row)
            {
                $date_sort[$key] = strtotime($row['datetime']);
                unset($data['ViewMyTrips'][$key]['datetime']);
            }
            array_multisort($date_sort, SORT_ASC, $data['ViewMyTrips']);
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getServiceProviderViewRequestDetail($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }

        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $loggedInUser = $db->readSingle('ApplicationUser', array(
            array('ID','=',$users->userid)
        ));
        if (!($loggedInUser)) {
            $this->returnData($response,0,'Logged In user not found',$data);
            return 404;
        }
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID']),
            array('Status','=','Request Pending')
        ));

        // Handle empty cases
        if (!($bookings)) {
            $data['ViewMyTrips']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        $booking = $bookings->toOutputArray();
        $busCapacity= $db->readSingle('BusType', array(
            array('ID','=',$booking['BusTypeID'])
        ));
        $applicationUser = $db->readSingle('ApplicationUser', array(
            array('ID','=',$booking['CustomerApplicationUserID'])
        ));
        if (!($applicationUser)) {
            $this->returnData($response,0,'User not found that booked a bus',$data);
            return 404;
        }

        if($loggedInUser->toOutputArray()['UserType'] == 'Private Service Provider') {
            if($loggedInUser->toOutputArray()['BusCapacity'] >= $busCapacity->toOutputArray()['MaxCapacity'])
                $requirementMet=true;
            else
                $requirementMet=false;
        }else{
            $requirementMet=true;
        }
        $data['ViewMyTrips']['ID'] = $booking['ID'];
        $data['ViewMyTrips']['BookingNumber'] = $booking['BookingNumber'];
        $data['ViewMyTrips']['TripType'] = $booking['TripType'];
        $data['ViewMyTrips']['Name'] = $applicationUser->toOutputArray()['Name'];
        $data['ViewMyTrips']['Mobile'] = $applicationUser->toOutputArray()['Mobile'];
        $data['ViewMyTrips']['Photo'] = $applicationUser->toOutputArray()['Photo'];
        $data['ViewMyTrips']['PickUpPoint'] = $booking['PickUpPoint'];
        $data['ViewMyTrips']['Destination'] = $booking['Destination'];
        $data['ViewMyTrips']['PickUpPointLatitude'] = explode(',',$booking['PickUpPointCoordinate'])[0];
        $data['ViewMyTrips']['PickUpPointLongitude'] = explode(',',$booking['PickUpPointCoordinate'])[1];
        $data['ViewMyTrips']['DestinationLatitude'] = explode(',',$booking['DestinationCoordinate'])[0];
        $data['ViewMyTrips']['DestinationLongitude'] = explode(',',$booking['DestinationCoordinate'])[1];
        $data['ViewMyTrips']['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
        $data['ViewMyTrips']['RequirementMet'] = $requirementMet;
        $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
        $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
        if($booking['TripType'] == 'Shuttle') {
            $data['ViewMyTrips']['DateTime'] = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
            $data['ViewMyTrips']['IntervalHour'] =$this->IntervalHour($booking['IntervalHour']);
        }elseif($booking['TripType'] == 'Return') {
            $data['ViewMyTrips']['DateTime'] = $pickup .' & '.$return;
        }else{
            $data['ViewMyTrips']['DateTime'] = $pickup;
        }
        $data['ViewMyTrips']['OfferedPrice'] =  number_format($booking['OfferingPrice'],2);
        $data['ViewMyTrips']['NoteToDriver'] = $booking['NoteToDriver'];
        $data['ViewMyTrips']['Status'] = $booking['Status'];
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getServiceProviderViewMyTrips($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookingIDs=$db->readMultiple('BookingDriverDetails', array(
            array('ServiceProviderApplicationUserID','=',$users->userid),
            array('IsActive','=','1')
        ));
        if (!is_array($bookingIDs)) {
            $this->returnData($response,0,'No Trips Found','');
            return 202;
        }
        foreach($bookingIDs as $bookingID){
            $booking = $db->readSingle('Booking', array(
                array('ID','=',$bookingID->toOutputArray()['BookingID'])
            ));
            if($booking) {
                $booking = $booking->toOutputArray();
                if ($booking['Status'] == 'On Dispute' || $booking['Status'] == 'Request Accepted') {
                    $unread = $db->readSingle('BookingNotification', array(
                        array('BookingID','=',$booking['ID']),
                        array('RecipientUserID','=',$users->userid),
                        array('IsRead','=',0)
                    ));
                    $busCapacity = $db->readSingle('BusType', array(
                        array('ID', '=', $booking['BusTypeID'])
                    ));
                    $data['ID'] = $booking['ID'];
                    $data['datetime'] = ltrim(date('d/m/Y', $booking['PickUpDate']),0) .' '.ltrim(date('h:i:s', $booking['PickUpTime']),0);
                    $data['TripType'] = $booking['TripType'];
                    $data['BookingNumber'] = $booking['BookingNumber'];
                    $data['PickUpPoint'] = $booking['PickUpPoint'];
                    $data['Destination'] = $booking['Destination'];
                    $data['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
                    $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
                    $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                    if($booking['TripType'] == 'Shuttle') {
                        $data['DateTime']  = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                        $data['IntervalHour']= $this->IntervalHour($booking['IntervalHour']);
                    }elseif($booking['TripType'] == 'Return') {
                        $data['DateTime']  = $pickup .' & '.$return;
                        $data['IntervalHour']=='';
                    }else{
                        $data['DateTime']  = $pickup;
                        $data['IntervalHour']='';
                    }
                    $date=$booking['PickUpDate'];
                    $PickUpDateTime = mktime(date('h',$date),date('i',$date),date('s',$date),date('m',$date),date('d',$date),date('Y',$date));
                    $now =(new DateTime())->getTimestamp();
                    if($now > $PickUpDateTime){
                        $data['PastTime'] = true;
                    }else{
                        $data['PastTime'] = false;
                    }
                    $data['OfferedPrice'] = number_format($booking['OfferingPrice'],2);
                    $data['sortdatetime'] = ltrim(date('d M Y', $booking['PickUpDate']),0).ltrim(date('h:ia', $booking['PickUpTime']),0);
                    $data['Status'] = $booking['Status'];
                    if($unread){
                        $data['IsRead'] = false;
                        $undata=$data;
                        $datas['ViewMyTrips'][] = $undata;
                    }else{
                        $data['IsRead'] = true;
                        $rdata=$data;
                        $rdatas[] = $rdata;
                    }

                }
            }
        }
        if(!empty($datas['ViewMyTrips'])){
            $date_sort = array();
            foreach ($datas['ViewMyTrips'] as $key => $row)
            {
                $date_sort[$key] = strtotime($row['sortdatetime']);
                unset($datas['ViewMyTrips'][$key]['sortdatetime']);
            }
            array_multisort($date_sort, SORT_ASC, $datas['ViewMyTrips']);
        }
        if(!empty($rdatas)){
            $date_sort = array();
            foreach ($rdatas as $key => $row)
            {
                $date_sort[$key] = strtotime($row['sortdatetime']);
                unset($rdatas[$key]['sortdatetime']);
            }
            array_multisort($date_sort, SORT_ASC, $rdatas);
        }
        if(is_array($rdatas)) {
            foreach ($rdatas as $value) {
                $datas['ViewMyTrips'][] = $value;
            }
        }elseif(!empty($rdatas)){
            $datas['ViewMyTrips'][] = $rdatas;
        }
        $this->returnData($response,1,'Successful',$datas);
        return 200;
    }

    function getNumberServiceProviderViewMyTrips($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookingIDs=$db->readMultiple('BookingDriverDetails', array(
            array('ServiceProviderApplicationUserID','=',$users->userid),
            array('IsActive','=','1')
        ));
        if (!is_array($bookingIDs)) {
            $this->returnData($response,0,'No Trips Found','');
            return 202;
        }
        $bookingData=array();
        foreach($bookingIDs as $bookingID){
            $booking = $db->readSingle('Booking', array(
                array('ID','=',$bookingID->toOutputArray()['BookingID'])
            ));
            if($booking) {
                $booking = $booking->toOutputArray();
                if ($booking['Status'] == 'On Dispute' || $booking['Status'] == 'Request Accepted') {
                    $unread = $db->readSingle('BookingNotification', array(
                        array('BookingID','=',$booking['ID']),
                        array('RecipientUserID','=',$users->userid),
                        array('IsRead','=',0)
                    ));
                    if($unread){
                        $bookingData[]=$booking;
                    }
                }
            }
        }
        $data['Count']=count($bookingData);
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getServiceProviderViewMyTripDetail($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID']),
        ));

        // Handle empty cases
        if (!($bookings)) {
            $data['ViewMyTrips']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        $booking = $bookings->toOutputArray();
        if ($booking['Status'] == 'On Dispute' ||$booking['Status'] == 'Request Accepted') {
            $unread = $db->readSingle('BookingNotification', array(
                array('BookingID','=',$booking['ID']),
                array('RecipientUserID','=',$users->userid),
                array('IsRead','=',0)
            ),TRUE);
            if($unread){
                $unread->update(array(
                    'IsRead' => 1,
                    'ReadDateTime' => time()
                ));

                if (!$db->update($unread)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking"');
                    $this->returnData($response,0,'DB unable to create Booking','');
                    return 500;
                }
            }
            $busCapacity= $db->readSingle('BusType', array(
                array('ID','=',$booking['BusTypeID'])
            ));
            $applicationUser = $db->readSingle('ApplicationUser', array(
                array('ID','=',$booking['CustomerApplicationUserID'])
            ));
            if (!($applicationUser)) {
                $this->returnData($response,0,'User not found that booked a bus',$data);
                return 404;
            }
            $busdriver= $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$booking['ID']),
                array('IsActive','=','1')
            ));
            if($busdriver){
                $drivername=$busdriver->toOutputArray()['DriverName'];
                $busnumber=$busdriver->toOutputArray()['BusNumber'];
                $returndrivername=$busdriver->toOutputArray()['ReturnDriverName'];
                $returnbusnumber=$busdriver->toOutputArray()['ReturnBusNumber'];
            }else{
                $drivername='';
                $busnumber='';
                $returndrivername='';
                $returnbusnumber='';
            }
            $setting = $db->readSingle('Setting', array(
                array('SettingName', '=', 'TripCompletionInMinutes')
            ));
            if(!$setting){
                $this->returnData($response,0,'Setting not found TripCompletionInMinutes',$data);
                return 404;
            }
            $data['ViewMyTrips']['ID'] = $booking['ID'];
            $data['ViewMyTrips']['TripType'] = $booking['TripType'];
            $data['ViewMyTrips']['BookingNumber'] = $booking['BookingNumber'];
            $data['ViewMyTrips']['Name'] = $applicationUser->toOutputArray()['Name'];
            $data['ViewMyTrips']['Mobile'] = $applicationUser->toOutputArray()['Mobile'];
            $data['ViewMyTrips']['Photo'] = $applicationUser->toOutputArray()['Photo'];
            $data['ViewMyTrips']['PickUpPoint'] = $booking['PickUpPoint'];
            $data['ViewMyTrips']['Destination'] = $booking['Destination'];
            $data['ViewMyTrips']['BusDriver'] = $drivername;
            $data['ViewMyTrips']['BusNumber'] = $busnumber;
            $data['ViewMyTrips']['ReturnBusDriver'] = $returndrivername;
            $data['ViewMyTrips']['ReturnBusNumber'] = $returnbusnumber;
            $data['ViewMyTrips']['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
            $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
            $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
            if($booking['TripType'] == 'Shuttle') {
                $data['ViewMyTrips']['DateTime']  = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                $data['ViewMyTrips']['IntervalHour']= $this->IntervalHour($booking['IntervalHour']);
            }elseif($booking['TripType'] == 'Return') {
                $data['ViewMyTrips']['DateTime']  = $pickup .' & '.$return;
            }else{
                $data['ViewMyTrips']['DateTime']  = $pickup;
            }
            $data['ViewMyTrips']['OfferedPrice'] = number_format($booking['OfferingPrice'],2);
            $data['ViewMyTrips']['NoteToDriver'] = $booking['NoteToDriver'];
            $data['ViewMyTrips']['Status'] = $booking['Status'];
            if($booking['TripType'] == 'Return'){
                $date = $booking['ReturnDate'];
                $checktime=$booking['ReturnTime'];
            }else{
                $date = $booking['PickUpDate'];
                $checktime=$booking['PickUpTime'];
            }
            $checkDateTime = mktime(date('H',$checktime),date('i',$checktime),date('s',$checktime),date('m',$date),date('d',$date),date('Y',$date))+($setting->toOutputArray()['SettingValue']*60);
            $now =(new DateTime())->getTimestamp();
            if($now > $checkDateTime){
                $data['ViewMyTrips']['IsTripComplete']=true;
            }else{
                $data['ViewMyTrips']['IsTripComplete']=false;
            }

        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getServiceProviderHistory($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookingIDs=$db->readMultiple('BookingDriverDetails', array(
            array('ServiceProviderApplicationUserID','=',$users->userid),
            array('IsActive','=','1')
        ));
        if (!is_array($bookingIDs)) {
            $this->returnData($response,0,'No History on it','');
            return 202;
        }

        //$distinctBookings = array_unique($bookingIDs);
        $fillBookingIDs = array();
        foreach($bookingIDs as $bookingID){
            $currentBookingID = $bookingID->toOutputArray()['BookingID'];
//            $fillBookingIDs[] = $currentBookingID;
//            $countFilledBookingIDs = array_count_values($fillBookingIDs);
//            if(!empty($fillBookingIDs)){
//                if(($countFilledBookingIDs[$currentBookingID]>1)){
//                    continue;
//                }
//            }

            $booking = $db->readSingle('Booking', array(
                array('ID','=',$bookingID->toOutputArray()['BookingID'])
            ));
            if($booking){
                $booking = $booking->toOutputArray();
                if ($booking['Status'] == 'Trip Completed' ||$booking['Status'] == 'Cancelled By Customer' ||$booking['Status'] == 'Cancelled By Driver') {
                    $unread = $db->readSingle('BookingNotification', array(
                        array('BookingID','=',$booking['ID']),
                        array('RecipientUserID','=',$users->userid),
                        array('IsRead','=',0)
                    ));
                    $busCapacity= $db->readSingle('BusType', array(
                        array('ID','=',$booking['BusTypeID'])
                    ));
                    $data['ID'] = $booking['ID'];
                    $data['TripType'] = $booking['TripType'];
                    $data['datetime'] = ltrim(date('d M Y', $booking['PickUpDate']),0) .ltrim(date('h:ia', $booking['PickUpTime']), 0);
                    $data['BookingNumber'] = $booking['BookingNumber'];
                    $data['PickUpPoint'] = $booking['PickUpPoint'];
                    $data['Destination'] = $booking['Destination'];
                    $data['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
                    $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
                    $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                    if($booking['TripType'] == 'Shuttle') {
                        $data['DateTime']  = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                        $data['IntervalHour']= $this->IntervalHour($booking['IntervalHour']);
                        $data['TotalTime']=ltrim(date('h:ia',($booking['ReturnTime']-$booking['PickUpTime'])), 0);
                    }elseif($booking['TripType'] == 'Return') {
                        $data['DateTime']  = $pickup .' & '.$return;
                        $data['IntervalHour']='';
                        $data['TotalTime']='';
                    }else{
                        $data['DateTime']  = $pickup;
                        $data['IntervalHour']='';
                        $data['TotalTime']='';
                    }
                    $data['ReturnDateTime'] = ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                    $data['OfferedPrice'] = number_format($booking['OfferingPrice'],2);
                    $data['Status'] = $booking['Status'];
                    if($unread){
                        $data['IsRead'] = false;
                        $undata=$data;
                        $datas['ViewMyHistory'][] = $undata;
                    }else{
                        $data['IsRead'] = true;
                        $rdata=$data;
                        $rdatas[] = $rdata;
                    }
                }
            }
        }
        if(!empty($datas['ViewMyHistory'])){
            $date_sort = array();
            foreach ($datas['ViewMyHistory'] as $key => $row)
            {
                $date_sort[$key] = strtotime($row['datetime']);
                unset($datas['ViewMyHistory'][$key]['datetime']);
            }
            array_multisort($date_sort, SORT_DESC, $datas['ViewMyHistory']);
        }
        if(!empty($rdatas)){
            $date_sort = array();
            foreach ($rdatas as $key => $row)
            {
                $date_sort[$key] = strtotime($row['datetime']);
                unset($rdatas[$key]['datetime']);
            }
            array_multisort($date_sort, SORT_DESC, $rdatas);
        }
        if(is_array($rdatas)) {
            foreach ($rdatas as $value) {
                $datas['ViewMyHistory'][] = $value;
            }
        }elseif(!empty($rdatas)){
            $datas['ViewMyHistory'][] = $rdatas;
        }
        if (!$datas) {
            $this->returnData($response,0,'No History','');
            return 202;
        }
        $this->returnData($response,1,'Successful',$datas);
        return 200;
    }

    function getNumberServiceProviderHistory($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookingIDs=$db->readMultiple('BookingDriverDetails', array(
            array('ServiceProviderApplicationUserID','=',$users->userid)
        ));
        if (!is_array($bookingIDs)) {
            $this->returnData($response,0,'No History','');
            return 202;
        }
        $historyData = array();
        foreach($bookingIDs as $bookingID){
            $booking = $db->readSingle('Booking', array(
                array('ID','=',$bookingID->toOutputArray()['BookingID'])
            ));
            if($booking){
                $booking = $booking->toOutputArray();
                if ($booking['Status'] == 'Trip Completed' ||$booking['Status'] == 'Cancelled By Customer' ||$booking['Status'] == 'Cancelled By Driver') {
                    $unread = $db->readSingle('BookingNotification', array(
                        array('BookingID','=',$booking['ID']),
                        array('RecipientUserID','=',$users->userid),
                        array('IsRead','=',0)
                    ));
                    if($unread){
                        $historyData[]=$booking;
                    }
                }
            }
        }
        $data['Count']=count($historyData);
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function getServiceProviderHistoryDetail($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();

        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID']),
        ));

        // Handle empty cases
        if (!($bookings)) {
            $data['ViewMyHistory']=array();
            $this->returnData($response,0,'Empty data ',$data);
            return 202;
        }

        $booking = $bookings->toOutputArray();
        if ($booking['Status'] == 'Trip Completed' ||$booking['Status'] == 'Cancelled By Customer' ||$booking['Status'] == 'Cancelled By Driver') {
            $unread = $db->readSingle('BookingNotification', array(
                array('BookingID','=',$booking['ID']),
                array('RecipientUserID','=',$users->userid),
                array('IsRead','=',0)
            ),TRUE);
            if($unread){
                $unread->update(array(
                    'IsRead' => 1,
                    'ReadDateTime' => time()
                ));

                if (!$db->update($unread)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking"');
                    $this->returnData($response,0,'DB unable to create Booking','');
                    return 500;
                }
            }
            $busCapacity= $db->readSingle('BusType', array(
                array('ID','=',$booking['BusTypeID'])
            ));
            $applicationUser = $db->readSingle('ApplicationUser', array(
                array('ID','=',$booking['CustomerApplicationUserID'])
            ));
            if (!($applicationUser)) {
                $this->returnData($response,0,'User not found that booked a bus',$data);
                return 404;
            }
            if ($booking['Status'] == 'Trip Completed') {
                $displayStatus = ' ';
            } else if ($booking['Status'] == 'Cancelled By Customer') {
                $displayStatus = 'Cancelled by Customer';
            } else {
                $displayStatus = 'Cancelled by You';
            }
            $busdriver= $db->readSingle('BookingDriverDetails', array(
                array('BookingID','=',$booking['ID']),
                array('IsActive','=','1')
            ));
            if($busdriver){
                $drivername=$busdriver->toOutputArray()['DriverName'];
                $busnumber=$busdriver->toOutputArray()['BusNumber'];
                $returndrivername=$busdriver->toOutputArray()['ReturnDriverName'];
                $returnbusnumber=$busdriver->toOutputArray()['ReturnBusNumber'];
            }else{
                $drivername='';
                $busnumber='';
                $returndrivername='';
                $returnbusnumber='';
            }
            $data['ViewMyHistory']['ID'] = $booking['ID'];
            $data['ViewMyHistory']['TripType'] = $booking['TripType'];
            $data['ViewMyHistory']['BookingNumber'] = $booking['BookingNumber'];
            $data['ViewMyHistory']['Name'] = $applicationUser->toOutputArray()['Name'];
            $data['ViewMyHistory']['Mobile'] = $applicationUser->toOutputArray()['Mobile'];
            $data['ViewMyHistory']['Photo'] = $applicationUser->toOutputArray()['Photo'];
            $data['ViewMyHistory']['PickUpPoint'] = $booking['PickUpPoint'];
            $data['ViewMyHistory']['Destination'] = $booking['Destination'];
            $data['ViewMyHistory']['BusNumber'] = $busnumber;
            $data['ViewMyHistory']['BusDriver'] = $drivername;
            $data['ViewMyHistory']['ReturnBusNumber'] = $returnbusnumber;
            $data['ViewMyHistory']['ReturnBusDriver'] = $returndrivername;
            $data['ViewMyHistory']['BusCapacity'] = $busCapacity->toOutputArray()['BusType'];
            $pickup=ltrim(date('d M Y', $booking['PickUpDate']),0) . ' (' . date('D', $booking['PickUpDate']) . ') ' . ltrim(date('h:ia', $booking['PickUpTime']),0);
            $return=empty($booking['ReturnDate'])?'':ltrim(date('d M Y', $booking['ReturnDate']),0) . ' (' . date('D', $booking['ReturnDate']) . ') ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
            if($booking['TripType'] == 'Shuttle') {
                $data['ViewMyHistory']['DateTime']  = $pickup . ' to ' . ltrim(date('h:ia', $booking['ReturnTime']),0);
                $data['ViewMyHistory']['IntervalHour']= $this->IntervalHour($booking['IntervalHour']);
                $data['ViewMyHistory']['TotalTime']=ltrim(date('h:ia',($booking['ReturnTime']-$booking['PickUpTime'])), 0);
            }elseif($booking['TripType'] == 'Return') {
                $data['ViewMyHistory']['DateTime']  = $pickup .' & '.$return;
            }else{
                $data['ViewMyHistory']['DateTime']  = $pickup;
            }
            $data['ViewMyHistory']['OfferedPrice'] = number_format($booking['OfferingPrice'],0);
            $data['ViewMyHistory']['NoteToDriver'] = $booking['NoteToDriver'];
            $data['ViewMyHistory']['Status'] = $booking['Status'];
            $data['ViewMyHistory']['DisplayStatus'] = $displayStatus;
        }
        $this->returnData($response,1,'Successful',$data);
        return 200;
    }

    function confirmBooking($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a customer
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);

        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }
            $booking = $db->readSingle('Booking', array(
                array('ID','=',$_POST['BookingID'])
            ));
            if (!($booking)) {
                $this->returnData($response,0,'Booking not found','');
                return 404;
            }

            if($booking->getStatus() != 'Request Pending') {
                $response->addData("error_code", 1);
                $this->returnData($response,0,'This booking is no longer pending.','');
                return 400;
            }
            if($users->usertype == 'Private Service Provider'){
                $applicationUser = $db->readSingle('ApplicationUser', array(
                    array('ID','=',$users->userid)
                ));
                if (!($applicationUser)) {
                    $this->returnData($response,0,'User not found ','');
                    return 404;
                }
                $drierName=$applicationUser->getName();
                $busNumber=$applicationUser->getBusNumber();
                $returnbusNumber=$applicationUser->getBusNumber();
                $returndrierName=$applicationUser->getName();
            }else{
                $drierName=$_POST['DriverName'];
                $busNumber=$_POST['BusNumber'];
                $returndrierName=$_POST['ReturnDriverName'];
                $returnbusNumber=$_POST['ReturnBusNumber'];
            }
            //$PickUpDateTime = StrToTime($booking->getPickUpDate().$booking->getPickUpTime());
            $date=$booking->getPickUpDate();
            $picktime=$booking->getPickUpTime();
            $PickUpDate = date('Y-m-d',$booking->getPickUpDate());
            $PickUpTime = date('h:i:s',$booking->getPickUpTime());
            //print_r($PickUpDate); print_r($PickUpTime);die;
            $PickUpDateTime = mktime(date('H',$picktime),date('i',$picktime),date('s',$picktime),date('m',$date),date('d',$date),date('Y',$date));
            $now =(new DateTime())->getTimestamp();
            if($now > $PickUpDateTime){
                $this->returnData($response,0,'Booking Expired','');
                return 400;
            }

            // Update booking
            $updated = $booking->update(array(
                'Status' => 'Request Accepted',
                'LastUpdatedDateTime' => $time,
            ));
            if (!$db->update($booking)) {
                Log::error(__METHOD__.'() - DB unable to update '.$booking);
                $this->returnData($response,0,'DB unable to update Booking','');
                return 500;
            }
            if($booking->getTripType() == 'Return'){
                try {
                    $bookingDriverDetails = new BookingDriverDetails (array(
                        'BookingID' => $booking->getID(),
                        'ServiceProviderApplicationUserID' =>$users->userid,
                        'DriverName' => $drierName,
                        'BusNumber' => $busNumber,
                        'ReturnDriverName' => $returndrierName,
                        'ReturnBusNumber' => $returnbusNumber,
                        'IsActive' =>'1',
                        'LastUpdatedDateTime' => $time
                    ));
                }
                catch (Exception $e) {
                    $this->returnData($response,0,$e->getMessage(),'');
                    return 400;
                }
            }else{
                try {
                    $bookingDriverDetails = new BookingDriverDetails (array(
                        'BookingID' => $booking->getID(),
                        'ServiceProviderApplicationUserID' =>$users->userid,
                        'DriverName' => $drierName,
                        'BusNumber' => $busNumber,
                        'IsActive' =>'1',
                        'LastUpdatedDateTime' => $time
                    ));
                }
                catch (Exception $e) {
                    $this->returnData($response,0,$e->getMessage(),'');
                    return 400;
                }
            }

            if (!$db->create($bookingDriverDetails)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }
            $card = $db->readSingle('BookingPaymentMethod', array(
                array('BookingID','=',$booking->getID()),
                array('Status','=','Verified')
            ));
            if (!($card)) {
                $this->returnData($response,0,'Data not correct. Not found Data in BookingPaymentMethod','');
                return 404;
            }
            try {
                $bookingPaymentMethod = new BookingPaymentMethod (array(
                    'BookingID' => $booking->getID(),
                    'CardID' =>$card->getCardID(),
                    'Status' =>'Hold',
                    'CreatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingPaymentMethod)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to add Booking Payment Method','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $booking->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => 'Request Accepted',
                    'Remarks' => '',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'Booking Already Accepted','');
                return 500;
            }
            $_notification = $db->readSingle('ApplicationUserNotificationSetting', array(
                array('ApplicationUserID','=',$booking->getCustomerApplicationUserID())
            ));
            if ($_notification){
                $notification['UserNotificationSetting'] = array(
                    'SMS' => $_notification->toOutputArray()['SMS'],
                    'Email' => $_notification->toOutputArray()['Email']
                );
            }
            if(!$notification) {
                $this->returnData($response, 0, 'Notification Setting not set', '');
                return 500;
            }


            $busType =  $db->readSingle('BusType',array(
                array('ID','=',$booking->getBusTypeID())
            ));
            if(!$busType) {
                $this->returnData($response, 0, 'BusType not Found', '');
                return 500;
            }
            $bookingUser =  $db->readSingle('ApplicationUser',array(
                array('ID','=',$booking->getCustomerApplicationUserID())
            ));
            if(!$bookingUser) {
                $this->returnData($response, 0, 'Booking User not found', '');
                return 500;
            }
            try {
                $BookingNotification = new BookingNotification (array(
                    'BookingID' => $booking->getID(),
                    'NotificationType' =>'Request Accepted',
                    'NotificationMediumType' => 'App Notification',
                    'Notification' => 'Your booking request '.$booking->getBookingNumber().' has been accepted. Please login to the app to view your driver details.',
                    'NotificationSentDateTime' =>$time,
                    'RecipientUserID' =>$bookingUser->getID(),
                    'RecipientUserType' =>$bookingUser->getUserType(),
                    'IsRead' =>'0',
                    'ReadDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($BookingNotification)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                $this->returnData($response,0,'DB unable to create Booking Notification','');
                return 500;
            }
            if($notification['UserNotificationSetting']['Email']== 1) {
                try {
                    $BookingNotification = new BookingNotification (array(
                        'BookingID' => $booking->getID(),
                        'NotificationType' => 'Request Accepted',
                        'NotificationMediumType' => 'Email',
                        'Notification' => 'Congratulations! Your booking request ' . $booking->getBookingNumber() . ' for a '.$busType->getBusType().' Bus on '.date('m/d/y',$booking->getPickUpDate()).' at '.ltrim(date('h:ia',$booking->getPickUpTime()), 0).' has been accepted. Please login to the app to view your driver details.',
                        'NotificationSentDateTime' => $time,
                        'RecipientUserID' => $bookingUser->getID(),
                        'RecipientUserType' => $bookingUser->getUserType(),
                        'IsRead' => '0',
                        'ReadDateTime' => $time
                    ));
                } catch (Exception $e) {
                    $this->returnData($response, 0, $e->getMessage(), '');
                    return 400;
                }
                if (!$db->create($BookingNotification)) {
                    Log::error(__METHOD__ . '() - DB Unable to create Booking Notification"');
                    $this->returnData($response, 0, 'DB unable to create Booking Notification', '');
                    return 500;
                }
                $message='Congratulations! Your booking request ' . $booking->getBookingNumber() . ' for a '.$busType->getBusType().' Bus on '.date('m/d/y',$booking->getPickUpDate()).' at '.ltrim(date('h:ia',$booking->getPickUpTime()), 0).' has been accepted. Please login to the app to view your driver details.';
                $mailDetails = array(
                    'subject'=>'Booking Accepted',
                    'email'=>$bookingUser->getEmail(),
                    'message'=> $message,
                    'file'=>'registration-code'
                );
                $sendEmail=$this->sendEmail($mailDetails);
            }
            if($notification['UserNotificationSetting']['SMS']== 1){
                try {
                    $BookingNotification = new BookingNotification (array(
                        'BookingID' => $booking->getID(),
                        'NotificationType' => 'Request Accepted',
                        'NotificationMediumType' => 'SMS',
                        'Notification' => 'WantBus - Your booking request '. $booking->getBookingNumber().' has been accepted. Please login to the app to view your driver details.',
                        'NotificationSentDateTime' => $time,
                        'RecipientUserID' => $bookingUser->getID(),
                        'RecipientUserType' => $bookingUser->getUserType(),
                        'IsRead' => '0',
                        'ReadDateTime' => $time
                    ));
                } catch (Exception $e) {
                    $this->returnData($response, 0, $e->getMessage(), '');
                    return 400;
                }
                if (!$db->create($BookingNotification)) {
                    Log::error(__METHOD__ . '() - DB Unable to create Booking Notification"');
                    $this->returnData($response, 0, 'DB unable to create Booking Notification', '');
                    return 500;
                }
                $message="WantBus - Your booking request '. $booking->getBookingNumber().' has been accepted. Please login to the app to view your driver details.";
                if($bookingUser->getMobile()){
                    /* $sendSMS=$this->sendSMS($bookingUser->getMobile(),$message);
                     $data['sms']=$sendSMS;*/
                }
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Confirm Booking',
                'ModelID' => $booking->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        }
        //$data['BookaBus']=$booking->toOutputArray();
        $this->returnData($response,1,'Booking Successful','');
        return 200;
    }

    function getNotificationSetting($users) {
        $data = array();
        //Read Languages from DB
        $db = Database::getConnection();
        $usernotificationsettings = $db->readSingle('ApplicationUserNotificationSetting', array(
            array('ApplicationUserID','=',$users->userid)
        ));
        // Handle empty cases
        if ($usernotificationsettings){
            $data['UserNotificationSetting'] = array(
                'SMS' => $usernotificationsettings->toOutputArray()['SMS'],
                'Email' => $usernotificationsettings->toOutputArray()['Email']
            );
            return $data ;
        }else{
            return null;
        }
    }

    function sendSMS($mobile,$message){
        $sURL = "https://www.commzgate.net/gateway/SendMsg"; // The POST URL
        $sPD = "ID=74960002&Password=wantbus8888&Mobile=9779841763886&Type=A&Message=testingsms"; // The POST Data
        $aHTTP = array(
            'http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $sPD
                )
        );
        $context = stream_context_create($aHTTP);
        $contents = file_get_contents($sURL, false, $context);
        return true;
    }

    function sendEmail($data){
        $mail = new Mail();
        $mail->setSubject($data['subject']);
        $mail->setTo($data['email']);
        $mail->setFrom(CONTACT_EMAIL);
        $mail->setReplyTo(CONTACT_EMAIL);
        $mail->addHTMLMail(APPROOT.'/mails/'.$data['file'].'.html');
        $mail->addTextMail(APPROOT.'/mails/'.$data['file'].'.txt');
        $mail->setVariable(array (
            'title'=>$data['subject'],
            'message' => $data['message']
        ));
        // Send email
        if (!$mail->send()) {
            return false;
        }
        return true;
    }

    /***   *** *** -- Report Issue -- *** **** ***/
    function reportAnIssue($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }

        if (!isUnsignedInt($_POST['BookingID'])) {
            $response->addData('error', 'illegal value for BookingID: '.$_POST['BookingID']);
            return 400;
        }

        // Init return data
        $data = array();
        $time=getTimeInMs();
        //Read from DB
        $db = Database::getConnection();

        $bookings = $db->readSingle('Booking', array(
            array('ID','=',$_POST['BookingID'])
        ));

        if (!($bookings)) {
            $this->returnData($response,0,'Empty data ','');
            return 404;
        }

        /*if($bookings->getStatus() == 'On Dispute') {
            $this->returnData($response,0,'This booking is already in dispute.','');
            return 400;
        }
*/
        $bookingDriverDetail = $db->readSingle('BookingDriverDetails', array(
            array('BookingID','=',$_POST['BookingID']),
            array('IsActive','=','1')
        ));

        if (!($bookingDriverDetail)) {
            $this->returnData($response,0,'Active Booking Driver Detail not found','');
            return 404;
        }

        if($users->usertype == 'Customer') {
            if($users->userid != $bookings->getCustomerApplicationUserID()) {
                $this->returnData($response,0,'Attempt to report issue on unauthorized Booking','');
                return 401;
            }
        }

        if($users->usertype == 'Private Service Provider' || $users->usertype == 'Corporate Service Provider') {
            if($users->userid != $bookingDriverDetail->getServiceProviderApplicationUserID()) {
                $this->returnData($response,0,'Unauthorized Booking','');
                return 401;
            }
        }

        $issueReportedBy = $db->readSingle('ApplicationUser', array(
            array('ID','=',$users->userid)
        ));

        $busType = $db->readSingle('BusType', array(
            array('ID','=',$bookings->getBusTypeID())
        ));
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            $status='On Dispute';


            $notification = $this->getNotificationSetting($users);
            if(!$notification){
                $this->returnData($response,0,'No medium for notification is selected','');
                return 500;
            }

            $preDisputeBookingStatus = $bookings->getStatus();
            if($preDisputeBookingStatus != 'On Dispute') {
                $bookings->update(array(
                    'Status' => $status
                ));

                if (!$db->update($bookings)) {
                    Log::error(__METHOD__ . '() - DB Unable to upate Booking"');
                    $this->returnData($response, 0, 'DB unable to update Booking', '');
                    return 500;
                }
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $bookings->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => $status,
                    'Remarks' =>$_POST['Remarks'],
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }

            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }

            try {
                $bookingDispute = new BookingDispute (array(
                    'BookingID' => $bookings->getID(),
                    'RaisedBy' => $users->userid,
                    'Issue' =>$_POST['Remarks'],
                    'RaisedDateTime' => $time,
                    'DisputeTicket' => $this->calculateDisputeNumber(),
                    'BookingStatus' => $preDisputeBookingStatus
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }

            if (!$db->create($bookingDispute)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Dispute"');
                $this->returnData($response,0,'DB unable to create Booking Dispute','');
                return 500;
            }

            if($users->usertype == 'Private Service Provider' || $users->usertype == 'Corporate Service Provider') {
                $disputeraisedto=$db->readSingle('ApplicationUser', array(
                    array('ID','=',$bookings->getCustomerApplicationUserID())
                ));
                $notificationRaisedBy=$this->sendNotificationIssueRaisedBy($bookings,$bookingDispute,$busType->getBusType(),$issueReportedBy->getEmail(),$users->userid,$users->usertype);
                $notificationRaisedTo= $this->sendNotificationIssueRaisedTo($bookings,$busType->getBusType(),$disputeraisedto->getEmail(),$bookings->getCustomerApplicationUserID(),'Customer');
            }elseif($users->usertype == 'Customer'){
                $disputeraisedto=$db->readSingle('ApplicationUser', array(
                    array('ID','=',$bookingDriverDetail->getServiceProviderApplicationUserID())
                ));
                $notificationRaisedBy=$this->sendNotificationIssueRaisedBy($bookings,$bookingDispute,$busType->getBusType(),$issueReportedBy->getEmail(),$users->userid,$users->usertype);
                $notificationRaisedTo=$this->sendNotificationIssueRaisedTo($bookings,$busType->getBusType(),$disputeraisedto->getEmail(),
$bookingDriverDetail->getServiceProviderApplicationUserID(),($disputeraisedto->getUserType)?$disputeraisedto->getUserType:'');
            }

            if (!$db->create($notificationRaisedBy['notificationLogInAppNotification'])) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                $this->returnData($response,0,'DB unable to create Booking Notification','');
                return 500;
            }
            if($notification['UserNotificationSetting']['SMS'] == '1'){
                if (!$db->create($notificationRaisedBy['notificationLogsms'])) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }

            }
            if($notification['UserNotificationSetting']['Email'] == '1'){
                if (!$db->create($notificationRaisedBy['notificationLogemail'])) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }
                $sendEmail=$this->sendEmail($notificationRaisedTo['mailDetails']);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
            }

            if (!$db->create($notificationRaisedTo['notificationLogInAppNotification'])) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                $this->returnData($response,0,'DB unable to create Booking Notification','');
                return 500;
            }
            if (!$db->create($notificationRaisedTo['notificationLogemail'])) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                $this->returnData($response,0,'DB unable to create Booking Notification','');
                return 500;
            }
            $sendEmail=$this->sendEmail($notificationRaisedTo['mailDetails']);
            if(!$sendEmail){
                $this->returnData($response,0,'Unable to send Email','');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'On Dispute',
                'ModelID' => $bookings->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }
            }
        }

        $this->returnData($response,1,'Issue reported successfully.','');
        return 200;
    }

    function sendNotificationIssueRaisedBy($bookings,$bookingDispute,$busType,$issueReportedBy,$userID,$userType){

        $sms = 'WantBus - The booking status for '.$bookings->getBookingNumber().' is not under dispute. Your ticket number is '.$bookingDispute->getDisputeTicket().'.';

        $inAppNotification='The booking status for '.$bookings->getBookingNumber().' is now under dispute. Your ticket number is '. $bookingDispute->getDisputeTicket() .'.';

        $email = 'Hello there! This is with regards to the booking request '.$bookings->getBookingNumber().' for a '.$busType.' on '. gmdate("d/m/y", $bookings->getPickUpDate()) .' at '. gmdate("g:ia", $bookings->getPickUpTime()) .'. <br /> You had submitted the following dispute on '. gmdate("d/m/y", $bookingDispute->getRaisedDateTime()) . ' at '. gmdate("g:ia", $bookingDispute->getRaisedDateTime()) . ':"'. $bookingDispute->getIssue() .'"<br />A ticket number '. $bookingDispute->getDisputeTicket() .' has been generated and we will keep you informed of our investigations as soon as possible. Please quote this ticket number to us if you are calling to enquire about this issue.';
        $notificationLogInAppNotification = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'App Notification',
            'Notification' => $inAppNotification,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $notificationLogsms = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'SMS',
            'Notification' => $sms,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $notificationLogemail = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'Email',
            'Notification' => $email,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $mailDetails = array(
            'subject'=>'Issue Reported',
            'email'=>$issueReportedBy,
            'message'=>$email,
            'file'=>'notification'
        );
        $data=array(
            'notificationLogInAppNotification'=>$notificationLogInAppNotification,
            'notificationLogsms'=>$notificationLogsms,
            'notificationLogemail'=>$notificationLogemail,
            'mailDetails'=>$mailDetails
        );
        return $data;
}
    function sendNotificationIssueRaisedTo($bookings,$busType,$disputeraisedto,$userID,$userType){

        $smsAndAppNotification = 'WantBus - The booking status for '.$bookings->getBookingNumber().' is now under dispute. We are looking into the matter and will get back to you soon.';

        $email = 'Hello there! This is with regards to the booking request '.$bookings->getBookingNumber().' for a '.$busType.' Bus on '.gmdate("d/m/y", $bookings->getPickUpDate()).' at '.gmdate("g:ia", $bookings->getPickUpTime()).'. A dispute has been submitted with regards to this booking. We are now looking into the matter and will keep you updated as soon as we have arrived at a resolution.';

        $notificationLogInAppNotification = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'App Notification',
            'Notification' => $smsAndAppNotification,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $notificationLogsms = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'SMS',
            'Notification' => $smsAndAppNotification,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $notificationLogemail = new BookingNotification (array(
            'BookingID' => $bookings->getID(),
            'NotificationType' => 'On Dispute',
            'NotificationMediumType' => 'Email',
            'Notification' => $email,
            'NotificationSentDateTime'=>time(),
            'RecipientUserID'=>$userID,
            'RecipientUserType'=>$userType,
            'IsRead'=>0
        ));
        $mailDetails = array(
            'subject'=>'Issue Reported',
            'email'=>$disputeraisedto,
            'message'=>$email,
            'file'=>'notification'
        );
        $data=array(
            'notificationLogInAppNotification'=>$notificationLogInAppNotification,
            'notificationLogsms'=>$notificationLogsms,
            'notificationLogemail'=>$notificationLogemail,
            'mailDetails'=>$mailDetails
        );
        return $data;
}
    function serviceProviderCancelBooking($response) {
        if(json_decode(file_get_contents('php://input'),true)){
            $_POST = json_decode(file_get_contents('php://input'),true);
        }
        // Ensure action is legal - check that the user is indeed a staff
        if(!isset(getallheaders()['Authorization'])){
            $this->returnData($response,0,'No Token','');
            return 401;
        }
        $users = $this->checkTokenExpiry(getallheaders()['Authorization']);
        if(!$users){
            $this->returnData($response,0,'Token Expired','');
            return 401;
        }
        // Init return data
        $data = array();
        $time=getTimeInMs();
        //Read from DB
        $db = Database::getConnection();

        $data=$this->getCancellationPrice($db,$response,$_POST['BookingID']);
        $bookings=$data['bookings'];

        $bustype = $db->readSingle('BusType', array(
            array('ID','=',$bookings->getBusTypeID())
        ));
        // Handle empty cases
        if (!($bustype)) {
            $this->returnData($response,0,'Data not found','');
            return 404;
        }

        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__.'() - DB unable to begin transaction');
                $this->returnData($response,0,'DB unable to begin transaction.','');
                return 500;
            }

            if($bookings->getStatus() == 'Request Accepted') {
                $status='Cancelled By Driver';
            } else {
                $this->returnData($response,0,'Booking cannot be cancelled because its current status is '.$bookings->getStatus(),'');
                return 400;
            }
            $notification = $db->readSingle('ApplicationUserNotificationSetting', array(
                array('ApplicationUserID','=',$bookings->getCustomerApplicationUserID())
            ));

            $customer = $db->readSingle('ApplicationUser', array(
                array('ID', '=', $bookings->getCustomerApplicationUserID())
            ));

            if(!$notification){
                $this->returnData($response,0,'No medium for notification is selected','');
                return 500;
            }

            $sms = 'WantBus - Your booking request '.$bookings->getBookingNumber().' has been cancelled. Please login to the app to submit a new request.';

            $email = 'We are sorry! Your booking request {bookingNumber} for a {bustype} Bus on {date} at {time} has been cancelled. Please login to the app to submit a new request.

WantBus takes cancellations by drivers seriously and will perform our due dilligence in identifying irresponsible drivers and have them removed from the system regularly.

We deeply apologise for any inconveniences caused.';
            $email=strtr($email, array(
                '{bookingNumber}' => $bookings->getBookingNumber(),
                '{bustype}' => $bustype->getBusType(),
                '{date}' => date('d/m/Y',$bookings->getPickUpDate()),
                '{time}' =>  date('h:ia',$bookings->getPickUpTime())
            ));
            $notificationLogsms = new BookingNotification (array(
                'BookingID' => $bookings->getID(),
                'NotificationType' => 'Cancelled By Driver',
                'NotificationMediumType' => 'SMS',
                'Notification' => $sms,
                'NotificationSentDateTime'=>time(),
                'RecipientUserID'=>$bookings->getCustomerApplicationUserID(),
                'RecipientUserType'=>$customer->getUserType(),
                'IsRead'=>0
            ));
            $notificationLogemail = new BookingNotification (array(
                'BookingID' => $bookings->getID(),
                'NotificationType' => 'Cancelled By Driver',
                'NotificationMediumType' => 'Email',
                'Notification' => $email,
                'NotificationSentDateTime'=>time(),
                'RecipientUserID'=>$bookings->getCustomerApplicationUserID(),
                'RecipientUserType'=>$customer->getUserType() ,
                'IsRead'=>0
            ));
            $mailDetails = array(
                'subject'=>'Booking Cancelled By Service Provider',
                'email'=>$customer->getEmail(),
                'message'=>$email,
                'file'=>'notification'
            );
            //Added by Aayush - 16 Nov 2016.
            $sendEmail=$this->sendEmail($mailDetails);
            if(!$sendEmail){
                $this->returnData($response,0,'Unable to send Email','');
                return 500;
            }
            //Commented by Aayush - 16 Nov 2016.
            //Service Provider doesnot have any notification settings.
            if($notification['UserNotificationSetting']['SMS'] == '1'){
                if (!$db->create($notificationLogsms)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }

            }

            if($notification['UserNotificationSetting']['Email'] == '1'){
                if (!$db->create($notificationLogemail)) {
                    Log::error(__METHOD__.'() - DB Unable to create Booking Notification"');
                    $this->returnData($response,0,'DB unable to create Booking Notification','');
                    return 500;
                }
                $sendEmail=$this->sendEmail($mailDetails);
                if(!$sendEmail){
                    $this->returnData($response,0,'Unable to send Email','');
                    return 500;
                }
            }

            $bookings->update(array(
                'Status' => $status
            ));

            if (!$db->update($bookings)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking"');
                $this->returnData($response,0,'DB unable to create Booking','');
                return 500;
            }
            try {
                $bookingLog = new BookingLog (array(
                    'BookingID' => $bookings->getID(),
                    'LastUpdatedByUserType' =>$users->usertype,
                    'LastUpdatedByUserID' => $users->userid,
                    'Status' => $status,
                    'Remarks' =>'',
                    'LastUpdatedDateTime' => $time
                ));
            }
            catch (Exception $e) {
                $this->returnData($response,0,$e->getMessage(),'');
                return 400;
            }
            if (!$db->create($bookingLog)) {
                Log::error(__METHOD__.'() - DB Unable to create Booking Log"');
                $this->returnData($response,0,'DB unable to create Booking Log','');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'UserID' => $users->userid,
                'UserType' => $users->usertype,
                'Action' => 'Cancel',
                'ModelID' => $bookings->getID(),
                'ModelType' => 'Booking',
                'CreatedDateTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__.'() - DB Unable to create activity create Booking"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__.'() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
                    $this->returnData($response,0,'DB unable to commit transaction','');
                    return 500;
                }

                $time = getTimeInMs();
            }
        }

        $this->returnData($response,1,'Successful cancelled Booking','');
        return 200;
    }

    function cardType($number)
    {
        $number=preg_replace('/[^\d]/','',$number);
        if (preg_match('/^3[47][0-9]{13}$/',$number))
        {
            return 'American Express';
        }
        elseif (preg_match('/^6(?:011|5[0-9][0-9])[0-9]{12}$/',$number))
        {
            return 'Discover';
        }
        elseif (preg_match('/^5[1-5][0-9]{14}$/',$number))
        {
            return 'MasterCard';
        }
        elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/',$number))
        {
            return 'Visa';
        }
        else
        {
            return 'Unknown';
        }
    }

    function mc_encrypt($encrypt, $key){
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
        return $encoded;
    }
// Decrypt Function
    function mc_decrypt($decrypt, $key){
        $decrypt = explode('|', $decrypt.'|');
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if($calcmac!==$mac){ return false; }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    function bookingRequestExpiring()
    {
        //Read BusType from DB
        $db = Database::getConnection();
        $requestExpiryNotificationSettingValue = $db->readSingle('Settings', array(
            array('SettingName','=','RequestExpiryNotificationBeforeTripInMinutes')
        ));
        // Handle empty cases
        if (!$requestExpiryNotificationSettingValue) {
            $this->returnData($response,0,'No request expiry notification before trip setting found.','');
            return 202;
        }
        $data['Date'] = ltrim($this->dateForMenu($booking['PickUpDate']),0);
        $data['Time'] = ltrim(date('h:i', $booking['PickUpTime']),0);
        $PickUpDateTime = strtotime($_POST['PickUpDate'].' '.$_POST['PickUpTime']);

        $bookings = $db->readMultiple('Booking', array(
            //array('PickUpTime', '>=', ((new DateTime())->getTimestamp() + ($requestExpiryNotificationSettingValue * 60 * 1000)))
        ));

        // Handle empty cases
        if (is_array($bookings)) {
            $bookingData = array();
            foreach ($bookings as $key => $bookings) {
                $booking = $bookings->toOutputArray();
                $PickUpDateTime = strtotime($booking['PickUpDate'].' '.$booking['PickUpTime']) - ($requestExpiryNotificationSettingValue * 60 * 1000);
                $currentStamp = (new DateTime())->getTimestamp();
                $PickUpDateTimeStamp = $PickUpDateTime->getTimestamp();
                if($currentStamp >= $PickUpDateTimeStamp){
                    if ($booking['Status'] == 'Request Pending') {
                        $alredySent = $db->readMultiple('BookingNotification', array(
                            array('BookingID', '=', $booking['ID']),
                            array('NotificationType','=','BookingExpiring')
                        ));
                        if(!$alredySent) {
                            $email = $db->readSingle('ApplicationUser', array(
                                array('ID', '=', $booking['CustomerApplicationUserID'])
                            ));
                            if ($email) {
                                $bustype = $db->readSingle('BusType', array(
                                    array('ID', '=', $booking['BusTypeID'])
                                ));
                                if ($bustype) {
                                    $emailmsg = 'Your booking request {bookingNumber} for a {bustype} Bus on {date} at {time} is  expiring in {hour} hours You may want to login to the app to cancel this request and submit a new one. Hint: A higher offering price may help get some attention.';
                                    $message ='WantBus - Your booking request '.$bookings->getBookingNumber().' is expiring in '.date('h', ($bookings->getPickUpDate()-(new DateTime())->getTimestamp())).' hours. You may want to login to the app to cancel this request and submit a new one.';
                                    $emailmsg = strtr($emailmsg, array(
                                        '{bookingNumber}' => $bookings->getBookingNumber(),
                                        '{bustype}' => $bustype->getBusType(),
                                        '{date}' => date('d/m/Y', $bookings->getPickUpDate()),
                                        '{time}' => date('h:ia', $bookings->getPickUpTime()),
                                        '{hour}' => date('h', ($bookings->getPickUpDate()-(new DateTime())->getTimestamp()))
                                    ));
                                    $notificationLogemail = new BookingNotification (array(
                                        'BookingID' => $bookings->getID(),
                                        'NotificationType' => 'BookingExpiring',
                                        'NotificationMediumType' => 'Email',
                                        'Notification' => $emailmsg,
                                        'NotificationSentDateTime' => time(),
                                        'RecipientUserID' => $booking['CustomerApplicationUserID'],
                                        'RecipientUserType' => $email->getUserType(),
                                        'IsRead' => 1
                                    ));
                                    $mailDetails = array(
                                        'subject' => 'Booking Expiring',
                                        'email' => $email->getEmail(),
                                        'message' => $emailmsg,
                                        'file' => 'notification'
                                    );
                                    $db->create($notificationLogemail);
                                    $sendEmail = $this->sendEmail($mailDetails);
                                    if($email->getMobile()){
                                        /* $sendSMS=$this->sendSMS($email->getMobile(),$message);
                                         $data['sms']=$sendSMS;*/
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        return true;
    }

    function bookingRequestExpired($response)
    {
        //Read BusType from DB
        $db = Database::getConnection();
        $bookings = $db->readMultiple('Booking', array(
            array('PickUpDate', '<', (new DateTime())->getTimestamp()),
            array('Status', '=', 'Request Pending')
        ));

        // Handle empty cases
        if (is_array($bookings)) {

            $bookingData = array();
            foreach ($bookings as $key => $bookings) {
                $booking = $bookings->toOutputArray();
                if ($booking['Status'] == 'Request Pending') {
                    $alredySent = $db->readMultiple('BookingNotification', array(
                        array('BookingID', '=', $booking['ID']),
                        array('NotificationType','=','Request Expired')
                    ));
                    if(!$alredySent) {
                        $email = $db->readSingle('ApplicationUser', array(
                            array('ID', '=', $booking['CustomerApplicationUserID'])
                        ));
                        if ($email) {
                            $bustype = $db->readSingle('BusType', array(
                                array('ID', '=', $booking['BusTypeID'])
                            ));
                            if ($bustype) {
                                $bookingStatus=$bookings = $db->readSingle('Booking', array(
                                    array('ID', '=', $booking['ID'])
                                ),TRUE);
                                $bookingStatus->update(array(
                                    'Status' => 'Request Expired'
                                ));

                                if ($db->update($bookingStatus)) {
                                    $bookingLog = new BookingLog (array(
                                        'BookingID' => $booking['ID'],
                                        'LastUpdatedByUserType' =>'',
                                        'LastUpdatedByUserID' => '',
                                        'Status' => 'Request Expired',
                                        'Remarks' =>'',
                                        'LastUpdatedDateTime' => time()
                                    ));
                                    if ($db->create($bookingLog)) {
                                        $emailmsg = 'We are sorry! Your booking request {bookingNumber} for a {bustype} Bus on {date} at {time} has not gotten a response. Please login to the app to submit a new request.';
                                        $message ='WantBus - Your booking request '.$bookings->getBookingNumber().' has expired. Please login to the app to submit a new request.';
                                        $emailmsg = strtr($emailmsg, array(
                                            '{bookingNumber}' => $bookings->getBookingNumber(),
                                            '{bustype}' => $bustype->getBusType(),
                                            '{date}' => date('d/m/Y', $bookings->getPickUpDate()),
                                            '{time}' => date('h:ia', $bookings->getPickUpTime())
                                        ));
                                        $notificationLogemail = new BookingNotification (array(
                                            'BookingID' => $bookings->getID(),
                                            'NotificationType' => 'RequestExpired',
                                            'NotificationMediumType' => 'Email',
                                            'Notification' => $emailmsg,
                                            'NotificationSentDateTime' => time(),
                                            'RecipientUserID' => $booking['CustomerApplicationUserID'],
                                            'RecipientUserType' => $email->getUserType(),
                                            'IsRead' => 1
                                        ));
                                        $mailDetails = array(
                                            'subject' => 'Request Expired',
                                            'email' => $email->getEmail(),
                                            'message' => $emailmsg,
                                            'file' => 'notification'
                                        );
                                        $db->create($notificationLogemail);
                                        $sendEmail = $this->sendEmail($mailDetails);
                                        if($email->getMobile()){
                                            /* $sendSMS=$this->sendSMS($email->getMobile(),$message);
                                             $data['sms']=$sendSMS;*/
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    function bookABusValidateInMinutes($response){
        $db = Database::getConnection();
        $setting = $db->readSingle('Setting', array(
            array('SettingName', '=', 'BookABusValidateInMinutes')
        ));

        $this->returnData($response,1,'', $setting->toOutputArray()['SettingValue']);
        return 500;
    }

    function IntervalHour($time){
        if($time) {
            $hour = floor($time / 60);
            $minute = $time % 60;
            $returnTime = '';
            if ($hour != '0') {
                $returnTime .= $hour . 'hour ';
            }
            if ($minute != '0') {
                $returnTime .= $minute . 'minutes';
            }
            return $returnTime;
        }else{
            return '';
        }
    }

    public function verifyCard($number){
        return true;
    }

} //class CustomerAPI



