<?php
/***
 * Not exposed to public - called only internally within the application
 * - All methods should be static
 * - Typically common functions called by multiple APIs, and within the context of a DB transaction.
 ***/
class InternalAPI {
    
} // class InternalAPI

