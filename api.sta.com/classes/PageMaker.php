<?php

/* * *
 * Creates pages for each website.
 * - register HTML pages and their associated CSS and JS files here.
 * - each website should use its own function, e.g.
 *   getDesktopPage() for desktop site.
 * * */

class PageMaker {

    static function getDesktopPage($name) {
        // Set the layout of the pages
        $page = new Page($name);
        if($name == 'map') {
            $page->setLayout(array(
                'content' => WEBROOT . '/html/' . $name . '.php'
            ));
        }else { 
            $page->setLayout(array(
                'head' => WEBROOT . '/html/templates/HTMLhead.php',
                'header' => WEBROOT . '/html/templates/header.php',
                'content' => WEBROOT . '/html/' . $name . '.php',
                'footer' => WEBROOT . '/html/templates/footer.php',
            ));
        }
        
        // Set page data
        switch ($name) {
            case '401':
                $page->setTitle('401 Unauthorized');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case '404':
                $page->setTitle('404 Page Not Found');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case '500':
                $page->setTitle('500 Internal Server Error');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case 'event':
                $page->setTitle('Events | Singapore Timber Association');
                $page->setMenuTitle('Event');
                $page->setPageTitle('Events');
                $page->setCss(array('event.css','themes/purple-theme.css'));
                $page->setJs(array('event.js'));
                break;
            case 'services':
                $page->setTitle('Services | Singapore Timber Association');
                $page->setMenuTitle('Services');
                $page->setPageTitle('Services');
                $page->setCss(array('services.css','themes/dark-yellow-theme.css'));
                $page->setJs(array('services.js'));
                break;
            case 'library':
                $page->setTitle('Library | Singapore Timber Association');
                $page->setMenuTitle('Library');
                $page->setPageTitle('Library');
                $page->setCss(array('library.css','themes/light-blue-theme.css'));
                $page->setJs(array('library.js'));
                break;
            case 'biz-match':
                $page->setTitle('Biz-match | Singapore Timber Association');
                $page->setMenuTitle('Biz-match');
                $page->setPageTitle('Biz-match');
                $page->setCss(array('biz-match.css','themes/orange-theme.css'));
                $page->setJs(array('bizmatch.js'));
                break;
            case 'members':
                $page->setTitle('Members | Singapore Timber Association');
                $page->setMenuTitle('Members');
                $page->setPageTitle('Members');
                $page->setCss(array('members.css','themes/green-theme.css'));
                $page->setJs(array('members.js'));
                break;
            case 'about':
                $page->setTitle('About Us | Singapore Timber Association');
                $page->setMenuTitle('About');
                $page->setPageTitle('About Us');
                $page->setCss(array('about.css','themes/pale-yellow-theme.css'));
                $page->setJs(array('about.js'));
                break;
            case 'contact':
                $page->setTitle('Contact Us | Singapore Timber Association');
                $page->setMenuTitle('Contact');
                $page->setPageTitle('Contact Us');
                $page->setCss(array('contact.css','themes/teal-theme.css'));
                $page->setJs(array('contact.js'));
                break;
            case 'privacy-policy':
                $page->setTitle('Privacy Policy | Singapore Timber Association');
                $page->setMenuTitle('privacy');
                $page->setPageTitle('Privacy Policy');
                $page->setCss(array('privacy.css','themes/teal-theme.css'));
                $page->setJs(array('privacy.js'));
                break;
            case 'map':
                $page->setTitle('Map | Singapore Timber Association');
                $page->setMenuTitle('Map');
                $page->setPageTitle('Map');
//                $page->setCss(array('map.css','themes/teal-theme.css'));
                $page->setJs(array('map.js'));
                break;
            default:
                throw new Exception('Error in PageMaker::getDesktopPage(): page requested is not defined: ' . $name);
                break;
        } //switch
        
        return $page;
    }

// getDesktopPage()

    static function getStaffPage($name) {

        $page = new Page($name);
        $page->setLayout(array(
            'head' => WEBROOT . '/html/templates/staff-HTMLhead.php',
            'header' => WEBROOT . '/html/templates/staff-header.php',
            'content' => WEBROOT . '/html/' . $name . '.php',
            'footer' => WEBROOT . '/html/templates/staff-footer.php',
        ));
        $page->setAccessLevel(GENERAL_STAFF_ACCESS_LEVEL);

        switch ($name) {
            case 'staff-login':
                $page->setTitle('Login');
                $page->setCss(array('staff-login.css'));
                $page->setJs(array('staff-login.js'));
                $page->setAccessLevel(DEFAULT_ACCESS_LEVEL);
                break;
            default:
                throw new Exception('Error in PageMaker::getStaffPage(): page requested is not defined: ' . $name);
                break;
        } //switch

        return $page;
    }

//getStaffPage()
}

// class PageMaker
