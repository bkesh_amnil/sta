<?php
/**************
 * - Includes files used by the server scripts (constants & helpers)
 * - Sets up autoloader
 **************/
// Don't display any errors
//ini_set('display_errors', 'Off');
error_reporting(E_ALL ^ E_NOTICE); 	// DEBUG: display all errors except notices
ini_set('display_errors', 'On'); 	// DEBUG: display all errors except notices

// Set timezone and charset
date_default_timezone_set('Asia/Singapore');
mb_internal_encoding("UTF-8");

// Defines constants used throughout (ought to be included first)
require dirname(__FILE__) . '/config.php'; // dirname(__FILE___) necessary for cases where webserver doesn't serve relative paths from this current directory

// Include helper functions (be aware of performance hit - each include/require can be a disk IO operation)
require APPROOT . '/helpers/core.php';
require APPROOT . '/helpers/strings.php';
require APPROOT . '/helpers/files.php';

// Include classes that will definitely be used by server
require APPROOT . '/classes/Session.php';
require APPROOT . '/classes/output/HTTPResponse.php';

// Set CORS domains
//HTTPResponse::addCORSAllowedOrigin('http://staging.www.estebartin.com'); // staging
HTTPResponse::addCORSAllowedOrigin('http://localhost'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://localhost:8080'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://localhost:1111'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://192.168.88.184:1111'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://192.168.88.135'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://192.168.88.153'); //for testing
HTTPResponse::addCORSAllowedOrigin('http://192.168.88.155:8080'); //for testing
//HTTPResponse::addCORSAllowedOrigin('http://amnils.dlinkddns.com:8080'); //for staging
HTTPResponse::addCORSAllowedOrigin('*'); //for live


// Autoloads classes: expects one file per class
function class_autoload($class) {
	//Search base directory
	$filename = APPROOT . '/classes/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	//Search apis directory
	$filename = APPROOT . '/classes/apis/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	//Search models directory
	$filename = APPROOT . '/classes/models/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	//Search utilities directory
	$filename = APPROOT . '/classes/utilities/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	//Search vendor directory
	$filename = APPROOT . '/classes/vendor/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	//Search output directory
	$filename = APPROOT . '/classes/output/' . $class . '.php';
	if (is_readable($filename)) {
		require $filename;
		return;
	}

	// //Nimachi JOSE
	// $filename = APPROOT .'/vendor/autoload.php';
	// if (is_readable($filename)) {
	// 	require $filename;
	// 	return;
	// }


	// //Nimachi JOSE
	// $filename = APPROOT .'/vendor/autoload.php';
	// if (is_readable($filename)) {
	// 	require $filename;
	// 	return;
	// }


	throw new Exception('Error while autoloading: "' . $class . '" is not found / readable');
} //autoload()
spl_autoload_register('class_autoload');
