<?php
/**************************************************************************
 * Serves the api function requested.
 * @input: $_GET['api', 'action']
 **************************************************************************/
try {
	//Log::debug($_SERVER['REQUEST_METHOD'] . ' ' . $_GET['api'] . ' ' . $_GET['action'] . ' [' . $_SERVER['HTTP_ORIGIN'] . ']');
	// Handle CORS Pre-flight: simply return immediately
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		$response = new HTTPResponse();
		$response->deliver();
		exit(0);
	}
	
	// Argument checks
	if (!isset($_GET['api'])) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Missing argument(s).');
		$response->deliver();
		exit(0);
	}
	if (!isset($_GET['action'])) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Missing argument: action');
		$response->deliver();
		exit(0);
	}

	// Check valid API class
	$api_class = formatPascalCase($_GET['api']) . 'API';
	if (!is_readable(APPROOT . '/classes/apis/' . $api_class . '.php')) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'No such API: ' . $_GET['api']);
		$response->deliver();
		exit(0);
	}
	$api = new $api_class();
	unset($_GET['api']);

	// Check that action is a callable function
	$fn = formatCamelCase($_GET['action']);
	if (!is_callable(array($api, $fn))) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Requested action "' . $fn . '" is not callable in ' . $api_class);
		$response->deliver();
		exit(0);
	}
	unset($_GET['action']);

	Session::start();
	
	// Call fn and return response. Result will be stored in $response->data
	$response = new HTTPResponse();
	$response_code = $api->$fn($response);
	$response->setResponseCode($response_code);
	$response->deliver();
	exit(0);
} //try
catch (Exception $e) {
	//Log the error
	Log::error('serve-api.php error - ' . $e->getMessage());

	//Return error and terminate
	$response = new HTTPResponse(500);
	$response->setData(array('error' => $e->getMessage()));
	$response->deliver();
	exit(1);
} //catch
