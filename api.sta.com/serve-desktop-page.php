<?php
/**************************************************************************
 * Serves the page requested, or returned as json/xml with its parts
 * @input: $_GET['page']
 **************************************************************************/
try {
	// Since we definitely need these classes, include now rather than let it autoload
	require APPROOT . '/classes/PageMaker.php';
	require APPROOT . '/classes/output/Page.php';

	// Default page
	if (!isset($_GET['page'])) {
		$_GET['page'] = 'event';
	}
	// Re-direct to mobile domain if necessary
	if (defined(MOBILE_DOMAIN)) {
		handleMobile();
	}

	// Check if page exists 
	if (!is_readable(WEBROOT . '/html/' . $_GET['page'] . '.php')) {
            
       
		$page = PageMaker::getDesktopPage('404');
		$page->setResponseCode(404);
		$page->deliver();
		exit(1);
	} //Page not found
	
	// Get the page
	if (startsWith($_GET['page'], 'staff-')) {
		$page = PageMaker::getStaffPage($_GET['page']);
	}
	else {
		$page = PageMaker::getDesktopPage($_GET['page']);
	}
	unset($_GET['page']);

	// Check access level
	Session::start();
//	if ($_SESSION['user']->getAccessLevel() < $page->getAccessLevel()) {
//		$page = PageMaker::getStaffPage('staff-login');
//		$page->setResponseCode(401);
//		$page->deliver();
//		exit(1);
//	} //Access Denied
	
	// Set CSRF Token for desktop pages
	if (!isset($_SESSION['CSRF_TOKEN'])){
		$_SESSION['CSRF_TOKEN'] = Encryptor::generateRandomAlphanumericCode(CSRF_TOKEN_LENGTH);
	}
	
	// Serve the page
	$page->load();
	$page->deliver();
	exit(0);
} //try
catch (Exception $e) {
	//Log the error
	Log::error('serve-desktop-page.php caught exception - ' . $e->getMessage());

	//Return error and terminate
	$page = PageMaker::getDesktopPage('500');
	$page->setData(array('error' => $e->getMessage()));
	$page->deliver();
	exit(1);
} //catch

/***
 * Detects whether mobile view is requested.
 * If not specified, attempts to detect and forward
 ***/
function handleMobile() {
	// Redirect to mobile page if requested
	if (isset($_GET['view'])) {
		if ($_GET['view'] == 'mobile') {
			header('HTTP/1.1 307 Temporary Redirect');
			header('Location: ' . PROTOCOL . MOBILE_DOMAIN . '/?' . $_SERVER['QUERY_STRING']);
			exit(0);
		}

		if ($_GET['view'] == 'desktop') {
			return;
		}

		Log::warning('serve-desktop-page.php handleMobile() - unknown view type ' . $_GET['view']);
	} // view is requested
	// Detect whether user is on mobile (exclude tablets)
	$detect = new Mobile_Detect();
	if ($detect->isMobile() && !$detect->isTablet()) {
		header('HTTP/1.1 307 Temporary Redirect');
		header('Location: ' . PROTOCOL . MOBILE_DOMAIN . '/?' . $_SERVER['QUERY_STRING']);
		exit(0);
	}
} //handleMobile()
