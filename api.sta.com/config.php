<?php

/* * **************************** */
/*  Set up pathing constants   */
/* * **************************** */
define('PROJECT_NAME', 'Singapore Timber Association');

define('PROJECT_DOMAIN', 'sta.front.dev'); //localhost
//define('PROJECT_DOMAIN', 'staging.www.singaporetimber.com');//staging

define('SERVER_PATH', '');

// Derive paths related to web server
define('PROTOCOL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) ? 'https://' : 'http://'));
define('DOMAIN', $_SERVER['SERVER_NAME'] . SERVER_PATH);
define('BASE_URL', PROTOCOL . DOMAIN);
define('WEBROOT', $_SERVER['DOCUMENT_ROOT'] . SERVER_PATH);
define('APPROOT', __DIR__);

// Mobile domain accessing this server (defining it will cause re-direction for phone devices)
//define ('MOBILE_DOMAIN', 'm.estebartin.com');

/* * **************************** */
/*  Set up Database constants  */
/* * **************************** */


// local - remove the 2 preceding slashes to comment out this whole section
///*
define('WWW_ROOT', dirname(APPROOT) . '/www.sta.com');
define('API_URL', PROTOCOL.'sta.api.dev');
define('WWW_URL', PROTOCOL.'sta.front.dev');
define('STAFF_URL', PROTOCOL.'sta.staff.dev');
define('DB_HOST', 'localhost'); // Use 127.0.0.1 instead of localhost (seems to cause slowness)
define('DB_NAME', 'sta');
define('DB_USER', 'root');
define('DB_PASS', '');
//*/
// deserver - remove the 2 preceding slashes to comment out this whole section
/*

  define('DB_HOST', '192.168.88.7'); // Use 127.0.0.1 instead of localhost (seems to cause slowness)
  define('DB_NAME', 'db_sta');
  define('DB_USER', 'root');
  define('DB_PASS', 'Nirvana3');
  // */

//#staging
/*
  define('WWW_ROOT', dirname(APPROOT) . '/staging.www.singaporetimber.com');
  define('API_URL', PROTOCOL.'staging.api.singaporetimber.com');
  define('WWW_URL', PROTOCOL.'staging.www.singaporetimber.com');
  define('STAFF_URL', PROTOCOL.'staging.staff.singaporetimber.com');
  define('DB_HOST', 'localhost');
  define('DB_NAME', 'db_sta');
  define('DB_USER', 'server_rojeena');
  define('DB_PASS', 'rojeena@amnil');
  // */




/* * **************************** */
/*      Names and Emails       */
/* * **************************** */
define('SESSION_DURATION', 3600);
// define('SESSION_NAME', 'ESTEBARTIN');
define('SESSION_NAME', 'Singapore Timber Association');
define('SITE_NAME', 'Singapore Timber Association');
define('WEBMASTER_FIRST_NAME', 'Singapore Timber Association');
define('WEBMASTER_LAST_NAME', '');
define('WEBMASTER_EMAIL', 'admin@sta.com');
define('NO_REPLY_EMAIL', 'noreply@' . PROJECT_DOMAIN);
//define('CONTACT_EMAIL', 'bikesh@amniltech.com');
define('CONTACT_EMAIL', 'bikesh@amniltech.com,nishit@amniltech.com');
//define('CONTACT_EMAIL', 'bikesh@amniltech.com,nishit@amniltech.com,isen.majennt@techplusart.com');
//define('CONTACT_EMAIL', 'nishit@amniltech.com,isen.majennt@techplusart.com');


// Facebook
define('FACEBOOK_APP_ID', 'to be filled');
define('FACEBOOK_APP_SECRET', 'to be filled');

/* * **************************** */
/*    Accounts and Passwords   */
/* * **************************** */
define('MIN_NAME_LENGTH', 3);
define('EMAIL_VERIFICATION_CODE_LENGTH', 6);
define('REGISTRATION_CODE_LENGTH', 6);
//define('EMAIL_VERIFICATION_EXPIRY', 300000); // 5 mins == 300,000 milliseconds
define('EMAIL_VERIFICATION_EXPIRY', 432000000); // 5 days == 432000000 milliseconds
define('CSRF_TOKEN_LENGTH', 32);

define('TOKEN_KEY', '5c+9~{7bdd1aD2SCqxt2');
define('TOKEN_KEY_EXPIRY', 432000000); // 5 days == 432000000 milliseconds

define('MIN_PASSWORD_LENGTH', 6);
define('MIN_CARD_LENGTH', 16);
define('MAX_CCV_LENGTH', 4);
define('MIN_CCV_LENGTH', 3);

define('BOOKING_NUMBER_LENGTH', 10);

define('PERSISTENT_LOGIN_COOKIE_LENGTH', 32);
define('PERSISTENT_LOGIN_PERIOD', 5184000000); // 60 days == 5184,000,000 milliseconds


/* * **************************** */
/*     User Access Levels      */
/* * **************************** */
define('DEFAULT_ACCESS_LEVEL', 0);
define('CUSTOMER_ACCESS_LEVEL', 1);
define('GENERAL_STAFF_ACCESS_LEVEL', 20);
define('ADMIN_ACCESS_LEVEL', 90);
define('SUPER_ADMIN_ACCESS_LEVEL', 99);
