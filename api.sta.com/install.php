<?php
/**************************************************************************
 * Installation script.
 * @input: $_POST['action'], optionally other stuff like $_FILE['file'] depending on action type
 * @output: $result - string
 **************************************************************************/
try {
	// Arg check
	if (!isset($_POST['action'])) {
		$result = 'No action set';
		return;
	}
	
	// Call and store result
	$result = $_POST['action']();
} //try
catch (Exception $e) {
	//Log the error
	$result = 'Error: ' . $e->getMessage();
} //catch

/*** 
 * Drops all tables and recreates a fresh copy of the DB
 ***/
function recreateTables() {
	$db = Database::getConnection();
	$return = '';
	
	// Drop Payment
	$q = "DROP TABLE IF EXISTS WB_Payment";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Payment: DB query failure<br>';
	else $return .= 'Payment table dropped.<br>';
	
	// Drop TailorKit Order Items
	$q = "DROP TABLE IF EXISTS WB_TailorKitOrderItem";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table TailorKitOrderItem: DB query failure<br>';
	else $return .= 'TailorKitOrderItem table dropped.<br>';
	
	// Drop Jacket Order Items
	$q = "DROP TABLE IF EXISTS WB_JacketOrderItem";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table JacketOrderItem: DB query failure<br>';
	else $return .= 'JacketOrderItem table dropped.<br>';
	
	// Drop Pants Order Items
	$q = "DROP TABLE IF EXISTS WB_PantsOrderItem";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table PantsOrderItem: DB query failure<br>';
	else $return .= 'PantsOrderItem table dropped.<br>';
	
	// Drop Shirt Order Items
	$q = "DROP TABLE IF EXISTS WB_ShirtOrderItem";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table ShirtOrderItem: DB query failure<br>';
	else $return .= 'ShirtOrderItem table dropped.<br>';
	
	// Drop Order
	$q = "DROP TABLE IF EXISTS WB_Order";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Order: DB query failure<br>';
	else $return .= 'Order table dropped.<br>';
	
	// Drop TailorKit Design
	$q = "DROP TABLE IF EXISTS WB_TailorKitDesign";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table TailorKitDesign: DB query failure<br>';
	else $return .= 'TailorKitDesign table dropped.<br>';
	
	// Drop Jacket Design
	$q = "DROP TABLE IF EXISTS WB_JacketDesign";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table JacketDesign: DB query failure<br>';
	else $return .= 'JacketDesign table dropped.<br>';
	
	// Drop Pants Design
	$q = "DROP TABLE IF EXISTS WB_PantsDesign";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table PantsDesign: DB query failure<br>';
	else $return .= 'PantsDesign table dropped.<br>';
	
	// Drop Shirt Design
	$q = "DROP TABLE IF EXISTS WB_ShirtDesign";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table ShirtDesign: DB query failure<br>';
	else $return .= 'ShirtDesign table dropped.<br>';
	
	// Drop Jacket Fitting Profiles
	$q = "DROP TABLE IF EXISTS WB_JacketFittingProfile";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table JacketFittingProfile: DB query failure<br>';
	else $return .= 'JacketFittingProfile table dropped.<br>';
	
	// Drop Pants Fitting Profiles
	$q = "DROP TABLE IF EXISTS WB_PantsFittingProfile";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table PantsFittingProfile: DB query failure<br>';
	else $return .= 'PantsFittingProfile table dropped.<br>';
	
	// Drop Shirt Fitting Profiles
	$q = "DROP TABLE IF EXISTS WB_ShirtFittingProfile";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table ShirtFittingProfile: DB query failure<br>';
	else $return .= 'ShirtFittingProfile table dropped.<br>';
	
	// Drop discount rules
	$q = "DROP TABLE IF EXISTS WB_DiscountRule";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table DiscountRule: DB query failure<br>';
	else $return .= 'DiscountRule table dropped.<br>';
	
	// Drop fabrics
	$q = "DROP TABLE IF EXISTS WB_Fabric";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Fabric: DB query failure<br>';
	else $return .= 'Fabric table dropped.<br>';
	
	// Drop fabric cares
	$q = "DROP TABLE IF EXISTS WB_FabricCare";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table FabricCare: DB query failure<br>';
	else $return .= 'FabricCare table dropped.<br>';
	
	// Drop fabric weaves
	$q = "DROP TABLE IF EXISTS WB_FabricWeave";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table FabricWeave: DB query failure<br>';
	else $return .= 'FabricWeave table dropped.<br>';
	
	// Drop fabric patterns
	$q = "DROP TABLE IF EXISTS WB_FabricPattern";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table FabricPattern: DB query failure<br>';
	else $return .= 'FabricPattern table dropped.<br>';
	
	// Drop fabric materials
	$q = "DROP TABLE IF EXISTS WB_FabricMaterial";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table FabricMaterial: DB query failure<br>';
	else $return .= 'FabricMaterial table dropped.<br>';
	
	// Drop fabric color
	$q = "DROP TABLE IF EXISTS WB_FabricColor";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table FabricColor: DB query failure<br>';
	else $return .= 'FabricColor table dropped.<br>';
	
	// Drop button threads
	$q = "DROP TABLE IF EXISTS WB_ButtonThread";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table ButtonThread: DB query failure<br>';
	else $return .= 'ButtonThread table dropped.<br>';
	
	// Drop button holes
	$q = "DROP TABLE IF EXISTS WB_ButtonHole";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table ButtonHole: DB query failure<br>';
	else $return .= 'ButtonHole table dropped.<br>';
	
	// Drop buttons
	$q = "DROP TABLE IF EXISTS WB_Button";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Button: DB query failure<br>';
	else $return .= 'Button table dropped.<br>';
	
	// Drop pleats
	$q = "DROP TABLE IF EXISTS WB_Pleat";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Pleat: DB query failure<br>';
	else $return .= 'Pleat table dropped.<br>';
	
	// Drop plackets
	$q = "DROP TABLE IF EXISTS WB_Placket";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Placket: DB query failure<br>';
	else $return .= 'Placket table dropped.<br>';
	
	// Drop pockets
	$q = "DROP TABLE IF EXISTS WB_Pocket";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Pocket: DB query failure<br>';
	else $return .= 'Pocket table dropped.<br>';
	
	// Drop cuffs
	$q = "DROP TABLE IF EXISTS WB_Cuff";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Cuff: DB query failure<br>';
	else $return .= 'Cuff table dropped.<br>';
	
	// Drop collars
	$q = "DROP TABLE IF EXISTS WB_Collar";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Collar: DB query failure<br>';
	else $return .= 'Collar table dropped.<br>';
	
	// Drop announcements
	$q = "DROP TABLE IF EXISTS WB_Announcement";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Announcement: DB query failure<br>';
	else $return .= 'Announcement table dropped.<br>';
	
	// Drop testimonials
	$q = "DROP TABLE IF EXISTS WB_Testimonial";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Testimonial: DB query failure<br>';
	else $return .= 'Testimonial table dropped.<br>';
	
	// Drop mail list entries
	$q = "DROP TABLE IF EXISTS WB_MailListEntry";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table MailListEntry: DB query failure<br>';
	else $return .= 'MailListEntry table dropped.<br>';
	
	// Drop Credit Activities
	$q = "DROP TABLE IF EXISTS WB_CreditActivity";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table CreditActivity: DB query failure<br>';
	else $return .= 'CreditActivity table dropped.<br>';
	
	// Drop activities
	$q = "DROP TABLE IF EXISTS WB_Activity";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Activity: DB query failure<br>';
	else $return .= 'Activity table dropped.<br>';
	
	// Drop customers
	$q = "DROP TABLE IF EXISTS WB_Customer";
	$result = $db->query($q);
	if ($result === FALSE) return '[FAILURE] Unable to drop table Customer: DB query failure<br>';
	else $return .= 'Customer table dropped.<br>';
	
	// Drop staffs
	$q = "DROP TABLE IF EXISTS WB_Staff";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Staff: DB query failure<br>';
	else $return .= 'Staff table dropped.<br>';
	
	// Drop Table States
	$q = "DROP TABLE IF EXISTS WB_State";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table State: DB query failure<br>';
	else $return .= 'State table dropped.<br>';
	
	// Drop Table Countries
	$q = "DROP TABLE IF EXISTS WB_Country";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to drop table Country: DB query failure<br>';
	else $return .= 'Country table dropped.<br>';
	
	$return .= '-------------<br>';
	
	///* Create Country
	$q = "CREATE TABLE IF NOT EXISTS `WB_Country`(
			`id` int unsigned not null auto_increment primary key,
			`code` char(2) not null unique,
			`name` varchar(40) not null unique,
			`enabled` tinyint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Country: DB query failure<br>';
	else $return .= 'Country table created.<br>';
	//*/
	
	///* Create State
	$q = "CREATE TABLE IF NOT EXISTS `WB_State`(
			`id` int unsigned not null auto_increment primary key,
			`countryID` int unsigned not null,
			`code` char(3) not null,
			`name` varchar(40) not null,
			`type` varchar(40) not null,
			`enabled` tinyint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`countryID`) REFERENCES `WB_Country`(`id`),
			UNIQUE KEY(countryID,code)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table State: DB query failure<br>';
	else $return .= 'State table created.<br>';
	//*/
	
	///* Create Staff
	$q = "CREATE TABLE IF NOT EXISTS `WB_Staff`(
			`id` int unsigned not null auto_increment primary key,
			`firstName` varchar(100),
			`lastName` varchar(100),
			`email` varchar(100) not null unique,
			`passwordHash` varchar(100),
			`persistentLoginHashes` varchar(2000) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`lastActivityTime` bigint unsigned not null,
			`accessLevel` tinyint unsigned not null,
			`banned` tinyint unsigned not null,
			`active` tinyint unsigned not null,
			`language` char(2) not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Staff: DB query failure<br>';
	else $return .= 'Staff table created.<br>';
	//*/
	
	///* Create Customer
	$q = "CREATE TABLE IF NOT EXISTS `WB_Customer`(
			`id` int unsigned not null auto_increment primary key,
			`firstName` varchar(100),
			`lastName` varchar(100),
			`email` varchar(100) not null unique,
			`emailVerified` tinyint unsigned not null,
			`emailVerificationHash` varchar(100),
			`passwordHash` varchar(100),
			`persistentLoginHashes` varchar(2000) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`lastActivityTime` bigint unsigned not null,
			`accessLevel` tinyint unsigned not null,
			`banned` tinyint unsigned not null,
			`active` tinyint unsigned not null,
			`language` char(2) not null,
			`profession` varchar(100),
			`company` varchar(100),
			`residenceCountryID` int unsigned,
			`residenceStateID` int unsigned,
			`residencePhone` varchar(15),
			`homeCountryID` int unsigned,
			`homeStateID` int unsigned,
			`homePostalCode` char(10),
			`homeAddress` varchar(100),
			`homePhone` varchar(15),
			`officeCountryID` int unsigned,
			`officeStateID` int unsigned,
			`officePostalCode` char(10),
			`officeAddress` varchar(100),
			`officePhone` varchar(15),
			`birthTime` bigint,
			`referrerID` int unsigned,
			`referralCount` mediumint unsigned,
			`credits` int unsigned,
			FOREIGN KEY (`referrerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`residenceCountryID`) REFERENCES `WB_Country`(`id`),
			FOREIGN KEY (`residenceStateID`) REFERENCES `WB_State`(`id`),
			FOREIGN KEY (`homeCountryID`) REFERENCES `WB_Country`(`id`),
			FOREIGN KEY (`homeStateID`) REFERENCES `WB_State`(`id`),
			FOREIGN KEY (`officeCountryID`) REFERENCES `WB_Country`(`id`),
			FOREIGN KEY (`officeStateID`) REFERENCES `WB_State`(`id`),
			FOREIGN KEY (`referrerID`) REFERENCES `WB_Customer`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Customer: DB query failure<br>';
	else $return .= 'Customer table created.<br>';
	//*/
	
	///* Create Activity
	$q = "CREATE TABLE IF NOT EXISTS `WB_Activity`(
			`id` int unsigned not null auto_increment primary key,
			`createdTime` bigint unsigned not null,
			`userID` int unsigned not null,
			`userType` varchar(20) not null,
			`action` varchar(100) not null,
			`modelID` int unsigned,
			`modelType` varchar(20)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Activity: DB query failure<br>';
	else $return .= 'Activity table created.<br>';
	//*/
	
	///* Create CreditActivity
	$q = "CREATE TABLE IF NOT EXISTS `WB_CreditActivity`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`amount` int not null,
			`description` varchar(100) not null,
			`createdTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table CreditActivity: DB query failure<br>';
	else $return .= 'CreditActivity table created.<br>';
	//*/
	
	///* Create MailListEntry
	$q = "CREATE TABLE IF NOT EXISTS `WB_MailListEntry`(
			`id` int unsigned not null auto_increment primary key,
			`email` varchar(100) not null,
			`list` varchar(100) not null,
			`createdTime` bigint unsigned not null,
			UNIQUE KEY (email,list)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table MailListEntry: DB query failure<br>';
	else $return .= 'MailListEntry table created.<br>';
	//*/
	
	///* Create Testimonial
	$q = "CREATE TABLE IF NOT EXISTS `WB_Testimonial`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(201) not null,
			`message` varchar(500) not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`profession` varchar(100)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Testimonial: DB query failure<br>';
	else $return .= 'Testimonial table created.<br>';
	//*/
	
	///* Create Announcement
	$q = "CREATE TABLE IF NOT EXISTS `WB_Announcement`(
			`id` int unsigned not null auto_increment primary key,
			`message` varchar(200) not null,
			`url` varchar(200),
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Announcement: DB query failure<br>';
	else $return .= 'Announcement table created.<br>';
	//*/
	
	///* Create Collar
	$q = "CREATE TABLE IF NOT EXISTS `WB_Collar`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null,
			`length` varchar(16) not null,
			`allowsSingleButton` tinyint unsigned not null,
			`allowsDoubleButton` tinyint unsigned not null,
			`hasWingButtons` tinyint unsigned not null,
			`allowsTrimmingInnerBand` tinyint unsigned not null,
			`allowsTrimmingOuterBand` tinyint unsigned not null,
			`allowsTrimmingOuterWing` tinyint unsigned not null,
			`description` varchar(400) not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`singleButtonX` smallint unsigned,
			`singleButtonY` smallint unsigned,
			`singleButtonR` smallint unsigned,
			`singleButtonS` smallint unsigned,
			`singleButtonSkewX` smallint unsigned,
			`singleButtonSkewY` smallint unsigned,
			`doubleBottomButtonX` smallint unsigned,
			`doubleBottomButtonY` smallint unsigned,
			`doubleBottomButtonR` smallint unsigned,
			`doubleBottomButtonS` smallint unsigned,
			`doubleBottomButtonSkewX` smallint unsigned,
			`doubleBottomButtonSkewY` smallint unsigned,
			`doubleTopButtonX` smallint unsigned,
			`doubleTopButtonY` smallint unsigned,
			`doubleTopButtonR` smallint unsigned,
			`doubleTopButtonS` smallint unsigned,
			`doubleTopButtonSkewX` smallint unsigned,
			`doubleTopButtonSkewY` smallint unsigned,
			`wingLeftButtonX` smallint unsigned,
			`wingLeftButtonY` smallint unsigned,
			`wingLeftButtonR` smallint unsigned,
			`wingLeftButtonS` smallint unsigned,
			`wingLeftButtonSkewX` smallint unsigned,
			`wingLeftButtonSkewY` smallint unsigned,
			`wingRightButtonX` smallint unsigned,
			`wingRightButtonY` smallint unsigned,
			`wingRightButtonR` smallint unsigned,
			`wingRightButtonS` smallint unsigned,
			`wingRightButtonSkewX` smallint unsigned,
			`wingRightButtonSkewY` smallint unsigned,
			UNIQUE KEY(name,length)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Collar: DB query failure<br>';
	else $return .= 'Collar table created.<br>';
	//*/
	
	///* Create Cuff
	$q = "CREATE TABLE IF NOT EXISTS `WB_Cuff`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null,
			`length` varchar(16) not null,
			`allowsSingleButton` tinyint unsigned not null,
			`allowsDoubleButton` tinyint unsigned not null,
			`allowsTrimmingInner` tinyint unsigned not null,
			`allowsTrimmingOuter` tinyint unsigned not null,
			`allowsTrimmingSleeve` tinyint unsigned not null,
			`description` varchar(400) not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`singleButtonX` smallint unsigned,
			`singleButtonY` smallint unsigned,
			`singleButtonR` smallint unsigned,
			`singleButtonS` smallint unsigned,
			`singleButtonSkewX` smallint unsigned,
			`singleButtonSkewY` smallint unsigned,
			`doubleBottomButtonX` smallint unsigned,
			`doubleBottomButtonY` smallint unsigned,
			`doubleBottomButtonR` smallint unsigned,
			`doubleBottomButtonS` smallint unsigned,
			`doubleBottomButtonSkewX` smallint unsigned,
			`doubleBottomButtonSkewY` smallint unsigned,
			`doubleTopButtonX` smallint unsigned,
			`doubleTopButtonY` smallint unsigned,
			`doubleTopButtonR` smallint unsigned,
			`doubleTopButtonS` smallint unsigned,
			`doubleTopButtonSkewX` smallint unsigned,
			`doubleTopButtonSkewY` smallint unsigned,
			UNIQUE KEY(name,length)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Cuff: DB query failure<br>';
	else $return .= 'Cuff table created.<br>';
	//*/
	
	///* Create Pocket
	$q = "CREATE TABLE IF NOT EXISTS `WB_Pocket`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`allowsPenPouch` tinyint unsigned not null,
			`allowsTrimming` tinyint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Pocket: DB query failure<br>';
	else $return .= 'Pocket table created.<br>';
	//*/
	
	///* Create Placket
	$q = "CREATE TABLE IF NOT EXISTS `WB_Placket`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`allowsTrimming` tinyint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Placket: DB query failure<br>';
	else $return .= 'Placket table created.<br>';
	//*/
	
	///* Create Pleat
	$q = "CREATE TABLE IF NOT EXISTS `WB_Pleat`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`allowsDarts` tinyint unsigned not null,
			`allowsTrimming` tinyint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Pleat: DB query failure<br>';
	else $return .= 'Pleat table created.<br>';
	//*/
	
	///* Create Button
	$q = "CREATE TABLE IF NOT EXISTS `WB_Button`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Button: DB query failure<br>';
	else $return .= 'Button table created.<br>';
	//*/
	
	///* Create Button Hole
	$q = "CREATE TABLE IF NOT EXISTS `WB_ButtonHole`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table ButtonHole: DB query failure<br>';
	else $return .= 'ButtonHole table created.<br>';
	//*/
	
	///* Create Button Thread
	$q = "CREATE TABLE IF NOT EXISTS `WB_ButtonThread`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table ButtonThread: DB query failure<br>';
	else $return .= 'ButtonThread table created.<br>';
	//*/
	
	///* Create Fabric Color
	$q = "CREATE TABLE IF NOT EXISTS `WB_FabricColor`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`r` tinyint unsigned not null,
			`g` tinyint unsigned not null,
			`b` tinyint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table FabricColor: DB query failure<br>';
	else $return .= 'FabricColor table created.<br>';
	//*/
	
	///* Create Fabric Material
	$q = "CREATE TABLE IF NOT EXISTS `WB_FabricMaterial`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table FabricMaterial: DB query failure<br>';
	else $return .= 'FabricMaterial table created.<br>';
	//*/
	
	///* Create Fabric Pattern
	$q = "CREATE TABLE IF NOT EXISTS `WB_FabricPattern`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table FabricPattern: DB query failure<br>';
	else $return .= 'FabricPattern table created.<br>';
	//*/
	
	///* Create Fabric Weave
	$q = "CREATE TABLE IF NOT EXISTS `WB_FabricWeave`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table FabricWeave: DB query failure<br>';
	else $return .= 'FabricWeave table created.<br>';
	//*/
	
	///* Create Fabric Care
	$q = "CREATE TABLE IF NOT EXISTS `WB_FabricCare`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table FabricCare: DB query failure<br>';
	else $return .= 'FabricCare table created.<br>';
	//*/
	
	///* Create Fabric
	$q = "CREATE TABLE IF NOT EXISTS `WB_Fabric`(
			`id` int unsigned not null auto_increment primary key,
			`name` varchar(100) not null unique,
			`color1ID` int unsigned not null,
			`color2ID` int unsigned,
			`color3ID` int unsigned,
			`materialID` int unsigned not null,
			`patternID` int unsigned not null,
			`weaveID` int unsigned not null,
			`careID` int unsigned not null,
			`price` mediumint unsigned not null,
			`trimmingPrice1` mediumint unsigned not null,
			`trimmingPrice2` mediumint unsigned not null,
			`trimmingPrice3` mediumint unsigned not null,
			`stock` int unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`color1ID`) REFERENCES `WB_FabricColor`(`id`),
			FOREIGN KEY (`color2ID`) REFERENCES `WB_FabricColor`(`id`),
			FOREIGN KEY (`color3ID`) REFERENCES `WB_FabricColor`(`id`),
			FOREIGN KEY (`materialID`) REFERENCES `WB_FabricMaterial`(`id`),
			FOREIGN KEY (`patternID`) REFERENCES `WB_FabricPattern`(`id`),
			FOREIGN KEY (`weaveID`) REFERENCES `WB_FabricWeave`(`id`),
			FOREIGN KEY (`careID`) REFERENCES `WB_FabricCare`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Fabric: DB query failure<br>';
	else $return .= 'Fabric table created.<br>';
	//*/
	
	///* Create Disount Rule
	$q = "CREATE TABLE IF NOT EXISTS `WB_DiscountRule`(
			`id` int unsigned not null auto_increment primary key,
			`type` varchar(10) not null,
			`threshold` mediumint unsigned not null,
			`value` mediumint unsigned not null,
			`fabricID` int unsigned,
			`validFromTime` bigint unsigned not null,
			`validToTime` bigint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`fabricID`) REFERENCES `WB_Fabric`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table DiscountRule: DB query failure<br>';
	else $return .= 'DiscountRule table created.<br>';
	//*/
	
	///* Create Shirt Fitting Profile
	$q = "CREATE TABLE IF NOT EXISTS `WB_ShirtFittingProfile`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`name` varchar(100) not null,
			`measurementMethod` varchar(10) not null,
			`posture` varchar(20) not null,
			`shoulderType` varchar(20) not null,
			`measurementCollar` mediumint unsigned not null,
			`measurementShoulders` mediumint unsigned not null,
			`measurementChest` mediumint unsigned not null,
			`measurementBicep` mediumint unsigned not null,
			`measurementElbow` mediumint unsigned,
			`measurementWrist` mediumint unsigned not null,
			`measurementSleeveLength` mediumint unsigned not null,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementShirtLength` mediumint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			UNIQUE KEY(customerID,name)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table ShirtFittingProfile: DB query failure<br>';
	else $return .= 'ShirtFittingProfile table created.<br>';
	//*/
	
	///* Create Pants Fitting Profile
	$q = "CREATE TABLE IF NOT EXISTS `WB_PantsFittingProfile`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`name` varchar(100) not null,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementCrotch` mediumint unsigned not null,
			`measurementThigh` mediumint unsigned not null,
			`measurementKnee` mediumint unsigned not null,
			`measurementAnkle` mediumint unsigned not null,
			`measurementLength` mediumint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			UNIQUE KEY(customerID,name)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table PantsFittingProfile: DB query failure<br>';
	else $return .= 'PantsFittingProfile table created.<br>';
	//*/
	
	///* Create Jacket Fitting Profile
	$q = "CREATE TABLE IF NOT EXISTS `WB_JacketFittingProfile`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`name` varchar(100) not null,
			`measurementCollar` mediumint unsigned not null,
			`measurementShoulders` mediumint unsigned not null,
			`measurementChest` mediumint unsigned not null,
			`measurementBicep` mediumint unsigned not null,
			`measurementArmHole` mediumint unsigned not null,
			`measurementSleeveLength` mediumint unsigned not null,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementShirtLength` mediumint unsigned not null,
			`displayPriority` smallint unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			UNIQUE KEY(customerID,name)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table JacketFittingProfile: DB query failure<br>';
	else $return .= 'JacketFittingProfile table created.<br>';
	//*/
	
	///* Create Shirt Design
	$q = "CREATE TABLE IF NOT EXISTS `WB_ShirtDesign`(
			`id` int unsigned not null auto_increment primary key,
			`fabricID` int unsigned not null,
			`collarID` int unsigned not null,
			`cuffID` int unsigned not null,
			`pocketID` int unsigned,
			`placketID` int unsigned not null,
			`pleatID` int unsigned not null,
			`buttonID` int unsigned not null,
			`buttonHoleID` int unsigned not null,
			`buttonThreadID` int unsigned not null,
			`hasPenPouch` tinyint unsigned not null,
			`hasDarts` tinyint unsigned not null,
			`collarButtonType` varchar(16) not null,
			`cuffButtonType` varchar(16) not null,
			`trimmingInnerCollarBandFabricID` int unsigned,
			`trimmingOuterCollarBandFabricID` int unsigned,
			`trimmingOuterCollarWingFabricID` int unsigned,
			`trimmingInnerCuffFabricID` int unsigned,
			`trimmingOuterCuffFabricID` int unsigned,
			`trimmingCuffSleeveFabricID` int unsigned,
			`trimmingPocketFabricID` int unsigned,
			`trimmingPlacketFabricID` int unsigned,
			`trimmingPleatFabricID` int unsigned,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`fabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`collarID`) REFERENCES `WB_Collar`(`id`),
			FOREIGN KEY (`cuffID`) REFERENCES `WB_Cuff`(`id`),
			FOREIGN KEY (`pocketID`) REFERENCES `WB_Pocket`(`id`),
			FOREIGN KEY (`placketID`) REFERENCES `WB_Placket`(`id`),
			FOREIGN KEY (`pleatID`) REFERENCES `WB_Pleat`(`id`),
			FOREIGN KEY (`buttonID`) REFERENCES `WB_Button`(`id`),
			FOREIGN KEY (`buttonHoleID`) REFERENCES `WB_ButtonHole`(`id`),
			FOREIGN KEY (`buttonThreadID`) REFERENCES `WB_ButtonThread`(`id`),
			FOREIGN KEY (`trimmingInnerCollarBandFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingOuterCollarBandFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingOuterCollarWingFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingInnerCuffFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingOuterCuffFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingCuffSleeveFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingPocketFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingPlacketFabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`trimmingPleatFabricID`) REFERENCES `WB_Fabric`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table ShirtDesign: DB query failure<br>';
	else $return .= 'ShirtDesign table created.<br>';
	//*/
	
	///* Create Pants Design
	$q = "CREATE TABLE IF NOT EXISTS `WB_PantsDesign`(
			`id` int unsigned not null auto_increment primary key,
			`description` varchar(300) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table PantsDesign: DB query failure<br>';
	else $return .= 'PantsDesign table created.<br>';
	//*/
	
	///* Create Jacket Design
	$q = "CREATE TABLE IF NOT EXISTS `WB_JacketDesign`(
			`id` int unsigned not null auto_increment primary key,
			`description` varchar(300) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table JacketDesign: DB query failure<br>';
	else $return .= 'JacketDesign table created.<br>';
	//*/
	
	///* Create Tailor Kit Design
	$q = "CREATE TABLE IF NOT EXISTS `WB_TailorKitDesign`(
			`id` int unsigned not null auto_increment primary key,
			`fabric1ID` int unsigned not null,
			`fabric2ID` int unsigned not null,
			`fabric3ID` int unsigned not null,
			`fabric4ID` int unsigned not null,
			`fabric5ID` int unsigned not null,
			`fabric6ID` int unsigned not null,
			`fabric7ID` int unsigned not null,
			`fabric8ID` int unsigned not null,
			`fabric9ID` int unsigned not null,
			`fabric10ID` int unsigned not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`fabric1ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric2ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric3ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric4ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric5ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric6ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric7ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric8ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric9ID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`fabric10ID`) REFERENCES `WB_Fabric`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table TailorKitDesign: DB query failure<br>';
	else $return .= 'TailorKitDesign table created.<br>';
	//*/
	
	///* Create Order
	$q = "CREATE TABLE IF NOT EXISTS `WB_Order`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`subTotal` int unsigned not null,
			`discount` int unsigned not null,
			`creditsUsed` int unsigned not null,
			`deliveryCost` int unsigned not null,
			`eta` int unsigned not null,
			`deliveryCountryID` int unsigned not null,
			`deliveryStateID` int unsigned,
			`deliveryPostalCode` char(10) not null,
			`deliveryAddress` varchar(100) not null,
			`deliveryPhone` varchar(15) not null,
			`status` char(9) not null,
			`invoiceNumber` char(16) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`paidTime` bigint unsigned,
			`cancelledTime` bigint unsigned,
			`deliveredTime` bigint unsigned,
			FOREIGN KEY (`deliveryCountryID`) REFERENCES `WB_Country`(`id`),
			FOREIGN KEY (`deliveryStateID`) REFERENCES `WB_State`(`id`),
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Order: DB query failure<br>';
	else $return .= 'Order table created.<br>';
	//*/

	///* Create Shirt Order Item
	$q = "CREATE TABLE IF NOT EXISTS `WB_ShirtOrderItem`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`orderID` int unsigned,
			`shirtDesignID` int unsigned not null,
			`fabricID` int unsigned not null,
			`shirtFittingProfileID` int unsigned,
			`measurementMethod` varchar(10) not null,
			`posture` varchar(20) not null,
			`shoulderType` varchar(20) not null,
			`measurementCollar` mediumint unsigned not null,
			`measurementShoulders` mediumint unsigned not null,
			`measurementChest` mediumint unsigned not null,
			`measurementBicep` mediumint unsigned not null,
			`measurementElbow` mediumint unsigned,
			`measurementWrist` mediumint unsigned not null,
			`measurementSleeveLength` mediumint unsigned not null,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementShirtLength` mediumint unsigned not null,
			`quantity` mediumint unsigned not null,
			`unitPrice` mediumint unsigned not null,
			`remarks` varchar(300),
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`orderID`) REFERENCES `WB_Order`(`id`),
			FOREIGN KEY (`shirtDesignID`) REFERENCES `WB_ShirtDesign`(`id`),
			FOREIGN KEY (`fabricID`) REFERENCES `WB_Fabric`(`id`),
			FOREIGN KEY (`shirtFittingProfileID`) REFERENCES `WB_ShirtFittingProfile`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table ShirtOrderItem: DB query failure<br>';
	else $return .= 'ShirtOrderItem table created.<br>';
	//*/
	
	///* Create Pants Order Item
	$q = "CREATE TABLE IF NOT EXISTS `WB_PantsOrderItem`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`orderID` int unsigned,
			`pantsDesignID` int unsigned not null,
			`pantsFittingProfileID` int unsigned,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementCrotch` mediumint unsigned not null,
			`measurementThigh` mediumint unsigned not null,
			`measurementKnee` mediumint unsigned not null,
			`measurementAnkle` mediumint unsigned not null,
			`measurementLength` mediumint unsigned not null,
			`quantity` mediumint unsigned not null,
			`unitPrice` mediumint unsigned not null,
			`remarks` varchar(300),
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`orderID`) REFERENCES `WB_Order`(`id`),
			FOREIGN KEY (`pantsDesignID`) REFERENCES `WB_PantsDesign`(`id`),
			FOREIGN KEY (`pantsFittingProfileID`) REFERENCES `WB_PantsFittingProfile`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table PantsOrderItem: DB query failure<br>';
	else $return .= 'PantsOrderItem table created.<br>';
	//*/
	
	///* Create Jacket Order Item
	$q = "CREATE TABLE IF NOT EXISTS `WB_JacketOrderItem`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`orderID` int unsigned,
			`jacketDesignID` int unsigned not null,
			`jacketFittingProfileID` int unsigned,
			`measurementCollar` mediumint unsigned not null,
			`measurementShoulders` mediumint unsigned not null,
			`measurementChest` mediumint unsigned not null,
			`measurementBicep` mediumint unsigned not null,
			`measurementArmHole` mediumint unsigned not null,
			`measurementSleeveLength` mediumint unsigned not null,
			`measurementWaist` mediumint unsigned not null,
			`measurementHips` mediumint unsigned not null,
			`measurementShirtLength` mediumint unsigned not null,
			`quantity` mediumint unsigned not null,
			`unitPrice` mediumint unsigned not null,
			`remarks` varchar(300),
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`orderID`) REFERENCES `WB_Order`(`id`),
			FOREIGN KEY (`jacketDesignID`) REFERENCES `WB_JacketDesign`(`id`),
			FOREIGN KEY (`jacketFittingProfileID`) REFERENCES `WB_JacketFittingProfile`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table JacketOrderItem: DB query failure<br>';
	else $return .= 'JacketOrderItem table created.<br>';
	//*/
	
	///* Create TailorKit Order Item
	$q = "CREATE TABLE IF NOT EXISTS `WB_TailorKitOrderItem`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`orderID` int unsigned,
			`tailorKitDesignID` int unsigned not null,
			`quantity` mediumint unsigned not null,
			`unitPrice` mediumint unsigned not null,
			`remarks` varchar(300),
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`orderID`) REFERENCES `WB_Order`(`id`),
			FOREIGN KEY (`tailorKitDesignID`) REFERENCES `WB_TailorKitDesign`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table TailorKitOrderItem: DB query failure<br>';
	else $return .= 'TailorKitOrderItem table created.<br>';
	//*/
	
	///* Create Payment
	$q = "CREATE TABLE IF NOT EXISTS `WB_Payment`(
			`id` int unsigned not null auto_increment primary key,
			`customerID` int unsigned not null,
			`orderID` int unsigned not null,
			`amount` int unsigned not null,
			`receiptNumber` varchar(32) not null,
			`createdTime` bigint unsigned not null,
			`lastUpdateTime` bigint unsigned not null,
			`cancelledTime` bigint unsigned not null,
			FOREIGN KEY (`customerID`) REFERENCES `WB_Customer`(`id`),
			FOREIGN KEY (`orderID`) REFERENCES `WB_Order`(`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci";
	$result = $db->query($q);
	if ($result === FALSE) echo '[FAILURE] Unable to create table Payment: DB query failure<br>';
	else $return .= 'Payment table created.<br>';
	//*/
	
	return $return;
} //recreateTables()

/***
 * Loads countries from CSV file. Each line:
 * - name, code
 ***/
function loadCountries() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$code = trim($arr[1]);
		
		// Create country
		$country = new Country(array(
			'name' => $name,
			'code' => $code,
		));
		if (!$db->create($country)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' countries.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadCountries()

/***
 * Loads states from CSV file. Each line:
 * - country code, name, type, code
 ***/
function loadStates() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$countryCode = trim($arr[0]);
		$name = trim($arr[1]);
		$type = trim($arr[2]);
		$code = trim($arr[3]);
		$regionNumber = trim($arr[4]); // unused
		
		// Read country
		$country = $db->readSingle('Country', array(
			array('code','=',$countryCode)
		), FALSE);
		if (!isset($country)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		// Create state
		$state = new State(array(
			'name' => $name,
			'code' => $code,
			'type' => $type,
			'countryID' => $country->getID(), 
		));
		if (!$db->create($state)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' states.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadStates()

/***
 * Loads staff from CSV file. Each line:
 * - firstName, lastName, email, password, accessLevel
 ***/
function loadStaffs() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$firstName = trim($arr[0]);
		$lastName = trim($arr[1]);
		$email =trim($arr[2]);
		$passwordHash = Encryptor::getHash(trim($arr[3]));
		$accessLevel = trim($arr[4]);
		
		// Create staff
		$staff = new Staff(array(
			'firstName' => $firstName,
			'lastName' => $lastName,
			'email' => $email,
			'passwordHash' => $passwordHash,
			'accessLevel' => $accessLevel
		));
		if (!$db->create($staff)) {
			$failed++;
			$failedNames .= '- '.$firstName.' '.$lastName.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' staffs.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadStaffs()

/***
 * Loads customers (verified) from CSV file. Each line:
 * - firstName, lastName, email, password
 ***/
function loadCustomers() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$firstName = trim($arr[0]);
		$lastName = trim($arr[1]);
		$email =trim($arr[2]);
		$passwordHash = Encryptor::getHash(trim($arr[3]));
		
		// Create customer
		$customer = new Customer(array(
			'firstName' => $firstName,
			'lastName' => $lastName,
			'email' => $email,
			'emailVerified' => TRUE,
			'passwordHash' => $passwordHash,
		));
		if (!$db->create($customer)) {
			$failed++;
			$failedNames .= '- '.$firstName.' '.$lastName.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' customers.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadCustomers()

/***
 * Loads announcements from CSV file. Each line:
 * - message, displayPriority, url
 ***/
function loadAnnouncements() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$message = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		$url = trim($arr[2]);
		
		// Create announcement
		$announcement = new Announcement(array(
			'message' => $message,
			'url' => $url,
			'displayPriority' => $displayPriority,
			'url' => $url,
		));
		if (!$db->create($announcement)) {
			$failed++;
			$failedNames .= '- '.$message.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' announcements.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadAnnouncements()

/***
 * Loads testimonials from CSV file. Each line:
 * - name, message, displayPriority, profession
 ***/
function loadTestimonials() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$message = trim($arr[1]);
		$displayPriority = trim($arr[2]);
		$url =trim($arr[3]);
		
		// Create testi
		$testimonial = new Testimonial(array(
			'name' => $name,
			'message' => $message,
			'img' => $img,
			'url' => $url,
			'displayPriority' => $displayPriority,
			'url' => $url,
		));
		if (!$db->create($testimonial)) {
			$failed++;
			$failedNames .= '- '.$message.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' testimonials.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadTestimonials()



/***
 * Loads FabricCares from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadFabricCares() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create FabricCare
		$model = new FabricCare(array(
			'name' => $name,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' FabricCares.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabricCares()

/***
 * Loads FabricColors from CSV file. Each line:
 * - name, r, g, b, displayPriority
 ***/
function loadFabricColors() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$r = trim($arr[1]);
		$g = trim($arr[2]);
		$b = trim($arr[3]);
		$displayPriority = trim($arr[4]);
		
		// Create FabricColor
		$model = new FabricColor(array(
			'name' => $name,
			'r' => $r,
			'g' => $g,
			'b' => $b,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' FabricColors.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabricColors()

/***
 * Loads FabricMaterials from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadFabricMaterials() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create FabricMaterial
		$model = new FabricMaterial(array(
			'name' => $name,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' FabricMaterials.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabricMaterials()

/***
 * Loads FabricPatterns from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadFabricPatterns() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create FabricPattern
		$model = new FabricPattern(array(
			'name' => $name,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' FabricPatterns.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabricPatterns()

/***
 * Loads FabricWeaves from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadFabricWeaves() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create FabricWeave
		$model = new FabricWeave(array(
			'name' => $name,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' FabricWeaves.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabricWeaves()

/***
 * Loads Fabrics from CSV file. Each line:
 * - name, color1, color2, color3, material, pattern, weave, care, price, trimPrice1, trimPrice2, trimPrice3, stock, displayPriority
 ***/
function loadFabrics() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$color1ID = trim($arr[1]);
		$color2ID = trim($arr[2]);
		$color3ID = trim($arr[3]);
		$materialID = trim($arr[4]);
		$patternID = trim($arr[5]);
		$weaveID = trim($arr[6]);
		$careID = trim($arr[7]);
		$price = trim($arr[8]);
		$trimmingPrice1 = trim($arr[9]);
		$trimmingPrice2 = trim($arr[10]);
		$trimmingPrice3 = trim($arr[11]);
		$stock = trim($arr[12]);
		$displayPriority = trim($arr[13]);
		
		// Create Fabric
		$model = new Fabric(array(
			'name' => $name,
			'color1ID' => $color1ID,
			'color2ID' => $color2ID,
			'color3ID' => $color3ID,
			'materialID' => $materialID,
			'patternID' => $patternID,
			'weaveID' => $weaveID,
			'careID' => $careID,
			'price' => $price,
			'trimmingPrice1' => $trimmingPrice1,
			'trimmingPrice2' => $trimmingPrice2,
			'trimmingPrice3' => $trimmingPrice3,
			'stock' => $stock,
			'displayPriority' => $displayPriority,
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Fabrics.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadFabrics()

/***
 * Loads Collars from CSV file. Each line:
 * - name, length, allowsSingleButton, allowsDoubleButton, hasWingButtons, description,  X, Y, R, S, [double buttons X, Y, R, S, X, Y, R, S], [wing buttons X, Y, R, S, X, Y, R, S], displayPriority
 ***/
function loadCollars() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$length = trim($arr[1]);
		$allowsSingleButton = trim($arr[2]);
		$allowsDoubleButton = trim($arr[3]);
		$hasWingButtons = trim($arr[4]);
		$description = trim($arr[5]);
		$singleButtonX = trim($arr[6]);
		$singleButtonY = trim($arr[7]);
		$singleButtonR = trim($arr[8]);
		$singleButtonS = trim($arr[9]);
		$doubleBottomButtonX = trim($arr[10]);
		$doubleBottomButtonY = trim($arr[11]);
		$doubleBottomButtonR = trim($arr[12]);
		$doubleBottomButtonS = trim($arr[13]);
		$doubleTopButtonX = trim($arr[14]);
		$doubleTopButtonY = trim($arr[15]);
		$doubleTopButtonR = trim($arr[16]);
		$doubleTopButtonS = trim($arr[17]);
		$wingLeftButtonX = trim($arr[18]);
		$wingLeftButtonY = trim($arr[19]);
		$wingLeftButtonR = trim($arr[20]);
		$wingLeftButtonS = trim($arr[21]);
		$wingRightButtonX = trim($arr[22]);
		$wingRightButtonY = trim($arr[23]);
		$wingRightButtonR = trim($arr[24]);
		$wingRightButtonS = trim($arr[25]);
		$displayPriority = trim($arr[26]);
		
		// Create Collar
		$model = new Collar(array(
			'name' => $name,
			'length' => $length,
			'allowsSingleButton' => $allowsSingleButton,
			'allowsDoubleButton' => $allowsDoubleButton,
			'hasWingButtons' => $hasWingButtons,
			'description' => $description,
			'singleButtonX' => $singleButtonX,
			'singleButtonY' => $singleButtonY,
			'singleButtonR' => $singleButtonR,
			'singleButtonS' => $singleButtonS,
			'doubleBottomButtonX' => $doubleBottomButtonX,
			'doubleBottomButtonY' => $doubleBottomButtonY,
			'doubleBottomButtonR' => $doubleBottomButtonR,
			'doubleBottomButtonS' => $doubleBottomButtonS,
			'doubleTopButtonX' => $doubleTopButtonX,
			'doubleTopButtonY' => $doubleTopButtonY,
			'doubleTopButtonR' => $doubleTopButtonR,
			'doubleTopButtonS' => $doubleTopButtonS,
			'wingLeftButtonX' => $wingLeftButtonX,
			'wingLeftButtonY' => $wingLeftButtonY,
			'wingLeftButtonR' => $wingLeftButtonR,
			'wingLeftButtonS' => $wingLeftButtonS,
			'wingRightButtonX' => $wingRightButtonX,
			'wingRightButtonY' => $wingRightButtonY,
			'wingRightButtonR' => $wingRightButtonR,
			'wingRightButtonS' => $wingRightButtonS,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Collars.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadCollars()


/***
 * Loads Cuffs from CSV file. Each line:
 * - name, length, allowsSingleButton, allowsDoubleButton, description,  X, Y, R, S, [double buttons X, Y, R, S, X, Y, R, S], displayPriority
 ***/
function loadCuffs() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$length = trim($arr[1]);
		$allowsSingleButton = trim($arr[2]);
		$allowsDoubleButton = trim($arr[3]);
		$description = trim($arr[4]);
		$singleButtonX = trim($arr[5]);
		$singleButtonY = trim($arr[6]);
		$singleButtonR = trim($arr[7]);
		$singleButtonS = trim($arr[8]);
		$doubleBottomButtonX = trim($arr[9]);
		$doubleBottomButtonY = trim($arr[10]);
		$doubleBottomButtonR = trim($arr[11]);
		$doubleBottomButtonS = trim($arr[12]);
		$doubleTopButtonX = trim($arr[13]);
		$doubleTopButtonY = trim($arr[14]);
		$doubleTopButtonR = trim($arr[15]);
		$doubleTopButtonS = trim($arr[16]);
		$displayPriority = trim($arr[17]);
		
		// Create Cuff
		$model = new Cuff(array(
			'name' => $name,
			'length' => $length,
			'allowsSingleButton' => $allowsSingleButton,
			'allowsDoubleButton' => $allowsDoubleButton,
			'description' => $description,
			'singleButtonX' => $singleButtonX,
			'singleButtonY' => $singleButtonY,
			'singleButtonR' => $singleButtonR,
			'singleButtonS' => $singleButtonS,
			'doubleBottomButtonX' => $doubleBottomButtonX,
			'doubleBottomButtonY' => $doubleBottomButtonY,
			'doubleBottomButtonR' => $doubleBottomButtonR,
			'doubleBottomButtonS' => $doubleBottomButtonS,
			'doubleTopButtonX' => $doubleTopButtonX,
			'doubleTopButtonY' => $doubleTopButtonY,
			'doubleTopButtonR' => $doubleTopButtonR,
			'doubleTopButtonS' => $doubleTopButtonS,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Cuffs.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadCuffs()

/***
 * Loads Pockets from CSV file. Each line:
 * - name, allowsPenPouch, allowsTrimming, displayPriority
 ***/
function loadPockets() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$allowsPenPouch = trim($arr[1]);
		$allowsTrimming = trim($arr[2]);
		$displayPriority = trim($arr[3]);
		
		// Create Pocket
		$model = new Pocket(array(
			'name' => $name,
			'allowsPenPouch' => $allowsPenPouch,
			'allowsTrimming' => $allowsTrimming,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Pockets.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadPockets()

/***
 * Loads Plackets from CSV file. Each line:
 * - name, allowsTrimming, displayPriority
 ***/
function loadPlackets() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$allowsTrimming = trim($arr[1]);
		$displayPriority = trim($arr[2]);
		
		// Create Pocket
		$model = new Placket(array(
			'name' => $name,
			'allowsTrimming' => $allowsTrimming,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Plackets.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadPlackets()

/***
 * Loads Pleats from CSV file. Each line:
 * - name, allowsTrimming, displayPriority
 ***/
function loadPleats() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$allowsDarts = trim($arr[1]);
		$allowsTrimming = trim($arr[2]);
		$displayPriority = trim($arr[3]);
		
		// Create Pleat
		$model = new Pleat(array(
			'name' => $name,
			'allowsDarts' => $allowsDarts,
			'allowsTrimming' => $allowsTrimming,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Pleats.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadPleats()

/***
 * Loads Buttons from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadButtons() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create Button
		$model = new Button(array(
			'name' => $name,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' Buttons.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadButtons()


/***
 * Loads ButtonHoles from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadButtonHoles() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create ButtonHole
		$model = new ButtonHole(array(
			'name' => $name,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' ButtonHoles.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadButtonHoles()

/***
 * Loads ButtonThreads from CSV file. Each line:
 * - name, displayPriority
 ***/
function loadButtonThreads() {
	// Read from CSV file
	if ($_FILES['file']['error'] > 0) {
		return "[FAILURE] " . $_FILES['file']['error'] . "<br>";
	}
	$return =  "Uploaded: " . $_FILES['file']['name'] . ' ('.$_FILES['file']['size'] . ' bytes)<br>';
	if (!isset($_FILES['file']['tmp_name'])) {
		return $return.'Tmp file not found.';
	}
	
	// Init counter
	$count = 0;
	$failed = 0;
	$return .= 'Counter: ';
	$failedNames = '';
	$db = Database::getConnection();
	
	//Parse each line
	$lines = file($_FILES['file']['tmp_name']);
	foreach ($lines as $i => $line) {
		//Ignore comment lines
		if (startsWith($line, '//')) {
			continue;
		}
		
		// Parse this line
		$arr = str_getcsv($line);
		$name = trim($arr[0]);
		$displayPriority = trim($arr[1]);
		
		// Create ButtonThread
		$model = new ButtonThread(array(
			'name' => $name,
			'displayPriority' => $displayPriority
		));
		if (!$db->create($model)) {
			$failed++;
			$failedNames .= '- '.$name.'<br>';
			continue;
		}
		
		$count++;
		$return .= '|';
	} //for each line
	
	// Return with count
	if ($count > 0) $return .= '<br>Loaded '.$count.' ButtonThreads.<br>';
	else $return .= '<br>[FAILURE] Not loaded.<br>';
	if ($failed > 0) $return .= 'Failed '.$failed.' loads<br>'.$failedNames;
	return $return;
} //loadButtonThreads()