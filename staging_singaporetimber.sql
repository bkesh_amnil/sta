-- phpMyAdmin SQL Dump
-- version 4.6.0deb1.wily~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: 10.240.44.227
-- Generation Time: Dec 20, 2016 at 05:18 PM
-- Server version: 10.1.16-MariaDB-1~wily
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~wily+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `staging_singaporetimber`
--

-- --------------------------------------------------------

--
-- Table structure for table `sta_ActivityLog`
--

CREATE TABLE `sta_ActivityLog` (
  `ID` int(11) NOT NULL,
  `UserType` varchar(600) DEFAULT NULL COMMENT 'BackendUser, Customer, Service Provider',
  `UserID` int(11) DEFAULT NULL COMMENT 'BackendUserID, ApplicationUserID',
  `Action` char(100) DEFAULT NULL,
  `OldValue` text,
  `NewValue` text,
  `ModelType` varchar(255) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `CreatedDateTime` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_ActivityLog`
--

INSERT INTO `sta_ActivityLog` (`ID`, `UserType`, `UserID`, `Action`, `OldValue`, `NewValue`, `ModelType`, `ModelID`, `CreatedDateTime`) VALUES
(1, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481620013834),
(2, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481620181914),
(3, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481620225024),
(4, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481620304084),
(5, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481620866431),
(6, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 1, 1481620992252),
(7, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481621009726),
(8, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"undefined","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":null,"DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1481621069920}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"undefined","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"FRANCIS NG\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":\\"10\\"}]","Associates":"[]"}', 'Company', 1, 1481621069920),
(9, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481621079515),
(10, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481621327910),
(11, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 2, 1481621723095),
(12, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 3, 1481621881879),
(13, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 4, 1481622040739),
(14, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481622049801),
(15, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481623006710),
(16, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 5, 1481623309901),
(17, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 6, 1481623698962),
(18, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 7, 1481623897139),
(19, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481624387724),
(20, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 8, 1481624646731),
(21, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 9, 1481624907001),
(22, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 10, 1481625061048),
(23, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 11, 1481625512537),
(24, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 12, 1481625704346),
(25, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481625762838),
(26, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 13, 1481625872390),
(27, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 14, 1481627227585),
(28, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 15, 1481627366443),
(29, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 16, 1481627956685),
(30, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481628572488),
(31, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481629138867),
(32, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481629744316),
(33, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 20, 1481629883162),
(34, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481630985487),
(35, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631120024),
(36, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631188568),
(37, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631262522),
(38, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631302607),
(39, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631319089),
(40, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481631335269),
(41, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481693246910),
(42, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481694466033),
(43, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 1, 1481699424372),
(44, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481699574879),
(45, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481866927533),
(46, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481867460711),
(47, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481869693824),
(48, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481869729187),
(49, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481870702730),
(50, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872186903),
(51, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872895558),
(52, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872898436),
(53, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872920728),
(54, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872944449),
(55, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481872997427),
(56, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481873162323),
(57, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481873253084),
(58, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481873313198),
(59, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 2, 1481873425216),
(60, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 22, 1481873724886),
(61, 'BackendUser', 1, 'Update', '{"ID":16,"CompanyNameEnglish":"GENERAL LUMBER PRODUCTS PTE LTD","CompanyNameChinese":"\\u540d\\u79f0","Address":"158, Lorong Kismis, Singapore 598081","Phone":"64697260","Fax":"64671832","Email":"glp@general-lumber.com","Website":"www.general-lumber.com","Registration":"02482\\/1985K","ProductsServicesEnglish":"Sawn Timber, Moulding, Furniture, Furniture parts, Sawmill, Moulding, KD Plant, Treatment Plant,Import & Export","ProductsServicesChinese":null,"DisplayPriority":13,"CreatedTime":"1481627956685","LastUpdatedTime":1481873778317}', '{"ID":"16","CompanyNameEnglish":"GENERAL LUMBER PRODUCTS PTE LTD","CompanyNameChinese":"\\u540d\\u79f0","Address":"158, Lorong Kismis, Singapore 598081","Phone":"64697260","Fax":"64671832","Email":"glp@general-lumber.com","Website":"www.general-lumber.com","Registration":"02482\\/1985K","ProductsServicesEnglish":"Sawn Timber, Moulding, Furniture, Furniture parts, Sawmill, Moulding, KD Plant, Treatment Plant,Import & Export","DisplayPriority":"13","Representatives":"[{\\"ID\\":24,\\"CompanyID\\":\\"16\\",\\"NameEnglish\\":\\"LEE BENG CHUAN\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96306323\\",\\"DisplayPriority\\":2},{\\"ID\\":25,\\"CompanyID\\":\\"16\\",\\"NameEnglish\\":\\"CHEAH SOO MENG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"97576637\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 16, 1481873778317),
(62, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 3, 1481874192635),
(63, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1481877482369),
(64, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 9, 1481881188405),
(65, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 10, 1481881190717),
(66, 'BackendUser', 1, 'Update', '{"ID":10,"EventName":"hen is the Bryant Park Winter Village 2016?","EventDate":"October 29","EventFrom":"5:00","EventTo":"10:00","Description":"Out of all the things to do in the fall in NYC, no other event or holiday market receives as much hype as the Bryant Park Winter Village 2016. Why? Because once the 17,000-square-foot ice-skating rink and over 125 shopping kiosks and food vendors open for business, this means that Christmas in New York has officially arrived. This guide contains all the information you need to know, from new vendors and attractions to when the market is open. Enjoy the most wonderful time of the year!","Venue":"Bryant Park","Latitude":"1","Longitude":"1","CourseFee":"Free","Remarks":"Good","Pdf":"","Image":3453453,"DisplayPriority":1,"CreatedTime":"1481881190717","LastUpdatedTime":1481881247102}', '{"ID":"10","EventName":"hen is the Bryant Park Winter Village 2016?","EventDate":"October 29","EventFrom":"5:00","EventTo":"10:00","Description":"Out of all the things to do in the fall in NYC, no other event or holiday market receives as much hype as the Bryant Park Winter Village 2016. Why? Because once the 17,000-square-foot ice-skating rink and over 125 shopping kiosks and food vendors open for business, this means that Christmas in New York has officially arrived. This guide contains all the information you need to know, from new vendors and attractions to when the market is open. Enjoy the most wonderful time of the year!","Venue":"Bryant Park","Latitude":"1","Longitude":"1","CourseFee":"Free","Remarks":"Good","DisplayPriority":"1","img":"undefined","doc_prev":"undefined","img_prev":"1481881190_Little_island_colour_logo_200x200.jpg"}', 'Event', 10, 1481881247102),
(67, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 11, 1481881396201),
(68, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 12, 1481881515259),
(69, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 13, 1481881700687),
(70, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 14, 1481881831088),
(71, 'BackendUser', 1, 'Update', '{"ID":9,"EventName":"hen is the Bryant Park Winter Village 2016?","EventDate":"October 29","EventFrom":"5:00","EventTo":"10:00","Description":"By the time you choose a costume at a Halloween store in NYC, The Bank of America Winter Village in Bryant Park will already be open for business. The merriest holiday market opens its doors starting Saturday, October 29. The Holiday Shops will be hawking seasonal gifts through January 2, 2017. The Rink and Public Fair will run through March 5, 2017.","Venue":"Bryant Park","Latitude":"1.666","Longitude":"143343","CourseFee":"Free","Remarks":"Good","Pdf":"","Image":3453453,"DisplayPriority":3,"CreatedTime":"1481881188405","LastUpdatedTime":1481881883011}', '{"ID":"9","EventName":"hen is the Bryant Park Winter Village 2016?","EventDate":"October 29","EventFrom":"5:00","EventTo":"10:00","Description":"By the time you choose a costume at a Halloween store in NYC, The Bank of America Winter Village in Bryant Park will already be open for business. The merriest holiday market opens its doors starting Saturday, October 29. The Holiday Shops will be hawking seasonal gifts through January 2, 2017. The Rink and Public Fair will run through March 5, 2017.","Venue":"Bryant Park","Latitude":"1.666","Longitude":"143343","CourseFee":"Free","Remarks":"Good","DisplayPriority":"3","doc_prev":"undefined","img_prev":"1481881188_Little_island_colour_logo_200x200.jpg"}', 'Event', 9, 1481881883011),
(72, 'BackendUser', 1, 'Update', '{"ID":12,"EventName":"Central Park, Wollman Rink","EventDate":"Oct 19","EventFrom":"5:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"midtown west","Latitude":"344444","Longitude":"7555656","CourseFee":"Free","Remarks":"Good","Pdf":"","Image":3453453,"DisplayPriority":4,"CreatedTime":"1481881515259","LastUpdatedTime":1481881970650}', '{"ID":"12","EventName":"Central Park, Wollman Rink","EventDate":"Oct 19","EventFrom":"5:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"midtown west","Latitude":"344444","Longitude":"7555656","CourseFee":"Free","Remarks":"Good","DisplayPriority":"4","doc_prev":"undefined","img_prev":"1481881515_Little_island_colour_logo_200x200.jpg"}', 'Event', 12, 1481881970650),
(73, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 1, 1481882601998),
(74, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 2, 1481882694322),
(75, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 3, 1481882747014),
(76, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 4, 1481882803634),
(77, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 5, 1481882863360),
(78, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 6, 1481882920019),
(79, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 7, 1481882967447),
(80, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 8, 1481883017704),
(81, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 9, 1481883073619),
(82, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 10, 1481883114748),
(83, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 11, 1481883187613),
(84, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 12, 1481883265089),
(85, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 13, 1481883315423),
(86, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 14, 1481883373782),
(87, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 15, 1481883447222),
(88, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 16, 1481883529400),
(89, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 17, 1481883582031),
(90, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 18, 1481883671265),
(91, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 19, 1481883782817),
(92, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 20, 1481883829338),
(93, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 21, 1481883877573),
(94, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 22, 1481883939289),
(95, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 23, 1481883980898),
(96, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 24, 1481884013615),
(97, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 25, 1481884059103),
(98, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 26, 1481884101049),
(99, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 27, 1481884125174),
(100, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 4, 1482042896512),
(101, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","Image":"1481882601_default-avatar.png","DisplayPriority":1,"CreatedTime":"1481882601998","LastUpdatedTime":1482045494398}', '{"ID":"1","Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","DisplayPriority":"1","img":"undefined","img_prev":"1481882601_default-avatar.png"}', 'Boc', 1, 1482045494398),
(102, 'BackendUser', 1, 'Update', '{"ID":4,"Name":"z update","URL":"www.test.com","Image":"1482042896_avatar-2.png","DisplayPriority":1,"CreatedTime":"1482042896512","LastUpdatedTime":"1482042896512"}', '{"ID":"4","Name":"z update","URL":"www.test.com","DisplayPriority":"1","img":"undefined","img_prev":"1482042896_avatar-2.png"}', 'Advertisement', 4, 1482045528512),
(103, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","Image":"1481882601_default-avatar.png","DisplayPriority":1,"CreatedTime":"1481882601998","LastUpdatedTime":1482045546889}', '{"ID":"1","Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","DisplayPriority":"1","img":"undefined","img_prev":"1481882601_default-avatar.png"}', 'Boc', 1, 1482045546889),
(104, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","Image":"1481882601_default-avatar.png","DisplayPriority":1,"CreatedTime":"1481882601998","LastUpdatedTime":1482045596913}', '{"ID":"1","Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","DisplayPriority":"1","img":"undefined","img_prev":"1481882601_default-avatar.png"}', 'Boc', 1, 1482045596913),
(105, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046262458}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":1,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046262458),
(106, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046360243}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":36,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046360243),
(107, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046388661}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":37,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046388661),
(108, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046430289}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":38,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046430289),
(109, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046550463}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":39,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046550463),
(110, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046602853}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":40,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046602853),
(111, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046669404}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":41,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046669404),
(112, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046697748}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":42,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046697748),
(113, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046745838}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":42,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046745838),
(114, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482046768274}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":42,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482046768274),
(115, 'BackendUser', 1, 'Update', '{"ID":4,"CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"???????????","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"aa.aa.aa","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","ProductsServicesChinese":null,"DisplayPriority":2,"CreatedTime":"1481622040739","LastUpdatedTime":1482046782931}', '{"ID":"4","CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"???????????","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"aa.aa.aa","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","DisplayPriority":"2","Representatives":"[{\\"ID\\":4,\\"CompanyID\\":\\"4\\",\\"NameEnglish\\":\\"LEE BEE HOON\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"63672703\\",\\"DisplayPriority\\":1},{\\"ID\\":5,\\"CompanyID\\":\\"4\\",\\"NameEnglish\\":\\"ANG TIAN KIAT\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"63676614\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 4, 1482046782931),
(116, 'BackendUser', 1, 'Update', '{"ID":2,"CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"undefined","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"aaa.aa.a","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","ProductsServicesChinese":null,"DisplayPriority":10,"CreatedTime":"1481621723095","LastUpdatedTime":1482046791595}', '{"ID":"2","CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"undefined","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"aaa.aa.a","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","DisplayPriority":"10","Representatives":"[{\\"ID\\":2,\\"CompanyID\\":\\"2\\",\\"NameEnglish\\":\\"KASIM TAIYEBALI\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96170071\\",\\"DisplayPriority\\":1},{\\"ID\\":3,\\"CompanyID\\":\\"2\\",\\"NameEnglish\\":\\"SHABBIR Z. SHAKIR\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"90093997\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 2, 1482046791595),
(117, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482047005605}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":45,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482047005605),
(118, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482047028111}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":48,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482047028111),
(119, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482047062443}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":49,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482047062443),
(120, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482067259239),
(121, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482080642006),
(122, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482134760929),
(123, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482134827587),
(124, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482136738189),
(125, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482137191351),
(126, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482138632809),
(127, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482138931722),
(128, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481620992252","LastUpdatedTime":1482139059922}', '{"ID":"1","CompanyNameEnglish":"AEONIC INTERNATIONAL TRADE PTE LTD","CompanyNameChinese":"","Address":"20 Maxwell Road #03-08, Maxwell House, Singapore 069113","Phone":"62227518","Fax":"62242817","Email":"aeonic@singnet.com.sg","Website":"www.aeonic.com.sg","Registration":"189600411C","ProductsServicesEnglish":"Marketing all kinf of wood products,Internationally & genaral trading.","DisplayPriority":"1","Representatives":"[{\\"ID\\":50,\\"CompanyID\\":\\"1\\",\\"NameEnglish\\":\\"FRANCIS NG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"62227518\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 1, 1482139059922),
(129, 'BackendUser', 1, 'Update', '{"ID":2,"CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"aaa.aa.a","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481621723095","LastUpdatedTime":1482139130911}', '{"ID":"2","CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"aaa.aa.a","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","DisplayPriority":"10","Representatives":"[{\\"ID\\":47,\\"CompanyID\\":\\"2\\",\\"NameEnglish\\":\\"SHABBIR Z. SHAKIR\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"90093997\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 2, 1482139130911),
(130, 'BackendUser', 1, 'Update', '{"ID":3,"CompanyNameEnglish":"ALKEMAL SINGAPORE PTE LTD","CompanyNameChinese":"","Address":"3 Shenton Way # 12-01A, Shenton House, Singapore 068805","Phone":"62230239","Fax":"62250678","Email":"daljit@alkemal.com.sg","Website":"www.alkemal.com.sg","Registration":"198200704K","ProductsServicesEnglish":"Timber Trading.","ProductsServicesChinese":"na","DisplayPriority":2,"CreatedTime":"1481621881879","LastUpdatedTime":1482139192353}', '{"ID":"3","CompanyNameEnglish":"ALKEMAL SINGAPORE PTE LTD","CompanyNameChinese":"","Address":"3 Shenton Way # 12-01A, Shenton House, Singapore 068805","Phone":"62230239","Fax":"62250678","Email":"daljit@alkemal.com.sg","Website":"www.alkemal.com.sg","Registration":"198200704K","ProductsServicesEnglish":"Timber Trading.","DisplayPriority":"2","Representatives":"[]","Associates":"[]"}', 'Company', 3, 1482139192353),
(131, 'BackendUser', 1, 'Update', '{"ID":4,"CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"\\u6d2a\\u5929\\u5409\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"aa.aa.aa","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","ProductsServicesChinese":"na","DisplayPriority":2,"CreatedTime":"1481622040739","LastUpdatedTime":1482139234183}', '{"ID":"4","CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"\\u6d2a\\u5929\\u5409\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"aa.aa.aa","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","DisplayPriority":"2","Representatives":"[{\\"ID\\":46,\\"CompanyID\\":\\"4\\",\\"NameEnglish\\":\\"ANG TIAN KIAT\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"63676614\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 4, 1482139234183),
(132, 'BackendUser', 1, 'Update', '{"ID":5,"CompanyNameEnglish":"AUSTRALIAN TIMBER PRODUCTS PTE LTD","CompanyNameChinese":"","Address":"605A MacPherson Road, #06-01,Citimac Industrial Complex,Singapore 368240","Phone":"62894425","Fax":"62875935","Email":"atpr@atp.com.sg","Website":"aa.aa.aa","Registration":"199508025M","ProductsServicesEnglish":"Supply & Install Timber","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481623309901","LastUpdatedTime":1482139257711}', '{"ID":"5","CompanyNameEnglish":"AUSTRALIAN TIMBER PRODUCTS PTE LTD","CompanyNameChinese":"","Address":"605A MacPherson Road, #06-01,Citimac Industrial Complex,Singapore 368240","Phone":"62894425","Fax":"62875935","Email":"atpr@atp.com.sg","Website":"aa.aa.aa","Registration":"199508025M","ProductsServicesEnglish":"Supply & Install Timber","DisplayPriority":"10","Representatives":"[{\\"ID\\":6,\\"CompanyID\\":\\"5\\",\\"NameEnglish\\":\\"THIEW MENG KHAI\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"97678885\\",\\"DisplayPriority\\":10},{\\"ID\\":7,\\"CompanyID\\":\\"5\\",\\"NameEnglish\\":\\"REAGAN PAUL THIEW\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"82334972\\",\\"DisplayPriority\\":5}]","Associates":"[]"}', 'Company', 5, 1482139257711),
(133, 'BackendUser', 1, 'Update', '{"ID":2,"CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481621723095","LastUpdatedTime":1482139435823}', '{"ID":"2","CompanyNameEnglish":"AKBARWOOD ENTERPRISES PTE LTD","CompanyNameChinese":"","Address":"180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546","Phone":"62260254","Fax":"62225943","Email":"akbarwood@pacific.net.sg","Website":"","Registration":"198301171Z","ProductsServicesEnglish":"Sawn Timber, Plywood, Logs, Wood Products.","DisplayPriority":"10","Representatives":"[{\\"ID\\":52,\\"CompanyID\\":\\"2\\",\\"NameEnglish\\":\\"SHABBIR Z. SHAKIR\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"90093997\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 2, 1482139435823),
(134, 'BackendUser', 1, 'Update', '{"ID":4,"CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"\\u6d2a\\u5929\\u5409\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","ProductsServicesChinese":"na","DisplayPriority":2,"CreatedTime":"1481622040739","LastUpdatedTime":1482139480084}', '{"ID":"4","CompanyNameEnglish":"ANG TIAN KIAT TRADING PTE LTD","CompanyNameChinese":"\\u6d2a\\u5929\\u5409\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"49 Sungei Kadut Street 1,Singapore 729351","Phone":"63672703","Fax":"63676614","Email":"ang-tk@singnet.com.sg","Website":"","Registration":"199300687Z","ProductsServicesEnglish":"Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.","DisplayPriority":"2","Representatives":"[{\\"ID\\":53,\\"CompanyID\\":\\"4\\",\\"NameEnglish\\":\\"ANG TIAN KIAT\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"63676614\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 4, 1482139480084),
(135, 'BackendUser', 1, 'Update', '{"ID":5,"CompanyNameEnglish":"AUSTRALIAN TIMBER PRODUCTS PTE LTD","CompanyNameChinese":"","Address":"605A MacPherson Road, #06-01,Citimac Industrial Complex,Singapore 368240","Phone":"62894425","Fax":"62875935","Email":"atpr@atp.com.sg","Website":"","Registration":"199508025M","ProductsServicesEnglish":"Supply & Install Timber","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481623309901","LastUpdatedTime":1482139500754}', '{"ID":"5","CompanyNameEnglish":"AUSTRALIAN TIMBER PRODUCTS PTE LTD","CompanyNameChinese":"","Address":"605A MacPherson Road, #06-01,Citimac Industrial Complex,Singapore 368240","Phone":"62894425","Fax":"62875935","Email":"atpr@atp.com.sg","Website":"","Registration":"199508025M","ProductsServicesEnglish":"Supply & Install Timber","DisplayPriority":"10","Representatives":"[{\\"ID\\":54,\\"CompanyID\\":\\"5\\",\\"NameEnglish\\":\\"THIEW MENG KHAI\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"97678885\\",\\"DisplayPriority\\":10},{\\"ID\\":55,\\"CompanyID\\":\\"5\\",\\"NameEnglish\\":\\"REAGAN PAUL THIEW\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"82334972\\",\\"DisplayPriority\\":5}]","Associates":"[]"}', 'Company', 5, 1482139500754),
(136, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482139591390}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 6, 1482139591390),
(137, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482139765907),
(138, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482139961601}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 6, 1482139961601);
INSERT INTO `sta_ActivityLog` (`ID`, `UserType`, `UserID`, `Action`, `OldValue`, `NewValue`, `ModelType`, `ModelID`, `CreatedDateTime`) VALUES
(139, 'BackendUser', 1, 'Update', '{"ID":7,"CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"????????????","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"aa.aa.aa","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1481623897139","LastUpdatedTime":1482140077996}', '{"ID":"7","CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"????????????","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"aa.aa.aa","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","DisplayPriority":"12","Representatives":"[{\\"ID\\":10,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KATHERINE KOH LAY YEN \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"98329970\\",\\"DisplayPriority\\":1},{\\"ID\\":11,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KOH LEONG SHENG \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96674095\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 7, 1482140077996),
(140, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482140211257),
(141, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482140233877),
(142, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482140280389}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 6, 1482140280389),
(143, 'BackendUser', 1, 'Update', '{"ID":7,"CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"????????????","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"aa.aa.aa","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1481623897139","LastUpdatedTime":1482140290060}', '{"ID":"7","CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"????????????","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"aa.aa.aa","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","DisplayPriority":"12","Representatives":"[{\\"ID\\":61,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KATHERINE KOH LAY YEN \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"98329970\\",\\"DisplayPriority\\":1},{\\"ID\\":62,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KOH LEONG SHENG \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96674095\\",\\"DisplayPriority\\":2},{\\"NameEnglish\\":\\"asdfsadf\\",\\"DisplayPriority\\":\\"345\\",\\"Phone\\":\\"98329970\\"}]","Associates":"[]"}', 'Company', 7, 1482140290060),
(144, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482140305330}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"a\\",\\"Phone\\":\\"98745631\\",\\"DisplayPriority\\":\\"1\\"}]","Associates":"[]"}', 'Company', 6, 1482140305330),
(145, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482140329663}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 6, 1482140329663),
(146, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482140844815),
(147, 'BackendUser', 1, 'Update', '{"ID":6,"CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","ProductsServicesChinese":"na","DisplayPriority":1,"CreatedTime":"1481623698962","LastUpdatedTime":1482141452066}', '{"ID":"6","CompanyNameEnglish":"CARLTECH TRADING & INDUSTRIES PTE LTD","CompanyNameChinese":"\\u5609\\u8fbe\\u8d38\\u6613\\u5de5\\u4e1a\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370","Phone":"63671188","Fax":"63672233","Email":"carltech@singnet.com.sg","Website":"","Registration":"198900243D","ProductsServicesEnglish":"Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"DAVID TAN CHIN BEE\\",\\"DisplayPriority\\":\\"1\\"},{\\"NameEnglish\\":\\"RICHARD TAN CHIN YAU\\",\\"DisplayPriority\\":\\"2\\"}]","Associates":"[]"}', 'Company', 6, 1482141452066),
(148, 'BackendUser', 1, 'Update', '{"ID":7,"CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"\\u4e16\\u7eaa\\u94a2\\u94c1\\u56fd\\u9645\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1481623897139","LastUpdatedTime":1482141479012}', '{"ID":"7","CompanyNameEnglish":"CENTENNIAL METALS INTERNATIONAL PTE LTD","CompanyNameChinese":"\\u4e16\\u7eaa\\u94a2\\u94c1\\u56fd\\u9645\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"No. 11 Sungei Kadut St 5,Singapore 728957","Phone":"63621121","Fax":"63621131","Email":"info@centennialmetals.com","Website":"","Registration":"200709921W","ProductsServicesEnglish":"We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.","DisplayPriority":"12","Representatives":"[{\\"ID\\":67,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KATHERINE KOH LAY YEN \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"98329970\\",\\"DisplayPriority\\":1},{\\"ID\\":68,\\"CompanyID\\":\\"7\\",\\"NameEnglish\\":\\"KOH LEONG SHENG \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96674095\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 7, 1482141479012),
(149, 'BackendUser', 1, 'Update', '{"ID":8,"CompanyNameEnglish":"CHEN HOCK HENG MACHINERY PTE LTD","CompanyNameChinese":"\\u632f\\u798f\\u5174\\u673a\\u68b0\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CHEN HOCK HENG MACHINERY PTE LTD64 Sungei Kadut Loop,Singapore 729493","Phone":"63684288","Fax":"63682655","Email":"linda@chhforklift.com.sg","Website":"www.chhforklift.com.sg","Registration":"198904556W","ProductsServicesEnglish":"Sales of new & reconditional forklifts.Trade-in for all model of forklifts.Daily, weekly and monthly basis rental.","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481624646731","LastUpdatedTime":1482141570071}', '{"ID":"8","CompanyNameEnglish":"CHEN HOCK HENG MACHINERY PTE LTD","CompanyNameChinese":"\\u632f\\u798f\\u5174\\u673a\\u68b0\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"CHEN HOCK HENG MACHINERY PTE LTD64 Sungei Kadut Loop,Singapore 729493","Phone":"63684288","Fax":"63682655","Email":"linda@chhforklift.com.sg","Website":"www.chhforklift.com.sg","Registration":"198904556W","ProductsServicesEnglish":"Sales of new & reconditional forklifts.Trade-in for all model of forklifts.Daily, weekly and monthly basis rental.","DisplayPriority":"10","Representatives":"[{\\"ID\\":12,\\"CompanyID\\":\\"8\\",\\"NameEnglish\\":\\"ONG BEE CHEW\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96399148\\",\\"DisplayPriority\\":1},{\\"ID\\":13,\\"CompanyID\\":\\"8\\",\\"NameEnglish\\":\\"LINDA NEO\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96217158\\",\\"DisplayPriority\\":2}]","Associates":"[]"}', 'Company', 8, 1482141570071),
(150, 'BackendUser', 1, 'Update', '{"ID":9,"CompanyNameEnglish":"CHENG FONG ENTERPRISES (S) PTE  LTD","CompanyNameChinese":"\\u6b63\\u4e30\\u4f01\\u4e1a\\uff08\\u661f\\uff09\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"39 Sungei Kadut Street 4,Singapore 729059","Phone":"63672128","Fax":"68916916","Email":"info.chengfong@gmail.com","Website":"","Registration":"198901738N","ProductsServicesEnglish":"Specialise In Supply of Chengal,Balau, Kapur, Keruing,Plywood & Other Timber Products","ProductsServicesChinese":"na","DisplayPriority":5,"CreatedTime":"1481624907001","LastUpdatedTime":1482141594600}', '{"ID":"9","CompanyNameEnglish":"CHENG FONG ENTERPRISES (S) PTE  LTD","CompanyNameChinese":"\\u6b63\\u4e30\\u4f01\\u4e1a\\uff08\\u661f\\uff09\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"39 Sungei Kadut Street 4,Singapore 729059","Phone":"63672128","Fax":"68916916","Email":"info.chengfong@gmail.com","Website":"","Registration":"198901738N","ProductsServicesEnglish":"Specialise In Supply of Chengal,Balau, Kapur, Keruing,Plywood & Other Timber Products","DisplayPriority":"5","Representatives":"[{\\"ID\\":14,\\"CompanyID\\":\\"9\\",\\"NameEnglish\\":\\"ANG BANG YAO\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"83388888\\",\\"DisplayPriority\\":10}]","Associates":"[]"}', 'Company', 9, 1482141594600),
(151, 'BackendUser', 1, 'Update', '{"ID":10,"CompanyNameEnglish":"CHIANG LENG HUP PLYWOOD","CompanyNameChinese":"\\u660c\\u9f99\\u5408\\u5939\\u677f\\u5de5\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"INDUSTRIES PTE LTD,10, Sungei Kadut Street 6,Singapore 728857","Phone":"63686139","Fax":"63683893","Email":"chianglh@singnet.com.sg","Website":"www.chianglenghup.com","Registration":"199508826Z","ProductsServicesEnglish":"Plywood, Blockboard, Laminboard, Natural Fancy Plywood & Veneer,Engineered Fancy Plywood & Veneer, Chipboard, MDF, odgrain\\/ ,Colour PVC Laminated Board, Timber etc.","ProductsServicesChinese":"na","DisplayPriority":20,"CreatedTime":"1481625061048","LastUpdatedTime":1482141654877}', '{"ID":"10","CompanyNameEnglish":"CHIANG LENG HUP PLYWOOD","CompanyNameChinese":"\\u660c\\u9f99\\u5408\\u5939\\u677f\\u5de5\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"INDUSTRIES PTE LTD,10, Sungei Kadut Street 6,Singapore 728857","Phone":"63686139","Fax":"63683893","Email":"chianglh@singnet.com.sg","Website":"www.chianglenghup.com","Registration":"199508826Z","ProductsServicesEnglish":"Plywood, Blockboard, Laminboard, Natural Fancy Plywood & Veneer,Engineered Fancy Plywood & Veneer, Chipboard, MDF, odgrain\\/ ,Colour PVC Laminated Board, Timber etc.","DisplayPriority":"20","Representatives":"[{\\"ID\\":15,\\"CompanyID\\":\\"10\\",\\"NameEnglish\\":\\"JIMMY LIM HUAY SOON\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96719949\\",\\"DisplayPriority\\":5},{\\"ID\\":16,\\"CompanyID\\":\\"10\\",\\"NameEnglish\\":\\"LIM ENG HOCK\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96284034\\",\\"DisplayPriority\\":1}]","Associates":"[]"}', 'Company', 10, 1482141654877),
(152, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 23, 1482141878739),
(153, 'BackendUser', 1, 'Update', '{"ID":11,"CompanyNameEnglish":"EI CORPORATION PTE LTD","CompanyNameChinese":"","Address":"No. 27, Sungei Kadut St 2,Singapore 729240","Phone":"63621006","Fax":"63621006","Email":"service@eicorp.com.sg","Website":"","Registration":"197602185W","ProductsServicesEnglish":"Manufacture of Builder Carpentry and Joinery NEC.Manufacture of Doors and Fabrication of Metal & Steel Products.Trading of Timber, Plywood, Steel Bar, Wiremesh & Other Construction Material.","ProductsServicesChinese":"na","DisplayPriority":20,"CreatedTime":"1481625512537","LastUpdatedTime":1482141925912}', '{"ID":"11","CompanyNameEnglish":"EI CORPORATION PTE LTD","CompanyNameChinese":"","Address":"No. 27, Sungei Kadut St 2,Singapore 729240","Phone":"63621006","Fax":"63621006","Email":"service@eicorp.com.sg","Website":"","Registration":"197602185W","ProductsServicesEnglish":"Manufacture of Builder Carpentry and Joinery NEC.Manufacture of Doors and Fabrication of Metal & Steel Products.Trading of Timber, Plywood, Steel Bar, Wiremesh & Other Construction Material.","DisplayPriority":"20","Representatives":"[{\\"ID\\":17,\\"CompanyID\\":\\"11\\",\\"NameEnglish\\":\\"ANN LEE\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96301181\\",\\"DisplayPriority\\":20},{\\"ID\\":18,\\"CompanyID\\":\\"11\\",\\"NameEnglish\\":\\"SHAUN LEE\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"97397341\\",\\"DisplayPriority\\":11}]","Associates":"[]"}', 'Company', 11, 1482141925912),
(154, 'BackendUser', 1, 'Update', '{"ID":12,"CompanyNameEnglish":"EASTERN MARKETING CO PTE LTD","CompanyNameChinese":"","Address":"140 Paya Lebar Road, #10-04\\/05 AZ @Payalebar,Singapore 409015","Phone":"62266939","Fax":"67023428","Email":"EM@easternmktg.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Trading, Import And Export of Plywood, Blockboard, M.D.F. Timber,Logs, Furniture And Other Timber,Related Products","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1481625704346","LastUpdatedTime":1482141965606}', '{"ID":"12","CompanyNameEnglish":"EASTERN MARKETING CO PTE LTD","CompanyNameChinese":"","Address":"140 Paya Lebar Road, #10-04\\/05 AZ @Payalebar,Singapore 409015","Phone":"62266939","Fax":"67023428","Email":"EM@easternmktg.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Trading, Import And Export of Plywood, Blockboard, M.D.F. Timber,Logs, Furniture And Other Timber,Related Products","DisplayPriority":"12","Representatives":"[{\\"NameEnglish\\":\\"GEORGE NADER\\",\\"DisplayPriority\\":\\"33\\"}]","Associates":"[]"}', 'Company', 12, 1482141965606),
(155, 'BackendUser', 1, 'Update', '{"ID":12,"EventName":"Central Park, Wollman Rink","EventDate":"20\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"344444","Longitude":"7555656","CourseFee":"50","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","Pdf":"","Image":"3453453","DisplayPriority":4,"CreatedTime":"1481881515259","LastUpdatedTime":1482141968200}', '{"ID":"12","EventName":"Central Park, Wollman Rink","EventDate":"20\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"344444","Longitude":"7555656","CourseFee":"50","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","DisplayPriority":"4","doc_prev":"undefined","img_prev":"3453453"}', 'Event', 12, 1482141968200),
(156, 'BackendUser', 1, 'Update', '{"ID":13,"CompanyNameEnglish":"EASTERN UNION TRADING & SAWMILL CO PTE LTD","CompanyNameChinese":"\\u4e1c\\u8054\\u6728\\u4e1a\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"70, Sungei Kadut Street 1,Singapore 729371","Phone":"62693061","Fax":"63678843","Email":"bonpock@yahoo.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Timber Preservation & Fire Retardant Treatment,Manufacturer of CCA Treated Timber Pile, Etc.","ProductsServicesChinese":"na","DisplayPriority":5,"CreatedTime":"1481625872390","LastUpdatedTime":1482142015723}', '{"ID":"13","CompanyNameEnglish":"EASTERN UNION TRADING & SAWMILL CO PTE LTD","CompanyNameChinese":"\\u4e1c\\u8054\\u6728\\u4e1a\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"70, Sungei Kadut Street 1,Singapore 729371","Phone":"62693061","Fax":"63678843","Email":"bonpock@yahoo.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Timber Preservation & Fire Retardant Treatment,Manufacturer of CCA Treated Timber Pile, Etc.","DisplayPriority":"5","Representatives":"[{\\"NameEnglish\\":\\"CHUNG YEN SEN\\",\\"DisplayPriority\\":\\"33\\"},{\\"NameEnglish\\":\\"KOH BON POCK PBM \\",\\"Phone\\":\\"96734485\\",\\"DisplayPriority\\":\\"55\\"}]","Associates":"[]"}', 'Company', 13, 1482142015723),
(157, 'BackendUser', 1, 'Update', '{"ID":14,"CompanyNameEnglish":"ENG SENG CEMENT PRODUCTS (PTE) LTD","CompanyNameChinese":"\\u6c38\\u76db\\u673a\\u5236\\u7070\\u7816\\u5382\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"7, Sungei Kadut Drive,Singapore 729560","Phone":"63673188","Fax":"63670189","Email":"escppl@singnet.com.sg","Website":"","Registration":"197201657D","ProductsServicesEnglish":"Manufacturing & Distriburtion of Building Materials Products : 1) Pre-cast Concrete Products 2) Pc Concrete Components 3) Lightweight Partition Panels 4) Roofing Slabs","ProductsServicesChinese":"na","DisplayPriority":10,"CreatedTime":"1481627227585","LastUpdatedTime":1482142046194}', '{"ID":"14","CompanyNameEnglish":"ENG SENG CEMENT PRODUCTS (PTE) LTD","CompanyNameChinese":"\\u6c38\\u76db\\u673a\\u5236\\u7070\\u7816\\u5382\\uff08\\u79c1\\u4eba\\uff09\\u6709\\u9650\\u516c\\u53f8","Address":"7, Sungei Kadut Drive,Singapore 729560","Phone":"63673188","Fax":"63670189","Email":"escppl@singnet.com.sg","Website":"","Registration":"197201657D","ProductsServicesEnglish":"Manufacturing & Distriburtion of Building Materials Products : 1) Pre-cast Concrete Products 2) Pc Concrete Components 3) Lightweight Partition Panels 4) Roofing Slabs","DisplayPriority":"10","Representatives":"[]","Associates":"[]"}', 'Company', 14, 1482142046194),
(158, 'BackendUser', 1, 'Update', '{"ID":15,"CompanyNameEnglish":"ETH ENTERPRISE PTE LTD","CompanyNameChinese":"\\u6c38\\u6cf0\\u548c\\u4f01\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"14, Gul Lane, Singapore 629412","Phone":"62614717","Fax":"62616918","Email":"sales@eth.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Construction Materials Suppliers, Timber Products: Dark Red Meranti,Merbau, Ramin, Keruing, Balau, Kempas. Plywood, wooden cases,Crates, Skids, Pallets And Packing Services.","ProductsServicesChinese":"na","DisplayPriority":2,"CreatedTime":"1481627366443","LastUpdatedTime":1482142080449}', '{"ID":"15","CompanyNameEnglish":"ETH ENTERPRISE PTE LTD","CompanyNameChinese":"\\u6c38\\u6cf0\\u548c\\u4f01\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"14, Gul Lane, Singapore 629412","Phone":"62614717","Fax":"62616918","Email":"sales@eth.com.sg","Website":"","Registration":"11f","ProductsServicesEnglish":"Construction Materials Suppliers, Timber Products: Dark Red Meranti,Merbau, Ramin, Keruing, Balau, Kempas. Plywood, wooden cases,Crates, Skids, Pallets And Packing Services.","DisplayPriority":"2","Representatives":"[{\\"ID\\":22,\\"CompanyID\\":\\"15\\",\\"NameEnglish\\":\\"TOK CHING KWE \\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"98186236\\",\\"DisplayPriority\\":1},{\\"NameEnglish\\":\\"DANNY TOK\\",\\"DisplayPriority\\":\\"33\\"}]","Associates":"[]"}', 'Company', 15, 1482142080449),
(159, 'BackendUser', 1, 'Update', '{"ID":12,"EventName":"Central Park, Wollman Rink","EventDate":"30\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"344444","Longitude":"7555656","CourseFee":"$50 per participant","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","Pdf":"","Image":"3453453","DisplayPriority":4,"CreatedTime":"1481881515259","LastUpdatedTime":1482142586122}', '{"ID":"12","EventName":"Central Park, Wollman Rink","EventDate":"30\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"344444","Longitude":"7555656","CourseFee":"$50 per participant","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","DisplayPriority":"4","doc_prev":"undefined","img_prev":"3453453"}', 'Event', 12, 1482142586122),
(160, 'BackendUser', 1, 'Update', '{"ID":12,"EventName":"Central Park, Wollman Rink","EventDate":"30\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"1.3535405","Longitude":"103.8788134","CourseFee":"$50 per participant","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","Pdf":"","Image":"3453453","DisplayPriority":4,"CreatedTime":"1481881515259","LastUpdatedTime":1482143207917}', '{"ID":"12","EventName":"Central Park, Wollman Rink","EventDate":"30\\/12\\/2016","EventFrom":"05:00","EventTo":"10:00","Description":"If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won\\u2019t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you\\u2019re a skating greenhorn yourself, take heart\\u2014Trump Rink is home to the largest learn-to-skate program in the country.","Venue":"88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888","Latitude":"1.3535405","Longitude":"103.8788134","CourseFee":"$50 per participant","Remarks":"3-day course. Certificates of attendance will be given to participants who successfully complete the course.","DisplayPriority":"4","img":"undefined","doc_prev":"undefined","img_prev":"3453453"}', 'Event', 12, 1482143207917),
(161, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 15, 1482143483839),
(162, 'BackendUser', 1, 'Update', '{"ID":15,"EventName":"Test","EventDate":"04\\/07\\/2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"41","Longitude":"45","CourseFee":"500","Remarks":"Test","Pdf":"","Image":3453453,"DisplayPriority":0,"CreatedTime":"1482143483839","LastUpdatedTime":1482143542923}', '{"ID":"15","EventName":"Test","EventDate":"04\\/07\\/2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"41","Longitude":"45","CourseFee":"500","Remarks":"Test","DisplayPriority":"0","img":"undefined","doc_prev":"undefined","img_prev":"1482143483_about-page-office-room.jpg"}', 'Event', 15, 1482143542923),
(163, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 16, 1482144358899),
(164, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 17, 1482144650197),
(165, 'BackendUser', 1, 'Update', '{"ID":17,"EventName":"Test 3","EventDate":"04\\/02\\/2016","EventFrom":"01:00","EventTo":"02:00","Description":"Test 3","Venue":"Test","Latitude":"41","Longitude":"45","CourseFee":"325","Remarks":"Test","Pdf":"1482144650_WAPT_Report_TimeExtender_8-30-2016.pdf","Image":3453453,"DisplayPriority":1,"CreatedTime":"1482144650197","LastUpdatedTime":1482144700437}', '{"ID":"17","EventName":"Test 3","EventDate":"04\\/02\\/2016","EventFrom":"01:00","EventTo":"02:00","Description":"Test 3","Venue":"Test","Latitude":"41","Longitude":"45","CourseFee":"325","Remarks":"Test","DisplayPriority":"1","doc":"undefined","img":"undefined","doc_prev":"1482144650_WAPT_Report_TimeExtender_8-30-2016.pdf","img_prev":"1482144650_about-page-office-room.jpg"}', 'Event', 17, 1482144700437),
(166, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 18, 1482146155322),
(167, 'BackendUser', 1, 'Update', '{"ID":11,"EventName":"The best places to go ice-skating in NYC","EventDate":"11-11-2016","EventFrom":"17:00","EventTo":"24:00","Description":"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \\"de Finibus Bonorum et Malorum\\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \\"Lorem ipsum dolor sit amet..\\", comes from a line in section 1.10.32.\\r\\n\\r\\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \\"de Finibus Bonorum et Malorum\\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.","Venue":"NYC","Latitude":"1","Longitude":"1","CourseFee":"Free","Remarks":"Good","Pdf":"","Image":3453453,"DisplayPriority":0,"CreatedTime":"1481881396201","LastUpdatedTime":1482146378511}', '{"ID":"11","EventName":"The best places to go ice-skating in NYC","EventDate":"11-11-2016","EventFrom":"17:00","EventTo":"24:00","Description":"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \\"de Finibus Bonorum et Malorum\\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \\"Lorem ipsum dolor sit amet..\\", comes from a line in section 1.10.32.\\r\\n\\r\\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \\"de Finibus Bonorum et Malorum\\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.","Venue":"NYC","Latitude":"1","Longitude":"1","CourseFee":"Free","Remarks":"Good","DisplayPriority":"0","img":"undefined","doc_prev":"undefined","img_prev":"1481881396_Little_island_colour_logo_200x200.jpg"}', 'Event', 11, 1482146378511),
(168, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 19, 1482147236658),
(169, 'BackendUser', 1, 'Update', '{"ID":19,"EventName":"11","EventDate":"30-11-2016","EventFrom":"11:00","EventTo":"13:00","Description":"11","Venue":"11","Latitude":"11","Longitude":"11","CourseFee":"11","Remarks":"11","Pdf":"1482147236_testing-123.pdf","Image":3453453,"DisplayPriority":2222,"CreatedTime":"1482147236658","LastUpdatedTime":1482150847303}', '{"ID":"19","EventName":"11","EventDate":"30-11-2016","EventFrom":"11:00","EventTo":"13:00","Description":"11","Venue":"11","Latitude":"11","Longitude":"11","CourseFee":"11","Remarks":"11","DisplayPriority":"2222","doc_prev":"undefined","img_prev":"1482147236_White_Dove_Flight_PNG_Clipart.png"}', 'Event', 19, 1482150847303),
(170, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482155710069),
(171, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Private Outdoor","Title":null,"URL":"http:\\/\\/google.com","Image":"1482164907_banner-img.jpg","DisplayPriority":10,"CreatedTime":"1481699424372","LastUpdatedTime":1482164907738}', '{"ID":"1","Name":"Private Outdoor","URL":"http:\\/\\/google.com","DisplayPriority":"10","img_prev":"1.jpg"}', 'Advertisement', 1, 1482164907738),
(172, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Private Outdoor","Title":"","URL":"http:\\/\\/google.com","Image":"1482164954_room1.jpg","DisplayPriority":10,"CreatedTime":"1481699424372","LastUpdatedTime":1482164954407}', '{"ID":"1","Name":"Private Outdoor","URL":"http:\\/\\/google.com","DisplayPriority":"10","img_prev":"1482164907_banner-img.jpg"}', 'Advertisement', 1, 1482164954407),
(173, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 5, 1482166960696),
(174, 'BackendUser', 1, 'Update', '{"ID":5,"Name":"File upload test","Title":"Title test","URL":"www.title.com","Image":"1482167046_black-separator.png","DisplayPriority":1,"CreatedTime":"1482166960696","LastUpdatedTime":1482167046890}', '{"ID":"5","Name":"File upload test","Title":"Title test","URL":"www.title.com","DisplayPriority":"1","img_prev":"1482166960_banner-img.jpg"}', 'Advertisement', 5, 1482167046890),
(175, 'BackendUser', 1, 'Update', '{"ID":5,"Name":"File upload test","Title":"Title test","URL":"www.title.com","Image":"1482167046_black-separator.png","DisplayPriority":2,"CreatedTime":"1482166960696","LastUpdatedTime":1482167064326}', '{"ID":"5","Name":"File upload test","Title":"Title test","URL":"www.title.com","DisplayPriority":"2","img":"undefined","img_prev":"1482167046_black-separator.png"}', 'Advertisement', 5, 1482167064326),
(176, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","Image":"1482167461_banner-img.jpg","DisplayPriority":1,"CreatedTime":"1481882601998","LastUpdatedTime":1482167461479}', '{"ID":"1","Name":"Mr. Chua Seng Chong PBM","Designation":"President","Company":"Tat Hin Timber Pte Ltd","DisplayPriority":"1","img_prev":"1481882601_default-avatar.png"}', 'Boc', 1, 1482167461479),
(177, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 28, 1482167509522),
(178, 'BackendUser', 1, 'Update', '{"ID":28,"Name":"Image Upload","Designation":"Test","Company":"Test","Image":"1482167550_1.png","DisplayPriority":21,"CreatedTime":"1482167509522","LastUpdatedTime":1482167550595}', '{"ID":"28","Name":"Image Upload","Designation":"Test","Company":"Test","DisplayPriority":"21","img_prev":"1482167509_moonbean-bg.jpg"}', 'Boc', 28, 1482167550595),
(179, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 20, 1482168922366),
(180, 'BackendUser', 1, 'Update', '{"ID":20,"EventName":"Upload Component Test","EventDate":"25-12-2016","EventFrom":"23:11","EventTo":"24:00","Description":"Test","Venue":"Test","Latitude":"14","Longitude":"14","CourseFee":"250","Remarks":"Test","Pdf":"1482168922_WAPT_Report_TimeExtender_8-30-2016.pdf","Image":3453453,"DisplayPriority":1,"CreatedTime":"1482168922366","LastUpdatedTime":1482168968211}', '{"ID":"20","EventName":"Upload Component Test","EventDate":"25-12-2016","EventFrom":"23:11","EventTo":"24:00","Description":"Test","Venue":"Test","Latitude":"14","Longitude":"14","CourseFee":"250","Remarks":"Test","DisplayPriority":"1","doc_prev":"1482168922_WAPT_Report_TimeExtender_8-30-2016.pdf","img_prev":"1482168922_ad-commercial-outdoor.jpg"}', 'Event', 20, 1482168968211),
(181, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 21, 1482169221564),
(182, 'BackendUser', 1, 'Update', '{"ID":21,"EventName":"Test Test","EventDate":"07-08-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"54","Longitude":"85","CourseFee":"450","Remarks":"Test","Pdf":"1482169221_WAPT_Report_TimeExtender_8-30-2016.pdf","Image":3453453,"DisplayPriority":1,"CreatedTime":"1482169221564","LastUpdatedTime":1482169348668}', '{"ID":"21","EventName":"Test Test","EventDate":"07-08-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"54","Longitude":"85","CourseFee":"450","Remarks":"Test","DisplayPriority":"1","img":"undefined","doc_prev":"1482169221_WAPT_Report_TimeExtender_8-30-2016.pdf","img_prev":"1482169221_avatar-2.png"}', 'Event', 21, 1482169348668),
(183, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 24, 1482208667695),
(184, 'BackendUser', 1, 'Update', '{"ID":24,"CompanyNameEnglish":"GEY HWA TIMBER (S) PTE LTD","CompanyNameChinese":"\\u827a\\u534e\\u6728\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"57, Sungei Kadut Street 1, Singapore 729360","Phone":"63688010","Fax":"63688011","Email":"geyhwa@singnet.com.sg","Website":"undefined","Registration":"201110270D","ProductsServicesEnglish":"Manufacturing of Timbers \\/ Wooden products.","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1482208667695","LastUpdatedTime":1482209051137}', '{"ID":"24","CompanyNameEnglish":"GEY HWA TIMBER (S) PTE LTD","CompanyNameChinese":"\\u827a\\u534e\\u6728\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"57, Sungei Kadut Street 1, Singapore 729360","Phone":"63688010","Fax":"63688011","Email":"geyhwa@singnet.com.sg","Website":"undefined","Registration":"201110270D","ProductsServicesEnglish":"Manufacturing of Timbers \\/ Wooden products.","DisplayPriority":"12","Representatives":"[{\\"ID\\":88,\\"CompanyID\\":\\"24\\",\\"NameEnglish\\":\\"LIAU BOON HWA\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"98169828\\",\\"DisplayPriority\\":33},{\\"ID\\":89,\\"CompanyID\\":\\"24\\",\\"NameEnglish\\":\\"LIAU CHONG SENG\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"\\",\\"DisplayPriority\\":55},{\\"NameEnglish\\":\\"aa\\",\\"DisplayPriority\\":\\"3\\"}]","Associates":"[]"}', 'Company', 24, 1482209051137),
(185, 'BackendUser', 1, 'Update', '{"ID":24,"CompanyNameEnglish":"GEY HWA TIMBER (S) PTE LTD","CompanyNameChinese":"\\u827a\\u534e\\u6728\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"57, Sungei Kadut Street 1, Singapore 729360","Phone":"63688010","Fax":"63688011","Email":"geyhwa@singnet.com.sg","Website":"undefined","Registration":"201110270D","ProductsServicesEnglish":"Manufacturing of Timbers \\/ Wooden products.","ProductsServicesChinese":"na","DisplayPriority":12,"CreatedTime":"1482208667695","LastUpdatedTime":1482209070830}', '{"ID":"24","CompanyNameEnglish":"GEY HWA TIMBER (S) PTE LTD","CompanyNameChinese":"\\u827a\\u534e\\u6728\\u4e1a\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"57, Sungei Kadut Street 1, Singapore 729360","Phone":"63688010","Fax":"63688011","Email":"geyhwa@singnet.com.sg","Website":"undefined","Registration":"201110270D","ProductsServicesEnglish":"Manufacturing of Timbers \\/ Wooden products.","DisplayPriority":"12","Representatives":"[{\\"ID\\":90,\\"CompanyID\\":\\"24\\",\\"NameEnglish\\":\\"LIAU BOON HWA\\",\\"NameChinese\\":\\"-\\",\\"Phone\\":\\"98169828\\",\\"DisplayPriority\\":33},{\\"ID\\":91,\\"CompanyID\\":\\"24\\",\\"NameEnglish\\":\\"LIAU CHONG SENG\\",\\"NameChinese\\":\\"-\\",\\"Phone\\":\\"\\",\\"DisplayPriority\\":55}]","Associates":"[]"}', 'Company', 24, 1482209070830),
(186, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 28, 1482209258115),
(187, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 29, 1482209511846),
(188, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 30, 1482209917997),
(189, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 31, 1482210044928),
(190, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 32, 1482210262757),
(191, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 33, 1482210422643),
(192, 'BackendUser', 1, 'Update', '{"ID":32,"CompanyNameEnglish":"HIN KEE TRADING CO PTE LTD","CompanyNameChinese":"\\u5174\\u8bb0\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"6, Angklong Lane, #01-01,  Faber Garden Condo, Singapore 579980","Phone":"67388371","Fax":"62538573","Email":"hnksin@singnet.com.sg","Website":"undefined","Registration":"197802216C","ProductsServicesEnglish":"Import & Export.","ProductsServicesChinese":"na","DisplayPriority":98,"CreatedTime":"1482210262757","LastUpdatedTime":1482210491805}', '{"ID":"32","CompanyNameEnglish":"HIN KEE TRADING CO PTE LTD","CompanyNameChinese":"\\u5174\\u8bb0\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"6, Angklong Lane, #01-01,  Faber Garden Condo, Singapore 579980","Phone":"67388371","Fax":"62538573","Email":"hnksin@singnet.com.sg","Website":"undefined","Registration":"197802216C","ProductsServicesEnglish":"Import & Export.","DisplayPriority":"98","Representatives":"[{\\"ID\\":102,\\"CompanyID\\":\\"32\\",\\"NameEnglish\\":\\"ANG LAY WAH\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"96383167\\",\\"DisplayPriority\\":88}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"HNK(S) Pte Ltd\\",\\"DisplayPriority\\":\\"33\\"}]"}', 'Company', 32, 1482210491805),
(193, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 34, 1482210592019),
(194, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482210854164),
(195, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 35, 1482210882795),
(196, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 36, 1482210986592),
(197, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 37, 1482212492403),
(198, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 38, 1482212745628),
(199, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 39, 1482212854750),
(200, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 40, 1482212935996),
(201, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 41, 1482213062220),
(202, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 42, 1482213242077),
(203, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 43, 1482213397004),
(204, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 44, 1482213513690),
(205, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 45, 1482213582594),
(206, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 46, 1482213888682),
(207, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 47, 1482213974935),
(208, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 48, 1482214099113),
(209, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 49, 1482214273377),
(210, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 50, 1482214410324),
(211, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 51, 1482214535922),
(212, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 52, 1482214626510),
(213, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 53, 1482214775444),
(214, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 54, 1482214871090),
(215, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 55, 1482215096131),
(216, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 56, 1482215205723),
(217, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 57, 1482215331286),
(218, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 58, 1482215432824),
(219, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 59, 1482215540235),
(220, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 60, 1482215637391),
(221, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 61, 1482215971069),
(222, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 62, 1482216170641),
(223, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 63, 1482216266636),
(224, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 64, 1482216369712),
(225, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 65, 1482216454295),
(226, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 66, 1482216546518),
(227, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 67, 1482216701547),
(228, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 68, 1482216806226),
(229, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 69, 1482216889479),
(230, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 70, 1482216951052),
(231, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 71, 1482217086282),
(232, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 72, 1482217235780),
(233, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 73, 1482217315848),
(234, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 74, 1482217405795),
(235, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 75, 1482217472696),
(236, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 76, 1482217581851),
(237, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 77, 1482217673077),
(238, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482217717359),
(239, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 78, 1482217841344),
(240, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 79, 1482217903916),
(241, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 80, 1482217981771),
(242, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 81, 1482218062363),
(243, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482218487397),
(244, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482219446152),
(245, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482219807882),
(246, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482220202225),
(247, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482220574936),
(248, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482221293218),
(249, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1482221771553),
(250, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 82, 1482222646262),
(251, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 83, 1482222852871),
(252, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 84, 1482222941587),
(253, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 85, 1482223095594),
(254, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 86, 1482223223981),
(255, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 6, 1482223330228),
(256, 'BackendUser', 1, 'Update', '{"ID":6,"Name":"Upload test 1","Title":"Lorem Ipsum Dolar","URL":"www.lorem.com","Image":"1482223361_black-separator.png","DisplayPriority":1,"CreatedTime":"1482223330228","LastUpdatedTime":1482223361101}', '{"ID":"6","Name":"Upload test 1","Title":"Lorem Ipsum Dolar","URL":"www.lorem.com","DisplayPriority":"1","img_prev":"1482223330_banner-img.jpg"}', 'Advertisement', 6, 1482223361101),
(257, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 87, 1482223406960),
(258, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 7, 1482223411119),
(259, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 88, 1482223618603),
(260, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 89, 1482223745102),
(261, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 90, 1482223886637),
(262, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 91, 1482223962491),
(263, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 92, 1482224053981),
(264, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 93, 1482224131937),
(265, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 94, 1482224203802),
(266, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 95, 1482224327903),
(267, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 96, 1482224434562),
(268, 'BackendUser', 1, 'Update', '{"ID":96,"CompanyNameEnglish":"THIAM PENG TRADING PTE LTD","CompanyNameChinese":"\\u6dfb\\u5e73\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"No. 5, Sungei Kadut Way, Singapore 728781","Phone":"63681133","Fax":"63682628","Email":"sktpt@singnet.com.sg","Website":"undefined","Registration":"197800638N","ProductsServicesEnglish":"Specialise in all Building Materials, Plywood, MDF Board, Chipboard, Woodgrain \\/ Colour PVC Laminated Board. Natural & Reconstituted Veneer Plywood, Exotic  & Construction Timber etc.","ProductsServicesChinese":"na","DisplayPriority":54,"CreatedTime":"1482224434562","LastUpdatedTime":1482224450923}', '{"ID":"96","CompanyNameEnglish":"THIAM PENG TRADING PTE LTD","CompanyNameChinese":"\\u6dfb\\u5e73\\u8d38\\u6613\\u79c1\\u4eba\\u6709\\u9650\\u516c\\u53f8","Address":"No. 5, Sungei Kadut Way, Singapore 728781","Phone":"63681133","Fax":"63682628","Email":"sktpt@singnet.com.sg","Website":"undefined","Registration":"197800638N","ProductsServicesEnglish":"Specialise in all Building Materials, Plywood, MDF Board, Chipboard, Woodgrain \\/ Colour PVC Laminated Board. Natural & Reconstituted Veneer Plywood, Exotic  & Construction Timber etc.","DisplayPriority":"54","Representatives":"[{\\"ID\\":199,\\"CompanyID\\":\\"96\\",\\"NameEnglish\\":\\"KOH BOON THIAM\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"\\",\\"DisplayPriority\\":22},{\\"ID\\":200,\\"CompanyID\\":\\"96\\",\\"NameEnglish\\":\\"DEW KOH\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"\\",\\"DisplayPriority\\":32},{\\"ID\\":201,\\"CompanyID\\":\\"96\\",\\"NameEnglish\\":\\"GARY GOH\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"\\",\\"DisplayPriority\\":12}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"Thiam Peng Plywood Trading Pte Ltd\\",\\"DisplayPriority\\":\\"11\\"}]"}', 'Company', 96, 1482224450923),
(269, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 97, 1482224554897),
(270, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 98, 1482224718597),
(271, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 99, 1482224803708),
(272, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 22, 1482224869350),
(273, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 100, 1482224882119);
INSERT INTO `sta_ActivityLog` (`ID`, `UserType`, `UserID`, `Action`, `OldValue`, `NewValue`, `ModelType`, `ModelID`, `CreatedDateTime`) VALUES
(274, 'BackendUser', 1, 'Update', '{"ID":22,"EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","Pdf":"1482224869_sdf.pdf","Image":"1482224919_ad-commercial-outdoor.jpg","DisplayPriority":1,"CreatedTime":"1482224869350","LastUpdatedTime":1482224919194}', '{"ID":"22","EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","DisplayPriority":"1","doc_prev":"1482224869_sdf.pdf","img_prev":"1482224869_1.png"}', 'Event', 22, 1482224919194),
(275, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 101, 1482224974994),
(276, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 102, 1482225054489),
(277, 'BackendUser', 1, 'Update', '{"ID":22,"EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","Pdf":"1482224869_sdf.pdf","Image":"1482224919_ad-commercial-outdoor.jpg","DisplayPriority":1,"CreatedTime":"1482224869350","LastUpdatedTime":1482225131315}', '{"ID":"22","EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","DisplayPriority":"1","img":"undefined","doc_prev":"1482224869_sdf.pdf","img_prev":"1482224919_ad-commercial-outdoor.jpg"}', 'Event', 22, 1482225131315),
(278, 'BackendUser', 1, 'Update', '{"ID":22,"EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","Pdf":"1482224869_sdf.pdf","Image":"1482224919_ad-commercial-outdoor.jpg","DisplayPriority":1,"CreatedTime":"1482224869350","LastUpdatedTime":1482225207295}', '{"ID":"22","EventName":"New upload test","EventDate":"10-12-2016","EventFrom":"04:00","EventTo":"05:00","Description":"Test","Venue":"Test","Latitude":"145","Longitude":"52","CourseFee":"250","Remarks":"Test","DisplayPriority":"1","img":"undefined","doc_prev":"1482224869_sdf.pdf","img_prev":"1482224919_ad-commercial-outdoor.jpg"}', 'Event', 22, 1482225207295);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Advertisement`
--

CREATE TABLE `sta_Advertisement` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL DEFAULT '1',
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Advertisement`
--

INSERT INTO `sta_Advertisement` (`ID`, `Name`, `URL`, `Title`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'Private Outdoor', 'http://google.com', '', '1482164954_room1.jpg', 10, 1481699424372, 1482164954407),
(2, 'Commercial Outdoor', 'http://google.com', 'Timber Decor Specialist ', '2.jpg', 20, 1481873425216, 1481873425216),
(3, 'Test', 'www.test.com', '', '1481874192_avatar-2.png', 1, 1481874192635, 1481874192635),
(4, 'z update', 'www.test.com', '', '1482042896_avatar-2.png', 1, 1482042896512, 1482042896512),
(5, 'File upload test', 'www.title.com', 'Title test', '1482167046_black-separator.png', 2, 1482166960696, 1482167064326),
(6, 'Upload test 1', 'www.lorem.com', 'Lorem Ipsum Dolar', '1482223361_black-separator.png', 1, 1482223330228, 1482223361101),
(7, 'Cubes', 'www.cube.com', 'Cube Service', '1482223411_1.png', 1, 1482223411119, 1482223411119);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Associatecompany`
--

CREATE TABLE `sta_Associatecompany` (
  `ID` int(11) NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `AssociateCompanyNameEnglish` varchar(255) NOT NULL,
  `AssociateCompanyNameChinese` varchar(255) DEFAULT NULL,
  `DisplayPriority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Associatecompany`
--

INSERT INTO `sta_Associatecompany` (`ID`, `CompanyID`, `AssociateCompanyNameEnglish`, `AssociateCompanyNameChinese`, `DisplayPriority`) VALUES
(1, 29, 'Goodhill Enterprise (Cambodia) Ltd.', '', 2),
(2, 29, 'Johore Sawmills Sdn Bhd,', '', 3),
(3, 29, 'Sim Lee Huat Pte Ltd.', '', 43),
(4, 33, 'Yonyi Timber Sdn. Bhd.', '', 32),
(5, 32, 'HNK(S) Pte Ltd', '', 33),
(6, 37, 'Intrimex (Pte) Ltd', '', 66),
(7, 38, 'Hock Heng Timber Pte Ltd', '', 76),
(8, 38, 'Hock Hoe Heng Trading Pte Ltd', '', 76),
(9, 39, 'Pintatco Sdn Bhd', '', 90),
(10, 42, 'Hertslet Woodworking Pte Ltd', '', 34),
(11, 42, 'JS Door Manufacturer Pte Ltd', '', 76),
(12, 42, 'Hertslet Woodworking Sdn Bhd', '', 44),
(13, 44, 'Kim Lian Seng Trading Co. (Pte) Ltd', '', 56),
(14, 44, 'Kim Import & Export Corpn (Pte) Ltd', '', 98),
(15, 51, 'Anglo-American Corporation Sdn Bhd.', '', 77),
(16, 53, 'Kian Huat Timber Trading Pte Ltd', '', 45),
(17, 57, 'Kim Hiap Lee Co (Pte) Ltd', '', 33),
(18, 57, 'LHT Ecotech Resources Pte Ltd', '', 23),
(19, 59, 'Sheng Lim Timber Trading ( Malaysia )', '', 22),
(20, 72, 'Kim How Timber Trading Pte Ltd', '', 44),
(21, 72, 'Seng Kiat Industrial Supplies', '', 765),
(22, 72, 'Ying Chuan Timber Co,. Pte. Ltd', '', 678),
(23, 76, 'Sen Wan Capital Pte Ltd', '', 54),
(24, 76, 'Sen Wan Timber (M) Sdn Bhd', '', 66),
(25, 78, 'Kien Seng Long Trading Company', '', 333),
(26, 89, 'Sunrise Doors (KunShan) Co.Ltd. ', '', 33),
(27, 89, 'PT Sunwood Timber Industries', '', 43),
(28, 93, 'Tat Hin Builders Pte Ltd', '', 22),
(29, 96, 'Thiam Peng Plywood Trading Pte Ltd', '', 11),
(30, 100, 'Wason Resources Sdn Bhd (K.L),', '', 5),
(31, 100, 'Wason (Far East) Limited   (H.K),', '', 3),
(32, 100, 'Nanzai Shoji Co Ltd  (Japan).', '', 6),
(33, 101, 'Decor Viz System Pte Ltd', '', 33);

-- --------------------------------------------------------

--
-- Table structure for table `sta_BackendUser`
--

CREATE TABLE `sta_BackendUser` (
  `ID` int(11) NOT NULL,
  `FirstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PasswordHash` text COLLATE utf8_unicode_ci NOT NULL,
  `PersistentLoginHashes` text COLLATE utf8_unicode_ci,
  `RoleID` int(11) NOT NULL,
  `LastUpdatedDateTime` bigint(20) UNSIGNED NOT NULL,
  `Status` enum('Active','Disabled','Delete') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sta_BackendUser`
--

INSERT INTO `sta_BackendUser` (`ID`, `FirstName`, `LastName`, `Email`, `PasswordHash`, `PersistentLoginHashes`, `RoleID`, `LastUpdatedDateTime`, `Status`) VALUES
(1, 'bkesh', 'maharjan', 'admin@sta.com', 'sha256:1000:XDrDbZCKZrS0wZCtzGwdnQuluZwCuaeg:uyYHY7DeuzSYmr8iCqPkm1hkPIghz7dZ', '', 1, 1482221771553, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `sta_Boc`
--

CREATE TABLE `sta_Boc` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Company` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Boc`
--

INSERT INTO `sta_Boc` (`ID`, `Name`, `Designation`, `Company`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'Mr. Chua Seng Chong PBM', 'President', 'Tat Hin Timber Pte Ltd', '1482167461_banner-img.jpg', 1, 1481882601998, 1482167461479),
(2, 'Mr. Tay Ghee Hian Paul', 'Vice President', 'Kim San Trading (Kilning) Pte Ltd', '1481882694_default-avatar.png', 3, 1481882694322, 1481882694322),
(3, 'Mr. Freddie Ng Heok Kwee', 'Vice President', 'Ng Guan Seng Woodworking Ind. P/L', '1481882747_default-avatar.png', 3, 1481882747014, 1481882747014),
(4, 'Mr. Low Boh Kee', 'Hon Secretary', 'Prime Timber Industries Pte Ltd', '1481882803_default-avatar.png', 3, 1481882803634, 1481882803634),
(5, 'Mr. Teoh Boon Chong ', 'Asst Secretary', 'Goodhill Enterprise (S) Pte Ltd', '1481882863_default-avatar.png', 3, 1481882863360, 1481882863360),
(6, 'Mr. Teng Kian Eng Desmond', 'Hon Treasurer', 'Kian Huat Timber Industries (1978) Pte Ltd', '1481882920_default-avatar.png', 44, 1481882920019, 1481882920019),
(7, 'Mr. Ng Lian.Seng', 'Asst Hon Treasurer', 'Plony Timber & Trading Co Pte Ltd', '1481882967_default-avatar.png', 6, 1481882967447, 1481882967447),
(8, 'Mr. Henry Sim Beng Huat ', 'Public Relation', 'NS Trading Pte Ltd', '1481883017_default-avatar.png', 65, 1481883017704, 1481883017704),
(9, 'Mr. Ang Bang Yao', 'Asst Public Relation ', 'Thiam Soon Woodproducts Pte Ltd', '1481883073_default-avatar.png', 43, 1481883073619, 1481883073619),
(10, 'Mr. Ong Chee Hien Sebastian ', 'Welfare', 'JS Timber Pte Ltd', '1481883114_default-avatar.png', 87, 1481883114748, 1481883114748),
(11, 'Mr. Wong Weng Kee George', 'Asst Welfare', 'Kim Timber (Pte) Ltd', '1481883187_default-avatar.png', 33, 1481883187613, 1481883187613),
(12, 'Mr. Lau Koi Fon', 'Executive Members', 'Kenwood Industries Pte Ltd', '1481883265_default-avatar.png', 54, 1481883265089, 1481883265089),
(13, 'Mr. Neo Koon Boo', 'Executive Members', 'LHT Holdings Limited', '1481883315_default-avatar.png', 22, 1481883315423, 1481883315423),
(14, 'Mr. Chong Ha Lee', 'Executive Members', 'Hong Huat Timber Trading Pte Ltd', '1481883373_default-avatar.png', 65, 1481883373782, 1481883373782),
(15, 'Mr. Koh Bon Pock PBM', 'Executive Members', 'Eastern Union Trading & Sawmill Co Pte Ltd', '1481883447_default-avatar.png', 43, 1481883447222, 1481883447222),
(16, 'Mr. Simon Oei Eng Hauw', 'Executive Members', 'Nature Wood Pte Ltd', '1481883529_default-avatar.png', 65, 1481883529400, 1481883529400),
(17, 'Mr. Ho Peng Hoe Peter', 'Executive Members', 'Kwong Maw Co (Pte) Ltd', '1481883582_default-avatar.png', 67, 1481883582031, 1481883582031),
(18, 'Mr. Chua Chin Cheng Michael', 'Internal Auditor', 'Hock Siong Huat (Pte) Ltd', '1481883671_default-avatar.png', 88, 1481883671265, 1481883671265),
(19, 'Mr. Choi Keok Bang PBM', 'Internal Auditor', 'Hup Huat Timber Co (S) Pte Ltd', '1481883782_default-avatar.png', 54, 1481883782817, 1481883782817),
(20, 'Mr. Chua Seng Chong PBM ', 'Trustees', 'Tat Hin limber Pte Ltd', '1481883829_default-avatar.png', 123, 1481883829338, 1481883829338),
(21, 'Mr. Chong Ha Lee', 'Trustees', 'Hong Huat Timber Trading Pte Ltd', '1481883877_default-avatar.png', 54, 1481883877573, 1481883877573),
(22, 'Mr. Neo Koon Boo', 'Trustees', 'LHT Holdings Ltd', '1481883939_default-avatar.png', 88, 1481883939289, 1481883939289),
(23, 'Mr. Seng Song Wen', 'Reserved Committee ', 'Lee Wung Wooden Boxes Co (Pte) Ltd', '1481883980_default-avatar.png', 98, 1481883980898, 1481883980898),
(24, 'Mr. Chang Lung Yuen', 'Reserved Committee ', 'Sen Wan Timber (S) Pte Ltd', '1481884013_default-avatar.png', 43, 1481884013615, 1481884013615),
(25, 'Mr. Chua Chin Seong BBM', 'Reserved Committee ', 'Kian Guan Industries Pte Ltd', '1481884059_default-avatar.png', 66, 1481884059103, 1481884059103),
(26, 'Mr.4. Koh Bon Pock PBM', 'Manufacturers/ International Trade Committee', 'Eastern Union Trading & Sawmill Co Pte Ltd', '1481884101_default-avatar.png', 99, 1481884101049, 1481884101049),
(27, 'Mr. Chong Ha Lee', 'Manufacturers/ International Trade Committee', 'Hong Huat Timber Trading Pte Ltd', '1481884125_default-avatar.png', 45, 1481884125174, 1481884125174),
(28, 'Image Upload', 'Test', 'Test', '1482167550_1.png', 21, 1482167509522, 1482167550595);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Company`
--

CREATE TABLE `sta_Company` (
  `ID` int(11) NOT NULL,
  `CompanyNameEnglish` varchar(255) NOT NULL,
  `CompanyNameChinese` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Website` varchar(255) NOT NULL,
  `Registration` varchar(255) NOT NULL,
  `ProductsServicesEnglish` varchar(255) NOT NULL,
  `ProductsServicesChinese` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Company`
--

INSERT INTO `sta_Company` (`ID`, `CompanyNameEnglish`, `CompanyNameChinese`, `Address`, `Phone`, `Fax`, `Email`, `Website`, `Registration`, `ProductsServicesEnglish`, `ProductsServicesChinese`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'AEONIC INTERNATIONAL TRADE PTE LTD', '', '20 Maxwell Road #03-08, Maxwell House, Singapore 069113', '62227518', '62242817', 'aeonic@singnet.com.sg', 'www.aeonic.com.sg', '189600411C', 'Marketing all kinf of wood products,Internationally & genaral trading.', 'na', 1, 1481620992252, 1482139059922),
(2, 'AKBARWOOD ENTERPRISES PTE LTD', '', '180 Cecil Street #13-04, Bangkok Bank Building, Singapore 069546', '62260254', '62225943', 'akbarwood@pacific.net.sg', '', '198301171Z', 'Sawn Timber, Plywood, Logs, Wood Products.', 'na', 10, 1481621723095, 1482139435823),
(3, 'ALKEMAL SINGAPORE PTE LTD', '', '3 Shenton Way # 12-01A, Shenton House, Singapore 068805', '62230239', '62250678', 'daljit@alkemal.com.sg', 'www.alkemal.com.sg', '198200704K', 'Timber Trading.', 'na', 2, 1481621881879, 1482139192353),
(4, 'ANG TIAN KIAT TRADING PTE LTD', '洪天吉贸易私人有限公司', '49 Sungei Kadut Street 1,Singapore 729351', '63672703', '63676614', 'ang-tk@singnet.com.sg', '', '199300687Z', 'Import and Export of Timber, Plywood and Building Materials.Manufacture of Articles of Concrete Cement and Plaster.', 'na', 2, 1481622040739, 1482139480084),
(5, 'AUSTRALIAN TIMBER PRODUCTS PTE LTD', '', '605A MacPherson Road, #06-01,Citimac Industrial Complex,Singapore 368240', '62894425', '62875935', 'atpr@atp.com.sg', '', '199508025M', 'Supply & Install Timber', 'na', 10, 1481623309901, 1482139500754),
(6, 'CARLTECH TRADING & INDUSTRIES PTE LTD', '嘉达贸易工业（私人）有限公司', 'CARLTECH TRADING & INDUSTRIES PTE LTD,68 Sungei Kadut Street 1,Singapore 729370', '63671188', '63672233', 'carltech@singnet.com.sg', '', '198900243D', 'Interior Design, Contract & Manufacturing of Furniture Products.Construction & Related Structural Engineering Works.Trading of Furniture & Machineries.', 'na', 1, 1481623698962, 1482141452066),
(7, 'CENTENNIAL METALS INTERNATIONAL PTE LTD', '世纪钢铁国际私人有限公司', 'No. 11 Sungei Kadut St 5,Singapore 728957', '63621121', '63621131', 'info@centennialmetals.com', '', '200709921W', 'We Specialise In Collection and Processing of Secondary Steel,Materials and Scrap Metals.', 'na', 12, 1481623897139, 1482141479012),
(8, 'CHEN HOCK HENG MACHINERY PTE LTD', '振福兴机械（私人）有限公司', 'CHEN HOCK HENG MACHINERY PTE LTD64 Sungei Kadut Loop,Singapore 729493', '63684288', '63682655', 'linda@chhforklift.com.sg', 'www.chhforklift.com.sg', '198904556W', 'Sales of new & reconditional forklifts.Trade-in for all model of forklifts.Daily, weekly and monthly basis rental.', 'na', 10, 1481624646731, 1482141570071),
(9, 'CHENG FONG ENTERPRISES (S) PTE  LTD', '正丰企业（星）私人有限公司', '39 Sungei Kadut Street 4,Singapore 729059', '63672128', '68916916', 'info.chengfong@gmail.com', '', '198901738N', 'Specialise In Supply of Chengal,Balau, Kapur, Keruing,Plywood & Other Timber Products', 'na', 5, 1481624907001, 1482141594600),
(10, 'CHIANG LENG HUP PLYWOOD', '昌龙合夹板工业私人有限公司', 'INDUSTRIES PTE LTD,10, Sungei Kadut Street 6,Singapore 728857', '63686139', '63683893', 'chianglh@singnet.com.sg', 'www.chianglenghup.com', '199508826Z', 'Plywood, Blockboard, Laminboard, Natural Fancy Plywood & Veneer,Engineered Fancy Plywood & Veneer, Chipboard, MDF, odgrain/ ,Colour PVC Laminated Board, Timber etc.', 'na', 20, 1481625061048, 1482141654877),
(11, 'EI CORPORATION PTE LTD', '', 'No. 27, Sungei Kadut St 2,Singapore 729240', '63621006', '63621006', 'service@eicorp.com.sg', '', '197602185W', 'Manufacture of Builder Carpentry and Joinery NEC.Manufacture of Doors and Fabrication of Metal & Steel Products.Trading of Timber, Plywood, Steel Bar, Wiremesh & Other Construction Material.', 'na', 20, 1481625512537, 1482141925912),
(12, 'EASTERN MARKETING CO PTE LTD', '', '140 Paya Lebar Road, #10-04/05 AZ @Payalebar,Singapore 409015', '62266939', '67023428', 'EM@easternmktg.com.sg', '', '11f', 'Trading, Import And Export of Plywood, Blockboard, M.D.F. Timber,Logs, Furniture And Other Timber,Related Products', 'na', 12, 1481625704346, 1482141965606),
(13, 'EASTERN UNION TRADING & SAWMILL CO PTE LTD', '东联木业贸易私人有限公司', '70, Sungei Kadut Street 1,Singapore 729371', '62693061', '63678843', 'bonpock@yahoo.com.sg', '', '11f', 'Timber Preservation & Fire Retardant Treatment,Manufacturer of CCA Treated Timber Pile, Etc.', 'na', 5, 1481625872390, 1482142015723),
(14, 'ENG SENG CEMENT PRODUCTS (PTE) LTD', '永盛机制灰砖厂（私人）有限公司', '7, Sungei Kadut Drive,Singapore 729560', '63673188', '63670189', 'escppl@singnet.com.sg', '', '197201657D', 'Manufacturing & Distriburtion of Building Materials Products : 1) Pre-cast Concrete Products 2) Pc Concrete Components 3) Lightweight Partition Panels 4) Roofing Slabs', 'na', 10, 1481627227585, 1482142046194),
(15, 'ETH ENTERPRISE PTE LTD', '永泰和企业私人有限公司', '14, Gul Lane, Singapore 629412', '62614717', '62616918', 'sales@eth.com.sg', '', '11f', 'Construction Materials Suppliers, Timber Products: Dark Red Meranti,Merbau, Ramin, Keruing, Balau, Kempas. Plywood, wooden cases,Crates, Skids, Pallets And Packing Services.', 'na', 2, 1481627366443, 1482142080449),
(16, 'GENERAL LUMBER PRODUCTS PTE LTD', '名称', '158, Lorong Kismis, Singapore 598081', '64697260', '64671832', 'glp@general-lumber.com', 'www.general-lumber.com', '02482/1985K', 'Sawn Timber, Moulding, Furniture, Furniture parts, Sawmill, Moulding, KD Plant, Treatment Plant,Import & Export', '', 13, 1481627956685, 1481873778317),
(23, 'CHONG SUN WOOD PRODUCTS PTE LTD', '中山木业股份私营有限公司', '25A Sungei Kadut Street 1, Singapore 729331', '63689911', '63675172', 'chongsw@singnet.com.sg', 'csw@kaogroup.com', '1', 'Import & Export Timber Products/ Manufacturing of Timber Door.', 'na', 22, 1482141878739, 1482141878739),
(24, 'GEY HWA TIMBER (S) PTE LTD', '艺华木业私人有限公司', '57, Sungei Kadut Street 1, Singapore 729360', '63688010', '63688011', 'geyhwa@singnet.com.sg', 'undefined', '201110270D', 'Manufacturing of Timbers / Wooden products.', 'na', 12, 1482208667695, 1482209070830),
(28, 'GIM HUP TIMBER TRADING CO PTE LTD', '锦合木业贸易私人有限公司', '8 Sungei Kadut Street 5, Singapore 728955', '62698519', '63689542', 'aa@aa.aa', '', '2755/82-W', 'Leading Manufacture of Wooden Pallets, Sawn Timber & Building Materials.', 'na', 88, 1482209258115, 1482209258115),
(29, 'GOODHILL ENTERPRISE (S) PTE LTD', '良山企业（星）私人有限公司', '26 Kranji Loop, Singapore 739562', '62695481', '63681003', 'ghe@goodhill.com', 'http://www.goodhill.com', '197302422-C', 'Timber Door (Supply and/or install), Timber Flooring (Supply and/or install), Decking / Vinyl Tiles, American Woods (Sawn Timber), New Zealand Pine ( Sawn Timber), Natural Resourane:  Sand, Granite, Coal', 'na', 88, 1482209511846, 1482209511846),
(30, 'HANSIN TIMBER SPECIALIST & TRADING PTE LTD', 'undefined', '48, Toh Guan Road East, #03-107,Enterprise Hub, Singapore 608586', '62855325', '62845171', 'hansin8001@singnet.com.sg', 'aa@aa.aa', '201416131W-PTE-01', 'FSC & Greenlabel, Timber Flooring: Timber Staircase & Railing, Timber Trellis:, Timber & Fabric Acoustic Wall Panelling, Timber Decking:, Greenlabel Wood Plastic Composite (WPC)', 'na', 65, 1482209917997, 1482209917997),
(31, 'HOCK HUAT SAWMILL CO (PTE) LTD', '福发木业（私人）有限公司', '36 Sungei Kadut Street 2, Singapore 729244', '62696265', '62698855', 'hockhuattimber@singnet.com.sg', 'aa@aa.aa', '197700829W', 'Heat Treatment (ISPM), Kiln Drying, Bundling and Packing, Moulding and Woodworking, Import and Export of Sawn Timber, Logs and Plywood.', 'na', 88, 1482210044928, 1482210044928),
(32, 'HIN KEE TRADING CO PTE LTD', '兴记贸易私人有限公司', '6, Angklong Lane, #01-01,  Faber Garden Condo, Singapore 579980', '67388371', '62538573', 'hnksin@singnet.com.sg', 'undefined', '197802216C', 'Import & Export.', 'na', 98, 1482210262757, 1482210491805),
(33, 'HOCK SIONG HUAT (PTE) LTD', '福松发（私人）有限公司', '51 Bukit Batok Crescent #08-29, Unity Centre, Singapore 658077', '63163103', '63163262', 'chuacc@hocksionghuat.com.sg', 'undefined', 'aa', 'Sawmiller, Importer & Exporter of Sawn Timber.', 'na', 99, 1482210422643, 1482210422643),
(34, 'HOCK AIK TRADING (PTE) LTD', '福益贸易（私人）有限公司', '1 Kay Siang Road, #10-02 Singapore 248922', '63689538', '63651420', 'admin@hockaik.com', 'undefined', '198900879-N', 'Import & Export of Sawn Timber & Timber Relatyed Products', 'na', 45, 1482210592019, 1482210592019),
(35, 'HON NAM LEE', '韩南利', '29 Sungei Kadut Street 6, Singapore 728867', '62692868', '63683830', 'hnl1966@singnet.com.sg', 'www.honnamlee.com', '05353000D', 'Outdoor Timber Deck (Supply & Install), Timber Flooring Business (Supply & Install), Timber Door Business (Supply & Install), Pallet Business, Laminate Timber Business, Timber logs, Squares, Post and Sawn Timber Business.', 'na', 66, 1482210882795, 1482210882795),
(36, 'HOCK HIN LEONG TIMBER TRADING (PTE) LTD', '福兴隆木业贸易（私人）有限公司', '56 Sungei Kadut Street 1, Singapore 729359', '63681300', '63678355', 'hhltt@singnet.com.sg', '', '198004621G', 'All kinds of treated timber, wholesaler & retail for furniture & Building. Timber & Plywood, import & export.', 'na', 95, 1482210986592, 1482210986592),
(37, 'HONG HUAT TIMBER TRADING (PTE) LTD', '丰发木业贸易私人有限公司', '31 Kranji Way, Singapore 739451 ', '62690171', '63682710', 'hhtimber@singnet.com.sg', 'undefined', '197401850D', 'Sawmiller & Exporter of Sawn Timber, Plywood & Wooden Products to Local & Overseas Markets.', 'na', 55, 1482212492403, 1482212492403),
(38, 'HOCK HOE HENG TIMBER PTE LTD', '福和兴木业私人有限公司', '23-C Sungei Kadut Street 1, Singapore 729326', '63681223', '62692291', 'csang@hhhpl.com.sg', '', '198404041-Z', 'Trading of Sawn Timbers / Plywoods.', 'na', 99, 1482212745628, 1482212745628),
(39, 'HUP HUAT TIMBER CO (S) PTE LTD', '合发锯木厂私人有限公司', '4 Sungei Kadut  Street 3, Singapore 729139', '62695651', '63678456', 'huphuatttimber@gmail.com', 'undefined', '197702321K', 'Wood Products, Plywood & Building Material Supplier.', 'na', 33, 1482212854750, 1482212854750),
(40, 'INTERCONTINENTAL TIMBER & TRADING CO. PTE LTD', '洲际木材贸易私人有限公司', 'Blk 809 French Road #05-166, Kitchener Complex, Singapore 200809', '62933789', '62933083', 'itt73@singnet.com.sg', 'undefined', '197301468C', 'Sawn Timber, Flooring and other wood related products.', 'na', 12, 1482212935996, 1482212935996),
(41, 'JASON PARQUET SPECIALIST (S) PTE LTD', 'undefined', '16 Tampines Street 92, JP Building, Singapore 528873.', '67832727', '67822727', 'info@jasonparquet.com', 'www.jasonparquet.com', '199308470M', 'Timber Flooring Related Materials.', 'na', 77, 1482213062220, 1482213062220),
(42, 'JS TIMBER PTE LTD', 'undefined', '14 Sungei Kadut Street 3, Singapore 729145', '63684665', '91111111', 'jstimber@singnet.com.sg', 'www.jstimber.com.sg', '20057786Z', 'Timber and Plywood Supplier.', 'na', 78, 1482213242077, 1482213242077),
(43, 'KAR HIN TIMBER CO (PTE) LTD', '嘉兴木业私人有限公司', '31 Sungei Kadut Street 4, Singapore 729055', '63682264', '63681166', 'aa@aa.aa', 'undefined', 'aa111', 'Trading of Timber & Plywood', 'na', 99, 1482213397004, 1482213397004),
(44, 'KIM TIMBER (PTE) LTD', '金木业（私人）有限公司', '29 Sungei Kadut Street 4, Singapore 729054', '62691788', '63688081', 'kimwood@pacific.net.sg', 'undefined', '91111aaaa', 'Sawn Timber / Furniture / Wooden Products.', 'na', 65, 1482213513690, 1482213513690),
(45, 'KAY HUAT TRADING CO PTE LTD', '启发贸易（私人）有限公司', '111 King George\'s Ave, Singapore 208559', '62948355', '62992398', 'accounts@kayhuatgroup.com', 'undefined', '911aaa', 'Plywoods, Fancy Plywoods, MDF Blockboards, Particle Boards, Timber, Wood Working Products,', 'na', 66, 1482213582594, 1482213582594),
(46, 'KING WAN CONSTRUCTION PTE  LTD', '庆源工程私人有限公司', '8 Sungei Kadut Loop, Singapore 729455', '63684300', '63665110', 'kwc@kingwan.com.sg', 'www.kingwan.com', '197702117N', 'Contractor, M & E Contractor', 'na', 11, 1482213888682, 1482213888682),
(47, 'KENWOOD INDUSTRIES PTE LTD', '建木业（私人）有限公司', '10 Sungei Kadut Street 3, Singapore 729144.', '62695433', '63689188', 'kenwd@singnet.com.sg', '', '198900864G', 'Manufacturer of Furniture Parts & Wood Product.', 'na', 66, 1482213974935, 1482213974935),
(48, 'KMK AGRO PTE LTD', 'undefined', '333 North Bridge Road, #09-00 K.H.KEA Building, Singapore 188721', '64314880', '63366150', 'aa@aa.aa', 'undefined', '911aa', 'Timber Products: Door, Furniture, Flooring, Moulding & Plywood.', 'na', 88, 1482214099113, 1482214099113),
(49, 'KER AND KER CO. PTE. LTD', '郭贸易私人有限公司', '36 Sungei Kadut Street 1, Singapore 729341', '63685511', '63685115', 'admin.kernker@kerrycorp.com.sg', 'undefined', '911aa', 'Foreign Workers Dormitory Services. Timber, Solid Surface Materials, Quartz Stone.', 'na', 98, 1482214273377, 1482214273377),
(50, 'K.T.S. (SINGAPORE) PTE LTD', '启德行（星）私人有限公司', '190 Middle Road, #09-01/07, Fortune Centre Singapore 188979', '63366077', '63390715', 'ktssgp@singnet.com.sg', 'undefined', '197401224C', 'Timber Importer & Exporter.', 'na', 34, 1482214410324, 1482214410324),
(51, 'KIAN GUAN INDUSTRIES PTE LTD', '建源工业私人有限公司', '48 Tuas Ave 9, Jurong Industrial Estate, Singapore 639191', '68610055', '68620055', 'teamkgi@kgi.com.sg', 'undefined', '197802318W', 'Import & Export of Sawn Timber Crude Petroleum and Oil Products Manufacture of Timber Pallets.', 'na', 32, 1482214535922, 1482214535922),
(52, 'KWONG MAW CO PTE LTD', '广茂私人有限公司', '22 Kranji Way, Singapore 739433', '62696836', '63689782', 'aa@aa.aa', 'undefined', '011aa', 'Sawmilling, Woodworking, \'Celcure\' Impregnation, Kiln Drying, Timber Exporter, Shipping Jetty, Supplier of \'Albi\' Fire Retardant For Fire Protection', 'na', 67, 1482214626510, 1482214626510),
(53, 'KIAN HUAT TIMBER INDUSTRIES (1978) PTE LTD', '健发木材工业（1978）私人有限公司', '6 Sungei Kadut Street 4, Singapore 729036', '62699686', '63689983', 'nrwpl1978@yahoo.com.sg', '', '197800081N', 'Wholesale & Retail Supply of All Types of Sawn Timber, Plywood, PVC Plywood , Plywood Lamination, Cement, Bricks, Sands,  Hardware and Other Building Materials.', 'na', 80, 1482214775444, 1482214775444),
(54, 'KWONG WAH SENG TIMBER TRADING CO.PTE LTD', '广华成木业贸易私人有限公司', '10, Admiralty Street, #01-58/59, North Link Building, Singapore 757695', '63655912', '63655310', 'kwsttcpl@hotmail.com', 'undefined', '199707848W', 'Timber Doors, Windows, Flooring, Fire-rated Doors,  Accoustic Timber Doors, Timber Trellis, Timber Handrail & Timber Fence.', 'na', 44, 1482214871090, 1482214871090),
(55, 'KIM SAN TRADING (KILNING) PTE LTD', '金山贸易（烘房）私人有限公司', '14 Kranji Way, Singapore 739427', '62696671', '63685516', 'kimsantrading@singnet.com.sg', 'undefined', '9111aa', 'Product: Sawn Timber, Moulding, Flooring, Decking, Plywoods and Other Forestry Products. Services: Kiln-Drying, Heat-Treatment', 'na', 66, 1482215096131, 1482215096131),
(56, 'LAM CHUAN IMPORT EXPORT PTE LTD', '南川贸易（私人）有限公司', '12 Sungei Kadut Way, Singapore 728778', '63686669', '63686665', 'marketing@lamchuan.com', 'www.lamchuan.com', '911aa', 'Agency & Stockist For High Pressure Laminates. Manufacturing For Postforming Element.', 'na', 11, 1482215205723, 1482215205723),
(57, 'LHT HOLDINGS LIMITED', '联合木业控股有限公司', '27 Sungei Kadut Street 1, Singapore 729335', '62697890', '63674907', 'kbneo@lht.com.sg', 'www.lht.com.sg', '198003094-E', 'Leading Manufacturer & Exporter of Wooden Pallets, Cases, Crates, Sawn Timber, Plywood & Packaging Materials. New Environmental Friendly Recycling Wood Wastes Products with New Technology.ECR Pallets with RFID Technology', 'na', 34, 1482215331286, 1482215331286),
(58, 'LEE WUNG WOODEN BOXES CO. PTE LTD', '利运木箱私人有限公司', '1A, Sungei Kadut Street 4, Singapore 729031', '62697388', '62697389', 'sales@leewung.com.sg', 'leewung.com.sg', '200711784E', 'Manufacture of Wooden Boxes, Pallets & etc. Provide Industrial,  Packing & ISPM15 Heat Treatment Services.', 'na', 56, 1482215432824, 1482215432824),
(59, 'LIM\'S TIMBER TRADING CO', '林木材贸易公司', '22 Sungei Kadut Loop, Singapore 729480', '63681983', '63689986', 'limtimber@yahoo.com.sg', 'undefined', '27313500K', 'Timber & Plywood, Serving The Marine & Ship Supply Stockist & Exporter', 'na', 54, 1482215540235, 1482215540235),
(60, 'LEONG YEW TIMBER CO PTE LTD', '良友木业（私人）有限公司', '62 Sungei Kadut St 1, Singapore 729363', '62811292', '62877764', 'rosalindtan@transcabservices.com.sg', 'undefined', '197200291N', 'Wholesales of Logs, Sawn Timber,  Plywood and Related Product.', 'na', 65, 1482215637391, 1482215637391),
(61, 'MAXCON ENTERPRISE PTE LTD', '集隆企业私人有限公司', 'No 10, Admiralty Street #01-51,  North Link Building, Singapore 757695', '64812008', '64842009', 'maxtoh@maxconenterprise.com', 'www.maxconenterprise.com', '199705125E', 'Trading of Wood Products', 'na', 98, 1482215971069, 1482215971069),
(62, 'NATURE WOOD PTE LTD', '自然木业私人有限公司', 'No 2, Yishun Industrial Street 1, #05-18, North Point, Singapore 768159', '63390300', '63690486', 'simonoei@naturewood.com.sg', 'www.naturewood.com.sg', '2002-04885-D', 'Teak Wood', 'na', 23, 1482216170641, 1482216170641),
(63, 'MINLY TIMBER CORPORATION', '敏利木业公司', '24, Sin Ming Lane, #05-93 Midview City Singapore 573970.', '62692888', '63672888', 'minly@singnet.com.sg', 'undefined', '06573100W', 'All Grades of Charcoal & Other Related,Wood Products.', 'na', 54, 1482216266636, 1482216266636),
(64, 'NG GUAN SENG WOODWORKING INDUSTRIAL PTE LTD', '黄源成木业私人有限公司', '28 Sungei Kadut Street 4, Singapore 729053', '62694166', '63684703', 'enquiry@ngs.com.sg', 'ngs.com.sg', '911aa', 'Sawmillers,Wooden Cases & Crates. Wooden Pallets, Packing Of Heavy Machinery.', 'na', 11, 1482216369712, 1482216369712),
(65, 'NAM SAN INDUSTRIAL WOOD PTE LTDD', '南山工业木品（私人）有限公司', '51 Sungei Kadut Street 1, Singapore 729354', '62695549', '63685537', 'dateba-yo@yahoo.com', 'undefined', '197800717M', 'Woodworking Manufacture, Products Moulding Profile, Dowel Post, Balau Timber, Timber Decking.', 'na', 43, 1482216454295, 1482216454295),
(66, 'NS TRADING PTE LTD', '强新贸易私人有限公司', '22 Sungei Kadut Avenue, Singapore 729657', '62698080', '63684848', 'admin@nstrading.sg', 'undefined', '198804586M', 'Plywood & Timber Supplier', 'na', 34, 1482216546518, 1482216546518),
(67, 'NAM SOON TIMBER PTE LTD', '南顺木业（私人）有限公司', '1 Tuas View Place #01-13 Westlink One, Singapore 637433', '63663890', '62693339', 'info@namsoontimber.com', 'undefined', '198600734D', 'Design / Consult / Supply & Install /  Maintenance of Timber Decking, Trading of Plywood /  Timber (FSC Balau, Composite (Green Label),  Chengal, Balau, Kapur ).', 'na', 55, 1482216701547, 1482216701547),
(68, 'OLAM INTERNATIONAL LIMITED', 'undefined', '9 Temasek Boulevard #25-01, Suntec Tower Two, Singapore 038989.', '63394100', '63419320', 'darshan@olamnet.com', 'olamonline.com', '199504676H', 'Trading of Agricultural Commodities.', 'na', 123, 1482216806226, 1482216806226),
(69, 'PT PLYWOOD & TIMBER TRADING', '好林木业贸易', '8 Sungei Kadut Street 4, Singapore 729038', '63622446', '63656445', 'ptplywoodtimber@singnet.com.sg', 'undefined', '53121810D', ' Specialise in all Building Matrials, Plywood, Chipboard, MDF Board, Natural & Reconstituted Veneer Plywood, Exotic & Construction Timber etc.', 'na', 344, 1482216889479, 1482216889479),
(70, 'PACIFIC FOREST PRODUCTS PTE LTD', '太平洋木业私人有限公司', '38, Sungei Kadut Street 2, Singapore 729245.', '63686675', '63687039', 'jason@pacificforest.com.sg', 'undefined', '198801748D', 'Timber Door, Wooden Products.', 'na', 234, 1482216951052, 1482216951052),
(71, 'ROYAL GLOBAL EXPORTS PTE LTD', 'undefined', '143 Cecil Street, #07-00 GB Building, Singapore 069542', '62244111', '62207666', 'mail@royalglobalexp.com.sg', 'undefined', '199607015G', 'Hardwood Logs From Sarawak. Plantation Teak Logs From Countries In South & Central America & West Africa, Beech & Ash Wood Gurjan & Teak from Myanmar.', 'na', 345, 1482217086282, 1482217086282),
(72, 'PLONY TIMBER & TRADING CO. PTE LTD', '宝利木业贸易（私人）有限公司', 'Blk 212, Hougang St.21, #01-325, Singapore 530212', '62862075', '62854017', 'plony@singnet.com.sg', 'undefined', '198400061Z', '1) Wholesale of Timber & Plywood. 2)Preservation & Fire-Retardant Treatment of Wood. 3)  Manufacture of Moulded Wooden Products.', 'na', 333, 1482217235780, 1482217235780),
(73, 'PRIME TIMBER INDUSTRIES PTE LTD', '优木工业私人有限公司', '8 Sungei Kadut Street 5, Singapore 728955', '62693333', '63678000', 'prime_timber2004@yahoo.com.sg', 'undefined', '197700685C', 'Sawmiller, Timber Importer & Exporter', 'na', 666, 1482217315848, 1482217315848),
(74, 'SEA CONTRACTOR (S) PTE LTD', 'undefined', '40 Gul Crescent, Singapore  629540', '62836363', '62816623', 'mark@seacontractor.com', 'www.seacontractor.com', '198903007R', 'Manufacture & Wholesaling of Marine  Deck Equipment.', 'na', 22, 1482217405795, 1482217405795),
(75, 'SINGAPORE TONG TEIK PTE LTD', '新同德（私人）有限公司', '7, Temasek Boulevard #20-02, Suntec Tower One, Singapore 038987', '63383773', '63373010', 'stt@tongaik.com.sg', 'undefined', '196600147k', 'Trading In Natural Rubber & Latex', 'na', 54, 1482217472696, 1482217472696),
(76, 'SEN WAN TIMBER (S) PTE LTD', '兴旺木业（星）私人有限公司', '38, Sungei Kadut Street 2, Singapore 729245', '62863388', '62862122', 'swtimber@singnet.com.sg', 'www.senwangroup.com', '198803603R', '1) Import And Export of Timber & Plywood. 2) Manufacturer of Colour Woodgrain, Plywood Panel.', 'na', 44, 1482217581851, 1482217581851),
(77, 'SIONG HUA SUPPLIER PTE LTD', '松华（私人）有限公司', '48, Sungei Kadut Loop, Singapore 729487', '67691001', '67636051', 'sionghua@singnet.com.sg', 'undefined', '198303657E', 'Supplies In All Kinds of Timber, Building Material & Hardwood', 'na', 45, 1482217673077, 1482217673077),
(78, 'SENG HIN SAWMILL CO (S) PTE LTD', '成兴锯木厂（新）私人有限公司', '6 Sungei Kadut Street 3, Singapore 729141', '62691341', '62691069', 'sales@senghin.com', 'www.senghinsawmill.com', '197702296Z', 'Sawmiller, Importer & Exporter of Various Species of Sawn Timber: Balau, Kapur, Kempas, Mixed Light Hardwood, etc. Supplier of Plywood, Wooden Products, Wooden Wedges, Wooden Fender Etc.', 'na', 55, 1482217841344, 1482217841344),
(79, 'SIT LEY TIMBER PTE LTD', 'undefined', '46 Sungei Kadut Street 1, Singapore 729350', '63685244', '63687606', 'sitley@singnet.com.sg', 'undefined', '911a', 'Manufacturing Of Wood Moulding & Wooden Doors.', 'na', 76, 1482217903916, 1482217903916),
(80, 'SENG HONG TIMBER PRODUCTS PTE LTD', '成丰木业私人有限公司', '6 Sungei Kadut Street 5, Singapore 728953', '62692061', '63683896', 'shtppltd@singnet.com.sg', 'undefined', '197700706E', 'Timber Trading & Warehousing', 'na', 11, 1482217981771, 1482217981771),
(81, 'S. K. K. ENTERPRISE PTE LTD', 'undefined', '49, Sungei Kadut Street 4, Singapore 729063', '63633298', '62690681', 'skkentpl@singnet.sg', 'www.skk.sg', '199608803E', 'Supply Of Chengal, Balau And Kapur Timber', 'na', 22, 1482218062363, 1482218062363),
(82, 'SIM SENG HIN SAWMILL CO (PTE) LTD', '森成兴火锯厂（私人）有限公司', '5 Sungei Kadut Street 3, Singapore 729140', '62698873', '63671518', 'sshmill@singnet.com.sg', 'undefined', '911a', 'Sawmilling, Planing, Moulding of Timber, Mfg Timber, Door, Frames, Pallets, Parts.', 'na', 76, 1482222646262, 1482222646262),
(83, 'SOH TIMBER BINDERS CO PTE LTD', '舒木业私人有限公司', '8 Sungei Kadut Street 4, Singapore 729038', '62695708', '62695132', 'stb.xm@yahoo.com.sg', 'undefined', '1976-02-009E', 'Bundling Sawn Timber, Kiln Dried. Supplier Of Sawn Timber ( Import&Export)', 'na', 87, 1482222852871, 1482222852871),
(84, 'SIN HUP LEE TIMBER TRADING PTE LTD', '新合利木业贸易私人有限公司', '23 Kranji Loop, Singapore 739559', '67776106', '67776106', 'aa@aa.aa', 'undefined', '1988004863H', 'Timber for Furniture & Construction use.', 'na', 87, 1482222941587, 1482222941587),
(85, 'SOON SENG BENG CO. PTE LTD', '顺成明私人有限公司', '10 Lor 26 Geylang, #02-01, Singapore 398485, No 2, Sungei Kadut Street 3, Sungei Kadut Ind Est Singapore 729137', '68421882', '68411882', 'soonsengbeng@singnet.com.sg', 'undefined', '199806300N', 'Supply Building Materials, Transporation', 'na', 565, 1482223095594, 1482223095594),
(86, 'SIN JOO LEE TIMBER PTE LTD', '新裕利木业私人有限公司', '17, Kranji Link, Singapore 728673', '63639266', '63664666', 'louis@sinjoolee.com.sg', 'www.sinjoolee.com.sg', '20-0311818-Z', '1. Wholesale Of Logs, Sawn Timber, Plywood And Related Products, 2. Manufacture Of Furniture And Fixture Of Wood  (Including Upholstrery)', 'na', 987, 1482223223981, 1482223223981),
(87, 'SUDIMA INTERNATIONAL PTE LTD', 'undefined', '151 Chin Swee Road, #15-03 Manhattan House Singapore 169876', '67327180', '67327125', 'ashwini@sudima.com', 'www.sudima.com', '199400641G', 'Sarawak Logs, PNG Logs, Myanmar Logs, African Logs. Pine from Russia.FSC certified Eucalyprus Logs, Teak Logs, Plywood Manufacturing. Manufacturing of Kitchen Cabinet Door & Finger Joined Boards.', 'na', 34, 1482223406960, 1482223406960),
(88, 'SIN LAM BEE SAWMILL PTE LTD', '新南美火锯私人有限公司', '2 Sungei Kadut Street 3, Singapore 729137', '62690872', '63689198', 'aa@aa.aa', '', '911a', 'Timber Importer & Exporter, Specialise In Supply Sawn Timber of the Species of Chengal, Balau, Kapur, Keruing, Kempas, Meranti, Mixed Light Hardwood, And Related Wood Products Including  Plywood. Provide Timber Dressing (Planning) Services.', 'na', 23, 1482223618603, 1482223618603),
(89, 'SUNRISE DOORS INTERNATIONAL PTE LTD', '日昇木品国际私人有限公司', '10 Anson Road #22-09, International Plaza Singapore 079903', '62236088', '62209500', 'sunrisedoors@sunrisedoors.com.sg', 'www.sunrisedoors.com.sg', '198904013C', '1) Manufacture Of Furniture & Fixtures Of Wood  ( including Upholstery). 2) Wholesale Of Sawn Timber, Plywood And Related Products.', 'na', 453, 1482223745102, 1482223745102),
(90, 'SIN SIONG LIM SAWMILL CO (PTE) LTD', '新松林火锯（私人）有限公司', '2 Sungei Kadut Street 5, Singapore 728949', '62699275', '63661986', 'aa@aa.aa', 'undefined', '43', 'Sawmiller, Importer & Exporter of Logs, Timber ( Kapur, Red/Dark Red Meranti, Balau, Keruing, Mixed Hight Hardwood, & Other Tropical Hardwood Species), Blockboard, All Kinds of Plywood,Etc.', 'na', 65, 1482223886637, 1482223886637),
(91, 'SUPREME LION HOLDING PTE LTD', 'undefined', '30 Marsiling Lane, Singapore 739149', '65555522', '65554333', 'edwin@supremefloors.com.sg', 'www.supremefloors.com.sg', '199902709H', 'Renovation Contractor. Manufacture of Prefabricated wooden building materials.', 'na', 33, 1482223962491, 1482223962491),
(92, 'TAI GIAP PTE LTD', '大业私营有限公司', '29 Sungei Kadut Street 2 , Singapore 729242', '63687711', '63689977', 'taigiap@singnet.com.sg', 'undefined', '197501255N', 'Woodworking & Moulding Manufacturer , Supplier Of Sawn Timber, Plywood And Building Materials Related Products.', 'na', 43, 1482224053981, 1482224053981),
(93, 'TAT HIN TIMBER PTE LTD', '达兴木业私人有限公司', '31 Sungei Kadut Street 4, Singapore 729055', '62695401', '63688602', 'enquiries@thtimber.com', 'www.thtimber.com', '197800549D', '1) Sawmiller 2) Supply Of Sawn Timber, eg. Balau, Chengal, Keruing,   Kapur, Kempas, etc. To Local Contractors, Shipbulldders   And Export To Overseas Market. 3) Building And Construction', 'na', 33, 1482224131937, 1482224131937),
(94, 'TECKCHING TIMBER PTE LTD', '德信木业私人有限公司', '56 Sungei Kadut Street 1, Singapore 729359 ', '62692711', '62692618', 'techin@singnet.com.sg', 'undefined', '911a', 'Importer / Exporter Of Sawn Timber & Plywood, Kiln-drying, Moulding & Woodworking Services.', 'na', 32, 1482224203802, 1482224203802),
(95, 'TEK LAI ENTERPRISE', 'undefined', 'Blk 237, Bukit Panjang Ring Rd, #07-71, Singapore 670237', '98186719', '67650409', 'teklaienterprise@yahoo.com.sg', 'undefined', '53027160E', 'Teak Parquet', 'na', 43, 1482224327903, 1482224327903),
(96, 'THIAM PENG TRADING PTE LTD', '添平贸易私人有限公司', 'No. 5, Sungei Kadut Way, Singapore 728781', '63681133', '63682628', 'sktpt@singnet.com.sg', 'undefined', '197800638N', 'Specialise in all Building Materials, Plywood, MDF Board, Chipboard, Woodgrain / Colour PVC Laminated Board. Natural & Reconstituted Veneer Plywood, Exotic  & Construction Timber etc.', 'na', 54, 1482224434562, 1482224450923),
(97, 'TOSEVA TIMBER PTE LTD', '同越木业私人有限公司', '50, Ubi Avenue 3, #02-12/13, Frontier, Singapore 408866', '67492166', '67491966', 'singapore@toseva.com', 'http://www.toseva.com', '198700145D', 'Timber Trading', 'na', 43, 1482224554897, 1482224554897),
(98, 'UEC PTE LTD', 'undefined', '190 Middle Road #15-08, Fortune Centre, Singapore 188979', '62202267', '62202667', 'uecom@singnet.com.sg', 'undefined', '200001090H', 'Timber Exporter / Distributor. Timber Logs / Square / Sawn /  Lumber Timber Flooring /  Furniture Parts / Teak Furniture/  Decking Teak / Walnut / Wenge / Oak / Ironwood.', 'na', 43, 1482224718597, 1482224718597),
(99, 'WAJILAM EXPORTS (SINGAPORE) PTE LTD', '', '10, Anson Road, #27-14, International Plaza, Singapore 079903', '62245561', '62254932', 'info@wajilam.com.sg', 'www.wajilam.com.sg', '198401487E', 'Timber Logs Exports', 'na', 321, 1482224803708, 1482224803708),
(100, 'WASON PRIVATE LIMITED', '桦松私人有限公司', '1 Sungei Kadut Street 6, Singapore 728850', '62696711', '62694082', 'wason@singnet.com.sg', 'www.wason.com.sg', '197601836K', 'Wood Related Products, PVC Lamiration.', 'na', 454, 1482224882119, 1482224882119),
(101, 'WEE TEE TONG CHEMICALS PTE LTD', '威利东化工私人有限公司', '18 Sunge Kadut Street 3, Singapore 729149', '63664231', '63664232', 'shseah@weeteetong.com', 'www.weeteetong.com', '04279/1984-N', '??????? ????? ?????????', 'na', 88, 1482224974994, 1482224974994),
(102, 'YING CHUAN TIMBER CO PTE LTD', '颖川木业（私营）有限公司', '17 Kranji Loop, Singapore 739551', '62691393', '63671938', 'aa@aa.aa', 'undefined', '911a', 'Supplier Of Sawn Timber, Plywood, Building Materials &  Woodworking Manufacturer.', 'na', 99, 1482225054489, 1482225054489);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Event`
--

CREATE TABLE `sta_Event` (
  `ID` int(11) NOT NULL,
  `EventName` varchar(255) NOT NULL,
  `EventDate` varchar(200) NOT NULL,
  `EventFrom` varchar(200) NOT NULL,
  `EventTo` varchar(200) NOT NULL,
  `Description` text NOT NULL,
  `Venue` varchar(255) NOT NULL,
  `Latitude` varchar(200) NOT NULL,
  `Longitude` varchar(200) NOT NULL,
  `CourseFee` varchar(50) NOT NULL,
  `Remarks` text NOT NULL,
  `Pdf` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Event`
--

INSERT INTO `sta_Event` (`ID`, `EventName`, `EventDate`, `EventFrom`, `EventTo`, `Description`, `Venue`, `Latitude`, `Longitude`, `CourseFee`, `Remarks`, `Pdf`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(9, 'test hen is the Bryant Park Winter Village 2016.', '2016-12-26', '5:00', '10:00', 'By the time you choose a costume at a Halloween store in NYC, The Bank of America Winter Village in Bryant Park will already be open for business. The merriest holiday market opens its doors starting Saturday, October 29. The Holiday Shops will be hawking seasonal gifts through January 2, 2017. The Rink and Public Fair will run through March 5, 2017.', 'Bryant Park', '1.666', '143343', 'Free', 'Good', '', '3453453', 3, 1481881188405, 1481881883011),
(10, 'hen is the Bryant Park Winter Village 2016?', '2016-12-25', '5:00', '10:00', 'Out of all the things to do in the fall in NYC, no other event or holiday market receives as much hype as the Bryant Park Winter Village 2016. Why? Because once the 17,000-square-foot ice-skating rink and over 125 shopping kiosks and food vendors open for business, this means that Christmas in New York has officially arrived. This guide contains all the information you need to know, from new vendors and attractions to when the market is open. Enjoy the most wonderful time of the year!', 'Bryant Park', '1', '1', 'Free', 'Good', '', '3453453', 1, 1481881190717, 1481881247102),
(11, 'The best places to go ice-skating in NYC', '11-11-2016', '17:00', '24:00', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'NYC', '1', '1', 'Free', 'Good', '', '3453453', 0, 1481881396201, 1482146378511),
(12, 'Central Park, Wollman Rink', '10-10-2016', '05:00', '10:00', 'If you decide to check out this famed rink, be prepared for hordes of children and slow-moving newbies. There won’t be room for speed skating or fancy tricks, but braving the crowds is worth it for the priceless Central Park scenery. If you’re a skating greenhorn yourself, take heart—Trump Rink is home to the largest learn-to-skate program in the country.', '88 Upper Serangoon Road #88-888, Serangoon Shopping Centre, Singapore 888888', '1.3535405', '103.8788134', '$50 per participant', '3-day course. Certificates of attendance will be given to participants who successfully complete the course.', '', '3453453', 4, 1481881515259, 1482143207917),
(13, 'Trump Lasker Rink', '2016-12-27', '5:00', '10:00', 'The second Donald-branded skating venue isn’t quite as popular as Wollman Rink, which means you just might be able to hit your Apolo Ohno stride. The rink is open until 11pm on Friday and Saturday nights. When the ice isn’t available to the public, good odds are there’s a hockey game or practice happening; if you’re a puck fan, stop by to cheer on the adult and youth teams that frequent the spot.', 'Central park', '1', '1', 'Free', 'Good', '', '1481881700_Little_island_colour_logo_200x200.jpg', 0, 1481881700687, 1481881700687),
(14, 'The Rink at Rockefeller Center', '2016-12-30', '5:00', '10:00', 'Even if the sidewalks are overrun with tourists, you’ll have ample room to skate at the city’s most iconic rink; only 150 people are allowed on the ice at once. Unfortunately, that also means that you should prepare for long lines. If you want the privilege of being among the first to hit the cold stuff in the morning, visit the rink’s website to make a reservation for the first skate of the day.', 'Midtown West', '4,555', '65,7777', 'Free', 'Good', '', '1481881831_Little_island_colour_logo_200x200.jpg', 0, 1481881831088, 1481881831088),
(15, 'Test', '04/07/2016', '04:00', '05:00', 'Test', 'Test', '41', '45', '500', 'Test', '', '3453453', 0, 1482143483839, 1482143542923),
(16, 'Test 2', '2-12-2016', '04:00', '05:00', 'Test', 'Test', '14', '45', '500', 'Test', '', '1482144358_320p-header-bg.png', 1, 1482144358899, 1482144358899),
(17, 'Test 3', '28-12-2016', '01:00', '02:00', 'Test 3', 'Test', '41', '45', '325', 'Test', '1482144650_WAPT_Report_TimeExtender_8-30-2016.pdf', '3453453', 1, 1482144650197, 1482144700437),
(19, '11', '30-11-2016', '11:00', '13:00', '11', '11', '11', '11', '11', '11', '1482147236_testing-123.pdf', '3453453', 2222, 1482147236658, 1482150847303),
(20, 'Upload Component Test', '25-12-2016', '23:11', '24:00', 'Test', 'Test', '14', '14', '250', 'Test', '1482168922_WAPT_Report_TimeExtender_8-30-2016.pdf', '3453453', 1, 1482168922366, 1482168968211),
(21, 'Test Test', '07-08-2016', '04:00', '05:00', 'Test', 'Test', '54', '85', '450', 'Test', '1482169221_WAPT_Report_TimeExtender_8-30-2016.pdf', '3453453', 1, 1482169221564, 1482169348668),
(22, 'New upload test', '10-12-2016', '04:00', '05:00', 'Test', 'Test', '145', '52', '250', 'Test', '1482224869_sdf.pdf', '1482224919_ad-commercial-outdoor.jpg', 1, 1482224869350, 1482225207295);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Module`
--

CREATE TABLE `sta_Module` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL COMMENT 'BackenUser, Application User, Booking, Bus Type',
  `Icon` varchar(100) DEFAULT NULL,
  `Caption` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Module`
--

INSERT INTO `sta_Module` (`ID`, `Name`, `Icon`, `Caption`) VALUES
(1, 'staff-bus-type', 'icon_bus_type.svg', 'Bus Type'),
(2, 'language', 'icon_language.svg', 'Language'),
(3, 'language-value', 'icon_language_value.svg', 'Language Value'),
(4, 'staff-backend-user', 'icon_backend_user.svg', 'Backend User'),
(6, 'role', 'icon_role.svg', 'Role'),
(8, 'service-provider', 'icon_service_provider.svg', 'Service Provider'),
(10, 'customer', 'icon_customer.svg', 'Customer'),
(11, 'notification-log', 'icon_notification_log.svg', 'Notification Log'),
(12, 'verification-code-log', 'icon_verification_code_log.svg', 'Verification Code Log'),
(13, 'enquiry', 'icon_enquiry.svg', 'Enquiry'),
(14, 'booking', 'icon_booking.svg', 'Booking');

-- --------------------------------------------------------

--
-- Table structure for table `sta_Representative`
--

CREATE TABLE `sta_Representative` (
  `ID` int(11) NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `NameEnglish` varchar(255) NOT NULL,
  `NameChinese` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Representative`
--

INSERT INTO `sta_Representative` (`ID`, `CompanyID`, `NameEnglish`, `NameChinese`, `Phone`, `DisplayPriority`) VALUES
(32, 20, 'Test', 'cc', '', 1),
(34, 22, 'asdsad', 'cc', '98513111', 1),
(35, 16, 'CHEAH SOO MENG', 'cc', '97576637', 2),
(51, 1, 'FRANCIS NG', 'cc', '62227518', 10),
(56, 2, 'SHABBIR Z. SHAKIR', 'cc', '90093997', 2),
(57, 4, 'ANG TIAN KIAT', 'cc', '63676614', 2),
(58, 5, 'THIEW MENG KHAI', 'cc', '97678885', 10),
(59, 5, 'REAGAN PAUL THIEW', 'cc', '82334972', 5),
(71, 6, 'DAVID TAN CHIN BEE', '-', '', 1),
(72, 6, 'RICHARD TAN CHIN YAU', '-', '', 2),
(73, 7, 'KATHERINE KOH LAY YEN ', '-', '98329970', 1),
(74, 7, 'KOH LEONG SHENG ', '-', '96674095', 2),
(75, 8, 'ONG BEE CHEW', '-', '96399148', 1),
(76, 8, 'LINDA NEO', '-', '96217158', 2),
(77, 9, 'ANG BANG YAO', '-', '83388888', 10),
(78, 10, 'JIMMY LIM HUAY SOON', '-', '96719949', 5),
(79, 10, 'LIM ENG HOCK', '-', '96284034', 1),
(80, 23, 'TSAI SUN QUE', 'cc', '98397568', 44),
(81, 11, 'ANN LEE', '-', '96301181', 20),
(82, 11, 'SHAUN LEE', '-', '97397341', 11),
(83, 12, 'GEORGE NADER', '-', '', 33),
(84, 13, 'CHUNG YEN SEN', '-', '', 33),
(85, 13, 'KOH BON POCK PBM ', '-', '96734485', 55),
(86, 15, 'TOK CHING KWE ', '-', '98186236', 1),
(87, 15, 'DANNY TOK', '-', '', 33),
(93, 24, 'LIAU BOON HWA', '-', '98169828', 33),
(94, 24, 'LIAU CHONG SENG', '-', '', 55),
(95, 28, 'LOH KWAN SENG', 'cc', '', 2),
(96, 28, 'HIAP HAN PENG', 'cc', '', 3),
(97, 29, 'TEOH BOON CHONG', 'cc', '96735481', 54),
(98, 29, 'TEOH BOON SENG', 'cc', '96737835', 77),
(99, 30, 'TAN TEE MENG', 'cc', '96753988', 2),
(100, 30, 'HENG SOON MIANG', 'cc', '96649662', 66),
(101, 31, 'TEO CHOON HONG', 'cc', '96316438', 77),
(103, 33, 'CHUA CHIN CHENG, MICHAEL', 'cc', '90903922', 33),
(104, 32, 'ANG LAY WAH', '-', '96383167', 88),
(105, 34, 'THOMAS TOK', 'cc', '', 33),
(106, 34, 'B. C. TOK', 'cc', '96781802', 5),
(107, 35, 'HON QUEK YANG', 'cc', '', 21),
(108, 36, 'SOH HOCK LEONG', 'cc', '', 33),
(109, 36, 'SOH HOCK SIAN', 'cc', '97379188', 87),
(110, 37, 'CHONG HA LEE', 'cc', '96333592', 55),
(111, 37, 'TEO BOON TENG', 'cc', '96708959', 98),
(112, 38, 'SEE KEONG TAN', 'cc', '97374908', 55),
(113, 38, 'KOH KENG CHEE', 'cc', '96199025', 87),
(114, 39, 'CHOI KEOK BANG PBM ', 'cc', '96698168', 55),
(115, 39, 'RICK CHOI CHEONG AIK', 'cc', '98168168', 94),
(116, 40, 'TAY NAM FUI', 'cc', '', 45),
(117, 40, 'KELVIN TAY', 'cc', '', 44),
(118, 41, 'JASON SIM CHON ANG', 'cc', '96779997', 56),
(119, 42, 'ONG CHEE HIEN', 'cc', '96681957', 99),
(120, 42, 'SHIRLEY BONG', 'cc', '97476657', 65),
(121, 43, 'CHUA PENG HONG', 'cc', '96248189', 87),
(122, 43, 'TEO BIOW HIN', 'cc', '96258189', 11),
(123, 44, 'SIAU BEE CHIN', 'cc', '', 44),
(124, 44, 'WONG WENG KEE', 'cc', '96319379', 66),
(125, 45, 'RAYMOND GAN', 'cc', '', 43),
(126, 46, 'CHUA ZHI HONG', 'cc', '', 44),
(127, 47, 'LAU KOI FONG', 'cc', '98169835', 33),
(128, 48, 'KEA BENG HIAN', 'cc', '', 34),
(129, 49, 'SAM KERR', 'cc', '91467993', 55),
(130, 50, 'CHENG HUA DOK ', 'cc', '98219258', 34),
(131, 50, 'GOH LICK CHAI', 'cc', '97431789', 87),
(132, 51, 'CHUA CHIN SEONG BBM', 'cc', '96608825', 33),
(133, 52, 'PETER HO', 'cc', '', 43),
(134, 52, 'LEE HOCK GIAP', 'cc', '', 34),
(135, 53, 'TENG YEW CHURK', 'cc', '96611666', 66),
(136, 53, 'TENG KIAN ENG DESMOND', 'cc', '98711666', 32),
(137, 54, 'NG KOK LAY', 'cc', '96898492', 45),
(138, 55, 'TAY CHEE HIAN', 'cc', '', 22),
(139, 56, 'YEO HEE LIAN PETER', 'cc', '', 33),
(140, 56, 'YEO HEE IN', 'cc', '', 22),
(141, 57, 'NEO KOON BOO ', 'cc', '96260785', 99),
(142, 57, 'MAY YAP MUI KEE', 'cc', '97588855', 22),
(143, 58, 'SENG SONG WEN', 'cc', '98163464', 55),
(144, 59, 'LIM BOK TECK', 'cc', '96505595', 23),
(145, 59, 'TING KIM CHEE', 'cc', '84415595', 34),
(146, 60, 'TEO KIANG ANG', 'cc', '', 54),
(147, 60, 'ROSALIND TAN ', 'cc', '63896903', 11),
(148, 61, 'MAX TOH CHEE BEN', 'cc', '97872008', 43),
(149, 61, 'ANG SHAO KAI', 'cc', '96970820', 66),
(150, 62, 'SIMON OEI ENG HAUW', 'cc', '90251628', 56),
(151, 63, 'QUEK SOE MEN', 'cc', '', 22),
(152, 63, ' RICHMOND QUEK', 'cc', '', 11),
(153, 64, 'NG CHIEW SENG BBM', 'cc', '97353828', 45),
(154, 64, 'NG HEOK KWEE FREDDIE', 'cc', '97363800', 55),
(155, 65, 'LIM KIA CHENG', 'cc', '', 34),
(156, 66, 'SIM BENG HUAT HENRY', 'cc', '96306898', 22),
(157, 66, 'CHUNG BOO HENG', 'cc', '96795669', 66),
(158, 67, 'LEE TUCK KEONG', 'cc', '96446169', 34),
(159, 67, 'ONG CHUE HOO', 'cc', '96791795', 55),
(160, 68, 'DARSHAN RAIYANI', 'cc', '97282606', 65),
(161, 69, 'JUDY KOH ', 'cc', '97278622', 432),
(162, 70, 'JASON CHANG ', 'cc', '96222168', 432),
(163, 71, 'MOHIT MAHESHWARI', 'cc', '97533944', 654),
(164, 72, 'NG LIAN SENG', 'cc', '97465287', 654),
(165, 72, 'PEK PENG SENG', 'cc', '98340655', 123),
(166, 73, 'LOW BOH KEE', 'cc', '96153030', 333),
(167, 73, 'WEE LAU CHOR WILLIAM', 'cc', '', 222),
(168, 74, 'CHOA GOAN HO', 'cc', '82928383', 654),
(169, 74, 'CHOA WEE KEONG', 'cc', '91198383', 55),
(170, 75, 'OEI HONG BIE', 'cc', '', 324),
(171, 76, 'CHAN LUNG YUEN', 'cc', '93639955', 22),
(172, 77, 'LEE CHENG HOA', 'cc', '98219277', 55),
(173, 77, 'LEE MING PENG', 'cc', '91389293', 87),
(174, 78, 'CHUA KAY CHERNG', 'cc', '96589563', 45),
(175, 78, 'CHUA CHIN LIAN', 'cc', '97871235', 33),
(176, 79, 'TAN ANG PIAW', 'cc', '', 32),
(177, 80, 'GWEE POO TEO', 'cc', '97908745', 33),
(178, 80, 'HOI KEM PONG', 'cc', '90908548', 44),
(179, 81, 'KAREN LIM', 'cc', '97532128', 76),
(180, 82, 'LIM HOO SIG', 'cc', '96601379', 543),
(181, 83, 'NG YIT FOOI', 'cc', '', 33),
(182, 83, 'LIM KOK THUN', 'cc', '', 54),
(183, 84, 'LIM ENG HONG', 'cc', '90039993', 33),
(184, 84, 'LIM CHOR YANG ', 'cc', '96600889', 12),
(185, 85, 'CHIA KOK BENG', 'cc', '96878282', 123),
(186, 86, 'TAY SIEW TECK', 'cc', '96330341', 54),
(187, 87, 'ASHWINI MUDGAL', 'cc', '91053560', 34),
(188, 88, 'NG LAM SIONG', 'cc', '', 43),
(189, 89, 'ONG YEE CHOK', 'cc', '', 23),
(190, 90, 'LIM ENG TUAN', 'cc', '', 43),
(191, 91, 'EDWIN POR HOW SHENG', 'cc', '98588181', 22),
(192, 92, 'NG KIM HAN', 'cc', '', 23),
(193, 92, 'NG KENG HUAT', 'cc', '', 11),
(194, 93, 'CHUA SENG CHONG PBM', 'cc', '', 32),
(195, 93, 'CHUA CHEE SHENG', 'cc', '98470838', 43),
(196, 94, 'SOH HOCK LEONG', 'cc', '', 22),
(197, 94, 'SOH HOCK SIAN', 'cc', '97379188', 54),
(198, 95, 'TAN TEK LAI', 'cc', '98186719', 11),
(202, 96, 'KOH BOON THIAM', '-', '', 22),
(203, 96, 'DEW KOH', '-', '', 32),
(204, 96, 'GARY GOH', '-', '', 12),
(205, 97, 'CHOU TIEN SHANG', 'cc', '', 32),
(206, 97, 'SHELLEY CHANG', 'cc', '', 11),
(207, 98, 'WIN KO KO THAUNG', 'cc', '92950167', 22),
(208, 99, 'TARUN CHAMANLAL MEHTA', 'cc', '90475425', 32),
(209, 99, 'YASH MEHTA', 'cc', '90234211', 11),
(210, 100, 'ONG MENG TEE', 'cc', '96791229', 44),
(211, 100, 'ONG CHEE KANG', 'cc', '', 32),
(212, 101, 'SEAH SIEW HENG', 'cc', '96718633', 34),
(213, 102, 'TANG POH CHYE', 'cc', '', 32),
(214, 102, 'TAN CHIN ANN', 'cc', '93839203', 54);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Role`
--

CREATE TABLE `sta_Role` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Status` enum('Active','Delete','Disabled') NOT NULL,
  `LastUpdatedDateTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_Role`
--

INSERT INTO `sta_Role` (`ID`, `Name`, `Status`, `LastUpdatedDateTime`) VALUES
(1, 'superadmin', 'Active', 0),
(2, 'normalAdmin', 'Active', 1478498280425);

-- --------------------------------------------------------

--
-- Table structure for table `sta_Roledetails`
--

CREATE TABLE `sta_Roledetails` (
  `ID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sta_Settings`
--

CREATE TABLE `sta_Settings` (
  `ID` int(11) NOT NULL,
  `SettingName` varchar(200) NOT NULL,
  `SettingValue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sta_ActivityLog`
--
ALTER TABLE `sta_ActivityLog`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Advertisement`
--
ALTER TABLE `sta_Advertisement`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Associatecompany`
--
ALTER TABLE `sta_Associatecompany`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_BackendUser`
--
ALTER TABLE `sta_BackendUser`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Boc`
--
ALTER TABLE `sta_Boc`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Company`
--
ALTER TABLE `sta_Company`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Event`
--
ALTER TABLE `sta_Event`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Module`
--
ALTER TABLE `sta_Module`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Representative`
--
ALTER TABLE `sta_Representative`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Role`
--
ALTER TABLE `sta_Role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Roledetails`
--
ALTER TABLE `sta_Roledetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_Settings`
--
ALTER TABLE `sta_Settings`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sta_ActivityLog`
--
ALTER TABLE `sta_ActivityLog`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;
--
-- AUTO_INCREMENT for table `sta_Advertisement`
--
ALTER TABLE `sta_Advertisement`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sta_Associatecompany`
--
ALTER TABLE `sta_Associatecompany`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `sta_BackendUser`
--
ALTER TABLE `sta_BackendUser`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sta_Boc`
--
ALTER TABLE `sta_Boc`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `sta_Company`
--
ALTER TABLE `sta_Company`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `sta_Event`
--
ALTER TABLE `sta_Event`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sta_Module`
--
ALTER TABLE `sta_Module`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sta_Representative`
--
ALTER TABLE `sta_Representative`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `sta_Role`
--
ALTER TABLE `sta_Role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sta_Roledetails`
--
ALTER TABLE `sta_Roledetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sta_Settings`
--
ALTER TABLE `sta_Settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
