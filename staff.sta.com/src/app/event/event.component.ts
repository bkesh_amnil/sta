import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

import {PageScrollConfig, PageScrollService, PageScrollInstance} from 'ng2-page-scroll';
import {PaginationInstance} from 'ng2-pagination';

import {Event, EventService, ValidationService} from '../models';

import {CustomPaginationComponent} from '../shared/custom-pagination/custom-pagination.component';

import {CONFIG, AlertService} from '../core';
import {FileUploadComponent} from "../shared/file-upload/file-upload.component";

let imageUrl = CONFIG.baseUrls.eventImages;

@Component({
    selector: 'event',
    templateUrl: 'event.component.html'
})
export class EventComponent implements AfterViewInit {
    objEventList: Event[];

    objEventAdd: Event = new Event();
    objEventEdit: Event = new Event();

    @ViewChild('uploadComponentAdd')
    private uploadComponentAdd: FileUploadComponent;
    @ViewChild('uploadComponentEdit')
    private uploadComponentEdit: FileUploadComponent;

    @ViewChild('topPagination')
    private topPagination: CustomPaginationComponent;
    @ViewChild('bottomPagination')
    private bottomPagination: CustomPaginationComponent;

    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    searchQuery: string = '';

    loadingData: boolean = false;
    savingAddData: boolean = false;
    savingEditData: boolean = false;
    saveAndSendEmailConfirm: boolean = false;
    editAndSendEmailConfirm: boolean = false;

    addEventDisplayPriority: boolean = false;
    editEventDisplayPriority: boolean;

    private sortIcon = {
        EventName: "asc",
        EventDate: "asc",
        EventFrom: "asc",
        // Description: "asc",
        Venue: "asc",
        // Latitude: "asc",
        CourseFee: "asc",
        // Remarks: "asc",
        DisplayPriority: "asc"
    };
    sortAscending: boolean = true;

    constructor(private eventService: EventService,
                private pageScrollService: PageScrollService,
                private alertService: AlertService,
                private validationService: ValidationService,
                @Inject(DOCUMENT) private document: Document) {
        PageScrollConfig.defaultDuration = 500;
    }

    ngAfterViewInit(): void {
        this.reloadData();
        this.topPagination.config = this.config;
        this.bottomPagination.config = this.config;

        this.uploadComponentAdd.showImageComponent("Upload Image (880 x 220 Pixels)");
        this.uploadComponentEdit.showImageComponent("Upload Image (880 x 220 Pixels)");

        this.uploadComponentAdd.showPdfComponent("Upload Brochure");
        this.uploadComponentEdit.showPdfComponent("Upload Brochure");
    }

    reloadData(): void {
        this.loadingData = true;
        this.objEventList = [];
        this.eventService
            .getEvents()
            .subscribe(
                events => {
                    this.objEventList = events;
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.')
                    this.loadingData = false;
                });
    }

    uploadAddImage(imageFile: File) {
        this.objEventAdd.ImageFile = imageFile;
    }

    uploadEditImage(imageFile: File) {
        this.objEventEdit.ImageFile = imageFile;
        if (!imageFile) {
            this.objEventEdit.ImageDeletedOnEdit = true;
        }
    }

    uploadAddPdf(pdfFile: File) {
        this.objEventAdd.BrochureFile = pdfFile;
    }

    uploadEditPdf(pdfFile: File) {
        this.objEventEdit.BrochureFile = pdfFile;
        if (!pdfFile) {
            this.objEventEdit.PdfDeletedOnEdit = true;
        }
    }

    saveAddData(): void {
        if (this.addEventDisplayPriority) {
            this.objEventAdd.DisplayPriority = 1;
        }
        else {
            this.objEventAdd.DisplayPriority = 0;
        }

        if (this.validationService.event(this.objEventAdd)) {
            this.savingAddData = true;

            let operation: Observable<Event>;

            operation = this.eventService.addEvent(this.objEventAdd)

            operation.subscribe(
                events => {
                    this.goToTop();
                    this.reloadData();

                    this.objEventAdd = new Event();

                    this.uploadComponentAdd.resetImageComponent();
                    this.uploadComponentAdd.resetPdfComponent();

                    this.savingAddData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingAddData = false;
                });
        }
    }

    saveAddDataAndSendEmail(): void {
        if (!this.validationService.event(this.objEventAdd)) {
            return;
        }
        this.saveAndSendEmailConfirm = true;
    }

    toggleSaveAndSendEmailConfirm() {
        this.saveAndSendEmailConfirm = !this.saveAndSendEmailConfirm;
    }

    confirmSaveAndSendEmail() {
        if (this.addEventDisplayPriority) {
            this.objEventAdd.DisplayPriority = 1;
        }
        else {
            this.objEventAdd.DisplayPriority = 0;
        }
        this.saveAndSendEmailConfirm = false;
        if (this.validationService.event(this.objEventAdd)) {
            this.savingAddData = true;

            let operation: Observable<Event>;

            operation = this.eventService.addEventAndEmail(this.objEventAdd)

            operation.subscribe(
                events => {
                    this.goToTop();
                    this.reloadData();

                    this.objEventAdd = new Event();

                    this.uploadComponentAdd.resetImageComponent();
                    this.uploadComponentAdd.resetPdfComponent();

                    this.savingAddData = false;
                },
                error => {
                    this.savingAddData = false;
                    this.alertService.alertWarning(error);
                });
        }
    }

    addDisplayPriority(displayPriority: number) {
        this.objEventAdd.DisplayPriority = displayPriority;
        console.log('hello', displayPriority);
    }

    saveEditData(): void {
        if (this.editEventDisplayPriority) {
            this.objEventEdit.DisplayPriority = 1;
        }
        else {
            this.objEventEdit.DisplayPriority = 0;
        }

        if (this.validationService.event(this.objEventEdit)) {
            this.savingEditData = true;

            let operation: Observable<Event>;

            operation = this.eventService.updateEvent(this.objEventEdit);

            operation.subscribe(
                events => {
                    this.goToTop();
                    this.reloadData();

                    this.objEventEdit = new Event();

                    this.uploadComponentEdit.resetImageComponent();
                    this.uploadComponentEdit.resetPdfComponent();

                    this.savingEditData = false;
                },
                error => {
                    this.alertService.alertWarning(error)
                    this.savingEditData = false;
                });
        }
    }

    saveEditDataAndSendEmail(): void {
        if (!this.validationService.event(this.objEventEdit)) {
            return;
        }
        this.editAndSendEmailConfirm = true;
    }

    editDisplayPriority(displayPriority: number) {
        this.objEventEdit.DisplayPriority = displayPriority;
        console.log('this.objEventEdit.DisplayPriority', this.objEventEdit.DisplayPriority);
    }

    toggleEditAndSendEmailConfirm() {
        this.editAndSendEmailConfirm = !this.editAndSendEmailConfirm;
    }

    confirmEditAndSendEmail() {
        if (this.editEventDisplayPriority) {
            this.objEventEdit.DisplayPriority = 1;
        }
        else {
            this.objEventEdit.DisplayPriority = 0;
        }
        this.editAndSendEmailConfirm = false;
        if (this.validationService.event(this.objEventEdit)) {
            this.savingEditData = true;

            let operation: Observable<Event>;

            operation = this.eventService.updateEventAndEmail(this.objEventEdit)

            operation.subscribe(
                events => {
                    this.goToTop();
                    this.reloadData();

                    this.objEventEdit = new Event();

                    this.uploadComponentEdit.resetImageComponent();
                    this.uploadComponentEdit.resetPdfComponent();

                    this.savingEditData = false;
                },
                error => {
                    this.savingEditData = false;
                    this.alertService.alertWarning(error);
                });
        }
    }


    editData(objEvent: Event): void {
        this.objEventEdit.ID = objEvent.ID;
        this.objEventEdit.EventName = objEvent.EventName;
        this.objEventEdit.EventDate = objEvent.EventDate;
        this.objEventEdit.EventFrom = objEvent.EventFrom;
        this.objEventEdit.EventTo = objEvent.EventTo;
        this.objEventEdit.Description = objEvent.Description;
        this.objEventEdit.Venue = objEvent.Venue;
        this.objEventEdit.Latitude = objEvent.Latitude;
        this.objEventEdit.Longitude = objEvent.Longitude;
        this.objEventEdit.CourseFee = objEvent.CourseFee;
        this.objEventEdit.Remarks = objEvent.Remarks;
        this.objEventEdit.DisplayPriority = objEvent.DisplayPriority;
        this.objEventEdit.PrevImage = objEvent.Image;
        this.objEventEdit.PrevDoc = objEvent.Pdf;

        this.uploadComponentEdit.resetImageComponent();
        this.uploadComponentEdit.resetPdfComponent();
        this.uploadComponentEdit.initDataImage(objEvent.Image, imageUrl + objEvent.Image);
        if (this.objEventEdit.PrevDoc != "") {
            this.uploadComponentEdit.initDataPdf(objEvent.Pdf);
        }
        this.objEventEdit.ImageDeletedOnEdit = false;
        this.objEventEdit.PdfDeletedOnEdit = false;

        if (this.objEventEdit.DisplayPriority === 1) {
            this.editEventDisplayPriority = true;
        }
        else {
            this.editEventDisplayPriority = false;
        }

        this.goToEdit();
    }

    goToTop(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#top');
        this.pageScrollService.start(pageScrollInstance);
    }

    goToEdit(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#editSectionWrapper');
        this.pageScrollService.start(pageScrollInstance);
    }

    undoEdit(): void {
        this.goToTop();

        this.uploadComponentEdit.resetImageComponent();
        this.uploadComponentEdit.resetPdfComponent();

        this.objEventEdit = new Event();
    }

    cancelAdd(): void {
        this.goToTop();

        this.uploadComponentAdd.resetImageComponent();
        this.uploadComponentAdd.resetPdfComponent();

        this.addEventDisplayPriority = false;

        this.objEventAdd = new Event();
    }

    filterEvent() {
        this.loadingData = true;
        this.objEventList = [];
        this.eventService.getEvents()
            .subscribe(
                events => {
                    this.objEventList = events.filter(
                        data =>
                        data.EventName.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.EventDate.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.EventFrom.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.EventTo.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Venue.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                    );
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                });
    }

    sort(sortIndex: number) {
        if (this.sortAscending) {
            this.objEventList = this.objEventList
                .sort((a, b) =>
                    sortIndex === 0 ? b.EventName.localeCompare(a.EventName) :
                        sortIndex === 1 ? b.EventDate.localeCompare(a.EventDate) :
                            sortIndex === 2 ? b.EventFrom.localeCompare(a.EventFrom) :
                                sortIndex === 3 ? b.Venue.localeCompare(a.Venue) :
                                    sortIndex === 4 ? b.CourseFee.localeCompare(a.CourseFee) :
                                        b.DisplayPriority - a.DisplayPriority);
            this.sortAscending = false;
        }
        else {
            this.objEventList = this.objEventList
                .sort((a, b) =>
                    sortIndex === 0 ? a.EventName.localeCompare(b.EventName) :
                        sortIndex === 1 ? a.EventDate.localeCompare(b.EventDate) :
                            sortIndex === 2 ? a.EventFrom.localeCompare(b.EventFrom) :
                                sortIndex === 3 ? a.Venue.localeCompare(b.Venue) :
                                    sortIndex === 4 ? a.CourseFee.localeCompare(b.CourseFee) :
                                        a.DisplayPriority - b.DisplayPriority);
            this.sortAscending = true;
        }

        sortIndex === 0 ?
            this.sortIcon.EventName = this.sortAscending ? "asc" : "desc" :
            sortIndex === 1 ?
                this.sortIcon.EventDate = this.sortAscending ? "asc" : "desc" :
                sortIndex === 2 ?
                    this.sortIcon.EventFrom = this.sortAscending ? "asc" : "desc" :
                    sortIndex === 3 ?
                        this.sortIcon.Venue = this.sortAscending ? "asc" : "desc" :
                        sortIndex === 4 ?
                            this.sortIcon.CourseFee = this.sortAscending ? "asc" : "desc" :
                            this.sortIcon.DisplayPriority = this.sortAscending ? "asc" : "desc";
    }
}
