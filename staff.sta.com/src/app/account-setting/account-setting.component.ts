import {Component, OnInit, EventEmitter} from '@angular/core';

import {StaffUser, ValidationService} from '../models';
import {LoginService} from "../models/login.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {AlertService} from "../core";
import {ChangePassword} from "../models/login-user.model";
import {EmitterService} from "../core/emitter.service";

@Component({
    selector: 'account-setting',
    templateUrl: 'account-setting.component.html'
})
export class AccountSettingComponent implements OnInit {
    private _savingAccountInfo: boolean = false;
    private _savingPasswordChange: boolean = false;

    private _savedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    private _savedAccountInfo: boolean = false;

    private _changePasswordEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    private _savedChangePassword: boolean = false;

    objStaffUser: StaffUser = new StaffUser();
    objChangePassword: ChangePassword = new ChangePassword();

    constructor(private loginService: LoginService,
                private validationService: ValidationService,
                private alertService: AlertService,
                private router: Router) {
        this._savedEvent
            .delay(2000)
            .subscribe((isSaved) => {
                if (isSaved)
                    this._savedAccountInfo = false
            });

        this._changePasswordEvent
            .delay(2000)
            .subscribe((isSaved) => {
                this._savedChangePassword = false;
                this.loginService.logout();
                this.router.navigate(['/']);
            });
    }

    ngOnInit() {
        if (this.loginService.isLoggedIn()) {
            this.objStaffUser.FirstName = this.loginService.getSessionUser().FirstName;
            this.objStaffUser.Email = this.loginService.getSessionUser().Email;
            this.objStaffUser.Phone = this.loginService.getSessionUser().Phone;
        } else {
            this.router.navigate(['/login']);
        }
    }

    private _saveAccountInfo() {
        if (this.validationService.accountInfo(this.objStaffUser)) {
            this._savingAccountInfo = true;

            let operation: Observable<StaffUser>;

            operation = this.loginService.editAccountInfo(this.objStaffUser);

            operation.subscribe(
                staffUser => {
                    this.objStaffUser.FirstName = staffUser.FirstName;
                    this.objStaffUser.Email = staffUser.Email;
                    this.objStaffUser.Phone = staffUser.Phone;

                    this._savingAccountInfo = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this._savingAccountInfo = false;
                },
                () => {
                    this._savedAccountInfo = true;
                    this._savedEvent.emit(true);
                });
        }
    }

    private _changePassword() {
        if (this.validationService.changePassword(this.objChangePassword)) {
            this._savingPasswordChange = true;

            let operation: Observable<boolean>;

            operation = this.loginService.changePassword(this.objChangePassword);

            operation.subscribe(
                isChanged => {
                    if (isChanged) {
                        this._savingPasswordChange = false;
                        this.objChangePassword = new ChangePassword();
                    }
                    else {
                        this.alertService.alertWarning('Something went wrong.')
                    }
                },
                error => {
                    this.alertService.alertWarning(error);
                    this._savingPasswordChange = false;
                },
                () => {
                    this._savedChangePassword = true;
                    this._changePasswordEvent.emit(true);
                });
        }
    }

    private _cancelPasswordChange() {

    }

    private _cancelAccountInfo() {

    }
}
