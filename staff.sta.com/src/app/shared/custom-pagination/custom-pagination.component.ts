import { Component, OnInit } from '@angular/core';
import {PaginationInstance} from 'ng2-pagination';

@Component({
    selector: 'custom-pagination',
    templateUrl: 'custom-pagination.component.html'
})
export class CustomPaginationComponent implements OnInit {

    public config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    constructor() { }

    ngOnInit() { }

    changeItemsPerPage(itemsPerPage: number) {
        this.config.itemsPerPage = itemsPerPage;
    }
}
