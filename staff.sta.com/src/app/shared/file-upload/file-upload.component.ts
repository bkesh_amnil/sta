import {Component, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import {AlertService} from "../../core";

@Component({
    selector: 'file-upload',
    templateUrl: 'file-upload.component.html'
})

export class FileUploadComponent {
    private _hasImage: boolean = false;
    private _showDeleteImage: boolean = false;

    private _hasPdf: boolean = false;
    private _showDeletePdf: boolean = false;

    private _imageUploadComponent: boolean = false;
    private _pdfUploadComponent: boolean = false;
    private _imageUploadTitle: string = 'Upload Image';
    private _pdfUploadTitle: string = 'Upload PDF';

    private _imageFileName: string = '';
    private _pdfFileName: string = '';
    private _imageSrc: string = '';

    @Output() imageUploaded = new EventEmitter<File>();
    @Output() pdfUploaded = new EventEmitter<File>();

    @ViewChild('imageUploadInput')
    private imageUploadInput: ElementRef;
    @ViewChild('pdfUploadInput')
    private pdfUploadInput: ElementRef;

    constructor(private alertService: AlertService) {

    }

    private handleImageUpload() {
        let imageFile: File = this.imageUploadInput.nativeElement.files[0];
        if (imageFile) {
            this._imageFileName = imageFile.name;

            let pattern = /image-*/;
            let reader = new FileReader();

            if (!imageFile.type.match(pattern)) {
                this.alertService.alertWarning("Please upload an Image file.");
                return;
            }

            reader.onload = this._loadPreview.bind(this);
            reader.readAsDataURL(imageFile);

            console.log(imageFile);
            console.log('_imageSrc', this._imageSrc);

            this.imageUploaded.emit(imageFile);
        }
    }

    private _loadPreview(event: ProgressEvent) {
        let readerResult: FileReader = event.target as FileReader;
        this.setImagePreview(readerResult.result);
        this._hasImage = true;
    }

    private setImagePreview(imageSrc: string): void {
        this._imageSrc = imageSrc;
    }

    private handlePdfUpload() {
        let pdfFile: File = this.pdfUploadInput.nativeElement.files[0];
        this._pdfFileName = pdfFile.name;

        let pattern = /pdf-*/;
        let reader = new FileReader();

        if (!pdfFile.type.match(pattern)) {
            this.alertService.alertWarning('Please upload PDF file.');
            return;
        }
        this._hasPdf = true;

        this.pdfUploaded.emit(pdfFile);
    }

    private deleteImage() {
        this.resetImageComponent();
    }

    private deletePdf() {
        this.resetPdfComponent();
    }

    private toggleDeleteImage(): void {
        this._showDeleteImage = !this._showDeleteImage;
    }

    private toggleDeletePdf(): void {
        this._showDeletePdf = !this._showDeletePdf;
    }

    showImageComponent(title: string) {
        this._imageUploadComponent = true;
        this._imageUploadTitle = title;
    }

    showPdfComponent(title: string) {
        this._pdfUploadComponent = true;
        this._pdfUploadTitle = title;
    }

    initDataImage(fileName: string, imageSrc: string) {
        this._imageFileName = fileName;
        this._imageSrc = imageSrc;
        this._hasImage = true;
    }

    initDataPdf(fileName: string) {
        this._pdfFileName = fileName;
        this._hasPdf = true;
    }

    resetImageComponent() {
        this.imageUploadInput.nativeElement.value = '';
        this.imageUploaded.emit(null);
        this._imageSrc = null;
        this._hasImage = false;
        this._showDeleteImage = false;
    }

    resetPdfComponent() {
        this.pdfUploadInput.nativeElement.value = '';
        this.pdfUploaded.emit(null);
        this._hasPdf = false;
        this._showDeletePdf = false;
    }
}
