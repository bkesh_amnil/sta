import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

import {PageScrollConfig, PageScrollService, PageScrollInstance} from 'ng2-page-scroll';
import {PaginationInstance} from 'ng2-pagination';

import {Company, Representative, Associate, CompanyService, ValidationService} from '../models';

import {CustomPaginationComponent} from '../shared/custom-pagination/custom-pagination.component';

import {AlertService} from '../core';

@Component({
    selector: 'company',
    templateUrl: 'company.component.html'
})
export class CompanyComponent implements AfterViewInit {
    objCompanyList: Company[];

    objRepresentativeList: Representative[] = [];
    objAssociateList: Associate[] = [];

    objRepresentativeEditList: Representative[] = [];
    objAssociateEditList: Associate[] = [];

    objCompanyAdd: Company = new Company();
    objCompanyEdit: Company = new Company();

    objRepresentativeAdd: Representative = new Representative();
    objAssociateAdd: Associate = new Associate();

    objRepresentativeEdit: Representative = new Representative();
    objAssociateEdit: Associate = new Associate();

    @ViewChild('topPagination')
    private topPagination: CustomPaginationComponent;
    @ViewChild('bottomPagination')
    private bottomPagination: CustomPaginationComponent;

    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    searchQuery: string = '';

    loadingData: boolean = false;
    savingAddData: boolean = false;
    savingEditData: boolean = false;

    addCompanyDisplayPriority = false;
    editCompanyDisplayPriority;

    private sortIcon = {
        CompanyNameEnglish: "asc",
        // CompanyNameChinese: "asc",
        // Address: "asc",
        Phone: "asc",
        // Fax: "asc",
        Email: "asc",
        Website: "asc",
        Registration: "asc",
        // ProductsServicesEnglish: "asc",
        // ProductsServicesChinese: "asc",
        DisplayPriority: "asc"
    };
    sortAscending: boolean = true;

    pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#top');

    constructor(private companyService: CompanyService,
                private pageScrollService: PageScrollService,
                private alertService: AlertService,
                private validationService: ValidationService,
                @Inject(DOCUMENT) private document: Document) {
        PageScrollConfig.defaultDuration = 500;
    }

    ngAfterViewInit(): void {
        this.reloadData();

        this.topPagination.config = this.config;
        this.bottomPagination.config = this.config;
    }

    reloadData(): void {
        this.loadingData = true;
        this.objCompanyList = [];
        this.companyService
            .getCompanies()
            .subscribe(
                companies => {
                    this.objCompanyList = companies;
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                }
            )
        ;
    }

    saveAddData(): void {
        if (this.addCompanyDisplayPriority) {
            this.objCompanyAdd.DisplayPriority = 1;
        }
        else {
            this.objCompanyAdd.DisplayPriority = 0;
        }

        this.objCompanyAdd.Representatives = this.objRepresentativeList;
        this.objCompanyAdd.Associates = this.objAssociateList;

        if (this.validationService.company(this.objCompanyAdd)) {
            this.savingAddData = true;

            let operation: Observable<Company>;

            operation = this.companyService.addCompany(this.objCompanyAdd);

            operation.subscribe(
                companies => {
                    this.goToTop();
                    this.reloadData();

                    this.objCompanyAdd = new Company();
                    this.objAssociateList = [];
                    this.objRepresentativeList = [];
                    this.objAssociateAdd = new Associate();
                    this.objRepresentativeAdd = new Representative();

                    this.savingAddData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingAddData = false;
                });
        }
    }

    saveEditData(): void {
        if (this.editCompanyDisplayPriority) {
            this.objCompanyEdit.DisplayPriority = 1;
        }
        else {
            this.objCompanyEdit.DisplayPriority = 0;
        }

        this.objCompanyEdit.Representatives = this.objRepresentativeEditList;
        this.objCompanyEdit.Associates = this.objAssociateEditList;

        console.log("this.objCompanyEdit", this.objCompanyEdit);

        if (this.validationService.company(this.objCompanyEdit)) {
            this.savingEditData = true;

            let operation: Observable<Company>;

            operation = this.companyService.updateCompany(this.objCompanyEdit);

            operation.subscribe(
                companies => {
                    this.goToTop();
                    this.reloadData();

                    this.objCompanyEdit = new Company();
                    this.objRepresentativeEdit = new Representative();
                    this.objAssociateEdit = new Associate();

                    this.objRepresentativeEditList = [];
                    this.objAssociateEditList = [];

                    this.savingEditData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingEditData = false;
                });
        }
    }

    editData(objCompanyEdit: Company): void {
        console.log("objCompanyEdit", objCompanyEdit);
        this.objCompanyEdit.ID = objCompanyEdit.ID;
        this.objCompanyEdit.CompanyNameEnglish = objCompanyEdit.CompanyNameEnglish;
        this.objCompanyEdit.CompanyNameChinese = objCompanyEdit.CompanyNameChinese;
        this.objCompanyEdit.Address = objCompanyEdit.Address;
        this.objCompanyEdit.Phone = objCompanyEdit.Phone;
        this.objCompanyEdit.Fax = objCompanyEdit.Fax;
        this.objCompanyEdit.Email = objCompanyEdit.Email;
        this.objCompanyEdit.Website = objCompanyEdit.Website;
        this.objCompanyEdit.Registration = objCompanyEdit.Registration;
        this.objCompanyEdit.ProductsServicesEnglish = objCompanyEdit.ProductsServicesEnglish;
        this.objCompanyEdit.ProductsServicesChinese = objCompanyEdit.ProductsServicesChinese;
        this.objCompanyEdit.DisplayPriority = objCompanyEdit.DisplayPriority;

        this.objRepresentativeEditList = objCompanyEdit.Representatives;
        this.objAssociateEditList = objCompanyEdit.Associates;

        if (this.objCompanyEdit.DisplayPriority === 1) {
            this.editCompanyDisplayPriority = true;
        }
        else {
            this.editCompanyDisplayPriority = false;
        }

        this.goToEdit();
    }

    goToTop(): void {
        this.pageScrollService.start(this.pageScrollInstance);
    }

    goToEdit(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#editSectionWrapper');
        this.pageScrollService.start(pageScrollInstance);
    }

    addAssociate(): void {
        if (this.validationService.associate(this.objAssociateAdd) && !this.validationService.hasDuplicateAssociate(this.objAssociateList, this.objAssociateAdd.AssociateCompanyNameEnglish)) {
            this.objAssociateList.push(this.objAssociateAdd);
            this.objAssociateAdd = new Associate();
        }
    }

    addRepresentative(): void {
        if (this.validationService.representative(this.objRepresentativeAdd) && !this.validationService.hasDuplicateRepresentative(this.objRepresentativeList, this.objRepresentativeAdd.NameEnglish)) {
            this.objRepresentativeList.push(this.objRepresentativeAdd);
            this.objRepresentativeAdd = new Representative();
        }
    }

    removeRepresentative(objRepresentative: Representative): void {
        this.objRepresentativeList.splice(this.objRepresentativeList.indexOf(objRepresentative), 1);
    }

    removeAssociate(objAssociate: Associate): void {
        this.objAssociateList.splice(this.objAssociateList.indexOf(objAssociate), 1);
    }

    addEditAssociate(): void {
        if (this.validationService
                .associate(this.objAssociateEdit)
            && !this.validationService
                .hasDuplicateAssociate(
                    this.objAssociateEditList,
                    this.objAssociateEdit.AssociateCompanyNameEnglish)) {
            this.objAssociateEditList.push(this.objAssociateEdit);
            this.objAssociateEdit = new Associate();
        }
    }

    addEditRepresentative(): void {
        if (this.validationService
                .representative(this.objRepresentativeEdit)
            && !this.validationService
                .hasDuplicateRepresentative(
                    this.objRepresentativeEditList,
                    this.objRepresentativeEdit.NameEnglish)) {
            this.objRepresentativeEditList.push(this.objRepresentativeEdit);
            this.objRepresentativeEdit = new Representative();
        }
    }

    removeEditRepresentative(objRepresentative: Representative): void {
        this.objRepresentativeEditList.splice(this.objRepresentativeEditList.indexOf(objRepresentative), 1);
    }

    removeEditAssociate(objAssociate: Associate): void {
        this.objAssociateEditList.splice(this.objAssociateEditList.indexOf(objAssociate), 1);
    }

    undoEdit(): void {
        this.goToTop();

        this.objCompanyEdit = new Company();
        this.objRepresentativeEdit = new Representative();
        this.objAssociateEdit = new Associate();

        this.objRepresentativeEditList = [];
        this.objAssociateEditList = [];
    }

    cancelAdd(): void {
        this.goToTop();
        this.objCompanyAdd = new Company();
        this.objRepresentativeAdd = new Representative();
        this.objAssociateAdd = new Associate();

        this.objRepresentativeList = [];
        this.objAssociateList = [];
    }

    filterCompany() {
        this.loadingData = true;
        this.objCompanyList = [];
        this.companyService.getCompanies()
            .subscribe(
                companies => {
                    this.objCompanyList = companies.filter(
                        data =>
                        data.CompanyNameEnglish.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        // data.CompanyNameChinese.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        // data.Address.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Phone.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        // data.Fax.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Email.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        // data.Website.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Registration.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                    );
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                });
        this.loadingData = false;
    }

    sort(sortIndex: number) {
        if (this.sortAscending) {
            this.objCompanyList = this.objCompanyList
                .sort((a, b) =>
                    sortIndex === 0 ? b.CompanyNameEnglish.localeCompare(a.CompanyNameEnglish) :
                        sortIndex === 1 ? b.Phone.localeCompare(a.Phone) :
                            sortIndex === 2 ? b.Email.localeCompare(a.Email) :
                                // sortIndex === 3 ? b.Website.localeCompare(a.Website) :
                                sortIndex === 3 ? b.Registration.localeCompare(a.Registration) :
                                    b.DisplayPriority - a.DisplayPriority);
            this.sortAscending = false;
        }
        else {
            this.objCompanyList = this.objCompanyList
                .sort((a, b) =>
                    sortIndex === 0 ? a.CompanyNameEnglish.localeCompare(b.CompanyNameEnglish) :
                        sortIndex === 1 ? a.Phone.localeCompare(b.Phone) :
                            sortIndex === 2 ? a.Email.localeCompare(b.Email) :
                                // sortIndex === 3 ? a.Website.localeCompare(b.Website) :
                                sortIndex === 3 ? a.Registration.localeCompare(b.Registration) :
                                    a.DisplayPriority - b.DisplayPriority
                )
            ;
            this.sortAscending = true;
        }

        sortIndex === 0 ?
            this.sortIcon.CompanyNameEnglish = this.sortAscending ? "asc" : "desc" :
            sortIndex === 1 ?
                this.sortIcon.Phone = this.sortAscending ? "asc" : "desc" :
                sortIndex === 2 ?
                    this.sortIcon.Email = this.sortAscending ? "asc" : "desc" :
                    // sortIndex === 3 ?
                    //     this.sortIcon.Website = this.sortAscending ? "asc" : "desc" :
                    sortIndex === 3 ?
                        this.sortIcon.Registration = this.sortAscending ? "asc" : "desc" :
                        this.sortIcon.DisplayPriority = this.sortAscending ? "asc" : "desc";
    }
}
