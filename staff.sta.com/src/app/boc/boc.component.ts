import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

import {PageScrollConfig, PageScrollService, PageScrollInstance} from 'ng2-page-scroll';
import {PaginationInstance} from 'ng2-pagination';

import {BOC, BOCService, ValidationService} from '../models';

import {CustomPaginationComponent} from '../shared/custom-pagination/custom-pagination.component';

import {CONFIG, AlertService} from '../core';
import {FileUploadComponent} from "../shared/file-upload/file-upload.component";

let imageUrl = CONFIG.baseUrls.bocImages;

@Component({
    selector: 'boc',
    templateUrl: 'boc.component.html'
})
export class BOCComponent implements AfterViewInit {
    objBOCList: BOC[];

    objBOCAdd: BOC = new BOC();
    objBOCEdit: BOC = new BOC();

    @ViewChild('uploadComponentAdd')
    private uploadComponentAdd: FileUploadComponent;
    @ViewChild('uploadComponentEdit')
    private uploadComponentEdit: FileUploadComponent;

    @ViewChild('topPagination')
    private topPagination: CustomPaginationComponent;
    @ViewChild('bottomPagination')
    private bottomPagination: CustomPaginationComponent;

    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    searchQuery: string = '';

    loadingData: boolean = false;
    savingAddData: boolean = false;
    savingEditData: boolean = false;

    private sortIcon = {
        Name: "asc",
        Designation: "asc",
        Company: "asc",
        DisplayPriority: "asc"
    };
    sortAscending: boolean = true;

    constructor(private bocService: BOCService,
                private pageScrollService: PageScrollService,
                private alertService: AlertService,
                private validationService: ValidationService,
                @Inject(DOCUMENT) private document: Document) {
        PageScrollConfig.defaultDuration = 500;
    }

    ngAfterViewInit(): void {
        this.reloadData();
        this.topPagination.config = this.config;
        this.bottomPagination.config = this.config;

        this.uploadComponentAdd.showImageComponent("Upload Image (180 x 180 Pixels)");
        this.uploadComponentEdit.showImageComponent("Upload Image (180 x 180 Pixels)");
    }

    reloadData(): void {
        this.loadingData = true;
        this.objBOCList = [];
        this.bocService
            .getBOCs()
            .subscribe(
                bocs => {
                    this.objBOCList = bocs;
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                });
    }

    uploadAddImage(imageFile: File) {
        this.objBOCAdd.ImageFile = imageFile;
    }

    uploadEditImage(imageFile: File) {
        this.objBOCEdit.ImageFile = imageFile;
        if (!imageFile) {
            this.objBOCEdit.ImageDeletedOnEdit = true;
        }
    }

    saveAddData(): void {
        if (this.validationService.boc(this.objBOCAdd)) {
            this.savingAddData = true;

            let operation: Observable<BOC>;

            operation = this.bocService.addBOC(this.objBOCAdd);

            operation.subscribe(
                bocs => {
                    this.goToTop();
                    this.reloadData();

                    this.objBOCAdd = new BOC();

                    this.uploadComponentAdd.resetImageComponent();

                    this.savingAddData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingAddData = false;
                });
        }
    }

    saveEditData(): void {
        if (this.validationService.boc(this.objBOCEdit)) {
            this.savingEditData = true;

            let operation: Observable<BOC>;

            operation = this.bocService.updateBOC(this.objBOCEdit);

            operation.subscribe(
                bocs => {
                    this.goToTop();
                    this.reloadData();

                    this.objBOCEdit = new BOC();

                    this.uploadComponentEdit.resetImageComponent();

                    this.savingEditData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingEditData = false;
                });
        }
    }

    editData(objBOC: BOC): void {
        this.objBOCEdit.ID = objBOC.ID;
        this.objBOCEdit.Name = objBOC.Name;
        this.objBOCEdit.Designation = objBOC.Designation;
        this.objBOCEdit.Company = objBOC.Company;
        this.objBOCEdit.DisplayPriority = objBOC.DisplayPriority;
        this.objBOCEdit.PrevImage = objBOC.Image;

        this.uploadComponentEdit.resetImageComponent();
        this.uploadComponentEdit.initDataImage(objBOC.Image, imageUrl + objBOC.Image);
        this.objBOCEdit.ImageDeletedOnEdit = false;

        this.goToEdit();
    }

    goToTop(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#top');
        this.pageScrollService.start(pageScrollInstance);
    }

    goToEdit(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#editSectionWrapper');
        this.pageScrollService.start(pageScrollInstance);
    }

    undoEdit(): void {
        this.goToTop();

        this.uploadComponentEdit.resetImageComponent();

        this.objBOCEdit = new BOC();
    }

    cancelAdd(): void {
        this.goToTop();

        this.uploadComponentAdd.resetImageComponent();

        this.objBOCAdd = new BOC();
    }

    filterBOC() {
        this.loadingData = true;
        this.objBOCList = [];
        this.bocService.getBOCs()
            .subscribe(
                bocs => {
                    this.objBOCList = bocs.filter(
                        data =>
                        data.Name.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Designation.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Company.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                    );
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                });
    }

    sort(sortIndex: number) {
        if (this.sortAscending) {
            this.objBOCList = this.objBOCList
                .sort((a, b) =>
                    sortIndex === 0 ? b.Name.localeCompare(a.Name) :
                        sortIndex === 1 ? b.Designation.localeCompare(a.Designation) :
                            sortIndex === 2 ? b.Company.localeCompare(a.Company) :
                                b.DisplayPriority - a.DisplayPriority);

            this.sortAscending = false;
        }
        else {
            this.objBOCList = this.objBOCList
                .sort((a, b) =>
                    sortIndex === 0 ? a.Name.localeCompare(b.Name) :
                        sortIndex === 1 ? a.Designation.localeCompare(b.Designation) :
                            sortIndex === 2 ? a.Company.localeCompare(b.Company) :
                                a.DisplayPriority - b.DisplayPriority);
            this.sortAscending = true;
        }

        sortIndex === 0 ?
            this.sortIcon.Name = this.sortAscending ? "asc" : "desc" :
            sortIndex === 1 ?
                this.sortIcon.Designation = this.sortAscending ? "asc" : "desc" :
                sortIndex === 2 ?
                    this.sortIcon.Company = this.sortAscending ? "asc" : "desc" :
                    this.sortIcon.DisplayPriority = this.sortAscending ? "asc" : "desc";
    }
}
