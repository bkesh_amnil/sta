import {Component, OnInit} from '@angular/core';

import {AlertService} from '../core';
import {Activity, ActivityLog, ActivityService} from '../models';

import '../../../public/css/staff-dashboard.css';

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    objActivityLog: ActivityLog;
    objActivityList: Activity[];

    currentPage: number = 1;
    prevPage: number = 0;
    nextPage: number = 1;
    totalPage: number = 1;

    constructor(private activityService: ActivityService,
                private alertService: AlertService) {
        this.objActivityLog = new ActivityLog();
        this.activityService
            .getActivities(1)
            .subscribe(
                activityLog => {
                    this.objActivityLog = activityLog;
                    this.totalPage = this.objActivityLog.TotalPage;
                    if (this.objActivityLog.TotalPage > 1) {
                        this.nextPage = 2;
                    }
                    this.prevPage = 0;
                },
                error =>
                    this.alertService.alertWarning('Error loading data.')
            );
    }

    ngOnInit() {
    }

    loadData(page: number) {
        this.currentPage = page;
        this.objActivityLog = new ActivityLog();
        this.activityService
            .getActivities(page)
            .subscribe(
                activityLog => {
                    console.log("activityLog", activityLog);
                    this.objActivityLog = activityLog;
                    this.totalPage = this.objActivityLog.TotalPage;
                    this.prevPage = this.currentPage - 1;
                    if (this.totalPage > this.currentPage) {
                        this.nextPage = this.currentPage + 1;
                    } else {
                        this.nextPage = this.currentPage;
                    }
                },
                error =>
                    this.alertService.alertWarning('Error loading data.')
            );

    }
}
