import {Component, OnInit, EventEmitter} from '@angular/core';

import {ValidationService} from '../models';
import {LoginService} from "../models/login.service";
import {Router, ActivatedRoute, Params} from "@angular/router";
import {Observable} from "rxjs";
import {AlertService} from "../core";
import {ResetPassword} from "../models";
import {EmitterService} from "../core/emitter.service";

@Component({
    selector: 'reset-password',
    templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {
    private _savingPasswordChange: boolean = false;

    private _changePasswordEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    private _savedChangePassword: boolean = false;

    objResetPassword: ResetPassword = new ResetPassword();

    constructor(private loginService: LoginService,
                private validationService: ValidationService,
                private alertService: AlertService,
                private router: Router,
                private route: ActivatedRoute) {

        this._changePasswordEvent
            .delay(2000)
            .subscribe((isSaved) => {
                this._savedChangePassword = false;

                EmitterService.get('RESET_PASSWORD_PAGE').emit(false);

                this.loginService.logout();
                this.router.navigate(['/']);
            });

        EmitterService.get('RESET_PASSWORD_PAGE').emit(true);
    }

    ngOnInit(): void {
        this.route.params
            .map((params: Params) => {
                return params['code'];
            })
            .subscribe(code => this.objResetPassword.Code = code);
    }

    private _changePassword() {
        if (this.validationService.resetPassword(this.objResetPassword)) {
            this._savingPasswordChange = true;

            let operation: Observable<boolean>;

            operation = this.loginService.resetPassword(this.objResetPassword);

            operation.subscribe(
                isChanged => {
                    if (isChanged) {
                        this._savingPasswordChange = false;
                        this.objResetPassword = new ResetPassword();
                    }
                    else {
                        this.alertService.alertWarning('Something went wrong.')
                    }
                },
                error => {
                    this.alertService.alertWarning(error);
                    this._savingPasswordChange = false;
                },
                () => {
                    this._savedChangePassword = true;
                    this._changePasswordEvent.emit(true);
                });
        }
    }
}
