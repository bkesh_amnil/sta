import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
// import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
// import {DemoComponent} from './demo/demo.component';
import {AccountSettingComponent} from "./account-setting/account-setting.component";
import {AdvertisementComponent} from './advertisement/advertisement.component';
import {EventComponent} from './event/event.component';
import {CompanyComponent} from './company/company.component';
import {BOCComponent} from './boc/boc.component';

import {AuthGuard} from './auth-guard.service';
import {ResetPasswordComponent} from "./reset-password/reset-password.component";

export const ROUTES = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    // { path: 'home', component: HomeComponent },
    {path: 'login', component: LoginComponent},
    // {path: 'demo', component: DemoComponent},
    {path: 'accountSetting', component: AccountSettingComponent, canActivate: [AuthGuard]},
    {path: 'advertisements', component: AdvertisementComponent, canActivate: [AuthGuard]},
    {path: 'events', component: EventComponent, canActivate: [AuthGuard]},
    {path: 'members', component: CompanyComponent, canActivate: [AuthGuard]},
    {path: 'boardofcommittee', component: BOCComponent, canActivate: [AuthGuard]},
    {path: 'resetPassword/:code', component: ResetPasswordComponent},
    {path: '**', pathMatch: 'full', component: PageNotFoundComponent}
];
