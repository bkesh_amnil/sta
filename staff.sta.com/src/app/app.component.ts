import { Component } from '@angular/core';

import '../../public/css/common/reset.css';
import '../../public/css/staff-login.css';
import '../../public/css/common/staff.css';
import '../../public/css/theme/blue-theme.css';
import '../../node_modules/sweetalert2/dist/sweetalert2.min.css';
import '../../public/css/common/sweetalert.css';

@Component({
    selector: 'sta-app',
    templateUrl: './app.component.html',
})
export class AppComponent {
}
