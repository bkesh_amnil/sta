import {Component, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {LoginUser, LoginResponse, LoginService} from '../models';
import {EmitterService} from '../core';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent {
    objLoginUser: LoginUser = new LoginUser();
    objLoginResponse: LoginResponse = new LoginResponse();
    loggingIn: boolean = false;
    showLoginPanel: boolean = false;
    forgotPasswordForm: boolean = false;

    emailInvalid: boolean = false;
    passwordInvalid: boolean = false;
    errorMessage: string = '';

    forgottenEmail: string;
    sendingEmail: boolean = false;
    forgottenEmailInvalid: boolean = false;

    sentEmail: boolean = false;

    @ViewChild('emailInput')
    private emailInput: ElementRef;
    @ViewChild('passwordInput')
    private passwordInput: ElementRef;
    @ViewChild('forgottenEmailInput')
    private forgottenEmailInput: ElementRef;

    constructor(private loginService: LoginService,
                private router: Router) {
        EmitterService.get('LOGIN_SHOW_REQUEST')
            .subscribe((status: boolean) => {
                this.showLoginPanel = true;
            });
    }

    onSubmit() {
        if (this.objLoginUser.Email && this.objLoginUser.Password) {
            this.loggingIn = true;
            let operation: Observable<LoginResponse>;
            operation = this.loginService.login(this.objLoginUser);
            operation.subscribe(
                loginResponse => {
                    this.loggingIn = false;
                    EmitterService.get('LOGIN_RESPONSE_EVENT').emit(loginResponse);
                    if (loginResponse.Token) this.router.navigate(['/dashboard']);
                }, error => {
                    this.loggingIn = false;
                    if (error) {
                        this.emailInvalid = false;
                        this.passwordInvalid = false;

                        if (error === 'Invalid email address.' || error === "Account is Disabled.") {
                            this.emailInvalid = true;
                            this.errorMessage = error;
                        }
                        if (error === "Invalid password." || error === "Wrong password." || error === "Account is Disabled.") {
                            this.passwordInvalid = true;
                            this.errorMessage = error;
                        }
                    }
                });
        }
        else {
            this.emailInvalid = false;
            this.passwordInvalid = false;

            if (!this.objLoginUser.Email) {
                this.errorMessage = 'Required';
                this.emailInvalid = true;
            }
            if (!this.objLoginUser.Password) {
                this.errorMessage = 'Required';
                this.passwordInvalid = true;
            }
        }
    }

    onClickOutside(event: Object) {
        if (event && event['id'] !== 'start-button')
            this.showLoginPanel = false;
        if (event && (
                event['id'] === 'get-forgot-password' ||
                event['id'] === 'get-login' ||
                event['id'] === 'emailInvalid' ||
                event['id'] === 'passwordInvalid' ||
                event['id'] === 'forgottenEmailInvalid'
            ))
            this.showLoginPanel = true;
    }

    toggleForgotPasswordForm() {
        EmitterService.get('LOGIN_SHOW_REQUEST').emit(true);
        this.forgotPasswordForm = !this.forgotPasswordForm;
    }

    inputFocus(event: any) {
        if (event.srcElement.name === 'Email')
            this.emailInvalid = false;
        if (event.srcElement.name === 'Password')
            this.passwordInvalid = false;
    }

    emailFocus() {
        this.emailInvalid = false;
        this.emailInput.nativeElement.focus();
    }

    passwordFocus() {
        this.passwordInvalid = false;
        this.passwordInput.nativeElement.focus();
    }

    forgotPassword() {
        if (this.forgottenEmail) {
            this.sendingEmail = true;
            let operation: Observable<string>;
            operation = this.loginService.forgotPassword(this.forgottenEmail);
            operation.subscribe(
                res => {
                    this.sendingEmail = false;
                    this.sentEmail = true;
                    this.forgottenEmail = null;
                }, error => {
                    this.sendingEmail = false;
                    if (error) {
                        this.forgottenEmailInvalid = true;
                        this.errorMessage = error;
                    }
                });
        }
        else {
            this.forgottenEmailInvalid = false;

            if (!this.forgottenEmail) {
                this.errorMessage = 'Required';
                this.forgottenEmailInvalid = true;
            }
        }
    }

    forgottenEmailFocus() {
        this.forgottenEmailInvalid = false;
        this.forgottenEmailInput.nativeElement.focus();
    }

    dismissSentEmail() {
        this.sentEmail = false;
    }
}