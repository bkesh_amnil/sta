import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ExceptionService {

    constructor() { }

    handleError(error: Response) {
        // console.error(error);
        // let msg = `Error status code ${error.status} at ${error.url}`;
        return Observable.throw(error.json().error);
    }
}
