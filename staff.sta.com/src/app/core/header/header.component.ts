import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {LoginService, LoginResponse} from '../../models';
import {EmitterService} from '../';

@Component({
    selector: 'header',
    templateUrl: 'header.component.html'
})
export class HeaderComponent implements OnInit {
    isLoggedIn: boolean = false;
    loggedInEmail: string;

    hideGetStarted: boolean = false;

    constructor(private loginService: LoginService,
                private router: Router) {
        EmitterService.get('LOGIN_RESPONSE_EVENT')
            .subscribe((objLoginResponse: LoginResponse) => {
                this.isLoggedIn = this.loginService.isLoggedIn();
                if (this.isLoggedIn)
                    this.loggedInEmail = objLoginResponse.Staff.Email;
            });
        EmitterService.get('LOG_OUT_EVENT').subscribe(() => {
            this.isLoggedIn = false;
            this.router.navigate(['/login']);
            this.showLogin();
        });
        EmitterService.get('RESET_PASSWORD_PAGE')
            .subscribe((hideGetStarted: boolean) => {
                this.hideGetStarted = hideGetStarted;
            });
    }

    ngOnInit() {
        this.isLoggedIn = this.loginService.isLoggedIn();
        if (this.isLoggedIn)
            this.loggedInEmail = this.loginService.getSessionUser().Email;
    }

    showLogin() {
        EmitterService.get('LOGIN_SHOW_REQUEST').emit(true);
    }

    logOut() {
        this.loginService.logout();
    }
}
