export let CONFIG = {
        baseUrls: {
            // api: 'http://127.0.0.2/staff-api/',
            // api: 'http://192.168.88.153/sta-api/staff-api/'
            // api: 'http://192.168.88.153/sta/api.sta.com/staff-api/'
            // api: 'http://staging.api.singaporetimber.com/',
            api: 'http://staging.api.singaporetimber.com/staff-api/',
            // api: 'http://api.sta.dev/staff-api/',
            advertisementImages: 'http://staging.www.singaporetimber.com/img/advertisements/',
            eventImages: 'http://staging.www.singaporetimber.com/img/events/',
            bocImages: 'http://staging.www.singaporetimber.com/img/bocs/',
            // advertisementImages: 'http://www.sta.dev/img/advertisements/',
            // eventImages: 'http://www.sta.dev/img/events/',
            // bocImages: 'http://www.sta.dev/img/bocs/'
        },
        validationRegEx: {
            email: new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
            phone: new RegExp(/^\d+$/),
            date: new RegExp(/^(\d{1,2})-(\d{1,2})-(\d{4})$/),
            time: new RegExp(/^(((([0-1][0-9])|(2[0-3])):?[0-5][0-9])|(24:?00))/)
        }
    }
    ;
