import {Injectable} from '@angular/core';
//noinspection TypeScriptUnresolvedFunction
const swal: any = require('sweetalert2');

@Injectable()
export class AlertService {
    constructor() {
    }

    alertWarning(errorMessage: string) {
        swal({
            title: 'ATTENTION',
            type: '',
            text: errorMessage,
            confirmButtonColor: '#18589f',
            allowOutsideClick: false,
            animation: false
        });
    }
}
