import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';

import {LoginService} from './models';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private loginService: LoginService,
                private router: Router) {
    }

    canActivate() {
        if (this.loginService.isLoggedIn()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}
