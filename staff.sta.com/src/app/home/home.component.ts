import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from "../models";

import '../../../public/css/staff-login.css';

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
})
export class HomeComponent implements OnInit {
    constructor(private loginService: LoginService, private router: Router) {
        if (this.loginService.isLoggedIn()) {
            this.router.navigate(['/dashboard']);
        }
    }

    ngOnInit() {
    }
}
