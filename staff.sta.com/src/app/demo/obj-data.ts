export class ObjData {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    gender: boolean;
    photo: string;
    file: string;
    department: string;
};

export class ObjBusType {
    ID: number;
    BusType: string;
    MaxCapacity: string;
    MaxPrice: string;
    MinCapacity: boolean;
    MinPrice: string;
    Status: string;
    Image: string;
}
