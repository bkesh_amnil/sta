import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ObjData, ObjBusType } from './obj-data';
// import { MOCKDATA } from './mock-data';

@Injectable()
export class DemoService {

    private _data: Observable<ObjData[]>;
    private _page: number = 1;
    private _total: number;

    constructor(private http: Http) { }

    getData(page: number) {
        return this.http.get('api/data.json')
            .do((response: Response) => {
                this._total = response.json().total;
                this._page = page;
            })
            .map((response: Response) => <ObjData[]>response.json().data)
            .toPromise()
            .catch(this.handleError);
    }

    getDataArrayList() {
        return this.http.get('api/data.json')
            .map((response: Response) => <ObjData[]>response.json().data)
            .catch(this.handleError);
    }

    getBusType() {
        return this.http.get('http://localhost/wantbus-API/staff-api/getBusType')
            .map((response: Response) => <ObjBusType[]>response.json().BusType)
            .toPromise()
            .catch(this.handleError);
    }

    updateBusType(objData: ObjBusType) {
        return this.http.post(
            'http://localhost/wantbus-API/staff-api/editBusType',
            JSON.stringify(objData))
            .map((response: Response) => response.json())
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        let msg = `Error status code ${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}
