import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { PageScrollConfig } from 'ng2-page-scroll';

import { DemoService } from './demo.service';
import { ObjData, ObjBusType } from './obj-data';

import { PaginationInstance } from 'ng2-pagination';

import { CustomPaginationComponent } from '../shared/custom-pagination/custom-pagination.component';

@Component({
    selector: 'demo',
    templateUrl: 'demo.component.html'
})
export class DemoComponent implements OnInit {
    @Input('data') objFilteredDataArrayList: ObjData[];
    objDataArrayList: ObjData[];
    @Input('data') objDataList: Promise<ObjData[]>;
    objBusTypeList: Promise<ObjBusType[]>;

    objDataEdit: ObjData = new ObjData();
    objBusTypeEdit: ObjBusType = new ObjBusType();

    searchQuery: string = '';

    @ViewChild('topPagination')
    private topPagination: CustomPaginationComponent;
    @ViewChild('bottomPagination')
    private bottomPagination: CustomPaginationComponent;

    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    constructor(
        private demoService: DemoService) {
        PageScrollConfig.defaultDuration = 500;
    }

    ngOnInit(): void {
        this.demoService
            .getDataArrayList()
            .subscribe(items => this.objDataArrayList = items);
        this.topPagination.config = this.config;
        this.bottomPagination.config = this.config;
        // this.objDataArrayList = [];
        this.objFilteredDataArrayList = this.objDataArrayList;
        this.objBusTypeList = this.demoService.getBusType();
        this.objBusTypeEdit.ID = 0;
        this.objBusTypeEdit.BusType = '';
        this.objBusTypeEdit.MaxCapacity = '';
        this.objBusTypeEdit.MaxPrice = '';
        this.objBusTypeEdit.MinCapacity = true;
        this.objBusTypeEdit.MinPrice = '';
        this.objBusTypeEdit.Status = '';
        this.objBusTypeEdit.Image = '';
    }

    editData(objData: ObjBusType): void {
        this.objBusTypeEdit = objData;
    }

    saveEditData() {
        this.demoService.updateBusType(this.objBusTypeEdit);
        console.log('save', this.objBusTypeEdit);
    }

    filterItems() {
        this.objFilteredDataArrayList = this.objDataArrayList;
        this.objDataArrayList = this.objDataArrayList
            .filter(data => data.first_name.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                || data.last_name.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                || data.email.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                || data.department.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1);
    }
}
