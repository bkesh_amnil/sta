/**
 * BOC
 */
export class BOC {
    ID: string;
    Name: string;
    Designation: string;
    Company: string;
    Image: string;
    DisplayPriority: number;
    ImageFile: File;
    PrevImage: string;
    ImageDeletedOnEdit: boolean = false;
}
