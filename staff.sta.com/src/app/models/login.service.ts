import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {CONFIG, ExceptionService} from '../core';

import {LoginUser, LoginResponse, StaffUser, ChangePassword, ResetPassword} from './login-user.model';
import {EmitterService} from "../core/emitter.service";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class LoginService {
    private loggedIn = false;

    constructor(private exceptionService: ExceptionService,
                private http: Http) {
        this.loggedIn = !!localStorage.getItem('auth_token') || !!sessionStorage.getItem('auth_token');
    }

    login(loginUser: LoginUser): Observable<LoginResponse> {
        let formData: FormData = new FormData();
        formData.append('email', loginUser.Email);
        formData.append('password', loginUser.Password);

        let headers = new Headers();
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'login', formData, options)
            .map((response: Response) => {
                console.log("response", response.json());
                if (response.json().Token) {
                    console.log("response.json()", response.json());
                    if (loginUser.Persistent) {
                        localStorage.setItem('auth_token', response.json().Token);

                        localStorage.setItem('first_name', response.json().Staff.FirstName);
                        localStorage.setItem('user_email', response.json().Staff.Email);
                        localStorage.setItem('phone', response.json().Staff.Phone);
                    }
                    else {
                        sessionStorage.setItem('auth_token', response.json().Token);

                        sessionStorage.setItem('first_name', response.json().Staff.FirstName);
                        sessionStorage.setItem('user_email', response.json().Staff.Email);
                        sessionStorage.setItem('phone', response.json().Staff.Phone);
                    }

                    this.loggedIn = true;
                    return <LoginResponse>response.json();
                }
                return new LoginResponse();
            })
            .catch(this.exceptionService.handleError);
    }

    logout() {
        localStorage.removeItem('auth_token');

        localStorage.removeItem('first_name');
        localStorage.removeItem('user_email');
        localStorage.removeItem('phone');

        sessionStorage.removeItem('auth_token');

        sessionStorage.removeItem('first_name');
        sessionStorage.removeItem('user_email');
        sessionStorage.removeItem('phone');

        this.loggedIn = false;

        EmitterService.get('LOG_OUT_EVENT').emit();
    }

    isLoggedIn() {
        return this.loggedIn;
    }

    editAccountInfo(staffUser: StaffUser): Observable<StaffUser> {
        let formData: FormData = new FormData();
        formData.append('FirstName', staffUser.FirstName);
        formData.append('Phone', staffUser.Phone);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editAccountProfile', formData, options)
            .delay(2000)
            .map((response: Response) => {
                if (response.json().BackendUser) {
                    console.log('response.json().BackendUser', response.json().BackendUser);

                    if (localStorage.getItem('auth_token')) {
                        localStorage.setItem('first_name', response.json().BackendUser.FirstName);
                        localStorage.setItem('user_email', response.json().BackendUser.Email);
                        localStorage.setItem('phone', response.json().BackendUser.Phone);
                    }

                    if (sessionStorage.getItem('auth_token')) {
                        sessionStorage.setItem('first_name', response.json().BackendUser.FirstName);
                        sessionStorage.setItem('user_email', response.json().BackendUser.Email);
                        sessionStorage.setItem('phone', response.json().BackendUser.Phone);
                    }

                    return <StaffUser>response.json().BackendUser;
                }
                return new LoginResponse();
            })
            .catch(this.exceptionService.handleError);
    }

    changePassword(changePassword: ChangePassword): Observable<boolean> {
        console.log(changePassword);
        let formData: FormData = new FormData();
        formData.append('OldPassword', changePassword.OldPassword);
        formData.append('NewPassword', changePassword.NewPassword);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'changePassword', formData, options)
            .delay(2000)
            .map((response: Response) => {
                return response.status === 200;
            })
            .catch(this.exceptionService.handleError);
    }

    resetPassword(resetPassword: ResetPassword): Observable<boolean> {
        let formData: FormData = new FormData();
        formData.append('NewPassword', resetPassword.NewPassword);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'resetPassword?code=' + resetPassword.Code, formData, options)
            .delay(2000)
            .map((response: Response) => {
                return response.status === 200;
            })
            .catch(this.exceptionService.handleError);
    }

    getToken() {
        if (sessionStorage.getItem('auth_token')) {
            return sessionStorage.getItem('auth_token')
        }
        else if (localStorage.getItem('auth_token')) {
            return localStorage.getItem('auth_token');
        }
        return null;
    }

    getSessionUser(): StaffUser {
        let sessionUser: StaffUser = new StaffUser();

        if (sessionStorage.getItem('auth_token') && sessionStorage.getItem('auth_token') && sessionStorage.getItem('auth_token')) {
            sessionUser.FirstName = sessionStorage.getItem('first_name');
            sessionUser.Email = sessionStorage.getItem('user_email');
            sessionUser.Phone = sessionStorage.getItem('phone');
        }
        else if (localStorage.getItem('auth_token') && localStorage.getItem('auth_token') && localStorage.getItem('auth_token')) {
            sessionUser.FirstName = localStorage.getItem('first_name');
            sessionUser.Email = localStorage.getItem('user_email');
            sessionUser.Phone = localStorage.getItem('phone');
        }
        else {
            sessionUser = null;
        }

        return sessionUser;
    }

    forgotPassword(forgottenEmail: string): Observable<string> {
        let formData: FormData = new FormData();
        formData.append('email', forgottenEmail);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'forgotPassword', formData, options)
            .delay(2000)
            .map((response: Response) => {
                console.log("response.json()", response.json());
                return response;
            })
            .catch(this.exceptionService.handleError);
    }
}
