import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {BOC} from './boc.model';
import {CONFIG, ExceptionService} from '../core';
import {LoginService} from "./";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class BOCService {
    constructor(private exceptionService: ExceptionService,
                private loginService: LoginService,
                private http: Http) {
    }

    addBOC(boc: BOC): Observable<BOC> {
        let formData: FormData = new FormData();
        formData.append('Name', boc.Name);
        formData.append('Designation', boc.Designation);
        formData.append('Company', boc.Company);
        formData.append('DisplayPriority', boc.DisplayPriority);
        formData.append('img', boc.ImageFile);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'addBOC', formData, options)
            .delay(2000)
            .map((response: Response) => <BOC>response.json().Boc)
            .catch(this.exceptionService.handleError);
    }

    getBOCs(): Observable<BOC[]> {
        let headers = new Headers();

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        return this.http
            .get(apiUrl + 'getbocs', {headers})
            .delay(2000)
            .map((response: Response) => <BOC[]>response.json().boc)
            .catch(this.exceptionService.handleError);
    }

    updateBOC(boc: BOC): Observable<BOC> {
        console.log('boc', boc);

        let formData: FormData = new FormData();
        formData.append('ID', boc.ID);
        formData.append('Name', boc.Name);
        formData.append('Designation', boc.Designation);
        formData.append('Company', boc.Company);
        formData.append('DisplayPriority', boc.DisplayPriority);
        formData.append('img', boc.ImageFile);
        formData.append('img_prev', boc.PrevImage);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editBoc', formData, options)
            .delay(2000)
            .map((response: Response) => <BOC>response.json().Boc)
            .catch(this.exceptionService.handleError);
    }
}
