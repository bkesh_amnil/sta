/**
 * Advertisement
 */
export class Advertisement {
    ID: number;
    Name: string;
    Title: string;
    URL: string;
    Image: string;
    DisplayPriority: number;
    PrevImage: string;
    ImageFile: File;
    ImageDeletedOnEdit: boolean = false;
}
