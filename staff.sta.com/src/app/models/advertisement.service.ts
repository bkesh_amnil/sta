import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {Advertisement} from './advertisement.model';
import {CONFIG, ExceptionService} from '../core';
import {LoginService} from "./";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class AdvertisementService {
    constructor(private exceptionService: ExceptionService,
                private loginService: LoginService,
                private http: Http) {
    }

    addAdvertisement(advertisement: Advertisement): Observable<Advertisement> {
        let formData: FormData = new FormData();
        formData.append('Name', advertisement.Name);
        formData.append('Title', advertisement.Title);
        formData.append('URL', advertisement.URL);
        formData.append('img', advertisement.ImageFile);
        formData.append('DisplayPriority', advertisement.DisplayPriority);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'addAdvertisement', formData, options)
            .delay(2000)
            .map((response: Response) => <Advertisement>response.json().Advertisement)
            .catch(this.exceptionService.handleError);
    }

    getAdvertisements(): Observable<Advertisement[]> {
        let headers = new Headers();

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        return this.http
            .get(apiUrl + 'getAdvertisements', {headers})
            .delay(5000)
            .map((response: Response) => <Advertisement[]>response.json().advertisements)
            .catch(this.exceptionService.handleError);
    }

    updateAdvertisement(advertisement: Advertisement): Observable<Advertisement> {
        let formData: FormData = new FormData();
        formData.append('ID', advertisement.ID);
        formData.append('Name', advertisement.Name);
        formData.append('Title', advertisement.Title);
        formData.append('URL', advertisement.URL);
        formData.append('DisplayPriority', advertisement.DisplayPriority);
        formData.append('img', advertisement.ImageFile);
        formData.append('img_prev', advertisement.PrevImage);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editAdvertisement', formData, options)
            .delay(2000)
            .map((response: Response) => <Advertisement>response.json().Advertisement)
            .catch(this.exceptionService.handleError);
    }
}
