import {Injectable} from '@angular/core';

import {Advertisement, BOC, Company, Representative, Associate, Event} from './';

import {CONFIG, AlertService} from '../core';
import {StaffUser, ChangePassword, ResetPassword} from "./login-user.model";

let emailRegex = CONFIG.validationRegEx.email;
let phoneRegex = CONFIG.validationRegEx.phone;
let dateRegex = CONFIG.validationRegEx.date;
let timeRegex = CONFIG.validationRegEx.time;

@Injectable()
export class ValidationService {
    constructor(private alertService: AlertService) {
    }

    advertisement(objAdvertisement: Advertisement): boolean {
        let errorMessage: string = '';
        if (objAdvertisement) {
            if (!objAdvertisement.Name) {
                errorMessage += "<span>Name is required.</span>";
            }
            if (!objAdvertisement.URL) {
                errorMessage += "<span>URL is required.</span>";
            }
            if (objAdvertisement.DisplayPriority == undefined || objAdvertisement.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display priority cannot be empty.</span>";
            }
            else if (isNaN(objAdvertisement.DisplayPriority)) {
                errorMessage += "<span>Display priority must contain only numbers.</span>";
            }

            if (!objAdvertisement.ImageFile) {
                if (!objAdvertisement.PrevImage || objAdvertisement.ImageDeletedOnEdit)
                    errorMessage += "<span>Please upload an Image.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    boc(objBOC: BOC): boolean {
        let errorMessage: string = '';
        if (objBOC) {
            if (!objBOC.Name) {
                errorMessage += "<span>Name is required.</span>";
            }
            if (!objBOC.Designation) {
                errorMessage += "<span>Designation is required.</span>";
            }
            if (!objBOC.Company) {
                errorMessage += "<span>Company name is required.</span>";
            }
            if (objBOC.DisplayPriority == undefined || objBOC.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display priority cannot be empty.</span>";
            }
            else if (isNaN(objBOC.DisplayPriority)) {
                errorMessage += "<span>Display priority must contain only numbers.</span>";
            }
            if (!objBOC.ImageFile) {
                if (!objBOC.PrevImage || objBOC.ImageDeletedOnEdit)
                    errorMessage += "<span>Please upload an Image.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    company(objCompany: Company): boolean {
        let errorMessage: string = '';
        if (objCompany) {
            if (!objCompany.CompanyNameEnglish)
                errorMessage += "<span>English name is required.</span>";
            if (!objCompany.Address)
                errorMessage += "<span>Address is required.</span>";

            if (!objCompany.Phone) {
                errorMessage += "<span>Phone number is required.</span>";
            }
            else if (isNaN(+objCompany.Phone)) {
                errorMessage += "<span>Phone must contain only numbers.</span>";
            }
            else if (!objCompany.Phone.startsWith('9')
                && !objCompany.Phone.startsWith('8')
                && !objCompany.Phone.startsWith('6')
                && !objCompany.Phone.startsWith('3')) {
                errorMessage += "<span>Phone number must start with 9, 8, 6 or 3.</span>"
            }
            else if (objCompany.Phone.trim().length != 8) {
                errorMessage += "<span>Phone number must contain 8 numbers.</span>"
            }
            else if (!phoneRegex.test(objCompany.Phone)) {
                errorMessage += "<span>Phone number is required.</span>"
            }

            if (!objCompany.Fax) {
                errorMessage += "<span>Fax number is required.</span>";
            }
            else if (isNaN(+objCompany.Fax)) {
                errorMessage += "<span>Fax must contain only numbers.</span>";
            }
            else if (!objCompany.Fax.startsWith('9')
                && !objCompany.Fax.startsWith('8')
                && !objCompany.Fax.startsWith('6')
                && !objCompany.Fax.startsWith('3')) {
                errorMessage += "<span>Fax number must start with 9, 8, 6 or 3.</span>"
            }
            else if (objCompany.Fax.trim().length != 8) {
                errorMessage += "<span>Fax number must contain 8 numbers.</span>"
            }
            else if (!phoneRegex.test(objCompany.Fax)) {
                errorMessage += "<span>Fax number is invalid.</span>"
            }

            else if (!Number(objCompany.Fax)) {
                errorMessage += "<span>Fax must contain only numbers.</span>";
            }
            if (objCompany.Email) {
                if (!emailRegex.test(objCompany.Email)) {
                    errorMessage += "<span>Email is invalid.</span>";
                }
            }
            if (!objCompany.ProductsServicesEnglish)
                errorMessage += "<span>Product Services in English cannot be empty.</span>";

            if (objCompany.DisplayPriority == undefined || objCompany.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display Priority cannot be empty.</span>";
            }
            else if (isNaN(objCompany.DisplayPriority)) {
                errorMessage += "<span>Display Priority must contain only numbers.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    representative(objRepresentative: Representative): boolean {
        let errorMessage: string = '';
        if (objRepresentative) {
            if (!objRepresentative.NameEnglish)
                errorMessage += "<span>English name is required.</span>";

            if (objRepresentative.Phone) {
                if (!Number(objRepresentative.Phone)) {
                    errorMessage += "<span>Phone must contain only numbers.</span>";
                }
                else if (!objRepresentative.Phone.startsWith('9')
                    && !objRepresentative.Phone.startsWith('8')
                    && !objRepresentative.Phone.startsWith('6')
                    && !objRepresentative.Phone.startsWith('3')) {
                    errorMessage += "<span>Phone number must start with 9, 8, 6 or 3.</span>"
                }
                else if (objRepresentative.Phone.trim().length != 8) {
                    errorMessage += "<span>Phone number must contain 8 numbers.</span>"
                }
                else if (!phoneRegex.test(objRepresentative.Phone)) {
                    errorMessage += "<span>Phone number is invalid.</span>"
                }
            }

            if (objRepresentative.DisplayPriority == undefined || objRepresentative.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display priority cannot be empty.</span>";
            }
            else if (isNaN(objRepresentative.DisplayPriority)) {
                errorMessage += "<span>Display priority must contain only numbers.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    associate(objAssociate: Associate): boolean {
        let errorMessage: string = '';
        if (objAssociate) {
            if (!objAssociate.AssociateCompanyNameEnglish)
                errorMessage += "<span>English name is required.</span>";
            if (objAssociate.DisplayPriority == undefined || objAssociate.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display priority cannot be empty.</span>";
            }
            else if (isNaN(objAssociate.DisplayPriority)) {
                errorMessage += "<span>Display priority must contain only numbers.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    hasDuplicateRepresentative(objRepresentativeList: Representative[], repName: string): boolean {
        if (objRepresentativeList.find(rep => rep.NameEnglish.toUpperCase() === repName.toUpperCase())) {
            this.alertService.alertWarning("Duplicate representative not allowed.");
            return true;
        }

        return false;
    }

    hasDuplicateAssociate(objAssociateList: Associate[], associateName: string): boolean {
        if (objAssociateList
                .find(associate => associate.AssociateCompanyNameEnglish.toUpperCase() === associateName.toUpperCase())) {
            this.alertService.alertWarning("Duplicate representative not allowed.")
            return true;
        }
        return false;
    }

    event(objEvent: Event): boolean {
        let errorMessage: string = '';
        if (objEvent) {
            if (!objEvent.EventName) {
                errorMessage += "<span>Name is required.</span>";
            }
            if (!objEvent.EventDate) {
                errorMessage += "<span>Event date is required.</span>";
            }
            else if (!dateRegex.test(objEvent.EventDate)) {
                errorMessage += "<span>Date must be in format dd-mm-yyyy</span>"
            }
            if (!objEvent.EventFrom) {
                errorMessage += "<span>From time is required.</span>";
            }
            else if (objEvent.EventFrom.localeCompare(objEvent.EventTo) >= 0) {
                errorMessage += "<span>Please adjust From and To time.</span>";
            }
            if (!objEvent.EventTo) {
                errorMessage += "<span>To time is required.</span>";
            }
            if (!timeRegex.test(objEvent.EventTo) || !timeRegex.test(objEvent.EventFrom)) {
                errorMessage += "<span>Time must be in 24-hour format.</span>"
            }
            if (objEvent.Description) {
                if (objEvent.Description.length > 512) {
                    errorMessage += "<span>Description cannot contain more than 512 characters.</span>";
                }
            } else {
                errorMessage += "<span>Description is required.</span>";
            }
            if (!objEvent.Venue) {
                errorMessage += "<span>Venue is required.</span>";
            }
            if (!objEvent.Latitude) {
                errorMessage += "<span>Latitude is required.</span>";
            }
            if (!objEvent.Longitude) {
                errorMessage += "<span>Longitude is required.</span>";
            }
            if (!objEvent.CourseFee) {
                errorMessage += "<span>Course fee is required.</span>";
            }
            if (objEvent.Remarks) {
                if (objEvent.Remarks.length > 140) {
                    errorMessage += "<span>Remarks cannot contain more than 140 characters.</span>";
                }
            } else {
                errorMessage += "<span>Remarks is required.</span>";
            }
            if (objEvent.DisplayPriority == undefined || objEvent.DisplayPriority.toString().trim() == "") {
                errorMessage += "<span>Display priority cannot be empty.</span>";
            }
            else if (isNaN(objEvent.DisplayPriority)) {
                errorMessage += "<span>Display priority must contain only numbers.</span>";
            }

            // if (!objEvent.BrochureFile) {
            //     if (!objEvent.PrevDoc || objEvent.PdfDeletedOnEdit)
            //         errorMessage += "<span>Please upload Brochure.</span>";
            // }

            if (!objEvent.ImageFile) {
                if (!objEvent.PrevImage || objEvent.ImageDeletedOnEdit)
                    errorMessage += "<span>Please upload an Image.</span>";
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    accountInfo(objStaffUser: StaffUser) {
        let errorMessage: string = '';
        if (objStaffUser) {
            if (!objStaffUser.FirstName) {
                errorMessage += "<span>First name is required.</span>";
            }
            if (objStaffUser.Phone) {
                if (isNaN(+objStaffUser.Phone)) {
                    errorMessage += "<span>Phone must contain only numbers.</span>";
                }
                else if (!objStaffUser.Phone.startsWith('9')
                    && !objStaffUser.Phone.startsWith('8')
                    && !objStaffUser.Phone.startsWith('6')
                    && !objStaffUser.Phone.startsWith('3')) {
                    errorMessage += "<span>Phone number must start with 9, 8, 6 or 3.</span>"
                }
                else if (objStaffUser.Phone.trim().length != 8) {
                    errorMessage += "<span>Phone number must contain 8 numbers.</span>"
                }
                else if (!phoneRegex.test(objStaffUser.Phone)) {
                    errorMessage += "<span>Phone number is invalid.</span>"
                }
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    changePassword(changePassword: ChangePassword) {
        let errorMessage: string = '';
        if (changePassword) {
            if (!changePassword.OldPassword) {
                errorMessage += "<span>Old password is required.</span>";
            }
            if (!changePassword.NewPassword) {
                errorMessage += "<span>New password is required.</span>";
            }
            if (!changePassword.ConfirmPassword) {
                errorMessage += "<span>Confirm password is required.</span>";
            }
            if (changePassword.NewPassword && changePassword.ConfirmPassword) {
                if (changePassword.NewPassword.localeCompare(changePassword.ConfirmPassword) !== 0) {
                    errorMessage += "<span>New password and Confirm password do not match.</span>";
                }
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

    resetPassword(resetPassword: ResetPassword) {
        let errorMessage: string = '';
        if (resetPassword) {
            if (!resetPassword.NewPassword) {
                errorMessage += "<span>New password is required.</span>";
            }
            if (!resetPassword.ConfirmPassword) {
                errorMessage += "<span>Confirm password is required.</span>";
            }
            if (resetPassword.NewPassword && resetPassword.ConfirmPassword) {
                if (resetPassword.NewPassword.localeCompare(resetPassword.ConfirmPassword) !== 0) {
                    errorMessage += "<span>New password and Confirm password do not match.</span>";
                }
            }

            if (errorMessage) {
                this.alertService.alertWarning(errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }
}

