/**
 * Company
 */
export class Company {
    ID: number;
    CompanyNameEnglish: string;
    CompanyNameChinese: string;
    Address: string;
    Phone: string;
    Fax: string;
    Email: string;
    Website: string;
    Registration: string;
    ProductsServicesEnglish: string;
    ProductsServicesChinese: string;
    DisplayPriority: number;
    Representatives: Representative[] = [];
    Associates: Associate[] = [];
}

/**
 * Representative
 */
export class Representative {
    ID: number;
    CompanyID: number;
    NameEnglish: string;
    NameChinese: string;
    Phone: string;
    DisplayPriority: number;
}

/**
 * Associate
 */
export class Associate {
    ID: number;
    CompanyID: number;
    AssociateCompanyNameEnglish: string;
    AssociateCompanyNameChinese: string;
    DisplayPriority: number;
}
