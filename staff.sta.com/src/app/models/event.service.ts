import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {Event} from './event.model';
import {CONFIG, ExceptionService} from '../core';
import {LoginService} from "./";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class EventService {
    constructor(private exceptionService: ExceptionService,
                private loginService: LoginService,
                private http: Http) {
    }

    addEvent(event: Event): Observable<Event> {
        let formData: FormData = new FormData();
        formData.append('EventName', event.EventName);
        formData.append('EventDate', event.EventDate);
        formData.append('EventFrom', event.EventFrom);
        formData.append('EventTo', event.EventTo);
        formData.append('Description', event.Description);
        formData.append('Venue', event.Venue);
        formData.append('Latitude', event.Latitude);
        formData.append('Longitude', event.Longitude);
        formData.append('CourseFee', event.CourseFee);
        formData.append('Remarks', event.Remarks);
        formData.append('DisplayPriority', event.DisplayPriority);
        formData.append('doc', event.BrochureFile);
        formData.append('img', event.ImageFile);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'addEvent', formData, options)
            .map((response: Response) => <Event>response.json().Event)
            .catch(this.exceptionService.handleError);
    }

    addEventAndEmail(event: Event): Observable<Event> {
        let formData: FormData = new FormData();
        formData.append('EventName', event.EventName);
        formData.append('EventDate', event.EventDate);
        formData.append('EventFrom', event.EventFrom);
        formData.append('EventTo', event.EventTo);
        formData.append('Description', event.Description);
        formData.append('Venue', event.Venue);
        formData.append('Latitude', event.Latitude);
        formData.append('Longitude', event.Longitude);
        formData.append('CourseFee', event.CourseFee);
        formData.append('Remarks', event.Remarks);
        formData.append('DisplayPriority', event.DisplayPriority);
        formData.append('doc', event.BrochureFile);
        formData.append('img', event.ImageFile);
        formData.append('sendNotification', 'Send Email');

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'addEvent', formData, options)
            .map((response: Response) => <Event>response.json().Event)
            .catch(this.exceptionService.handleError);
    }

    getEvents(): Observable<Event[]> {
        let headers = new Headers();

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        return this.http
            .get(apiUrl + 'getEvents', {headers})
            .map((response: Response) => <Event[]>response.json().events)
            .catch(this.exceptionService.handleError);
    }

    updateEvent(event: Event): Observable<Event> {
        let formData: FormData = new FormData();
        formData.append('ID', event.ID);
        formData.append('EventName', event.EventName);
        formData.append('EventDate', event.EventDate);
        formData.append('EventFrom', event.EventFrom);
        formData.append('EventTo', event.EventTo);
        formData.append('Description', event.Description);
        formData.append('Venue', event.Venue);
        formData.append('Latitude', event.Latitude);
        formData.append('Longitude', event.Longitude);
        formData.append('CourseFee', event.CourseFee);
        formData.append('Remarks', event.Remarks);
        formData.append('DisplayPriority', event.DisplayPriority);
        formData.append('doc', event.BrochureFile);
        formData.append('img', event.ImageFile);
        formData.append('doc_prev', event.PrevDoc);
        formData.append('img_prev', event.PrevImage);
        formData.append('PdfDeletedOnEdit', event.PdfDeletedOnEdit);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editEvent', formData, options)
            .map((response: Response) => <Event>response.json().Event)
            .catch(this.exceptionService.handleError);
    }

    updateEventAndEmail(event: Event): Observable<Event> {
        let formData: FormData = new FormData();
        formData.append('ID', event.ID);
        formData.append('EventName', event.EventName);
        formData.append('EventDate', event.EventDate);
        formData.append('EventFrom', event.EventFrom);
        formData.append('EventTo', event.EventTo);
        formData.append('Description', event.Description);
        formData.append('Venue', event.Venue);
        formData.append('Latitude', event.Latitude);
        formData.append('Longitude', event.Longitude);
        formData.append('CourseFee', event.CourseFee);
        formData.append('Remarks', event.Remarks);
        formData.append('DisplayPriority', event.DisplayPriority);
        formData.append('doc', event.BrochureFile);
        formData.append('img', event.ImageFile);
	formData.append('doc_prev', event.PrevDoc);
        formData.append('img_prev', event.PrevImage);
        formData.append('PdfDeletedOnEdit', event.PdfDeletedOnEdit);
        formData.append('sendNotification', 'Send Email');

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editEvent', formData, options)
            .map((response: Response) => <Event>response.json().Event)
            .catch(this.exceptionService.handleError);
    }
}
