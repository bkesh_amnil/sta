import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {ActivityLog} from './';
import {CONFIG, ExceptionService} from '../core';
import {LoginService} from "./";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class ActivityService {
    constructor(private exceptionService: ExceptionService,
                private loginService: LoginService,
                private http: Http) {
    }

    getActivities(pageNumber: number): Observable<ActivityLog> {
        let formData: FormData = new FormData();
        formData.append('pageNumber', pageNumber);

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'getActivity', formData, options)
            .map((response: Response) => <ActivityLog>response.json())
            .catch(this.exceptionService.handleError);
    }
}
