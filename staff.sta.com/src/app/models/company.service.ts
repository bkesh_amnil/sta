import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {Company} from './company.model';
import {CONFIG, ExceptionService} from '../core';
import {LoginService} from "./";

let apiUrl = CONFIG.baseUrls.api;

@Injectable()
export class CompanyService {
    constructor(private exceptionService: ExceptionService,
                private loginService: LoginService,
                private http: Http) {
    }

    addCompany(company: Company): Observable<Company> {
        let formData: FormData = new FormData();
        formData.append('CompanyNameEnglish', company.CompanyNameEnglish);
        formData.append('CompanyNameChinese', company.CompanyNameChinese);
        formData.append('Address', company.Address);
        formData.append('Phone', company.Phone);
        formData.append('Fax', company.Fax);
        formData.append('Email', company.Email);
        formData.append('Website', company.Website);
        formData.append('Registration', company.Registration);
        formData.append('ProductsServicesEnglish', company.ProductsServicesEnglish);
        formData.append('DisplayPriority', company.DisplayPriority);
        formData.append('Representatives', JSON.stringify(company.Representatives));
        formData.append('Associates', JSON.stringify(company.Associates));

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'addCompany', formData, options)
            .delay(2000)
            .map((response: Response) => <Company[]>response.json().Company)
            .catch(this.exceptionService.handleError);
    }

    getCompanies(): Observable<Company[]> {
        let headers = new Headers();

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        return this.http
            .get(apiUrl + 'getCompany', {headers})
            .delay(2000)
            .map((response: Response) => <Company[]>response.json().company)
            .catch(this.exceptionService.handleError);
    }

    updateCompany(company: Company): Observable<Company> {
        let formData: FormData = new FormData();
        formData.append('ID', company.ID);
        formData.append('CompanyNameEnglish', company.CompanyNameEnglish);
        formData.append('CompanyNameChinese', company.CompanyNameChinese);
        formData.append('Address', company.Address);
        formData.append('Phone', company.Phone);
        formData.append('Fax', company.Fax);
        formData.append('Email', company.Email);
        formData.append('Website', company.Website);
        formData.append('Registration', company.Registration);
        formData.append('ProductsServicesEnglish', company.ProductsServicesEnglish);
        formData.append('DisplayPriority', company.DisplayPriority);
        formData.append('Representatives', JSON.stringify(company.Representatives));
        formData.append('Associates', JSON.stringify(company.Associates));

        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let authToken = this.loginService.getToken();
        headers.append('Authorization', authToken);

        let options = new RequestOptions({headers: headers});

        return this.http
            .post(apiUrl + 'editCompany', formData, options)
            .delay(2000)
            .map((response: Response) => <Company[]>response.json().Company)
            .catch(this.exceptionService.handleError);
    }
}
