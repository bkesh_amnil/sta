/**
 * LoginUser
 */
export class LoginUser {
    Email: string;
    Password: string;
    Persistent: boolean;
}

/**
 * LoginResponse
 */
export class LoginResponse {
    Staff: StaffUser;
    Token: string;
}

/**
 * StaffUser
 */
export class StaffUser {
    ID: number;
    Email: string;
    FirstName: string;
    LastName: string;
    Phone: string;
    LastUpdatedDateTime: string;
    PasswordHash: string;
    PersistentLoginHashes: string;
    RoleID: string;
    Status: string;
}

/**
 * ChangePassword
 */
export class ChangePassword {
    OldPassword: string;
    NewPassword: string;
    ConfirmPassword: string;
}

/**
 * ResetPassword
 */
export class ResetPassword {
    Code: string;
    NewPassword: string;
    ConfirmPassword: string;
}

