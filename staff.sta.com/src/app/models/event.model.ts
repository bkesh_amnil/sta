/**
 * Event
 */
export class Event {
    ID: number;
    EventName: string;
    EventDate: string;
    EventFrom: string;
    EventTo: string;
    Description: string;
    Venue: string;
    Latitude: string;
    Longitude: string;
    CourseFee: string;
    Remarks: string;
    Pdf: string;
    Image: string;
    DisplayPriority: number;
    BrochureFile: File;
    ImageFile: File;
    PrevImage: string;
    PrevDoc: string;
    ImageDeletedOnEdit: boolean = false;
    PdfDeletedOnEdit: boolean = false;
}
