/**
 * Activity
 */
export class ActivityLog {
    Activity: Activity[];
    TotalPage: number;
}

/**
 * Activity
 */
export class Activity {
    ID: number;
    UserID: number;
    UserType: string;
    Action: string;
    ModelType: string;
    CreatedDateTime: string;
    UserEmail: string;
}
