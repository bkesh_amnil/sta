import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

import {PageScrollConfig, PageScrollService, PageScrollInstance} from 'ng2-page-scroll';
import {PaginationInstance} from 'ng2-pagination';

import {Advertisement, AdvertisementService, ValidationService} from '../models';

import {CustomPaginationComponent} from '../shared/custom-pagination/custom-pagination.component';

import {CONFIG, AlertService} from '../core';
import {FileUploadComponent} from "../shared/file-upload/file-upload.component";

let imageUrl = CONFIG.baseUrls.advertisementImages;

@Component({
    selector: 'advertisement',
    templateUrl: 'advertisement.component.html'
})
export class AdvertisementComponent implements AfterViewInit {
    objAdvertisementList: Advertisement[];

    objAdvertisementAdd: Advertisement = new Advertisement();
    objAdvertisementEdit: Advertisement = new Advertisement();

    @ViewChild('uploadComponentAdd')
    private uploadComponentAdd: FileUploadComponent;
    @ViewChild('uploadComponentEdit')
    private uploadComponentEdit: FileUploadComponent;

    @ViewChild('topPagination')
    private topPagination: CustomPaginationComponent;
    @ViewChild('bottomPagination')
    private bottomPagination: CustomPaginationComponent;

    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1
    };

    searchQuery: string = '';

    loadingData: boolean = false;
    savingAddData: boolean = false;
    savingEditData: boolean = false;

    private sortIcon = {
        Name: "asc",
        Title: "asc",
        Url: "asc",
        DisplayPriority: "asc"
    };
    sortAscending: boolean = true;

    dpPlaceHolder: string = "(Higher priority will be shown first; set to 0 to hide.)";

    constructor(private advertisementService: AdvertisementService,
                private pageScrollService: PageScrollService,
                private alertService: AlertService,
                private validationService: ValidationService,
                @Inject(DOCUMENT) private document: Document) {
        PageScrollConfig.defaultDuration = 500;
    }

    ngAfterViewInit(): void {
        this.reloadData();
        this.topPagination.config = this.config;
        this.bottomPagination.config = this.config;

        this.uploadComponentAdd.showImageComponent("Upload Image (280 x 280 Pixels)");
        this.uploadComponentEdit.showImageComponent("Upload Image (280 x 280 Pixels)");
    }

    reloadData(): void {
        this.loadingData = true;
        this.objAdvertisementList = [];
        this.advertisementService
            .getAdvertisements()
            .subscribe(
                advertisements => {
                    this.objAdvertisementList = advertisements;
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                });
    }

    uploadAddImage(imageFile: File) {
        this.objAdvertisementAdd.ImageFile = imageFile;
    }

    uploadEditImage(imageFile: File) {
        this.objAdvertisementEdit.ImageFile = imageFile;
        if (!imageFile) {
            this.objAdvertisementEdit.ImageDeletedOnEdit = true;
        }
    }

    saveAddData(): void {
        if (this.validationService.advertisement(this.objAdvertisementAdd)) {
            this.savingAddData = true;

            let operation: Observable<Advertisement>;

            operation = this.advertisementService.addAdvertisement(this.objAdvertisementAdd);

            operation.subscribe(
                advertisements => {
                    this.goToTop();
                    this.reloadData();

                    this.objAdvertisementAdd = new Advertisement();

                    this.uploadComponentAdd.resetImageComponent();

                    this.savingAddData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingAddData = false;
                });
        }
    }

    saveEditData(): void {
        if (this.validationService.advertisement(this.objAdvertisementEdit)) {
            this.savingEditData = true;

            let operation: Observable<Advertisement>;
            operation = this.advertisementService.updateAdvertisement(this.objAdvertisementEdit);

            operation.subscribe(
                advertisements => {
                    this.goToTop();
                    this.reloadData();

                    this.objAdvertisementEdit = new Advertisement();

                    this.uploadComponentEdit.resetImageComponent();

                    this.savingEditData = false;
                },
                error => {
                    this.alertService.alertWarning(error);
                    this.savingEditData = false;
                });
        }
    }

    editData(objAdvertisement: Advertisement): void {
        this.objAdvertisementEdit.ID = objAdvertisement.ID;
        this.objAdvertisementEdit.Name = objAdvertisement.Name;
        this.objAdvertisementEdit.Title = objAdvertisement.Title;
        this.objAdvertisementEdit.URL = objAdvertisement.URL;
        this.objAdvertisementEdit.DisplayPriority = objAdvertisement.DisplayPriority;
        this.objAdvertisementEdit.PrevImage = objAdvertisement.Image;

        this.uploadComponentEdit.resetImageComponent();
        this.uploadComponentEdit.initDataImage(objAdvertisement.Image, imageUrl + objAdvertisement.Image);
        this.objAdvertisementEdit.ImageDeletedOnEdit = false;

        this.goToEdit();
    }

    goToTop(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#top');
        this.pageScrollService.start(pageScrollInstance);
    }

    goToEdit(): void {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#editSectionWrapper');
        this.pageScrollService.start(pageScrollInstance);
    }

    undoEdit(): void {
        this.goToTop();

        this.uploadComponentEdit.resetImageComponent();

        this.objAdvertisementEdit = new Advertisement();
    }

    cancelAdd(): void {
        this.goToTop();

        this.uploadComponentAdd.resetImageComponent();

        this.objAdvertisementAdd = new Advertisement();
    }

    filterAdvertisement() {
        this.loadingData = true;
        this.objAdvertisementList = [];
        this.advertisementService.getAdvertisements()
            .subscribe(
                advertisements => {
                    this.objAdvertisementList = advertisements.filter(
                        data => data.Name.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 ||
                        data.Title.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1
                    );
                    this.loadingData = false;
                },
                error => {
                    this.alertService.alertWarning('Error loading data.');
                    this.loadingData = false;
                });
    }

    sort(sortIndex: number) {
        if (this.sortAscending) {
            this.objAdvertisementList = this.objAdvertisementList
                .sort((a, b) =>
                    sortIndex === 0 ? b.Name.localeCompare(a.Name) :
                        sortIndex === 1 ? b.Title.localeCompare(a.Title) :
                            sortIndex === 2 ? b.URL.localeCompare(a.URL) :
                                b.DisplayPriority - a.DisplayPriority);
            this.sortAscending = false;
        }
        else {
            this.objAdvertisementList = this.objAdvertisementList
                .sort((a, b) =>
                    sortIndex === 0 ? a.Name.localeCompare(b.Name) :
                        sortIndex === 1 ? a.Title.localeCompare(b.Title) :
                            sortIndex === 2 ? a.URL.localeCompare(b.URL) :
                                a.DisplayPriority - b.DisplayPriority);
            this.sortAscending = true;
        }

        sortIndex === 0 ?
            this.sortIcon.Name = this.sortAscending ? "asc" : "desc" :
            sortIndex === 1 ?
                this.sortIcon.Title = this.sortAscending ? "asc" : "desc" :
                sortIndex === 2 ?
                    this.sortIcon.Url = this.sortAscending ? "asc" : "desc" :
                    this.sortIcon.DisplayPriority = this.sortAscending ? "asc" : "desc";
    }
}
