import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

/* External */
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import {Ng2PaginationModule} from 'ng2-pagination';

/* App Root */
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {DemoComponent} from './demo/demo.component';
import {AccountSettingComponent} from './account-setting/account-setting.component';
import {AdvertisementComponent} from './advertisement/advertisement.component';
import {EventComponent} from './event/event.component';
import {CompanyComponent} from './company/company.component';
import {BOCComponent} from './boc/boc.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ResetPasswordComponent} from "./reset-password/reset-password.component";

/* App Core */
import {FooterComponent} from './core/footer/footer.component';
import {HeaderComponent} from './core/header/header.component';
import {AlertService, ExceptionService, EmitterService} from './core';

/* Shared */
import {CustomPaginationComponent} from './shared/custom-pagination/custom-pagination.component';
import {FileUploadComponent} from "./shared/file-upload/file-upload.component";

/* Directives */
import {ClickOutside} from "./shared/click-outside/click-outside.directive"

import {DemoService} from './demo/demo.service';

import {
    AdvertisementService,
    EventService,
    CompanyService,
    BOCService,
    LoginService,
    ValidationService,
    ActivityService
} from './models';

/* Routing */
import {ROUTES} from './app.routes';
import {AuthGuard} from './auth-guard.service';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        RouterModule.forRoot(ROUTES),
        Ng2PageScrollModule.forRoot(),
        HttpModule,
        Ng2PaginationModule
    ],
    declarations: [
        AppComponent,
        DashboardComponent,
        LoginComponent,
        HomeComponent,
        AccountSettingComponent,
        AdvertisementComponent,
        EventComponent,
        CompanyComponent,
        BOCComponent,
        ResetPasswordComponent,
        DemoComponent,

        FooterComponent,
        HeaderComponent,
        PageNotFoundComponent,
        FileUploadComponent,
        CustomPaginationComponent,
        ClickOutside
    ],
    providers: [
        AuthGuard,

        ExceptionService,
        AlertService,
        EmitterService,

        LoginService,

        DemoService,
        AdvertisementService,
        EventService,
        CompanyService,
        BOCService,
        ActivityService,
        ValidationService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
