/*** * Module - app.Module.Bizmatch 
 * * * Provides behaviour for bizmatch.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Bizmatch.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Bizmatch = function() {
            var config = {};

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
                $("form[name='bizMatch']").off("submit");
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                // Form submit validation 
                $("form[name='bizMatch']").on("submit", function(e) {
                    e.preventDefault();
                    var _this = $(this);
                    var contactName = $('[name="contact_name"]').val();
                    var companyName = $('[name="company_name"]').val();
                    var companyAddress = $('[name="company_address"]').val();
                    var email = $('[name="email"]').val();
                    var mobile = $('[name="mobile"]').val();
                    var err = 0;
                    function showErrSpan(outputClass, msg) {
                        _this.find('span.' + outputClass).text(msg).show();
                        err = 1;
                    }
                    if (contactName === '') {
                        showErrSpan('contact-name', 'Contact Name required');
                    }
                    if (companyName === '') {
                        showErrSpan('company-name', 'Company Name required');
                    }
                    if (companyAddress === '') {
                        showErrSpan('company-address', 'Company Address required');
                    }
                    if (email === '') {
                        showErrSpan('email', 'Email Address required');
                    } else if (!isValidEmail(email)) {
                        showErrSpan('email', 'Invalid Email Address');
                    }
                    if (mobile === '') {
                        showErrSpan('mobile', 'Contact Number required');
                    } else if (!isValidMobile(mobile)) {
                        showErrSpan('mobile', 'Invalid Mobile Number');
                    }
                    //type of business
                    var business_type = 0;
                    $('[name="business_type[]"]').each(function() {
                        if ($(this).prop("checked")) {
                            business_type = 1;
                            return;
                        }
                    });
                    if (business_type === 0) {
                        showErrSpan('business-type', 'Please select at least 1 business type');
                    }
                    //product interest
                    var product_interest = 0;
                    $('[name="product_interest[]"]').each(function() {
                        if ($(this).prop("checked")) {
                            product_interest = 1;
                            return;
                        }
                    });
                    if (product_interest === 0) {
                        showErrSpan('product-interest', 'Please select at least 1 product category');
                    }

                    //stop if there is error
                    if (err === 1) {
                        $('html, body').animate({
                            scrollTop: $('[name="bizMatch"]').offset().top
                        }, 1000);
                        return false;
                    }
                    var biz_data = _this.serializeFormObject();
                    $.when(publicAPI.sendBizMatchMessage(biz_data)).done(function() {
                        _this.find(':input').each(function() {
                            if ($(this).attr("type") != "radio") {
                                $(this).val('');
                            }
                        });
                        _this.find('input:checkbox').each(function(i) {
                            if (i != 0 && i != 5) {
                                $(this).prop('checked', false);
                            }
                        });
                        _this.find('textarea').each(function() {
                            $(this).val('');
                        });
                        $('[name="bizMatch"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Email Sent Successfully');
                        $('.form-success-msg').find('.form-success-msg-text').html('Thank you for getting in touch with us,<br/> we will get back to you shortly.');
                        $('.form-success-msg').removeClass('form-error-msg').show();
                        $('html, body').animate({
                            scrollTop: $("#main-nav").offset().top
                        }, 1000);

                    }).fail(function(data) {
                        $('[name="bizMatch"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Error');
                        $('.form-success-msg').find('.form-success-msg-text').html('Sorry, email was not sent.');
                        $('.form-success-msg').addClass('form-error-msg').show();
                        $('html, body').animate({
                            scrollTop: $("#main-nav").offset().top
                        }, 1000);
                    });
                });
                $('body').on('click', 'span.error', function() {
                    $(this).hide().prev('input').focus();
                });
                $('body').on('focus', 'input', function() {
                    $(this).next('span.error').hide();
                });

                $('body').on('click', '.form-success-msg-btn', function() {
                    $('[name="bizMatch"]').show();
                    $('.form-success-msg').hide();
                });
            } // initEvent()

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    //alert("test");//
                }
            } // initUI()

            /**
             * Initialize Module
             */
            function init($config) {
                config = config || {};
                config = $.extend({}, config, $config);
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init Bizmatch Module    
    window.app.Module.Bizmatch.init();
});