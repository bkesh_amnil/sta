var POINTERCLICK_THRESHOLD_X = 10;
var POINTERCLICK_THRESHOLD_Y = 10;

/**
 * Listens to pointer click event
 * @param element
 * @param optional childElementsSelector eg '#id', '.class'
 */
function listenForPointerClick(element, childElementsSelector) {
    if (childElementsSelector) {
        element.off("pointerdown", childElementsSelector);
        element.on("pointerdown", childElementsSelector, function(e) {
            var targetElement = $(this);
            // don't fire on right clicks
            if (e.originalEvent.pointerType == 'mouse' && e.which == 3) {
                return;
            }
            // Initial click position
            var pointerdown_x = e.clientX;
            var pointerdown_y = e.clientY;
            var displacement_x, displacement_y;

            $(document).off("pointermove");
            $(document).on("pointermove", function(evt) {
                // position during movement
                displacement_x = Math.abs(evt.clientX - pointerdown_x);
                displacement_y = Math.abs(evt.clientY - pointerdown_y);
                if (displacement_x > POINTERCLICK_THRESHOLD_X || displacement_y > POINTERCLICK_THRESHOLD_Y) {
                    $(document).off('pointermove');
                    targetElement.off('pointerup', childElementsSelector);
                }
            });

            element.off('pointerup', childElementsSelector);
            element.on('pointerup', childElementsSelector, function(evt) {
                $(document).off('pointermove');
                targetElement.off('pointerup', childElementsSelector);
                if (evt.originalEvent.pointerType == "mouse" && evt.which == 3) {
                    return;
                }
                targetElement.trigger("pointerclick", {
                    pointerEvent: evt
                });
            });
        });
    } else {
        element.off("pointerdown");
        element.on("pointerdown", function(e) {
            var targetElement = $(this);
            // don't fire on right clicks
            if (e.originalEvent.pointerType == 'mouse' && e.which == 3) {
                return;
            }
            // Initial click position
            var pointerdown_x = e.clientX;
            var pointerdown_y = e.clientY;
            var displacement_x, displacement_y;

            $(document).off("pointermove");
            $(document).on("pointermove", function(evt) {
                // position during movement
                displacement_x = Math.abs(evt.clientX - pointerdown_x);
                displacement_y = Math.abs(evt.clientY - pointerdown_y);
                if (displacement_x > POINTERCLICK_THRESHOLD_X || displacement_y > POINTERCLICK_THRESHOLD_Y) {
                    $(document).off('pointermove');
                    targetElement.off('pointerup');
                }
            });

            element.off('pointerup');
            element.on('pointerup', function(evt) {
                $(document).off('pointermove');
                targetElement.off('pointerup');
                if (evt.originalEvent.pointerType == "mouse" && evt.which == 3) {
                    return;
                }
                targetElement.trigger("pointerclick", {
                    pointerEvent: evt
                });
            });
        });
    }
}