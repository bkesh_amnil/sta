/***
 * Serialize form to a JS object.
 * - Note: radios may be probematic (TODO: confirm)
 * - Usage:
 *     var formData = $('#my-form').serializeFormObject();
 ***/
$.fn.serializeFormObject = function() {
    var result = {};
    var inputs = this.serializeArray();
    $.each(inputs, function() {
        // Check if this key already exists
        if (typeof result[this.name] !== "undefined") {
            // if this key is array, push.
            if (!result[this.name].push) {
                result[this.name] = [result[this.name]];
            }
            result[this.name].push(this.value || '');
        } else {
            result[this.name] = this.value || '';
        }
    });
    return result;
};