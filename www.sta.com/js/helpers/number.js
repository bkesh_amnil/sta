/**
 * Number helpers
 */

/***
 * check if the number is in between two numbers
 * Usage : var windowSize = 601;
 * windowSize.between(500, 600)
 ***/
Number.prototype.between = function(a, b) {
    var min = Math.min.apply(Math, [a, b]),
        max = Math.max.apply(Math, [a, b]);
    return this >= min && this <= max;
};