/***
 * Module - app.Module.contact
 *
 * Provides behaviour for contact.php page
 *
 * Dependencies
 *  - jQuery
 *
 * Before use
 *  - ensure document is ready
 *  - localDB is init
 *
 * Init instructions:
 *  - app.Module.contact.init(options);
 *
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(App, $) {
        "use strict";

        //var App = window.app = window.app || {};

        App.Module = App.Module = App.Module || {};
        App.Module.ContactUs = function() {

            /**
             * Resets Events
             */
            function resetEvent() {
                $("form[name='contactForm']").off("submit");
            }

            /**
             * Init Events
             */
            function initEvent() {
                // Form submit validation 
                $("form[name='contactForm']").on("submit", function(e) {
                    e.preventDefault();
                    var _this = $(this);
                    var contactName = $('[name="contact_name"]').val();
                    var companyName = $('[name="company_name"]').val();
                    var email = $('[name="email"]').val();
                    var mobile = $('[name="mobile"]').val();
                    var message = $('[name="message"]').val();
                    var err = 0;
                    function showErrSpan(outputClass, msg) {
                        _this.find('span.' + outputClass).text(msg).show();
                        err = 1;
                    }
                    if (contactName === '') {
                        showErrSpan('contact-name', 'Contact Name required');
                    }
                    if (email === '') {
                        showErrSpan('email', 'Email Address required');
                    } else if (!isValidEmail(email)) {
                        showErrSpan('email', 'Invalid Email Address');
                    }
                    if (mobile === '') {
                        showErrSpan('mobile', 'Mobile Number required');
                    } else if (!isValidMobile(mobile)) {
                        showErrSpan('mobile', 'Invalid Mobile Number');
                    }
                    if (message === '') {
                        showErrSpan('message', 'Message Name required');
                    }
                    if (err === 1) {
                        return false;
                    }
                    var contact_data = _this.serializeFormObject();
                    $.when(publicAPI.sendContactMessage(contact_data)).done(function() {
                        _this.find(':input').each(function() {
                            if ($(this).attr("type") != "radio") {
                                $(this).val('');
                            }
                        });
                        _this.find('textarea').each(function() {
                            $(this).val('');
                        });
                        $('[name="contactForm"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Email Sent Successfully');
                        $('.form-success-msg').find('.form-success-msg-text').html('Thank you for getting in touch with us,<br/> we will get back to you shortly.');
                        $('.form-success-msg').removeClass('form-error-msg').show();

                    }).fail(function(data) {
                        $('[name="contactForm"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Error');
                        $('.form-success-msg').find('.form-success-msg-text').html('Sorry, email was not sent.');
                        $('.form-success-msg').addClass('form-error-msg').show();
                    });
                });
                $('body').on('click', 'span.error', function() {
                    $(this).hide().prev('input').focus();
                });
                $('body').on('focus', 'input', function() {
                    $(this).next('span.error').hide();
                });
                $('body').on('click', '.form-success-msg-btn', function() {
                        $('[name="contactForm"]').show();
                        $('.form-success-msg').hide();
                });




            } // initEvent()

            /***
             * Initializes UI states
             ***/
            function initUI() {
                // loadGoogleMapScript();
            } // initUI()

            /***
             * Initialize Module
             ***/
            function init() {
                resetEvent();
                initEvent();
                initUI();
            } // init()

            return {
                init: init,
            };
        }();
    }(window.app = window.app || {}, jQuery));
    window.app.Module.ContactUs.init();

});