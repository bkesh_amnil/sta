/*** * Module - app.Module.Event 
 * * * Provides behaviour for event.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Event.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Event = function() {
            var advertisements = '';
            var events = '';
            var allEvents = '';
            var currentIndex = 0;

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                $(window.app).off("ready");
                $(window.app).on("ready", function() {
                    var hashEventSlug = window.location.hash.substr(1);
                    allEvents = ((localDB.STAevents).filter(function(e, i) {
                        return e["DisplayPriority"] > 0;
                    })).sortBy("EventTimestamp", true);
                    var hashEventData = (allEvents.filter(function(e, i) {
                        return hashEventSlug == slugify(e['EventName']);
                    }));
                    if (hashEventData.length != 0) {
                        var selectedHashEventName = hashEventData[0]['EventName'];
                        $.each(allEvents, function(i, v) {
                            if (v.EventName == selectedHashEventName) {
                                currentIndex = i;
                            }
                        });
                    }
                    console.log('hashEventData UI', hashEventSlug, hashEventData, currentIndex);
                    LoadEvent(currentIndex, 0);
                    console.log('event is ready');

                    $(window.app).off("ready");
                });

                $('.share-this-event-btn').off('click');
                $('.share-this-event-btn').on('click', function() {
                    var EventId = $(this).attr('rel');
                    var thisEvent = (events.filter(function(e, i) {
                        return e['id'] == EventId;
                    })).sortBy("EventTimestamp", true);
                    var data = thisEvent[0];
                    var img_URL = '';
                    if (data.hasImage == 1) {
                        img_URL = IMAGES_URL + '/events/' + data.Image;
                    } else {
                        img_URL = IMAGES_URL + "/events-page-outdoor-decking-tips.jpg";
                    }

                    //var sharedDescription = encodeURIComponent(data.Description);
                    var facebookShareUrl = DOMAIN_URL + "/share.php?sharetitle=" + data.EventName + "&sharedescription=" + data.Description + "&shareimg=" + img_URL + "&uid=" + data.ID + "&stamp=" + Math.random();
                    var mainUrl = "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(facebookShareUrl);
                    window.open(mainUrl, 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                    return false;


                });

                $('body').on('click', 'a.view-map', function(e) {
                    e.preventDefault();
                    var latt = $(this).attr('data-lat');
                    var long = $(this).attr('data-long');
                    var mapInfo = [{"id": 1, "latt": latt, "long": long}];
                    localDB.STAmap.mergeUniqueByID(mapInfo);
                    localDB.save('STAmap');
                    console.log(localDB.STAmap);
                    window.open(DOMAIN_URL + "/map", '_blank');
                });
                $('#events-prev').on('click', function(e) {
                    e.preventDefault();
                    currentIndex = $('#events-prev-next').attr('rel');
                    LoadEvent(currentIndex, 'prev');
                });
                $('#events-next').on('click', function(e) {
                    e.preventDefault();
                    var currentIndex = $('#events-prev-next').attr('rel');
                    LoadEvent(currentIndex, 'next');
                });

                // Form submit validation 
                $("form[name='eventRegisterForm']").off("submit");
                $("form[name='eventRegisterForm']").on("submit", function(e) {
                    e.preventDefault();
                    var _this = $(this);
                    var contactName = $('[name="contact_name"]').val();
                    var companyName = $('[name="company_name"]').val();
                    var email = $('[name="email"]').val();
                    var mobile = $('[name="mobile"]').val();
                    var participants = $('[name="participants"]').val();
                    var err = 0;
                    function showErrSpan(outputClass, msg) {
                        _this.find('span.' + outputClass).text(msg).show();
                        err = 1;
                    }
                    if (contactName === '') {
                        showErrSpan('contact-name', 'Contact Name required');
                    }
                    if (email === '') {
                        showErrSpan('email', 'Email Address required');
                    } else if (!isValidEmail(email)) {
                        showErrSpan('email', 'Invalid Email Address');
                    }
                    if (mobile === '') {
                        showErrSpan('mobile', 'Mobile Number required');
                    } else if (!isValidMobile(mobile)) {
                        showErrSpan('mobile', 'Invalid Mobile Number');
                    }
                    if (participants === '') {
                        showErrSpan('participants', 'Number of Participants required');
                    } else if (isNaN(participants)) {
                        showErrSpan('participants', 'Invalid Number of Participants');
                    } else if (participants.length > 3) {
                        showErrSpan('participants', 'Only 3 digits');
                    }
                    if (err === 1) {
                        return false;
                    }
                    var event_data = _this.serializeFormObject();
                    $.when(publicAPI.sendEventRegistrationMessage(event_data)).done(function() {
                        _this.find(':input').each(function() {
                            if ($(this).attr("type") != "radio") {
                                $(this).val('');
                            }
                        });
                        _this.find('textarea').each(function() {
                            $(this).val('');
                        });
                        $('[name="eventRegisterForm"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Email Sent Successfully');
                        $('.form-success-msg').find('.form-success-msg-text').html('Thank you for getting in touch with us,<br/> we will get back to you shortly.');
                        $('.form-success-msg').removeClass('form-error-msg').show();

                    }).fail(function(data) {
                        $('[name="eventRegisterForm"]').hide();
                        $('.form-success-msg').find('.form-success-msg-title').html('Error');
                        $('.form-success-msg').find('.form-success-msg-text').html('Sorry, email was not sent.');
                        $('.form-success-msg').addClass('form-error-msg').show();
                    });
                });
                $('body').on('click', 'span.error', function() {
                    $(this).hide().prev('input').focus();
                });
                $('body').on('focus', 'input', function() {
                    $(this).next('span.error').hide();
                });
                $('body').on('click', '.form-success-msg-btn', function() {
                    $('[name="eventRegisterForm"]').show();
                    $('.form-success-msg').hide();
                });
            } // initEvent()

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    var hashEventSlug = window.location.hash.substr(1);
                    allEvents = ((localDB.STAevents).filter(function(e, i) {
                        return e["DisplayPriority"] > 0;
                    })).sortBy("EventTimestamp", true);
                    var hashEventData = (allEvents.filter(function(e, i) {
                        return hashEventSlug == slugify(e['EventName']);
                    }));
                    if (hashEventData.length != 0) {
                        var selectedHashEventName = hashEventData[0]['EventName'];
                        $.each(allEvents, function(i, v) {
                            if (v.EventName == selectedHashEventName) {
                                currentIndex = i;
                            }
                        });
                    }
                    console.log('hashEventData UI', hashEventSlug, hashEventData, currentIndex);
                    LoadEvent(currentIndex, 0);
                    console.log('event UI is ready');
                }
            } // initUI()


            /**
             * loads event in page
             */
            function LoadEvent(ThiscurrentIndex, action) {
                currentIndex = parseInt(ThiscurrentIndex);
                if (action == 'prev') {
                    currentIndex--;
                } else if (action == 'next') {
                    currentIndex++;
                }
                console.log('currentindex', currentIndex, action);
                $("form[name='eventRegisterForm']").find(':input').each(function() {
                    if ($(this).attr("type") != "radio") {
                        $(this).val('');
                    }
                });
                $("form[name='eventRegisterForm']").find('textarea').each(function() {
                    $(this).val('');
                });
                events = (allEvents.filter(function(e, i) {
                    return i == currentIndex;
                })).sortBy("EventTimestamp", true);
                $.each(events, function(i, v) {
                    var img_URL = '';
                    var doc_URL = '';
                    var doc_title = '';
                    //# check if image exists
                    if (v.hasImage == 1) {
                        img_URL = IMAGES_URL + '/events/' + v.Image;
                    } else {
                        img_URL = IMAGES_URL + "/events-page-outdoor-decking-tips.jpg";
                    }

                    var eventDate = v.EventDay + '  ' + v.EventMonth + '  ' + v.EventYear;
                    var eventTime = v.EventFrom + ' to ' + v.EventTo;
                    var mapLink = '<a id="view-map" class="view-map" href="" target="_blank">(<span>View map</span>)</a>';
                    $('#event-title').html(v.EventName);
                    $('#events-page-outdoor-decking-tips').find('img').attr('src', img_URL);
                    $('#event-page-outdoor-decking-tips-info').find('.event-date').html(eventDate);
                    $('#event-page-outdoor-decking-tips-info').find('.event-time').html(eventTime);
                    $('#event-page-outdoor-decking-tips-info').find('#event-description').html(v.Description);
                    $('#event-venue-details-content').find('.venue').html(v.Venue + ' ' + mapLink);
                    $('#event-venue-details-content').find('.view-map').attr('data-lat', v.Latitude).attr('data-long', v.Longitude);
                    $('#event-venue-details-content').find('.remarks').html(v.Remarks);
                    $('#event-venue-details-content').find('.course-fee').html(v.CourseFee);
                    //# check if pdf exists
                    if (v.hasPdf == 1) {
                        doc_URL = DOC_URL + '/' + v.Pdf;
                        doc_title = v.EventName;
                        $('#event-venue-details-content').find('span.view-brochure').show();
                        $('#event-venue-details-content').find('#view-brochure').attr('href', doc_URL).attr('title', doc_title);
                    } else {
                        doc_URL = "javascript:void(0)";
                        doc_title = 'No document available';
                        $('#event-venue-details-content').find('span.view-brochure').hide();
                        $('#event-venue-details-content').find('#view-brochure').attr('href', doc_URL).attr('title', doc_title);
                    }

                    $('[name="event_name"]').val(v.EventName);
                    $('#events-prev-next').attr('rel', currentIndex);
                    $('.share-this-event-btn').attr('rel', v.id);
                    var target_page = 'event#' + slugify(v.EventName);
                    window.history.pushState("nav", "Page: " + target_page, DOMAIN_URL + "/" + target_page);

                    var allEventsIndexLength = (allEvents.length) - 1;
                    console.log('new index bkesh', currentIndex, 'all events length', allEventsIndexLength);
                    if ((currentIndex) == allEventsIndexLength) {
                        $('#events-next').addClass('disabled-link');
                    } else {
                        $('#events-next').removeClass('disabled-link');
                    }
                    if ((currentIndex) == 0) {
                        $('#events-prev').addClass('disabled-link');
                    } else {
                        $('#events-prev').removeClass('disabled-link');
                    }
                });
            }

            /**
             * Initialize Module
             */
            function init() {
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init Event Module    
    window.app.Module.Event.init();
});