/*** * Module - app.Module.About 
 * * * Provides behaviour for about.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.About.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.About = function() {
            var bocs = '';

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                $(window.app).off("ready");
                $(window.app).on("ready", function() {
                    console.log('about is ready');
                    LoadBocs();
                    $(window.app).off("ready");
                });
            } // initEvent()

            /**
             * loads event in page
             */
            function LoadBocs() {
                bocs = ((localDB.STAbocs).filter(function(e, i) {
                    return e["DisplayPriority"] > 0;
                })).sortBy("DisplayPriority", false);
                $('#committee-member-container').html('');
                $.each(bocs, function(i, v) {
                    if (v.hasImage == 1) {
                        var img_URL = IMAGES_URL + '/bocs/' + v.Image;
                    } else {
                        var img_URL = IMAGES_URL + "/default-sta-image.svg";
                    }
                    //html contents
                    var content = '<figure id="" class="committee-member-box">' +
                            '<div class="committee-member-img-container">' +
                            '<img class="committee-member-img" src="' + img_URL + '" alt="' + v.Name + '" />' +
                            '</div>' +
                            '<span class="committee-member-rank">' + v.Designation + '</span>' +
                            '<span class="committee-member-name">' + v.Name + '</span>' +
                            '<span class="committee-member-company">' + v.Company + '</span>' +
                            '</figure>';
                    //append to the page
                    $('#committee-member-container').append(content);
                });
            }

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    LoadBocs();
                }
            } // initUI()

            /**
             * Initialize Module
             */
            function init() {
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init About Module    
    window.app.Module.About.init();
});