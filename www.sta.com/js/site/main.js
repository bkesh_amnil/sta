jQuery(window).load(function($) {

    // Add svg for all icons image
    jQuery('.left-content h3 img[src*=".svg"]').addClass('svg');
    /*
     * Replace all SVG images with inline SVG
     */
    jQuery('.left-content h3 img.svg, img.svg').each(function($) {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });

});


//for mobile device menu
$("#mobile-menu-btn").click(function(e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $("body").toggleClass("hidden");
    $("#mobile-nav").toggleClass('active');
    $("#overlay-shadow").toggleClass("active");
});

// $("#mobile-nav").click(function (e) {
//     // $("body").removeClass("hidden");
//     $(this).removeClass('active');
//     $("#mobile-menu-btn").removeClass("active");
//     $("#overlay-shadow").removeClass("active");
// });

$("#overlay-shadow").click(function(e) {
    e.preventDefault();
    $("body").removeClass("hidden");
    $(this).removeClass('active');
    $("#mobile-menu-btn").removeClass("active");
    $("#mobile-nav").removeClass("active");
});

// $("#upcoming-events-next").click(function (e) {
//     e.preventDefault();
//     $(".static-upcoming-events").css("display", "none");
//     $(".more-upcoming-events").css("display", "inline-block");
// })
// $("#upcoming-events-prev").click(function (e) {
//     e.preventDefault();
//     $(".static-upcoming-events").css("display", "inline-block");
//     $(".more-upcoming-events").css("display", "none");
// })


//load more members
$(function() {
    $("#load-more-members-btn").on("click", function() {
        $("#member-result-container").find("li:hidden:lt(3)").show('500');
    });
});


// Slim Scroll Implementation (Biz-Match Page)
$(function() {
    $('.checkbox-wrapper').slimScroll({
        height: '210px',
        alwaysVisible: true
    });
});

