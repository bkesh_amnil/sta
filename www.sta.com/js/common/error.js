/* Error JS */
/***
 * This Script incorporates following features:
 * - Handles events for 404 page
 *
 * Dependencies
 * - JQuery
 *
 ***/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        App.Module.Error = function() {
            /*
             * Initialize Events
             */
            function initEvent() {
                /*
                 * Attaches event handler for #home-link
                 */
                console.log('error aayo');
                listenForPointerClick($("#home-link"));
                $("#home-link").suppressClick();
                $("#home-link").off("pointerclick");
                $("#home-link").on("pointerclick", function(e) {
                    var target_page = DEFAULT_PAGE;
                    $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                        // Remember history (state, title, URL)
                        window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    }).fail(function() {

                    }).always(function() {

                    });
                });
            }

            /*
             * Initializes UI
             */
            function initUI() {
                if (window.app.ready) {
                    $('#main-header').attr('class', 'inner-header');
                }

            } // initUI()

            function init() {
                initEvent();
                initUI();
            }
            return {
                init: init
            }
        }();
    }(window.app = window.app || {}, jQuery));
// init Error Js
    app.Module.Error.init();
});