// paginationArray  as localDB.category etc
// perpage integer
// currentpage integer
// paginationArray integer
function paginate(perPage, currentPage, arraylength) {
    var nextPage = parseInt(currentPage) + 1;
    var prevPage = parseInt(currentPage) - 1;
    var firstPage = 1;
    var lastPage = Math.ceil(parseInt(arraylength) / parseInt(perPage));

    if (currentPage <= 1) {
        prevPage = 1;
    }

    if (currentPage >= lastPage) {
        nextPage = lastPage;
    }

    var displayStart = 0;
    var displayEnd = arraylength;
    if (perPage < arraylength) {
        displayStart = perPage * (currentPage - 1);
        displayEnd = perPage * currentPage;
    }

    var pageStart = currentPage;
    var pageEnd = currentPage + 4;
    var classFirst = '';
    var classPrev = '';
    var classNext = '';
    var classLast = '';

    if (perPage > arraylength) {
        classFirst = 'disabled';
        classPrev = 'disabled';
        classNext = 'disabled';
        classLast = 'disabled';
    }

    if (currentPage == 1) {
        classFirst = 'disabled';
        classPrev = 'disabled';
    }

    if (currentPage == lastPage) {
        classNext = 'disabled';
        classLast = 'disabled';
    }

    if (currentPage - 4 < 0 && currentPage + 4 > lastPage) {
        pageStart = 1;
        pageEnd = lastPage;
    } else {
        if (currentPage - 4 < 0) {
            pageStart = 1;
            pageEnd = 5;
        } else if (currentPage + 4 > lastPage) {
            pageStart = lastPage - 4;
            pageEnd = lastPage;
        }
    }

    return {
        'perPage': perPage,
        'currentPage': currentPage,
        'firstPage': firstPage,
        'prevPage': prevPage,
        'pageStart': pageStart,
        'pageEnd': pageEnd,
        'nextPage': nextPage,
        'lastPage': lastPage,
        'displayStart': displayStart,
        'displayEnd': displayEnd,
        'classFirst': classFirst,
        'classPrev': classPrev,
        'classNext': classNext,
        'classLast': classLast
    };

}