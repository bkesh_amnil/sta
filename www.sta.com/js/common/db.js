/***
 * Makes use of local (persistent in internet cache) or session storage (persistent across page refreshes) to enable storage of Objects.
 *
 * Script defines:
 * - localDB, sessionDB objects (HTML5 storage)
 *
 * Before use:
 * - set localDB and sessionDB default data and fields in init()
 *
 * To init, either:
 *    localDB.init();
 *    sessionDB.init();
 *
 * Usage example (localDB):
 *    var user = localDB.session_user; // to access data
 *    localDB.products = Array(); // to create/mutate data
 *    localDB.save('products'); // to save data to DB
 ***/
var localDB = {
    init: function() {
        // TODO: fill in with default data

        // Front advertisements
        this.STAadvertisements = [];
        var stored = localStorage.getItem("STAadvertisements");
        if (stored != null) {
            this.STAadvertisements = JSON.parse(stored);
        }

        // front events
        this.STAevents = [];
        var stored = localStorage.getItem("STAevents");
        if (stored != null) {
            this.STAevents = JSON.parse(stored);
        }

        //front bocs
        this.STAbocs = [];
        var stored = localStorage.getItem("STAbocs");
        if (stored != null) {
            this.STAbocs = JSON.parse(stored);
        }
        //front companies
        this.STAcompanies = [];
        var stored = localStorage.getItem("STAcompanies");
        if (stored != null) {
            this.STAcompanies = JSON.parse(stored);
        }
        //front maps
        this.STAmap = [];
        var stored = localStorage.getItem("STAmap");
        if (stored != null) {
            this.STAmap = JSON.parse(stored);
        }

        this.hasInit = true;
    },
    // Flags whether localDB has init
    hasInit: false,
    // Start afresh
    clear: function() {
        localStorage.clear();
        this.init();
    },
    // Writes an item to local DB
    save: function(key) {
        var data = eval("this." + key);
        localStorage.setItem(key, JSON.stringify(data));
    },
    // Reverts to saved copy of an item
    revert: function(key) {
        var stored = localStorage.getItem(key);
        if (stored != null) {
            this[key] = JSON.parse(stored);
        }
    }
} //var localDB

var sessionDB = {
    // Inits session DB
    init: function() {
        // TODO: fill in with default session data
        // 
        // Session User
        this.user = {
            "id": "",
            "email": "",
            "emailVerified": false,
            "userType": "Customer",
            "accessLevel": 1,
            "banned": true,
            "language": "en",
            "isAdmin": "",
            "createdTime": 1234567890,
            "lastUpdateTime": 1234567980,
            "lastActivityTime": 1234567980,
            "firstName": "",
            "lastName": "",
            "profession": "",
            "residenceCountry": "",
            "residencePhone": "",
            "homeCountry": "",
            "homeState": "",
            "homePostalCode": "",
            "homePhone": "",
            "officeCountry": null,
            "officeState": null,
            "officePostalCode": null,
            "officePhone": ""
        }
        var stored = sessionStorage.getItem("user");
        if (stored != null) {
            this.user = JSON.parse(stored);
        }

        this.subscriptionStatussubscribed = 0;
        var stored = sessionStorage.getItem("subscriptionStatus");
        if (stored != null) {
            this.subscriptionStatus = stored;
        }

        this.hasInit = true;
    },
    // Flags whether sessionDB has init
    hasInit: false,
    // Start afresh
    clear: function() {
        sessionStorage.clear();
        this.init();
    },
    // Writes an item to local DB
    save: function(key) {
        var data = eval("this." + key);
        sessionStorage.setItem(key, JSON.stringify(data));
    },
    // Reverts to saved copy of an item
    revert: function(key) {
        var stored = sessionStorage.getItem(key);
        if (stored != null) {
            this.key = JSON.parse(stored);
        }
    }
} //var sessionDB