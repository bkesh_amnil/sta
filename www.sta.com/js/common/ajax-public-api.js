/***
 * This script defines functions to make AJAX calls to server's Public API.
 * - Each function correspond to one function in the API.
 * - Each function returns a jQuery Promise, so functions can be chained.
 *
 * Before use:
 * - PUBLIC_API_URL is defined
 * - ensure localDB & sessionDB are init
 ***/

var publicAPI = {
    /***
     * Get a number of advertisements to display on the site. The highest priority ones will be returned.
     * @param: e.g. {count: 2}
     ***/
    getAdvertisements: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getAdvertisements',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            console.log('holla',data.advertisements);
            localDB.STAadvertisements.mergeUniqueByID(data.advertisements);
            localDB.save('STAadvertisements');
        });
    },
    /***
     * Gets events list from server that have been updated since a certain time.
     * @param:
     ***/
    getUpdatedEvents: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedEvents',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.STAevents.mergeUniqueByID(data.events);
            localDB.save('STAevents');
        });
    },
    /***
     * Gets bocs list from server that have been updated since a certain time.
     * @param:
     ***/
    getUpdatedBocs: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedBocs',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.STAbocs.mergeUniqueByID(data.bocs);
            localDB.save('STAbocs');
        });
    },
    
    /***
     * Gets companies list from server that have been updated since a certain time.
     * @param:
     ***/
    getUpdatedCompanies: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedCompanies',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.STAcompanies.mergeUniqueByID(data.companies);
            localDB.save('STAcompanies');
        });
    },
    
    /***
     * Sends email to site admin for event registration.
     ***/
    sendEventRegistrationMessage: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/sendEventRegistrationMessage',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    /***
     * Sends email to site admin for biz match .
     ***/
    sendBizMatchMessage: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/sendBizMatchMessage',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    /***
     * Sends email to site admin abou enquire.
     ***/
    sendContactMessage: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/sendContactMessage',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {

        });
    }
}