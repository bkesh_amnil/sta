/***
 * Defines additional functions in `window` object to aid:
 * 1) AJAX switching
 * 2) Animated Scrolling
 * 3) Fix/unfix iPad bug [not an elegant solution, but really no choice until Apple fixes the bug]
 *
 * Adds the following functions to `window`:
 * - window.switchPage()
 * - window.getCurrentPageName()
 * - window.fixIpadBug()
 * - window.unfixIpadBug()
 * - window.animateScroll()
 *
 * It also defines these variables/functions/objects for use by apps:
 * - window.minHeight, window.minWidth
 *
 * Before use:
 * - ensure config file is included (animation settings, CSS_URL, JS_URL, DEFAULT_PAGE)
 * - ensure body min-height and min-width defined in CSS
 * - ensure body is {position: relative;} and not absolutely positioned in CSS.
 * - ensure <section id="content" data-page="example-page-name"> is present, with appropriate names for each page
 * - define animation settings in animateInPage(), animateOutPage(), animateScroll()
 ***/

/***
 * Get the name of the current page
 * @return: string of page name from #content, or empty string if nto found
 ***/
function getCurrentPageName() {
    var name = $('#content').attr('data-page');
    if (name !== undefined) return String(name);
    else return '';
} //getCurrentPageName()

/***
 * Loads new page using AJAX
 * - animates out old page
 * - removes old CSS
 * - fetches new page from server and adds new CSS, content, JS to document
 * - animates in new page and adds "active" class to all "nav-links"
 ***/
function switchPage(target_page) {
    //    showLoadingAjaxContainer(); // For better UI experience by showing loading gif at the beginning (and hope that every page fires ajax at beginning)
    $(document).unbind(".loading");
    /***
     * Function to be called after AJAX load
     * - #content opacity will become 1
     ***/
    function animateInPage() {
        return $('#content').stop().animate({
            opacity: 1
        }, CONTENT_ENTER_DURATION, CONTENT_ENTER_EASING);
    } //animateInPage()

    /***
     * Function to be called prior to every AJAX unload
     * - #content opacity will become 0
     ***/
    function animateOutPage() {
        return $('#content').stop().animate({
            opacity: 0
        }, CONTENT_EXIT_DURATION, CONTENT_EXIT_EASING);
    } //animateOutPage()

    /***
     * Dynamically adds a CSS file to this page
     ***/
    function addCss(css_file_name, page_name) {
        var $link = $('<link>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: CSS_URL + '/' + css_file_name
        }).appendTo(document.head);
        $link.data('page', page_name);
    } //addCss()

    /***
     * Dynamically removes all CSS files for a given page
     * - checks the <link> "data-page" attribute
     ***/
    function removeCss(page_name) {
        return $('link').each(function(index) {
            if ($(this).data('page') == page_name) {
                $(this).attr('disabled', 'disabled');
                $(this).remove();
                // console.log(page + " CSS disabled " + index);
            }
        });
    } //removeCss()

    /***
     * Dynamically adds a JS file to this page
     ***/
    function addJs(file_name) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if (file_name.startsWith('http://') || file_name.startsWith('https://') || file_name.startsWith('//')) {
            script.src = file_name;
        } // external link
        else {
            script.src = JS_URL + '/' + file_name;
        } // internal link
        document.body.appendChild(script);
    } //addJs()

    /***
     * Fetches page and stores it in "data" variable
     ***/
    var data; // stores return data of AJAX call in fetchPage()
    function fetchPage(target_page) {
        return $.ajax({
            url: target_page,
            dataType: 'json'
        }).done(function(d, s, x) {
            data = d;
        });
        
    } //fetchPage()

    // Animate out page, fetch new page, animate in page
    var curr_page = getCurrentPageName(); 
    return $.when(animateOutPage(), fetchPage(target_page)).done(function() {
//      alert(12);
        try {
            // Remove old CSS
            removeCss(curr_page);

            // Add CSS
            for (i = 0; i < data.css.length; i++) {
                addCss(data.css[i], target_page);
            }

            // Replace content
            $('#content').attr('data-page', target_page);
            $('#content').html(data.content);
            document.title = data.title;

            // Add JS (which will also run it)
            for (i = 0; i < data.js.length; i++) {
                addJs(data.js[i]);
            }

            // Set nav-links
            $('.nav-link').removeClass('active');
            $('.nav-link[data-for="' + target_page + '"]').addClass('active');

            //Animate in page
            animateInPage();
            bindLoadingDiv();
        } catch (e) {   

            //Force page change on error
            window.location = DOMAIN_URL + '/' + target_page;
            console.log('Error: AJAX return result error (Status: 200)' + e);
        } //catch
    }).fail(function(jqXHR, textStatus, errorThrown) {
        try {
            //Force page change on error
            if (errorThrown && errorThrown == "Unauthorized") {
                window.location = DOMAIN_URL + '/staff-login';
            } else {
                window.location = DOMAIN_URL + '/' + target_page;
            }
            //console.log(jqXHR.responseJSON.error, textStatus, errorThrown);
        } catch (err) {
            console.log('Unable to show jqXHR.responseJSON.error');
            console.log(jqXHR.responseText);
        }
    });
} //switchPage()

/***
 * Handler for Browser Back/Next buttons
 * - AJAX load page rather than let browser re-fetch
 ***/
window.onpopstate = function(evt) {
    //Get the latter part of URL (after the domain URL), includes path/query-string etc.
    var path = '';
    var new_url = window.location.href;
    var tmp = new_url.indexOf(DOMAIN_URL + '/');
    if (tmp != -1) {
        path = new_url.substr(tmp + DOMAIN_URL.length + 1);
    }

    // Parse query string to get target page
    var target_page = DEFAULT_PAGE; //default page
    if (path.indexOf("?page=") == 0) {
        var ampersandIndex = path.indexOf('&');
        if (ampersandIndex == -1) {
            target_page = path.substr(6);
        } else {
            target_page = path.substr(6, ampersandIndex - 6);
        }
    } // there's a query string - path is before query string
    else if (path != '') {
        target_page = path;
    } // no query string - path refers to a page

    var curr_page = getCurrentPageName();

    // Handle webkit browsers which fire pops on normal pageload, i.e. only proceed if event is caused by back/forward buttons
    tmp = target_page.indexOf('#', 0);
    if (tmp == -1) tmp = target_page.length;
    if (target_page.substr(0, tmp) == curr_page && evt.state == null) { // real back/next should have non-null evt state)
        //console.log('detected webkit pop');
        return false;
    } //detected webkit pop on load

    // Switch page
    switchPage(target_page);
};

/*******************************
 *  Device-targeted Bug Fixes  *
 *******************************/
/***
 * Fix iPad bug of having extra 20px below footer (due to wrong reporting of window.innerHeight)
 * - Needed only for pages that are strictly one full screen. To be called by each page's JS during init
 * - Must be undone if not needed.
 ***/
function fixIPadBug() {
    if (navigator.userAgent.match(/iPad/i) != null) {
        $('body').css({
            'position': 'fixed',
            'bottom': 0,
        });
    }
} //fixIPadBug()

/***
 * Undo any fixes done to target iPad extra 20px below footer
 * - Needed for screens that span more than one page. To be called by each page's JS during init
 ***/
function unfixIPadBug() {
    if (navigator.userAgent.match(/iPad/i) != null) {
        $('body').css({
            'position': 'relative',
            'bottom': 0,
        });
    }
} //unfixIPadBug()

/***
 * Reads and stores CSS-defined minimum dimensions in `window` object for future use,
 * e.g. by functions that depend on window size to resize #content
 ***/
(function() {
    window.minHeight = parseInt($('body').css('min-height'));
    window.minWidth = parseInt($('body').css('min-width'));
})();

/********************************
 *           Scrolls            *
 ********************************/
/***
 * Stores the target scroll element of the window
 ***/
(function() {
    // Detect webkit browsers, which scrolls 'body'. Other browsers scroll 'html'
    if ('WebkitAppearance' in document.documentElement.style) {
        window.scrollElement = 'body';
    } else {
        window.scrollElement = 'html';
    }
})();

function animateScroll(targetX, targetY) {
    $(window.scrollElement).stop().animate({
        'scrollTop': targetY,
        'scrollLeft': targetX
    }, PAGE_SCROLL_DURATION, PAGE_SCROLL_EASING);
} //animateScroll()

/***
 * This part of the script fixes cross-browser deltas for mousewheel
 * Just include it in the script and use delta normally
 * Raises 'mousewheel' event on mouse scroll
 ***/
(function(e) {
    function r(t) {
        var n = t || window.event,
            r = [].slice.call(arguments, 1),
            i = 0,
            s = true,
            o = 0,
            u = 0;
        t = e.event.fix(n);
        t.type = "mousewheel";
        if (n.wheelDelta) {
            i = n.wheelDelta / 120
        }
        if (n.detail) {
            i = -n.detail / 3
        }
        u = i;
        if (n.axis !== undefined && n.axis === n.HORIZONTAL_AXIS) {
            u = 0;
            o = -1 * i
        }
        if (n.wheelDeltaY !== undefined) {
            u = n.wheelDeltaY / 120
        }
        if (n.wheelDeltaX !== undefined) {
            o = -1 * n.wheelDeltaX / 120
        }
        r.unshift(t, i, o, u);
        return (e.event.dispatch || e.event.handle).apply(this, r)
    }
    var t = ["DOMMouseScroll", "mousewheel"];
    if (e.event.fixHooks) {
        for (var n = t.length; n;) {
            e.event.fixHooks[t[--n]] = e.event.mouseHooks
        }
    }
    e.event.special.mousewheel = {
        setup: function() {
            if (this.addEventListener) {
                for (var e = t.length; e;) {
                    this.addEventListener(t[--e], r, false)
                }
            } else {
                this.onmousewheel = r
            }
        },
        teardown: function() {
            if (this.removeEventListener) {
                for (var e = t.length; e;) {
                    this.removeEventListener(t[--e], r, false)
                }
            } else {
                this.onmousewheel = null
            }
        }
    };
    e.fn.extend({
        mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        },
        unmousewheel: function(e) {
            return this.unbind("mousewheel", e)
        }
    })
})(jQuery);