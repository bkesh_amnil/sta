/// <reference path="../jquery-2.1.1.js" />


/***
 * Script for main website - common to all main webpages. To be called once during full page load.
 * Defines the main `app` object, and these variables/functions/objects for use in the app:
 * - app.ready
 *
 * On load, performs:
 * - init of client-side DB
 * - sync client-side DB with server
 *
 * When load execution completes, sets var app.ready to true and triggers "appready" event on $(document)
 *     - Individual pages should bind their load execution script to "appready" event
 *     - Individual pages should check var app.ready and manually trigger "appready" event if it's true
 *
 * Before use, ensure:
 *  - Fill in logic in app.init()
 *  - All nav-links have "data-for" attribute (for page-switching)
 ***/
/**************************
 *    Execute on load     *
 **************************/

var app = window.app = window.app || {};
// Jquery extension functions
$.fn.extend({
    // Removes class "inactive"
    customShow: function() {
        return this.each(function() {
            $(this).removeClass("inactive");
            //$(this).addClass("login-fadein");
            //$(this).removeClass("login-fadeout");
        });
    },
    // Adds class "inactive"
    customHide: function() {
        return this.each(function() {
            $(this).addClass("inactive");
            //$(this).removeClass("login-fadein");
            //$(this).addClass("login-fadeout");
        });
    },
    activateLink: function() {
        return this.each(function() {
            if (!$(this).hasClass("active")) {
                $(this).addClass("active");
            }
        });
    },
    deactivateLink: function() {
        return this.each(function() {
            $(this).removeClass("active");
        });
    },
    suppressClick: function() {
        return this.each(function() {
            $(this).off("click");
            $(this).on("click", function(e) {
                e.preventDefault();
                //e.stopPropagation();
                return false;
            });
        });
    }
});
app = $.extend({}, app, {
    ready: false, //will be set to true when load execution is complete
    userDataLoaded: false, //will be set to true when logged in user's info such as shirtFittingProfile, cart, etc has been loaded after logging in, will be set to false when user logs out
    init: function() {
        // Init storage
        localDB.init();
        sessionDB.init();

        // Sync DB with server
        return $.when(
                app.syncAdvertisements(),
                app.syncEvents(),
                app.syncBoc(),
                app.syncCompanies()
                ).done(function() {
            app.ready = true;
            $(window.app).trigger("ready");
            $(window.app).trigger("readySidebar");
            listenForPointerClick($("#logo"));
            $("#logo").suppressClick();
            $("#logo").off("pointerclick");
            $("#logo").on("pointerclick", function(e) {
                var target_page = "about";
                if (getCurrentPageName() == target_page) {
                    return;
                }
                $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                    // Remember history (state, title, URL)
                    window.history.pushState("nav", "Page: " + target_page, DOMAIN_URL + "/" + target_page);
                    $("html, body").animate({
                        scrollTop: "0px"
                    }, SCROLL_TOP_DURATION);
                }).fail(function() {

                });
            });
        });
    },
    syncAdvertisements: function() {
        fromTime = app.getLastUpdatedTime('STAadvertisements', 'localDB');
        return publicAPI.getAdvertisements({
            fromTime: fromTime
        });
    },
    syncEvents: function() {
        fromTime = app.getLastUpdatedTime('STAevents', 'localDB');
        return publicAPI.getUpdatedEvents({
            fromTime: fromTime
        });
    },
    syncBoc: function() {
        fromTime = app.getLastUpdatedTime('STAbocs', 'localDB');
        return publicAPI.getUpdatedBocs({
            fromTime: fromTime
        });
    },
    syncCompanies: function() {
        fromTime = app.getLastUpdatedTime('STAcompanies', 'localDB');
        return publicAPI.getUpdatedCompanies({
            fromTime: fromTime
        });
    },
    getLastUpdatedTime: function(dbElement, storageType) {
        if (storageType === 'localDB') {
            //gets last updated time of a localdb element
            var fromTime = 0;

            if (localDB[dbElement] !== null) {
                var datas = localDB[dbElement];
                $.each(datas, function(i, v) {
                    if (+v.LastUpdatedTime > +fromTime) {
                        fromTime = +v.LastUpdatedTime;
                    }
                });
                fromTime = fromTime + 1;
                return fromTime;
            }

        } else if (storageType === 'sessionDB') {
            //gets last updated time of a localdb element
            var fromTime = 0;
            if (sessionDB[dbElement] !== null) {
                var datas = sessionDB[dbElement];
                $.each(datas, function(i, v) {
                    if (+v.LastUpdatedTime > +fromTime) {
                        fromTime = +v.LastUpdatedTime;
                    }
                });
                fromTime = fromTime + 1;
                return fromTime;
            }
        }
        ;


    }
});

// Init static elements (dont need app)

//init PageSeitcher 
app.Module.PageSwitcher.initPageSwitcher("content");
initNavLinks();

// Init dynamic elements
$.when(app.init()).done(function() {
    initLoginPanel();
    closePopupMessagesOnEscapeKey();
}).fail(function() {
    console.log('app.init fail. app.ready ' + app.ready);
});

/******************
 *     Header     *
 ******************/
/***
 *
 ***/
function initLoginPanel() {
//    app.Module.Login.init();
}

function initNavLinks() {
//    app.Module.Navigation.init();
}

function closePopupMessagesOnEscapeKey() {
    $(document).keydown(function(e) {
        // ESCAPE key pressed
        if (e.keyCode == 27) {
            if ($("#response-popup-close:visible").length > 0) {
                $("#response-popup-close").trigger("pointerclick");
            }
        }
    });
}