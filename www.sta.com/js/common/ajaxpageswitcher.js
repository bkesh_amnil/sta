﻿/// <reference path="../jquery-2.1.1.js" />

        (function (app, $) {
            "use strict";
            var App = window.app = window.app || {};
            App.Module = App.Module = App.Module || {};

            /***
             *   Module Ajax Page Switcher
             *   This module help for Ajax Page Switcher
             ***/
            $.fn.imagesLoaded = function () {
                var $imgs = this.find('img[src!=""]');

                // if there's no images, just return an already resolved promise
                var dfd = $.Deferred();
                if (!$imgs.length) {
                    return dfd.notify(100);
                }
                var imageLength = $imgs.length;
                var incrementPercentage = 100 / imageLength;
                var notifyPercentage = incrementPercentage;
                // for each image, add a deferred object to the array which resolves when the image is loaded

                $imgs.each(function () {
                    var img = new Image();
                    img.onload = function () {
                        dfd.notify(notifyPercentage);
                        notifyPercentage += incrementPercentage;
                        imageLength--;
                        if (imageLength == 0) {
                            dfd.resolve();
                        }
                    }
                    img.onerror = function () {
                        console.log("load error", img);
                    }
                    img.src = this.src;
                });

                // return a master promise object which will resolve when all the deferred objects have resolved
                // IE - when all the images are loaded
                return dfd.promise();
            }

            App.Module.PageSwitcher = function () {
                var
                        contentEl = "",
                        progress,
                        totalImageLength;

                /***
                 * Initilize initPageSwitcher
                 ***/
                function initPageSwitcher(el) {
                    contentEl = $(["#", el || "content"].join(""));
                    progress = new CircularProgress({
                        radius: 150,
                        strokeStyle: 'white',
                        lineCap: 'square',
                        lineJoin: 'round',
                        lineWidth: 2,
                        text: {
                            font: '200px ostrich_sansmedium',
                            fillStyle: 'white'
                        },
                        initial: {
                            strokeStyle: 'rgba(255,255,255,0.5)',
                            lineCap: 'square',
                            lineJoin: 'round',
                            lineWidth: 1
                        }
                    });

                    $("#content-progress").append(progress.el);
                    initEvents();
                }

                /***
                 * Initilize the event for page navigation
                 ***/
                function initEvents() {
                    initNavLinks();
                    window.onpopstate = function (evt) {
                        //Get the latter part of URL (after the domain URL), includes path/query-string etc.
                        var path = '';
                        var new_url = window.location.href;
                        var tmp = new_url.indexOf(DOMAIN_URL + '/');
                        if (tmp != -1) {
                            path = new_url.substr(tmp + DOMAIN_URL.length + 1);
                        }

                        // Parse query string to get target page
                        var target_page = DEFAULT_PAGE; //default page
                        if (path.indexOf("?page=") == 0) {
                            var ampersandIndex = path.indexOf('&');
                            if (ampersandIndex == -1) {
                                target_page = path.substr(6);
                            } else {
                                target_page = path.substr(6, ampersandIndex - 6);
                            }
                        } // there's a query string - path is before query string
                        else if (path != '') {
                            target_page = path;
                        } // no query string - path refers to a page

                        var curr_page = getCurrentPageName();

                        // Handle webkit browsers which fire pops on normal pageload, i.e. only proceed if event is caused by back/forward buttons
                        tmp = target_page.indexOf('#', 0);
                        if (tmp == -1)
                            tmp = target_page.length;
                        if (target_page.substr(0, tmp) == curr_page && evt.state == null) { // real back/next should have non-null evt state)
                            
                            return false;
                        } //detected webkit pop on load

                        // Switch page
                        switchPage(target_page);
                    };
                }

                /***
                 * Makes nav-link(s) for current page active, and all other nav-links inactive
                 ***/
                function initNavLinks() {
                    // Set current page's nav-link to be active
                    var current_page = getCurrentPageName();

                    $('.nav-link[data-for="' + current_page + '"]').addClass('active');
                    //Code to check if the current page is a sub nav or not
                    //var sub_nav = $('.sub-nav').find('[data-for="' + current_page + '"]');

                    // Suppress click event
                    $("#main-nav,#mobile-nav, #content,footer").off("click", ".nav-link");
                    $("#main-nav,#mobile-nav, #content,footer").on("click", ".nav-link", function (e) {
                        e.preventDefault();
                    });

                    // Bind pointer down event on navigation links and switches to corresponding page
                    listenForPointerClick($("#main-nav,#mobile-nav, #content,footer"), ".nav-link");
                    $("#main-nav,#mobile-nav, #content,footer").off("pointerclick", ".nav-link");
                    $("#main-nav,#mobile-nav, #content,footer").on("pointerclick", ".nav-link", function (e) {

                        var sub_menu_id = $(this).data('for');

                        var sub_menu = $('#' + sub_menu_id);
                        if (sub_menu.hasClass("active")) {
                            sub_menu.removeClass("active");
                        } else {
//                            app.Module.Navigation.addEventToCloseSubMenu();
                            sub_menu.addClass("active");
                            if (globalNavigationInterval !== undefined && globalNavigationInterval > 0) {
                                clearInterval(globalNavigationInterval);
                            }
                            // Remove active class if user does not click How it works or Este Bartin Commitment link for x seconds
                            globalNavigationInterval = setTimeout(function () {
                                sub_menu.removeClass("active");
                            }, AUTO_HIDE_ABOUT_SUB_NAV_TIMEOUT);
                        }

                        // If the page is the same page deactivate the click event
                        var current_page = getCurrentPageName();
                        var element = $(this);
                        var elementName = element.attr("data-for");
                        
                        if (current_page === elementName) {
                            if (current_page == 'event') {
                                var hashEventSlug = window.location.hash.substr(1);
                                var allEvents = ((localDB.STAevents).filter(function(e, i) {
                                    return e["DisplayPriority"] > 0;
                                })).sortBy("EventTimestamp", true);
                                var firstEventSlug = slugify(allEvents[0]['EventName']);
                                if (hashEventSlug == firstEventSlug) {
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                        // Go to new page
                        var target_page = $(this).data('for');
                        
                            $.when(switchPage(target_page)).done(function () {
                                window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                                $("#main-nav .nav-link").removeClass("active");
                                $("#mobile-nav .nav-link").removeClass("active");

                                //it is a parent menu
                                target_page = target_page.split('#')[0];
                                $('.nav-link[data-for="' + target_page + '"]').addClass("active");


                                contentEl.attr('data-page', target_page);
                                
                                //#special for sta >>
                                var mobile_page_title='';
                                if (target_page == 'event') {
                                   mobile_page_title = 'Events';
                                }else if (target_page == 'services') {
                                   mobile_page_title = 'Services';
                                }else if (target_page == 'library') {
                                   mobile_page_title = 'Library';
                                }else if (target_page == 'biz-match') {
                                   mobile_page_title = 'Biz-match';
                                }else if (target_page == 'members') {
                                   mobile_page_title = 'Members';
                                }else if (target_page == 'about') {
                                   mobile_page_title = 'About Us';
                                }else if (target_page == 'contact') {
                                   mobile_page_title = 'Contact Us';
                                }
                                $('.mobile-nav-title').text(mobile_page_title);
                                //#special for sta <<

                                $("html, body").animate({
                                    scrollTop: "0px"
                                }, SCROLL_TOP_DURATION);
                            });
                        
                    });

//                    $("#footer-top").off("click", ".nav-links");
//                    $("#footer-top").on("click", ".nav-links", function (e) {
//                        e.preventDefault();
//                    });
//
//                    listenForPointerClick($("#footer-top"), ".nav-links");
//                    $("#footer-top").off("pointerclick", ".nav-links");
//                    $("#footer-top").on("pointerclick", ".nav-links", function (e) {
//                        // Go to new page
//                        var target_page = $(this).data('for');
//                        // If the page is the same page deactivate the click event
//                        var current_page = getCurrentPageName();
//                        var element = $(this);
//                        var elementName = element.attr("data-for");
//                        if (current_page === elementName) {
//                            return;
//                        }
//
//                        if ($(this).attr("data-requires-login") == "yes") {
//                            if (!window.app.Module.Login.isUserLoggedIn()) {
//                                animateScroll(0, 0);
//                                window.app.Module.Login.displayLoginForm();
//                                return;
//                            }
//                        }
//                        $.when(switchPage(target_page)).done(function () {
//                            window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
//                            var sub_nav = $('.sub-nav').find('[data-for="' + target_page + '"]');
//                            $("#main-nav .nav-link").removeClass("active");
//                            if (sub_nav.length) {
//                                //if the current page is a sub/child menu
//                                var sub_nav_group_id = sub_nav.first().parent().attr('id');
//                                $("[data-for='" + sub_nav_group_id + "']").addClass("active");
//                            } else {
//                                //else it is a parent menu
//                                $('.nav-link[data-for="' + target_page + '"]').addClass("active");
//                            }
//                            $("html, body").animate({
//                                scrollTop: "0px"
//                            }, SCROLL_TOP_DURATION);
//                        });
//                    });
//
//                    $("#bottom-footer-nav").off("click", ".bottom-nav-links");
//                    $("#bottom-footer-nav").on("click", ".bottom-nav-links", function (e) {
//                        e.preventDefault();
//                    });
//
//                    listenForPointerClick($("#bottom-footer-nav"), ".bottom-nav-links");
//                    $("#bottom-footer-nav").off("pointerclick", ".bottom-nav-links");
//                    $("#bottom-footer-nav").on("pointerclick", ".bottom-nav-links", function (e) {
//                        // Go to new page
//                        var target_page = $(this).data('for');
//                        // If the page is the same page deactivate the click event
//                        var current_page = getCurrentPageName();
//                        var element = $(this);
//                        var elementName = element.attr("data-for");
//                        if (current_page === elementName) {
//                            return;
//                        }
//
//                        if ($(this).attr("data-requires-login") == "yes") {
//                            if (!window.app.Module.Login.isUserLoggedIn()) {
//                                animateScroll(0, 0);
//                                window.app.Module.Login.displayLoginForm();
//                                return;
//                            }
//                        }
//                        $.when(switchPage(target_page)).done(function () {
//                            window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
//                            var sub_nav = $('.sub-nav').find('[data-for="' + target_page + '"]');
//                            $("#main-nav .nav-link").removeClass("active");
//                            if (sub_nav.length) {
//                                //if the current page is a sub/child menu
//                                var sub_nav_group_id = sub_nav.first().parent().attr('id');
//                                $("[data-for='" + sub_nav_group_id + "']").addClass("active");
//                            } else {
//                                //else it is a parent menu
//                                $('.nav-link[data-for="' + target_page + '"]').addClass("active");
//                            }
//                            $("html, body").animate({
//                                scrollTop: "0px"
//                            }, SCROLL_TOP_DURATION);
//                        });
//                    });
                }

                /***
                 * Loads new page using AJAX
                 * - animates out old page
                 * - removes old CSS
                 * - fetches new page from server and adds new CSS, content, JS to document
                 * - animates in new page and adds "active" class to all "nav-links"
                 ***/
                function switchPage(target_page, queryString) {
                    
                    console.log('bkesh',target_page);
                    /***
                     * Function to be called after AJAX load
                     * - #content opacity will become 1
                     ***/
                    function animateInPage() {
                        $("#content-progress").fadeOut({
                            duration: CONTENT_ENTER_DURATION,
                            easing: CONTENT_ENTER_EASING
                        });
                        return contentEl.stop().animate({
                            opacity: 1
                        }, CONTENT_ENTER_DURATION, CONTENT_ENTER_EASING);
                    } //animateInPage()

                    /***
                     * Function to be called prior to every AJAX unload
                     * - #content opacity will become 0
                     ***/
                    function animateOutPage() {
                        progress.update(0, 1);
                        $("#content-progress").fadeIn({
                            duration: CONTENT_EXIT_DURATION,
                            easing: CONTENT_EXIT_EASING
                        });
                        return contentEl.stop().animate({
                            opacity: 0
                        }, CONTENT_EXIT_DURATION, CONTENT_EXIT_EASING, function () {
                            progress.update(0);
                        });
                    } //animateOutPage()

                    /***
                     * Dynamically adds a CSS file to this page
                     ***/
                    function addCss(css_file_name, page_name) {
                        var $link = $('<link>', {
                            rel: 'stylesheet',
                            type: 'text/css',
                            href: CSS_URL + '/' + css_file_name
                        }).appendTo(document.head);
                        $link.data('page', page_name);
                    } //addCss()

                    /***
                     * Dynamically removes all CSS files for a given page
                     * - checks the <link> "data-page" attribute
                     ***/
                    function removeCss(page_name) {
                        return $('link').each(function (index) {
                            if ($(this).data('page') == page_name) {
                                $(this).attr('disabled', 'disabled');
                                $(this).remove();
                            }
                        });
                    } //removeCss()

                    /***
                     * Dynamically removes all JS files for a given page
                     * - checks the <script> "data-page" attribute
                     ***/
                    function removeJs(page_name) {
                        return $('script').each(function (index) {
                            if ($(this).data('page') == page_name) {
                                $(this).attr('disabled', 'disabled');
                                $(this).remove();
                            }
                        });
                    } //removeJs()

                    /***
                     * Dynamically adds a JS file to this page
                     ***/
                    function addJs(file_name) {

                        var script = document.createElement('script');
                        script.type = 'text/javascript';
                        if (file_name.startsWith('http://') || file_name.startsWith('https://') || file_name.startsWith('//')) {
                            script.src = file_name;
                        } // external link
                        else {
                            script.src = JS_URL + '/' + file_name;
                        } // internal link
                        script.setAttribute('data-page', getCurrentPageName());
                        document.body.appendChild(script);


                    } //addJs()

                    /***
                     * Fetches page and stores it in "data" variable
                     ***/
                    var data; // stores return data of AJAX call in fetchPage()
                    function fetchPage(target_page) {
                        return $.ajax({
                            url: DOMAIN_URL + '/' + target_page,
                            dataType: 'json'
                        }).done(function (d, s, x) {
                            data = d;
                        });
                    } //fetchPage()

                    // Animate out page, fetch new page, animate in page
                    var curr_page = getCurrentPageName();
                    var percentageCollectionCache = [];
                    var totalImages = [];
                    var isLoading = false;
                    var _draw = function () {
                        if (totalImageLength <= totalImages.length && percentageCollectionCache.length == 0) {
                            animateInPage();
                        } else {
                            if (isLoading) {
                                return;
                            }
                            isLoading = true;
                            var per = percentageCollectionCache.shift();
                            if (per != null) {
                                progress.update(per);
                            }
                            setTimeout(function () {
                                if (totalImageLength <= totalImages.length && percentageCollectionCache.length == 0) {
                                    animateInPage();
                                } else {
                                    isLoading = false;
                                    _draw();
                                }
                            }, PRELOADER_IMAGE_LOADED_TEST_TIMEOUT); // Here 30 (atleast 30 for visual consistency) ms setTimeout() hack is used so that the callback event is added to the execution queue and will be executed after 30ms . This gives some time for values to be pushed in percentageCollectionCache before it is popped out resulting infinite loop
                        }

                    };

                    return $.when(animateOutPage(), fetchPage(target_page)).done(function () {
                        try {
                            // Remove old CSS
                            removeCss(curr_page);

                            // Add CSS
                            for (i = 0; i < data.css.length; i++) {
                                addCss(data.css[i], target_page);
                            }

                            // Replace content
                            contentEl.attr('data-page', target_page);
                            contentEl.html(data.content);
                            document.title = data.title;

                            // Remove old CSS
                            removeJs(curr_page);

                            // Add JS (which will also run it)

                            for (var i = 0; i < data.js.length; i++) {
                                addJs(data.js[i]);
                            }


                            try {
                                totalImageLength = contentEl.find("img[src!='']").length;
                                contentEl.imagesLoaded().progress(function (per) {
                                    percentageCollectionCache.push(per);
                                    totalImages.push($.extend({}, per));
                                    _draw();
                                }).done(function () {

                                });
                                var sub_nav = $('.sub-nav').find('[data-for="' + target_page + '"]');
                                $("#main-nav .nav-link").removeClass("active");
                                $("#mobile-nav .nav-link").removeClass("active");
                                if (sub_nav.length) {
                                    //if the current page is a sub/child menu
                                    var sub_nav_group_id = sub_nav.first().parent().attr('id');
                                    $("[data-for='" + sub_nav_group_id + "']").addClass("active");
                                } else {
                                    //else it is a parent menu
                                    $('.nav-link[data-for="' + target_page + '"]').addClass("active");
                                }

                            } catch (ee) {
                            }
                        } catch (e) {

                            //Force page change on error
                            window.location = DOMAIN_URL + '/' + target_page + (queryString == null ? "" : queryString);
                            console.log('Error: AJAX return result error (Status: 200)' + e);
                        } //catch
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        try {
                            //Force page change on error
                            window.location = DOMAIN_URL + '/' + target_page + (queryString == null ? "" : queryString);
                            console.log(jqXHR.responseJSON.error);
                        } catch (err) {
                            console.log('Unable to show jqXHR.responseJSON.error');
                            console.log(jqXHR.responseText);
                        }
                    });
                } //switchPage()

                /***
                 * Get the name of the current page
                 * @return: string of page name from #content, or empty string if nto found
                 ***/
                function getCurrentPageName() {
                    var name = contentEl.attr('data-page');
                    if (name !== undefined)
                        return String(name);
                    else
                        return '';
                }

                return {
                    initPageSwitcher: initPageSwitcher,
                    switchPage: switchPage,
                    getCurrentPageName: getCurrentPageName
                }
            }();
        }(window.app = window.app || {}, jQuery))