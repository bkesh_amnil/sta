/***
 * This file contains constants of various settings used within the application.
 ***/

/*****************
 * Path Settings *
 *****************/
// Server URL
var SERVER_URL = 'http://sta.front.dev'; //bkesh-localhost
//var SERVER_URL = 'http://staging.www.wlbuilder.com'; //staging

// Website domain URL
var DOMAIN_URL = 'http://sta.front.dev'; //bkesh-localhost
//var DOMAIN_URL = 'http://staging.www.singaporetimber.com'; //staging

// Static content URL (e.g. CDN for css, js, img, html)
var STATIC_URL = 'http://sta.front.dev'; //bkesh-localhost
//var STATIC_URL = 'http://staging.www.wlbuilder.com'; //dev

// Derive other useful URLs
var PUBLIC_API_URL = SERVER_URL + '/public-api';
var CUSTOMER_API_URL = SERVER_URL + '/customer-api';
var STAFF_API_URL = SERVER_URL + '/staff-api';
var CSS_URL = STATIC_URL + '/css';
var JS_URL = STATIC_URL + '/js';
var IMAGES_URL = STATIC_URL + '/img';
var DOC_URL = STATIC_URL + '/docs';
var DEFAULT_PAGE = 'about'; // which page to fetch if no page name is specified

/*****************
 *  Data checks  *
 *****************/
// Accounts
var MIN_NAME_LENGTH = 3;
var MIN_PASSWORD_LENGTH = 6;

// Contact Message
var MIN_CONTACT_MESSAGE_SUBJECT_LENGTH = 3;
var MAX_CONTACT_MESSAGE_SUBJECT_LENGTH = 78;
var MIN_CONTACT_MESSAGE_LENGTH = 3;
var MAX_CONTACT_MESSAGE_LENGTH = 5000;

/******************************
 * General Animation Settings *
 ******************************/
// Content
var CONTENT_ENTER_DURATION = 500;
var CONTENT_ENTER_EASING = 'easeInOutCubic';
var CONTENT_EXIT_DURATION = 500;
var CONTENT_EXIT_EASING = 'easeInOutCubic';

// Overlays
var OVERLAY_ENTER_DURATION = 300;
var OVERLAY_ENTER_EASING = 'easeInOutCubic';
var OVERLAY_EXIT_DURATION = 150;
var OVERLAY_EXIT_EASING = 'easeInOutCubic';
var POPUP_MESSAGE_ENTER_DURATION = 10;

// Scroll
var PAGE_SCROLL_DURATION = 1000;
var PAGE_SCROLL_EASING = 'easeInOutQuad';
var ELEMENT_SCROLL_DURATION = 100;
var ELEMENT_SCROLL_EASING = 'easeInOutQuad';
var SCROLL_TOP_DURATION = 10;

// Element fades
var ELEMENT_ENTER_DURATION = 500;
var ELEMENT_ENTER_EASING = 'easeInOutCubic';
var ELEMENT_EXIT_DURATION = 300;
var ELEMENT_EXIT_EASING = 'easeInOutCubic';

//Banner Slide config
var BANNER_SLIDE_DURATION = 3000;
var BANNER_ANIMATION_DURATION = 1000;


/******************************
 * Latitute and Longitude     *
 ******************************/
var LATITUDE = 1.3380815;
var LONGITUDE = 103.8436802;


/******************************
 * Paypal constants           *
 ******************************/
var PAYPALURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
var PAYPALFACILITATOR = "bikal_bikal-facilitator@ymail.com";
var PAYPALCURRENCY = "SGD";

/******************************
 * Settimeout constants       *
 ******************************/
var VIEW_UPDATE_MEASUREMENT_CLOSE_FORM_TIMEOUT = 1000;
var AUTO_HIDE_ABOUT_SUB_NAV_TIMEOUT = 10000; // Hide How it works and  Commitment nav after 10 sec
var PRELOADER_IMAGE_LOADED_TEST_TIMEOUT = 30;
var RADIO_BUTTON_TIMEOUT = 100; // Real radio buttons are hidden and replaced by spans and labels. Span's and label's for attribute are set to the radio button. Clicking in label is not changing radio button value instantaneously. So delay introduced.
var SET_FOCUS_TIMEOUT = 100; // To set focus on a hidden control, you first need the control to be visible.
var BUILD_SHIRT_TIMEOUT = 100;
var CSS_TIMEOUT = 10; // Timeout required for jquery.css property to take effect. for eg jquery.css('left', 10px) will not be reflected in jquery.offset().left immediately.
var FABRIC_PROPERTY_TIMEOUT = 100; // Used in Weave, Care, Pattern, Material Filter in Fabrics Gallery and Tailor Kit for animation
var WELCOME_MESSAGE_TIMEOUT = 2000;