/*** * Module - app.Module.Privacy 
 * * * Provides behaviour for privacy.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Privacy.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Privacy = function() {
            var config = {};

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                $(window.app).off("ready");
                $(window.app).on("ready", function() {
                    console.log('privacy is ready');
                    $(window.app).off("ready");
                });
            } // initEvent()

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    //alert("test");//
                }
            } // initUI()

            /**
             * Initialize Module
             */
            function init($config) {
                config = config || {};
                config = $.extend({}, config, $config);
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init Privacy Module    
    window.app.Module.Privacy.init();
});