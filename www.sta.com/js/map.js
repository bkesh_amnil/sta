/*** * Module - app.Module.Map 
 * * * Provides behaviour for map.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Map.init(options); 
 /***************************** *      Execute on load      * *****************************/
(function(app, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    app.Module.Map = function() {
        var latt = '';
        var long = '';

        /**
         * Removes all maps in this module: click, input, focus, pointerdown
         */
        function resetMap() {
        } // resetMap()

        /**
         * Adds all necessary maps: input, focus, pointerdown
         */
        function initMap() {

            loadScript();
            $(window.app).off("ready");
            $(window.app).on("ready", function() {

            });

        } // initMap()

        /**
         * Initializes UI states
         */
        function initUI() {
            if (window.app.ready) {

            }
        } // initUI()
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function initialize() {
            latt = parseFloat(getParameterByName("latt"));
            long = parseFloat(getParameterByName("long"));
            if (isNaN(latt) || isNaN(long)) { //#if not set from url
                var stored = localStorage.getItem("STAmap");
                var mapInfo = '';
                if (stored != null) {
                    var mapInfo = JSON.parse(stored);
                }
                console.log(mapInfo);
                if (mapInfo) {
                    latt = parseFloat(mapInfo[0].latt);
                    long = parseFloat(mapInfo[0].long);
                } else { //#defaults to
                    latt = LATITUDE;
                    long = LONGITUDE;
                }
            }
            var mapDiv = document.getElementById('map_canvas');
            var map = new google.maps.Map(mapDiv, {
                center: {lat: latt, lng: long},
//                center: {lat: 1.266206, lng: 103.8213044},
                scrollwheel: false,
                zoom: 15 //9
            });
            var marker = new google.maps.Marker({
                position: {lat: latt, lng: long},
                map: map,
            });
        }

        function loadScript() {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCxK6I8XJnlizNPV3nqUCc9veXmLQ3bcXI&callback=window.app.Module.Map.initialize';

            document.body.appendChild(script);
        }
        /**
         * Initialize Module
         */
        function init() {
            initMap();
            initUI();
        } // init()

        return {
            init: init,
            initialize: initialize
        };
    }();
}(window.app = window.app || {}, jQuery));
// Init Map Module    
window.app.Module.Map.init();