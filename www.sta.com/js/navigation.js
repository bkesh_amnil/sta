var globalNavigationInterval;
(function(app, $) {
    "use strict";

    app.Module.Navigation = function() {

        /***
         * Resets all Events
         */
        function resetEvent() {
            $('.about-nav').off("pointerclick");
            $('.about-nav').suppressClick();
        } // resetEvent()

        /***
         * Binds all events.
         */
        function initEvent() {
            /*
             * Clicking anywhere in document should hide sub navigation under about nav
             */
            /*listenForPointerClick($(document));
            $(document).on("pointerclick", function(e) {
                if (!$(e.target).hasClass("nav-link")) {
                    $(".sub-nav").removeClass("active");
                }
            });*/
        } // initEvent()

        /***
         * Adds Event on document to Close How It Works and The Este bartin commitment menu on clicking any part of document
         ***/
        function addEventToCloseSubMenu() {
            listenForPointerClick($(document));
            $(document).off("pointerclick");
            $(document).on("pointerclick", function(e) {
                if (!$(e.target).hasClass("nav-link")) {
                    $(document).off("pointerdown");
                    $(document).off('pointerclick');
                    $(".sub-nav").removeClass("active");
                }
            });
        }

        function init() {
            resetEvent();
            initEvent();
        } // init()

        return {
            init: init,
            addEventToCloseSubMenu: addEventToCloseSubMenu
        }
    }();
}(window.app = window.app || {}, jQuery));