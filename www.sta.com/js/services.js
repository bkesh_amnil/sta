/*** * Module - app.Module.Services 
 * * * Provides behaviour for services.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Services.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Services = function() {
            var config = {};

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                $(window.app).off("ready");
                $(window.app).on("ready", function() {
                    console.log('service is ready');
                    $(window.app).off("ready");
                });
            } // initEvent()

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    //alert("test");//
                }
            } // initUI()

            /**
             * Initialize Module
             */
            function init($config) {
                config = config || {};
                config = $.extend({}, config, $config);
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init Services Module    
    window.app.Module.Services.init();
});