/*** * Module - app.Module.Sidebar 
 * * * Provides behaviour for sidebar.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Sidebar.init(options); 
 /***************************** *      Execute on load      * *****************************/
(function(app, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    app.Module.Sidebar = function() {
        var advertisements = '';
        var eventPageName = 'event';
        var events = '';
        var owl1 = '';
        var searchedText = '';
        var companyPerPage = 5;
        var perPage = 3;

        /**
         * Removes all events in this module: click, input, focus, pointerdown
         */
        function resetEvent() {
        } // resetEvent()

        /**
         * Adds all necessary events: input, focus, pointerdown
         */
        function initEvent() {
            $(window.app).off("readySidebar");
            $(window.app).on("readySidebar", function() {
                initAdvertisements();
                console.log('sidebar loaded from here initevent');
                initSidebarEvents(0);
                initSidebarEventscarousel();
                $(window.app).off("readySidebar");
            });

            // Loading more upcoming events
            $("#btn-load-more-events").click(function(e) {
                e.preventDefault();
                var currentPage = $(this).attr('data-current');
                initSidebarEvents(currentPage);
                console.log('more loaded byb');
            });
            
            $('#form-search').off('submit');
            $('#form-search').on('submit', function(e) {
                e.preventDefault();
                searchedText = $.trim($('#search-field').val());
                var target_page = 'members';
                $.when(App.Module.PageSwitcher.switchPage('members', "?search=" + searchedText)).done(function() {
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page + '?search=' + searchedText);
                    $("#main-nav .nav-link").removeClass("active");
                    target_page = target_page.split('#')[0];
                    $('.nav-link[data-for="' + target_page + '"]').addClass("active");
                });
                $(window.app).trigger("search", {
                    text: 'jest'
                });
            });
        } // initEvent()

        /**
         * Initializes UI states
         */
        function initUI() {
            if (window.app.ready) {
                console.log('sidebar UI is ready');
                initAdvertisements();
                console.log('ui bata', 0);
                initSidebarEvents(0);
                initSidebarEventscarousel();
            }
        } // initUI()

        /**
         * init Advertisements
         ***/
        function initAdvertisements() {
            advertisements = ((localDB.STAadvertisements).filter(function(e) {
                return e["DisplayPriority"] > 0;
            })).sortBy("DisplayPriority", false);

            $('#aside-advertisements').html('');
            $.each(advertisements, function(i, v) {
                if (i == 2) {
                    return false;
                }
                var img_URL = '';
                if (v.hasImage == 1) {
                    img_URL = IMAGES_URL + '/advertisements/' + v.Image;
                } else {
                    img_URL = IMAGES_URL + "/default-sta-image.svg";
                }
                //html contents
                var content = '<div id="ad-commercial-outdoor" class="ad-item">' +
                        '<img src="' + img_URL + '">' +
                        '<div class="ad-title">' +
                        '<span>' + v.Name + '</span>' +
                        '<span>' + v.Title + ' ></span>' +
                        '</div>' +
                        '<a href="' + v.URL + '" target="_blank"></a>' +
                        '</div>';
                //append to the page
                $('#aside-advertisements').append(content);
            });

        }


        /**
         * init events for sidebar
         */
        function initSidebarEvents(thisCurrentPage) {
            $('#btn-load-more-events').addClass('show');
            var currentPage = parseInt(thisCurrentPage);
            var startIndex = (currentPage) * perPage;
            var endIndex = ((currentPage + 1) * perPage) - 1;
            var allEvents = ((localDB.STAevents).filter(function(e, i) {
                return e["DisplayPriority"] > 0;
            })).sortBy("EventTimestamp", true);
            events = (allEvents.filter(function(e, i) {
                return e["DisplayPriority"] > 0 && i >= startIndex && i <= endIndex;
                ;
            }));

            console.log('sidebar ko kura 1= ', allEvents);
            $('#list-upcoming-events').find('.upcoming-event-item').removeClass('new-item');
            var content = '';
            $.each(events, function(i, v) {
                var dataFor = eventPageName + '#' + slugify(v.EventName);
                var eventURL = SERVER_URL + '/' + eventPageName + '#' + slugify(v.EventName);
                //html contents
                content += '<div class="upcoming-event-item new-item">' +
                        '<div class="upcoming-event-date">' +
                        '<span class="upcoming-event-month">' + v.EventMonth + '</span>' +
                        '<span class="upcoming-event-day">' + v.EventDay + '</span>' +
                        '</div>' +
                        '<div class="upcoming-event-detail">' +
                        '<span>' + v.EventName + '</span>' +
                        '<span>' + v.EventFrom + ' to ' + v.EventTo + '</span>' +
                        '<a class="nav-link" data-for="' + dataFor + '" href="' + eventURL + '">' +
                        'View Event' +
                        '</a>' +
                        '</div>' +
                        '</div>';
                //append to the page
            });
            $('#list-upcoming-events').append(content);
            $('#list-upcoming-events').find('.new-item').hide().slideDown();
            $('#btn-load-more-events').attr('data-current', currentPage + 1);
            var totalEvents = allEvents.length;
            var displayedEvents = $('#list-upcoming-events').find('.upcoming-event-item').length;
            if (displayedEvents >= totalEvents) {
                $('#btn-load-more-events').removeClass('show');
                $('#btn-load-more-events').css('display', 'none');
            }
        }

        /**
         * init events for sidebar
         */
        function initSidebarEventscarousel() {
            var events = ((localDB.STAevents).filter(function(e, i) {
                return e["DisplayPriority"] > 0;
            })).sortBy("EventTimestamp", true);
            $('#owl-demo').html('');
            $.each(events, function(i, v) {
                var dataFor = eventPageName + '#' + slugify(v.EventName);
                var eventURL = SERVER_URL + '/' + eventPageName + '#' + slugify(v.EventName);
                //html contents
                var content = '<div class="upcoming-event-item">' +
                        '<div class="upcoming-event-date">' +
                        '<span class="upcoming-event-month">' + v.EventMonth + '</span>' +
                        '<span class="upcoming-event-day">' + v.EventDay + '</span>' +
                        '</div>' +
                        '<div class="upcoming-event-detail">' +
                        '<span>' + v.EventName + '</span>' +
                        '<span>' + v.EventFrom + ' to ' + v.EventTo + '</span>' +
                        '<a class="nav-link" data-for="' + dataFor + '" href="' + eventURL + '">' +
                        'View Event' +
                        '</a>' +
                        '</div>' +
                        '</div>';
                //append to the page
                $('#owl-demo').append(content);
            });
            
            

            //Owl Carousel 2
            $(".owl-carousel").owlCarousel({
                items: 2,
                autoWidth: true,
                margin: 20,
                nav: true
            });
        }




        /**
         * Initialize Module
         */
        function init() {
            initEvent();
            initUI();
        } // init()

        return {
            init: init
        };
    }();
}(window.app = window.app || {}, jQuery));
// Init Sidebar Module    
window.app.Module.Sidebar.init();