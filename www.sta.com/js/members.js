/*** * Module - app.Module.Members 
 * * * Provides behaviour for members.php page
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Members.init(options); 
 /***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/sidebar.js"])).done(function() {
    (function(app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Members = function() {
            var companies = '';
            var searchedText = '';
            var companyPerPage = 5;
            var allCompanies = '';

            /**
             * Removes all events in this module: click, input, focus, pointerdown
             */
            function resetEvent() {
            } // resetEvent()

            /**
             * Adds all necessary events: input, focus, pointerdown
             */
            function initEvent() {
                //# search ko lagi idea for on off thins with args sending.
                $(window.app).off("search");
                $(window.app).on("search", function(e, args) {
                    console.log('bkesh is here', document.location.href);
                });
                $(window.app).off("ready");
                $(window.app).on("ready", function() {
                    searchedText = getParameterByName("search");
                    console.log('searchedText', searchedText);
                    if (searchedText) {
                        $('#member-search-field').val(searchedText);
                        $('html, body').animate({
                            scrollTop: $("#members-listing").offset().top
                        }, 1000);
                    }
                    loadCompanies(0);
                    $('#member-search-btn').prop('disabled', true);
                    console.log('members is ready');
                    $(window.app).off("ready");
                });

                $('#member-search-field').keyup(function() {
                    if ($(this).val() != '') {
                        $('#member-search-btn').prop('disabled', false);
                    } else {
                        $('#member-search-btn').prop('disabled', true);
                        $('#member-search-field').focus();
                    }
                });

                $('#member-search-form').off('submit');
                $('#member-search-form').on('submit', function(e) {
                    e.preventDefault();
                    searchedText = $.trim($('#member-search-field').val());
                    $('#load-more-members-btn').attr('data-current', 0);
                    loadCompanies(0);
                });

                // Loading more companies
                $("#load-more-members-btn").click(function(e) {
                    e.preventDefault();
                    var currentPage = $(this).attr('data-current');
                    loadCompanies(currentPage);
                    console.log('more loaded byb');
                });

                // Scroll to Top
                $("#member-back-to-the-top").click(function() {
                    $('html, body').animate({
                        scrollTop: $("#members-listing").offset().top
                    }, 1000);
                });

            } // initEvent()

            function loadCompanies(thisCurrentPage) {
                var currentPage = parseInt(thisCurrentPage);
                var startIndex = (currentPage) * companyPerPage;
                var endIndex = ((currentPage + 1) * companyPerPage) - 1;
                console.log(currentPage, startIndex, endIndex);
                if (searchedText != '') {
                    var regex = new RegExp(searchedText, "i");
                    allCompanies = (localDB.STAcompanies).filter(function(el, i) {
                        return (regex.test(el['CompanyNameEnglish']) || regex.test(el['ProductsServicesEnglish']));
                    }).sortBy("CompanyNameEnglish", true);
                } else {
                    allCompanies = (localDB.STAcompanies).filter(function(el, i) {
                        return el["DisplayPriority"] > 0;
                    }).sortBy("CompanyNameEnglish", true);
                }
                companies = allCompanies.filter(function(el, i) {
                    return i <= endIndex;
                });
                var totalCompanies = allCompanies.length;
                $('#member-result-container').html('');
                if (totalCompanies == 0) {
                    $('#member-search-result-number').html('No search result found for the keyword: <span>' + searchedText + '</span>').removeClass('no-display');
                    $('#load-more-members-btn').addClass('no-display');
                    $('#member-back-to-the-top').addClass('no-display');
                } else {
                    $('#member-search-result-number').html(' Total Members Found: <span>' + totalCompanies + '</span>').removeClass('no-display');

                    ;
                    //# condition for showing load more button
                    if (totalCompanies > companyPerPage) {
                        $('#load-more-members-btn').removeClass('no-display');
                        $('#member-back-to-the-top').removeClass('no-display');
                    } else {
                        $('#load-more-members-btn').addClass('no-display');
                        $('#member-back-to-the-top').addClass('no-display');
                    }
                    console.log(companies);
                    $.each(companies, function(i, v) {
                        var sn = parseInt(i) + 1;
                        //html contents
                        var content = '<li class="member-search-result-items">' +
                                '<address class="member-contact">' +
                                '<h5>' + sn + '. ' + v.CompanyNameEnglish + '</h5>';
                        if (v.CompanyNameChinese != '' && v.CompanyNameChinese != 'undefined') {
                            content += '<h5>' + v.CompanyNameChinese + '</h5>';
                        }
                        content += '<span>' + v.Address + '</span>';
                        if (v.Phone != '') {
                            content += '<span>Tel: ' + v.Phone + '</span>';
                        }
                        if (v.Fax != '') {
                            content += '<span>Fax: ' + v.Fax + '</span>';
                        }
                        if (v.Email != '') {
                            content += '<span>Email: ' + v.Email + '</span>';
                        }
                        if (v.Website != '') {
                            content += '<span>Website: ' + v.Website + '</span>';
                        }
                        if (v.Registration != '') {
                            content += '<span>Co\'s Reg No.: ' + v.Registration + '</span>';
                        }
                        content += '</address>';
                        var representatives = v.Representatives;
                        if (representatives.length != 0) {
                            content += '<div class="member-representatives">' +
                                    '<h5>Representatives:</h5>';
                            $.each(representatives, function(i, vr) {
                                content += '<span>' + vr.NameEnglish + ' (' + vr.Phone + ')</span>';
                            });
                            content += '</div>';
                        }
                        content += '<div class"member-products-services">' +
                                '<h5>Products / Services:</h5>' +
                                '<p>' + v.ProductsServicesEnglish + '</p>' +
                                '</div>';
                        var associates = v.Associates;
                        if (associates.length != 0) {
                        content += '<div class="member-associate-companies">' +
                                '<h5>Associate Companies:</h5>';
                            $.each(associates, function(i, va) {
                                content += '<span>' + va.AssociateCompanyNameEnglish + ' </span>';
                            });
                            content += '</div>';
                        }
                        content += '</li>';
                        //append to the page
                        $('#member-result-container').append(content);
                    });
                    $('#load-more-members-btn').attr('data-current', currentPage + 1);
                    var displayedCompanies = $('#member-result-container').find('li').length;
                    if (displayedCompanies >= totalCompanies) {
                        $('#load-more-members-btn').addClass('no-display');
                    }
                }
            }

            /**
             * Initializes UI states
             */
            function initUI() {
                if (window.app.ready) {
                    searchedText = getParameterByName("search");
                    console.log('searchedText', searchedText);
                    if (searchedText) {
                        $('#member-search-field').val(searchedText);
                        $('html, body').animate({
                            scrollTop: $("#members-listing").offset().top
                        }, 1000);
                    }
                    loadCompanies(0);
                    console.log('members is ready from ui');
                }
            } // initUI()

            /**
             * Initialize Module
             */
            function init() {
                initEvent();
                initUI();
            } // init()

            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
// Init Members Module    
    window.app.Module.Members.init();
});