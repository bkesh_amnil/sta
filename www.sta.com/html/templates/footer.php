</div>
<footer>
    <div id="footer-container" class="container">
        <img src="img/icons/logo-sta-footer.svg" id="logo-sta-footer" alt="logo-sta-footer"/>
        <div class="footer-find-us">
            <img src="img/icons/icon-footer-find-us.svg" alt="icon-footer-find-us"/>
            <h5>Find Us</h5>
            <ul>
                <li>341B Beach Road</li>
                <li>Singapore 199567</li>
            </ul>
        </div>
        <div class="footer-contact-us">
            <img src="img/icons/icon-footer-contact-us.svg" alt="icon-footer-contact-us"/>
            <h5>Contact Us</h5>
            <ul>
                <li>secretariat@singaporetimber.com</li>
                <li>+65 6298 0709</li>
                <li>Mondays to Saturdays</li>
                <li>10am – 6pm</li>
                <li>Sundays & Public Holidays</li>
                <li>10am – 2pm</li>
            </ul>
        </div>
        <div class="footer-social-links">
            <a href="#" target="_blank">
                <img src="img/icons/icon-facebook-footer.svg" id="icon-facebook-footer" alt="icon-facebook-footer"/>
            </a>
            <a href="#" target="_blank">
                <img src="img/icons/icon-instagram-footer.svg" id="icon-instagram-footer" alt="icon-instagram-footer"/>
            </a>
        </div>
    </div>
    <div class="footer-copyright-section">
        <div id="footer-copyright-section-container" class="container">
            <span id="footer-copyright-text">
                <span>Copyright &copy; 2017</span>
                <span>Singapore Timber Association.</span>
                <span>All rights reserved.</span>
                <span>Site design by  <a href="http://techplusart.com/" target="_blank">Tech+Art</a>.</span>
            </span>
            <span id="terms-and-privacy-menu">
                <!-- <a href="terms-of-use.html" id="terms-of-use">Terms of Use</a> -->
                <a class="nav-link" data-for="privacy-policy" href="<?= SERVER_PATH ?>/privacy-policy" id="privacy-policy">Privacy Policy</a>
            </span>
        </div>
    </div>
</footer>
<!--vendor includes-->
<script type="text/javascript" src="js/vendor/jquery.js"></script>
<script type="text/javascript" src="js/vendor/jquery-easings.1.3.min.js"></script>
<script type="text/javascript" src="js/vendor/modernizr-3.3.1.min.js "></script>
<script type="text/javascript" src="js/vendor/preloader.js"></script>
<script type="text/javascript" src="js/vendor/hand-1.3.8.js"></script><!-- unify pointer events -->
<script type="text/javascript" src="js/vendor/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="js/vendor/slick.min.js"></script>
<!-- <script type="text/javascript" src="js/vendor/owl.carousel.js"></script> -->

<!-- Owl Carousel 2 -->
<script type="text/javascript" src="js/vendor/owl-carousel2/owl.carousel.min.js"></script>

<!--site includes-->
<script type="text/javascript" src="js/site/main.js"></script>

<!-- include helpers -->
<script src="js/helpers/pointerclick.js"></script>
<script src="js/helpers/array.js"></script>
<script src="js/helpers/string.js"></script>
<script src="js/helpers/date.js"></script>
<script src="js/helpers/form.js"></script>
<script src="js/helpers/conversion.js"></script>
<script src="js/helpers/amd.js"></script>

<!-- Include common items / modules -->
<script src="js/common/config.js"></script><!-- Config should always be included first -->
<script src="js/common/db.js"></script>
<script src="js/common/window.js"></script>
<script src="js/common/ajax-public-api.js"></script>
<script src="js/common/ajax-customer-api.js"></script>
<script type="text/javascript" src="js/common/ajaxpageswitcher.js"></script>
<script src="js/navigation.js"></script>

<!-- Run browser checks then inits main app -->
<script src="js/navigation.js"></script>
<script src="js/common/checks.js"></script>
<script src="js/common/init.js"></script>
<script src="js/common/main.js"></script>
<?php
foreach ($this->js as $js) {
    if (startsWith($js, 'http://') || startsWith($js, 'https://') || startsWith($js, '//')) {
        echo '<script src="' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
    } else {
        echo '<script src="' . SERVER_PATH . '/js/' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
    }
}
?>
</body>
</html>