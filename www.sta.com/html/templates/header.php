<section id="banner">
    <button type="button" id="mobile-menu-btn">
        <img id="mobile-menu-btn-open" src="img/icons/mobile-menu-btn.svg" alt="mobile-menu-btn" />
        <img id="mobile-menu-btn-close" src="img/icons/icon-mobile-menu-btn-close.svg" alt="icon-mobile-menu-btn-close" />
    </button>
    <div id="overlay-shadow">
        <nav id="mobile-nav" class="mobile-nav-bg-color">
            <a class="nav-link <?php if($this->menu_title=='Event') echo 'active'; ?>" href="<?= SERVER_PATH ?>/event"  data-for="event">Events</a>
            <a class="nav-link <?php if($this->menu_title=='Services') echo 'active'; ?>" href="<?= SERVER_PATH ?>/services"  data-for="services">Services</a>
            <a class="nav-link <?php if($this->menu_title=='Library') echo 'active'; ?>" href="<?= SERVER_PATH ?>/library"  data-for="library">Library</a>
            <a class="nav-link <?php if($this->menu_title=='Biz-match') echo 'active'; ?>" href="<?= SERVER_PATH ?>/biz-match" data-for="biz-match">Biz Match</a>
            <a class="nav-link <?php if($this->menu_title=='Members') echo 'active'; ?>" href="<?= SERVER_PATH ?>/members" data-for="members">Members</a>
            <a class="nav-link <?php if($this->menu_title=='About') echo 'active'; ?>" href="<?= SERVER_PATH ?>/about" data-for="about">About</a>
            <a class="nav-link <?php if($this->menu_title=='Contact') echo 'active'; ?>" href="<?= SERVER_PATH ?>/contact" data-for="contact">Contact</a>
            <a href="<?= SERVER_PATH ?>/event">
                <img id="logo-mobile-nav" src="img/icons/logo-mobile-nav.svg" alt="logo-mobile-nav" />
            </a>
        </nav>
    </div>
    <div class="container">
        <header>
            <a id="logo-sta-header" href="<?= SERVER_PATH ?>/event"><img src="img/icons/logo-sta-header-shadow.png"></a>
            <a id="logo-sta-header-320p" href="<?= SERVER_PATH ?>/event"><img src="img/icons/logo-sta-header-320p.svg"></a>

            <form id="form-search">
                <input id="search-field" type="search" placeholder="Search Members Directory..." />
                <input id="search-button" type="submit" value=" " />
            </form>
        </header>

        <h1 id="banner-title">
            <span>THE NEW LOOK OF</span>
            SINGAPORE TIMBER ASSOCIATION
        </h1>

        <nav id="main-nav">
            <a class="nav-link nav-events <?php if($this->menu_title=='Event') echo 'active'; ?>" href="<?= SERVER_PATH ?>/event"  data-for="event">Events</a><!--
            --><a class="nav-link nav-services <?php if($this->menu_title=='Services') echo 'active'; ?>" href="<?= SERVER_PATH ?>/services" data-for="services">Services</a><!--
            --><a class="nav-link nav-library <?php if($this->menu_title=='Library') echo 'active'; ?>" href="<?= SERVER_PATH ?>/library" data-for="library">Library</a><!--
            --><a class="nav-link nav-biz-match <?php if($this->menu_title=='Biz-match') echo 'active'; ?>" href="<?= SERVER_PATH ?>/biz-match" data-for="biz-match">Biz Match</a><!--
            --><a class="nav-link nav-members <?php if($this->menu_title=='Members') echo 'active'; ?> main-page-members" href="<?= SERVER_PATH ?>/members" data-for="members">Members</a><!--
            --><a class="nav-link nav-about <?php if($this->menu_title=='About') echo 'active'; ?>" href="<?= SERVER_PATH ?>/about" data-for="about">About</a><!--
            --><a class="nav-link nav-contact <?php if($this->menu_title=='Contact') echo 'active'; ?>" href="<?= SERVER_PATH ?>/contact" data-for="contact">Contact</a>
        </nav>

        <span class="mobile-nav-title"><?php echo $this->page_title; ?></span>
    </div>            
</section>

<div class="main-content" id="content" data-page="<?php echo $this->name; ?>">
