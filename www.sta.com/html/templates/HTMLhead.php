<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="description" content="Singapore Timber Association">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0,  target-densitydpi=device-dpi">
        <title><?php echo $this->title; ?></title>
        <meta name="csrf-token" content="<?php echo $_SESSION['CSRF_TOKEN']; ?>">

        <link href="css/common/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/common/main.css" rel="stylesheet" type="text/css" />

        <link href="css/side-static-section.css" rel="stylesheet" type="text/css" />

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <link rel="icon" href="favicon.ico" />


        <!-- Slider css -->
        <!-- <link href="js/vendor/owl.carousel.css" rel="stylesheet" type="text/css" /> -->

        <!-- Owl Carousel 2 -->
        <link rel="stylesheet" href="js/vendor/owl-carousel2/owl.carousel.min.css">
        <link rel="stylesheet" href="js/vendor/owl-carousel2/owl.theme.default.min.css">

        
        <?php
        foreach ($this->meta as $key => $value) {
            echo '<meta name="' . $key . '" content="' . $value . '">' . "\r\n";
        }
        ?>
        <?php
        foreach ($this->og_meta as $key => $value) {
            echo '<meta property="og:' . $key . '" content="' . $value . '">' . "\r\n";
        }
        ?>
        <?php
        foreach ($this->css as $css) {
            echo '<link rel="stylesheet" href="' . SERVER_PATH . '/css/' . $css . '" data-page="' . $this->name . '" />' . "\r\n";
        }
        ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- <img class="pixel" src="../../img/pixel-perfect-pics/event-1366p.jpg" /> -->