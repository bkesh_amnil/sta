<aside id="aside-section">

    <!-- Upcoming Event Slider for 1366p Screen -->
    <div id="upcoming-events">
        <h3>Upcoming Events</h3>
        
        <div id="upcoming-event-list-view" class="upcoming-events-list">
            <div class="static-upcoming-events" id="list-upcoming-events">

            </div>
        </div>

        <!-- Slider for 1024p screen -->

        <div id="owl-demo" class="owl-carousel">

        </div>
          
        <!--<div id="upcoming-event-slider-view" class="upcoming-events-list">
            
            <div class="owl-carousel" id="list-upcoming-events-slider">
            </div>
        </div>-->

        <a href="#" id="btn-load-more-events" data-current="0" class="custom-btn">Load More Events</a>
    </div>

    <!-- Aside Advertisement Section -->
    <div id="aside-advertisements">

    </div>
</aside>
