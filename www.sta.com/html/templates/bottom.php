<section id="content-services">
    <div id="content-services-wrapper" class="container">
        <img src="img/service-man.png" id="service-man">

        <p>
            Singapore Timber Association provides the following public services for free.
            Do <a class="nav-link" href="<?= SERVER_PATH ?>/contact" data-for="contact">contact us</a> for prompt correspondence on how we can help you. :)
        </p>

        <div id="content-services-list">
            <span class="content-services-items">
                <a href="<?= SERVER_PATH ?>/services" data-for="services" class="nav-link content-services-item-title timber-grading-color">
                    <img src="img/icons/icon-timber-grading.svg">
                    <span class="content-services-text">TIMBER GRADING</span>
                </a>
            </span>
            <span class="content-services-items">
                <a href="<?= SERVER_PATH ?>/services" data-for="services" class="nav-link content-services-item-title second-opinions-color">
                    <img src="img/icons/icon-second-opinion.svg">
                    <span class="content-services-text">SECOND OPINIONS</span>
                </a>
            </span>
            <span class="content-services-items">
                <a href="<?= SERVER_PATH ?>/services" data-for="services" class="nav-link content-services-item-title course-arrangement-color">
                    <img src="img/icons/icon-course-arrangement.svg">
                    <span class="content-services-text">COURSE ARRANGEMENT</span>
                </a>
            </span>
        </div>
    </div>
</section>