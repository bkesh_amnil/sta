<div class="container">

    <section id="library" class="left-content">
        <div class="page-content-heading">
            <img class="icon-heading-bullet-default svg" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default" />
            <h3>Library</h3>
        </div>

        <div id="services-page-timber-grading" class="service-item">

            <div class="service-item-details">

                <p>
                    Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus. Nulla mauris augue,  rutrum nec accumsan sed, eleifend quis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam purus ligula, sagittis a scelerisque at, luctus non augue. Proin a sapien ut quam mattis pharetra eget rutrum felis.  Praesent consectetur urna fermentum dui egestas, sit amet commodo nibh sodales. Etiam pharetra purus. Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus.
                </p>
                
                <p></p>

                <p>
                    Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus. Nulla mauris augue,  rutrum nec accumsan sed, eleifend quis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam purus ligula, sagittis a scelerisque at, luctus non augue. Proin a sapien ut quam mattis pharetra eget rutrum felis.  Praesent consectetur urna fermentum dui egestas, sit amet commodo nibh sodales. Etiam pharetra purus. Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus.
                </p>
            </div>
        </div>

    </section>
    <?php include('templates/sidebar.php'); ?>

</div>
<?php include('templates/bottom.php'); ?>