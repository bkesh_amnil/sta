<div class="container">

    <section id="biz-match" class="left-content">
        <h3>
            <img class="main-content-bullet-1" src="img/icons/icon-dark-yellow-bullet.svg" alt="" /><span class="title">
                Business Matching
            </span>
        </h3>

        <div class="biz-match-intro">
            <figure id="biz-match-img-container" class="image-banner">
                <img src="img/biz-match-bg.jpg">
            </figure>
            <p>
                Singapore Timber Association business matching service allows you to make prior contacts and pre-fix 
                appointments with Singapore companies. Simply fill in the form below indicating the type of products you are 
                interested in and we will forward the relevant contacts to you.
            </p>
        </div>

        <form name="bizMatch">
            <div class="event-contact-form half-input">
                <div class="form-input-wrapper">
                    <input type="text" placeholder="Contact Name*" name="contact_name">
                    <span class="error contact-name">Name is Required</span>
                </div>

                <div class="form-input-wrapper">
                    <input type="text" placeholder="Company Name*" name="company_name">
                    <span class="error company-name">Company Name is Required</span>
                </div>

                <div class="form-input-wrapper">
                    <input type="email" placeholder="Email Address*" name="email">
                    <span class="error email">Email Address is Required</span>
                </div>

                <div class="form-input-wrapper">
                    <input type="text" placeholder="Company Address*" name="company_address">
                    <span class="error company-address">Company Address is Required</span>
                </div>

                <div class="form-input-wrapper">
                    <input type="text" placeholder="Contact Number*" name="mobile">
                    <span class="error mobile">Contact Number is Required</span>
                </div>
                <div class="form-input-wrapper">
                    <input type="text" placeholder="Designation" name="designation">
                </div>

                <div class="input-holder pull-right">
                    <label for="" class="title-holder">
                        Type of Business*
                    </label>
                    <div class="checkbox-wrapper">
                        <span class="checkbox-group">
                            <input type="checkbox" name="business_type[]" id="check1" checked value="Alpha advertising agent" />
                            <label for="check1">Alpha Advertising Agent</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" name="business_type[]" id="check2" value="Bravo interior design">
                            <label for="check2">Bravo Interior Design</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" name="business_type[]" id="check3" value="Charlie home staging">
                            <label for="check3">Charlie Home Staging</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" name="business_type[]" id="check4" value="Delta distributor">
                            <label for="check4">Delta Distributor</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" name="business_type[]" id="check5" value="Echo event management">
                            <label for="check5">echo event management</label>
                        </span>
                    </div>
                </div>
                <div class="input-holder">
                    <label for="" class="title-holder">
                        Interest in Product Categories*
                    </label>
                    <div class="checkbox-wrapper">
                        <span class="checkbox-group">
                            <input type="checkbox" id="check11" name="product_interest[]" checked value="Outdoor design" />
                            <label for="check11">outdoor design</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" id="check22" name="product_interest[]" value="Home interior">
                            <label for="check22">home interior</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" id="check33" name="product_interest[]" value="Property developer">
                            <label for="check33">property developer</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" id="check44" name="product_interest[]" value="Garden landscaping">
                            <label for="check44">garden landscaping</label>
                        </span>
                        <span class="checkbox-group">
                            <input type="checkbox" id="check55" name="product_interest[]" value="Swimming pool decking">
                            <label for="check55">swimming pool decking</label>
                        </span>
                    </div>
                </div>
                <div class="input-holder full-width">
                    <label for="" class="title-holder">
                        What countries do you currently source from?
                    </label>
                    <textarea placeholder="Please write here." name="source_country"></textarea>
                </div>
                <div class="input-holder full-width">
                    <label for="" class="title-holder">
                        What products are you interested in business matching?
                    </label>
                    <textarea name="products" placeholder="Please provide specific requirements so selective suppliers can be arranged for you."></textarea>
                </div>
                <div class="input-holder full-width">
                    <label for="" class="title-holder">
                        More details about your company?
                    </label>
                    <textarea name="company_details" placeholder="Please write here."></textarea>
                </div>
                <div class="input-holder full-width">
                    <label for="" class="title-holder">
                        Disclaimer
                    </label>
                    <p>Singapore Timber Association reserves the right to modify these terms under business matching 
                        page at any time. We aim to keep the information on the business matching site as current, 
                        accurate and complete as possible. However, SFIC, content authors, any of their affiliates, partners, 
                        directors, employees or other representatives will not be liable for any consequence relating 
                        directly or indirectly to any action or inaction you take based on the information, services or other 
                        material on this site. Singapore Timber Association takes no responsibility for the contents of 
                        linked websites and links should not be taken as endorsement of any kind.</p>
                </div>

            </div>
            <button class="custom-btn form-btn">Submit</button>
        </form>
        <div class="form-success-msg">
            <span class="form-success-msg-title"></span>
            <span class="form-success-msg-text"></span>
            <button class="form-success-msg-btn" type="submit">OK</button>
        </div>
    </section>
    <?php include('templates/sidebar.php'); ?>
</div>
<?php include('templates/bottom.php'); ?>