<div class="container">

    <section id="privacy" class="left-content">
        <div class="page-content-heading svg">
            <img class="icon-heading-bullet-default" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default">
            <h3>Privacy Policy</h3>
        </div>

        <div id="privacy-page-timber-grading" class="privacy-item">
            <ol class="ol-decimal">
                <li>
                    This Privacy Policy set outs out how Singapore Timber Association Pte Ltd receives, uses and protects any Personal Data that you give us when you use this website (“Site”).
                </li>
                <li>
                    Should we ask you to provide your name, passport or other identification numbers, mailing addresses, email address, network data, telephone numbers, shareholding details, security holding or plan participation details and balances, bank account details, tax file numbers, date of birth, employment details and such other information regarding your identity (“Personal Data”), when using this Site, such information is used only in accordance with this Privacy Statement and the Singapore Personal Data Protection Act, 2012 (“the Act”) for the provision of or in relation to Services provided by Singapore Timber Association Pte Ltd.
                </li>
                <li>
                    If you provide us with any Personal Data relating to a third party, by submitting such information to us, you represent to us that you have obtained the consent of the third party to provide us with their Personal Data for the respective purposes. You should ensure that all Personal Data submitted to us is complete, accurate, true and correct. Failure on your part to do so may result in our inability to provide you with the Services you have requested.
                </li>
                <li>
                    Under the Act, Singapore Timber Association Pte Ltd is primarily defined as a Data Intermediary. However, we are also a Data Collector of Personal Data collected through the Site. By accessing to, interfacing with this Site, submitting Personal Data to us and/or using our Services, you agree and consent to be bound by the terms of this Privacy Statement and that your Personal Data may be disclosed for processing to any of the countries where we operate or interface with insofar as may be legally permitted. This Privacy Statement supplements but does not supersede nor replace any other consents you may have previously provided to us in respect of your Personal Data, and your consents herein are additional to any rights which we may have at law to collect, use and/or disclose your Personal Data.
                </li>
                <li>
                    Under the Act, Singapore Timber Association Pte Ltd is primarily defined as a Data Intermediary. However, we are also a Data Collector of Personal Data collected through the Site. By accessing to, interfacing with this Site, submitting Personal Data to us and/or using our Services, you agree and consent to be bound by the terms of this Privacy Statement and that your Personal Data may be disclosed for processing to any of the countries where we operate or interface with insofar as may be legally permitted. This Privacy Statement supplements but does not supersede nor replace any other consents you may have previously provided to us in respect of your Personal Data, and your consents herein are additional to any rights which we may have at law to collect, use and/or disclose your Personal Data.
                </li>
                <li>
                    When you browse our Site, you generally do so anonymously save for the cookies referred to below in paragraph 7. We do not, at our Site, automatically collect Personal Data unless you provide such information.
                </li>
                <li>
                    We are committed to ensuring that your Personal Data, whether stored electronically or in paper form, is secure. In order to prevent against risks of loss, destruction, duplication, use, modification, unauthorised access or disclosure, we have put in place reasonably suitable physical, electronic and managerial procedures to safeguard and secure the Personal Data we collect online. These include, but are not limited to:-
                    <ol class="ol-lower-alpha">
                        <li>
                            restricting access to Personal Data to those who have a need to know (i.e. authorised and designated officers of Singapore Timber Association Pte Ltd). Those individuals who need to have access to the Personal Data are required to maintain the confidentiality of such information;
                        </li>
                        <li>
                            having regular Personal Data housekeeping in terms of retention periods for and classification of Personal Data for legal and business requirements; and
                        </li>
                        <li>
                            removing and destroying documents and/or purging databases containing your Personal Data, or removing the means by which your Personal Data can be associated with you as soon as it is reasonable to assume that the purpose for which the Personal Data was provided to us is no longer being served by such retention, and retention is no longer necessary for legal or business purposes.
                        </li>
                    </ol>
                </li>
                <li>
                    We use cookies and web logs on this Site to improve its functionality. Cookies are small text files that our website may place on your computer and collect information such as your internal protocol address, your computer operating system, browser type and traffic patterns. You may adjust your internal browser to disable cookies or to inform you when one is being used. If you choose to disable cookies, you may be unable to access certain areas of this Site.
                </li>
                <li>
                    Please contact Singapore Timber Association Pte Ltd Personal Data Protection Officer (“PDPO”) at secretariat@singaporetimber.com for the purposes of:-
                    <ol class="ol-lower-alpha">
                        <li>
                            withdrawal of any consent or deemed consent given by you;
                        </li>
                        <li>
                            right of access to correct and/or update Personal Data provided by you; or
                        </li>
                        <li>
                            any questions or feedback relating to our administration of your Personal Data or our Privacy Statement.
                        </li>
                    </ol>
                    The PDPO will try to resolve the aforementioned matters within thirty (30) business days. If we are unable to resolve the matter within that time, we will contact you to let you know how long it will take to resolve the complaint or, if more appropriate, suggest the matter be referred to the Personal Data Protection Commissioner. Please note that if your Personal Data was provided to us by a third party, you should contact that organisation or individual to make such queries, complaints, and access and correction requests to us on your behalf.
                </li>

                <li>
                    We may from time to time update this Privacy Statement to ensure that this Privacy Statement is consistent with our future developments, industry trends and/or any changes in legal or regulatory requirements. You agree to be bound by the prevailing terms of the Privacy Statement as updated from time to time on our Site. When we make changes to this Privacy Statement, we will revise the updated date at the top of this page. This Privacy Statement is effective from 1 Jan 2017.
                </li>

                <li>
                    This Privacy Statement and your use of this Site shall be governed in all respects by the laws of Singapore.
                </li>
            </ol>

        </div>


    </section>
    <?php include('templates/sidebar.php'); ?>

</div>
<?php include('templates/bottom.php'); ?>