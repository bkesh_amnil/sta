<div class="container">
    <section class="container 404-container">
            <!--<h1 id="logo-este-404"><img src="img/logo.svg"/></h1>-->
        <div id="message-404">
            <h1>OOPS!</h1>
            <h3>PAGE NOT FOUND</h3>
            <span>It seems you have ventured onto a broken link, please use the menu bar to navigate to the desired page.</span>
        </div>
    </section>
    <?php include('templates/sidebar.php'); ?>
</div>

<?php include('templates/bottom.php'); ?>
<!--<p>You wanted: <?php echo $_GET['page']; ?></p>-->