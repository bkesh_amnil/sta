<div class="container">
    <section id="about" class="left-content">           
        <div class="page-content-heading">
            <img class="icon-heading-bullet-default svg" src="img/icons/icon-dark-yellow-bullet.svg" alt="icon-heading-bullet-default" />
            <h3>About STA</h3>
        </div>

        <div id="about-page-office-room-img-container" class="image-banner">
            <img src="img/about-page-office-room.jpg">
        </div>

        <div id="board-of-committee">
            THE BOARD OF COMMITTEE
        </div>

        <div id="committee-member-container">

        </div>

        <article id="article-our-corporate-story">
            <div id="our-corporate-story">
                <h4>OUR CORPORATE STORY</h4>
            </div>

            <p>
                Established in the 1950s and originally known as the Singapore Sawmillers Association, it represented sawmills located along Kallang River and Beach Road which processed timber imported from Sumatra. Back then sampans were used to tow the logs and boilers were used to generate power for cutting of the logs.
            </p>

            <p>
                In the 1960s and 1970s, due to the increased global demand in timber products, the number of sawmills increased exponentially across the island with large supply of logs coming from Malaysia. To alleviate traffic congestion, majority of these sawmills were consolidated in Sungei Kadut Industrial Estate in 1976.
            </p>

            <p>
                In 1983, the Association, then registered under the Labour Law, officially registered Singapore Timber Manufacturers’ Association with the Registry of Societies.
            </p>

            <p>
                In 2002, it merged with Singapore Timber Exporters Association and is renamed Singapore Timber Association, a Trade Association member of Singapore Chinese Chamber of Commerce & Industry and is the sole trade association representing the timber trade in Singapore.
            </p>

            <h5>OUR MISSION //</h5>
            <p>
                To establish global partnerships with other timber bodies in ensuring sustainable industrial practice
            </p>

            <h5>OUR VISION //</h5>
            <p>
                To be a leading organization in interests of timber, its products and responsible practices to create a sustainable industry.
            </p>


        </article>

    </section>
    <?php include('templates/sidebar.php'); ?>

</div>
<?php include('templates/bottom.php'); ?>