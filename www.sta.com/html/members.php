<div class="container">

    <section id="members-listing">
        <div class="page-content-heading">
            <img class="icon-heading-bullet-default svg" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default" />
            <h3>Members Listing</h3>
        </div>

        <form id="member-search-form">
            <div id="member-search-field-container">
                <input id="member-search-field" type="search" placeholder="Enter Search Terms..." />
                <button id="member-search-cancel-btn" type="reset">
                    <img id="member-search-cancel-btn-img" src="img/icons/cross-symbol.svg" alt="cancel-btn" />
                </button>
            </div>
            <button id="member-search-btn" class="custom-btn form-btn" type="submit" value="">Start Search</button>

            <span class="error-msg">No More Events to Load...</span>
            
        </form>

        <div id="member-search-result-number" class="no-display">
           
        </div>

        <ul id="member-result-container">
            
        </ul>

        <button id="load-more-members-btn" data-current="0" class="custom-btn form-btn no-display" type="submit" value="">Load More Members</button>

        <a href="javascript:void(0)" id="member-back-to-the-top" class="no-display">
            Back To Top
        </a>

    </section>
    <?php include('templates/sidebar.php'); ?>

</div>
<?php include('templates/bottom.php'); ?>