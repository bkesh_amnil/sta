<?php
$pid = $_GET['uid'];
$sharetitle = $_GET["sharetitle"];
if (strpos($_SERVER['HTTP_REFERER'], "facebook.com") > -1) {
    header("Location:event#" . slugify($sharetitle));
    exit();
}
$sharetitle = $_GET["sharetitle"];
$sheardescription = $_GET["sharedescription"];
$shearimg = $_GET["shareimg"];

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <!-- Open Graph url property -->
        <meta property="og:url" content="http://staging.www.singaporetimber.com/share.php?sharetitle=<?php echo $sharetitle; ?>&sharedescription=<?php echo $sheardescription; ?>&shareimg=<?php echo urlencode($shearimg); ?>&uid=<?php echo $pid; ?>" >	

        <!-- Open Graph title property -->
        <meta property="og:title" content="<?php echo $sharetitle; ?>" >

        <!-- Open Graph description property -->
        <meta property="og:description" content="<?php echo $sheardescription; ?>" >

        <!-- Open Graph image property -->

        <meta property="og:image"  content="<?php echo $shearimg; ?>">

        <meta property="og:image:type"       content="image/png">
        <meta property="og:image:width"      content="200">
        <meta property="og:image:height"     content="200">

        <!-- Open Graph type property -->
        <meta property="og:type" content="website" />

        <!-- Open Graph site_name property -->
        <meta property="og:site_name" content="staging.www.singaporetimber.com" >

        <title>Sta</title>
    </head>
    <body>

        <img src="<?php echo $shearimg; ?>" alt="">

        <p><?php echo $sheardescription; ?> ! </p>
    </body>
</html>