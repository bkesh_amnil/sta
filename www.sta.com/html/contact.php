<div class="container">
    <section id="contact-sta" class="left-content ">            
        <div class="page-content-heading">
            <img class="icon-heading-bullet-default svg" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default" />
            <h3>CONTACT STA</h3>
        </div>

        <div id="contact-sta-content">
            <address class="contact-sta-fields">
                <img src="img/icons/icon-contact-page-location.svg">
                <span class="contact-sta-fields-title">Location</span>
                <span class="contact-sta-fields-line-one">341B Beach Road,</span><!-- 
                --><span class="content-sta-fields-line-two">Singapore 19567</span>
            </address>

            <address class="contact-sta-fields">
                <img src="img/icons/icon-contact-page-telephone.svg">
                <span class="contact-sta-fields-title">Telephone</span>
                <span class="contact-sta-fields-line-one">Office: 6298 0709</span><!-- 
                --><span class="content-sta-fields-line-two">Fax: 6271 3004</span>
            </address>

            <address class="contact-sta-fields">
                <img src="img/icons/icon-contact-page-office-hours.svg" class="svg">
                <span class="contact-sta-fields-title">Office Hours</span>
                <span class="contact-sta-fields-line-one">Mon to Sat: 10am - 6pm</span><!-- 
                --><span class="content-sta-fields-line-two">Sun & PH: 10am - 2pm</span>
            </address>

            <address class="contact-sta-fields">
                <img src="img/icons/icon-contact-page-email-address.svg">
                <span class="contact-sta-fields-title">Email Address</span>
                <span class="contact-sta-fields-line-one">secretariat@</span><!-- 
                --><span class="content-sta-fields-line-two">singaporetimber.com</span>
            </address>

        </div>
    </section>
    <section id="email-sta" class="left-content">

        <div class="page-content-heading">
            <img class="icon-heading-bullet-default svg" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default" />
            <h3>EMAIL STA</h3>
        </div>

        <form name="contactForm">
            <div class="email-sta-form event-contact-form">
                <div class="email-sta-form-left-col">
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Contact Name*" name="contact_name">
                        <span class="error contact-name">Name is Required</span>
                    </div>
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Company Name" name="company_name">
                    </div>
                    <div class="form-input-wrapper">
                        <input type="email" placeholder="Email Address*" name="email">
                        <span class="error email">Email Address is Required</span>
                    </div>
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Mobile Number*" name="mobile">
                        <span class="error mobile">Mobile Number is Required</span>
                    </div>
                </div><!-- 
                --><div class="form-input-wrapper form-text-area-wrapper">
                    <textarea placeholder="Message*" name="message"></textarea>
                    <span class="error error-text-area message">Message is Required</span>
                </div>
            </div>
            <button type="submit" id="email-sta-form-btn" class="custom-btn form-btn">Submit</button>
        </form>
        <div class="form-success-msg">
            <span class="form-success-msg-title"></span>
            <span class="form-success-msg-text"></span>
            <button class="form-success-msg-btn" type="submit">OK</button>
        </div>
    </section>
    <?php include('templates/sidebar.php'); ?>

</div>
<?php include('templates/bottom.php'); ?>