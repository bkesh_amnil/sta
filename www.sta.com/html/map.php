<!DOCTYPE html>
<html>

    <head>
        <title>Map</title>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript">
//            function initialize() {
//                var mapDiv = document.getElementById('map_canvas');
//                var map = new google.maps.Map(mapDiv, {
//                    center: { lat: 1.266206, lng: 103.8213044 },
//                    scrollwheel:  false,
//                    zoom: 17
//                });
//                var marker = new google.maps.Marker({
//                    position: { lat: 1.266206, lng: 103.8213044 },
//                    map: map,
//                });
//            }


        </script>
    </head>
    <body>
        <div id="map_canvas"></div> 
    </body>
    
<?php

foreach ($this->js as $js) {
    if (startsWith($js, 'http://') || startsWith($js, 'https://') || startsWith($js, '//')) {
        echo '<script src="' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
    } else {
        echo '<script src="' . SERVER_PATH . '/js/' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
    }
}
?>
</html>
<!--<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxK6I8XJnlizNPV3nqUCc9veXmLQ3bcXI&callback=initialize" type="text/javascript"></script>-->

<style>
    body {
        margin: 0;
    }
    #map_canvas {
        /*height: 600px;*/
        /*border: 1px solid #000;*/

        position: absolute !important;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
</style>
