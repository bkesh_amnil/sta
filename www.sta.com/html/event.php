<div class="container">
    <section id="event-slider">         
        <div id="events">
            <div class="page-content-heading">
                <img class="icon-heading-bullet-default" src="img/icons/icon-heading-bullet-default.svg" alt="icon-heading-bullet-default" />
                <h3 id="event-title"></h3>
            </div>
            <span id="events-prev-next" rel="">
                <a href="javascript:void(0)" id="events-prev">
                    <img src="img/icons/navigation-left-arrow.svg" id="events-prev-img" alt="navigation-left-arrow" />
                    <span id="events-prev-text">Prev</span>
                </a> |
                <a href="javascript:void(0)" id="events-next">
                    <span id="events-next-text">Next</span>
                    <img src="img/icons/navigation-right-arrow.svg" id="events-next-img" alt="navigation-right-arrow" />
                </a>
            </span>

            <p class="error-msg" id="no-events"></p>

            <div id="events-page-outdoor-decking-tips" class="image-banner">
                <img src="">
            </div>

            <div id="event-page-outdoor-decking-tips-info">
                <span class="event-date"></span><span class="event-time"></span>

                <p id="event-description"></p>
            </div>
        </div>

        <div id="event-venue-details">
            <address id="event-venue-details-content">
                <h6>Venue:</h6>
                <p class="venue"></p>

                <h6>Course Fee:</h6>
                <p class="course-fee"></p>

                <h6>Remarks:</h6>
                <p class="remarks"></p>

                <span class="view-brochure"><a href="#" id="view-brochure">View Brochure</a> ></span>
            </address>
            <form name="eventRegisterForm" id="event-contact-form-container">
                <div id="event-contact-form">
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Contact Name*" name="contact_name">
                        <span class="error contact-name">Name is Required</span>
                    </div>
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Company Name" name="company_name">
                    </div>
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Email Address*" name="email">
                        <span class="error email">Email Address is Required</span>
                    </div>
                    <div class="form-input-wrapper">
                        <input type="text" placeholder="Mobile Number*" name="mobile">
                        <span class="error mobile">Mobile Number is Required</span>
                    </div>
                    <div class="form-input-wrapper">
                        <input id="input-last-child" type="text" placeholder="Number of Participants*" name="participants">
                        <span class="error participants">Number of Participants is Required</span>
                    </div>
                    <input type="hidden" name="event_name">
                </div>
                <button type="submit" id="register-interest-btn" class="custom-btn form-btn">Register <span>Interest</span></button>
            </form>
            <a href="#" class="custom-btn share-this-event-btn">Share <span>This Event</span></a>


            <div class="form-success-msg">
                <span class="form-success-msg-title"></span>
                <span class="form-success-msg-text"></span>
                <button class="form-success-msg-btn" type="submit">OK</button>
            </div>

        </div>
    </section><!--
    --><?php include('templates/sidebar.php'); ?>

</div>

<?php include('templates/bottom.php'); ?>