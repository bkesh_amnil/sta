<div class="container">

    <section id="services" class="left-content">
        <div class="page-content-heading">
            <img class="main-content-bullet-1" src="img/icons/icon-dark-yellow-bullet.svg" alt="icon-dark-yellow-bullet" />
            <h3>Services</h3>
        </div>

        <div id="services-page-timber-grading" class="service-item">
            <img src="img/icons/icon-timber-grading-services-page.svg" alt="icon-timber-grading-services-page" />
            <div class="service-item-details">
                <h5>Timber Grading</h5>
                <p>
                    Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus. Nulla mauris augue,  rutrum nec accumsan sed, eleifend quis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam purus ligula, sagittis a scelerisque at, luctus non augue. Proin a sapien ut quam mattis pharetra eget rutrum felis.  Praesent consectetur urna fermentum dui egestas, sit amet commodo nibh sodales. Etiam pharetra purus. Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus.
                </p>
            </div>
        </div>

        <div id="services-page-second-opinions" class="service-item">
            <img src="img/icons/icon-second-opinions-services-page.svg" alt="icon-second-opinions-services-page" />
            <div class="service-item-details">
                <h5>Second Opinions</h5>
                <p>
                    Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus. Nulla mauris augue,  rutrum nec accumsan sed, eleifend quis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam purus ligula, sagittis a scelerisque at, luctus non augue. Proin a sapien ut quam mattis pharetra eget rutrum felis.  Praesent consectetur urna fermentum dui egestas, sit amet commodo nibh sodales. Etiam pharetra purus. Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus.
                </p>
            </div>
        </div>

        <div id="services-page-course-arrangement" class="service-item">
            <img src="img/icons/icon-course-arrangement-services-page.svg" alt="icon-course-arrangement-services-page" />
            <div class="service-item-details">
                <h5>Course Arrangement</h5>
                <p>
                    Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus. Nulla mauris augue,  rutrum nec accumsan sed, eleifend quis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam purus ligula, sagittis a scelerisque at, luctus non augue. Proin a sapien ut quam mattis pharetra eget rutrum felis.  Praesent consectetur urna fermentum dui egestas, sit amet commodo nibh sodales. Etiam pharetra purus. Ut bibendum nisl sollicitudin, iaculis nisi quis, tempus lectus.
                </p>
            </div>
        </div>

    </section>
    <?php include('templates/sidebar.php');?>

</div>
<?php include('templates/bottom.php'); ?>