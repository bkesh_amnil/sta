-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2016 at 07:52 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sta`
--

-- --------------------------------------------------------

--
-- Table structure for table `sta_activitylog`
--

CREATE TABLE `sta_activitylog` (
  `ID` int(11) NOT NULL,
  `UserType` varchar(600) DEFAULT NULL COMMENT 'BackendUser, Customer, Service Provider',
  `UserID` int(11) DEFAULT NULL COMMENT 'BackendUserID, ApplicationUserID',
  `Action` char(100) DEFAULT NULL,
  `OldValue` text,
  `NewValue` text,
  `ModelType` varchar(255) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `CreatedDateTime` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_activitylog`
--

INSERT INTO `sta_activitylog` (`ID`, `UserType`, `UserID`, `Action`, `OldValue`, `NewValue`, `ModelType`, `ModelID`, `CreatedDateTime`) VALUES
(1, 'BackendUser', 46, 'Add', NULL, NULL, 'BackendUser', 1, 1479968300298),
(2, 'BackendUser', 46, 'Update', '{"ID":1,"Email":"justin@sta.com","FirstName":"bkesh","LastName":"maharjan","PasswordHash":"sha256:1000:XDrDbZCKZrS0wZCtzGwdnQuluZwCuaeg:uyYHY7DeuzSYmr8iCqPkm1hkPIghz7dZ","PersistentLoginHashes":"","LastUpdatedDateTime":-1795403379,"RoleID":1,"Status":"Active"}', '{"ID":1,"FirstName":"bkesh","LastName":"maharjan","Email":"justin@sta.com","RoleID":1,"Status":"Active"}', 'BackendUser', 1, 1479968313741),
(3, 'BackendUser', 46, 'Logout', NULL, NULL, NULL, NULL, 1479968341651),
(4, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1479968348259),
(5, 'BackendUser', 1, 'Logout', NULL, NULL, NULL, NULL, 1479970694590),
(6, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1479971249948),
(7, 'BackendUser', 1, 'Update', '{"ID":62,"BusType":"49-Seater","MinCapacity":"1","MaxCapacity":"49","MinPrice":"75","MaxPrice":"2500","Status":"Active","Image":null}', '{"ID":62,"BusType":"49-Seater","MinCapacity":"1","MaxCapacity":"49","MinPrice":"75","MaxPrice":"2500"}', 'Language', 62, 1479971967059),
(8, 'BackendUser', 1, 'Logout', NULL, NULL, NULL, NULL, 1479971979081),
(9, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1479971982027),
(10, 'BackendUser', 1, 'Logout', NULL, NULL, NULL, NULL, 1479972111660),
(11, 'BackendUser', 1, 'Login', NULL, NULL, NULL, NULL, 1479972116013),
(12, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 1, 1481182444123),
(13, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 2, 1481184158939),
(14, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 3, 1481184939798),
(15, 'BackendUser', 1, 'Update', '{"ID":1,"EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","Pdf":"1","Image":"1481185338_room-1.jpg","DisplayPriority":2,"CreatedTime":"1481182444123","LastUpdatedTime":1481185338937}', '{"ID":"1","EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","DisplayPriority":"2","doc_prev":"undefined","img_prev":"1"}', 'Event', 1, 1481185338937),
(16, 'BackendUser', 1, 'Update', '{"ID":1,"EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","Pdf":"1","Image":"1481185383_room-1.jpg","DisplayPriority":2,"CreatedTime":"1481182444123","LastUpdatedTime":1481185383684}', '{"ID":"1","EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","DisplayPriority":"2","doc":"undefined","doc_prev":"undefined","img_prev":"1481185338_room-1.jpg"}', 'Event', 1, 1481185383684),
(17, 'BackendUser', 1, 'Update', '{"ID":1,"EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","Pdf":"1","Image":"1481185409_room-1.jpg","DisplayPriority":2,"CreatedTime":"1481182444123","LastUpdatedTime":1481185409419}', '{"ID":"1","EventName":"Name","EventDate":"4\\/4\\/2016","EventFrom":"1:00","EventTo":"2:00","Description":"Description","Venue":"Venue","Latitude":"56","Longitude":"65","CourseFee":"547","Remarks":"Remarks","DisplayPriority":"2","doc":"undefined","doc_prev":"undefined","img_prev":"1481185383_room-1.jpg"}', 'Event', 1, 1481185409419),
(18, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 4, 1481185565646),
(19, 'BackendUser', 1, 'Add', NULL, NULL, 'Event', 5, 1481185893774),
(20, 'BackendUser', 1, 'Update', '{"ID":5,"EventName":"Update Test","EventDate":"4\\/4\\/2015","EventFrom":"8:00","EventTo":"9:00","Description":"Description","Venue":"Venue","Latitude":"85","Longitude":"58","CourseFee":"547","Remarks":"Remarks","Pdf":"1481185893_WAPT_Report_TimeExtender_8-30-2016.pdf","Image":"1481185968_avatar-2.png","DisplayPriority":3,"CreatedTime":"1481185893774","LastUpdatedTime":1481185968680}', '{"ID":"5","EventName":"Update Test","EventDate":"4\\/4\\/2015","EventFrom":"8:00","EventTo":"9:00","Description":"Description","Venue":"Venue","Latitude":"85","Longitude":"58","CourseFee":"547","Remarks":"Remarks","DisplayPriority":"3","doc":"undefined","doc_prev":"undefined","img_prev":"1481185893_room-1.jpg"}', 'Event', 5, 1481185968680),
(21, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 2, 1481194152368),
(22, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 3, 1481194155853),
(23, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 4, 1481194158233),
(24, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 5, 1481218929148),
(25, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 6, 1481219222344),
(26, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 7, 1481219308881),
(27, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 8, 1481219363712),
(28, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 9, 1481219419652),
(29, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 10, 1481219426151),
(30, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 11, 1481219450461),
(31, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 12, 1481219485996),
(32, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 13, 1481219493922),
(33, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 14, 1481219514096),
(34, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 15, 1481219518019),
(35, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 16, 1481219526397),
(36, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 17, 1481219624140),
(37, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 18, 1481219668463),
(38, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 19, 1481219673868),
(39, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 20, 1481219679273),
(40, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 21, 1481219682196),
(41, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 22, 1481219684314),
(42, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 23, 1481219733523),
(43, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 24, 1481219740215),
(44, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 25, 1481219741425),
(45, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 26, 1481260637445),
(46, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 27, 1481260639193),
(47, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 28, 1481260643838),
(48, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 29, 1481262908342),
(49, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 30, 1481262913199),
(50, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 31, 1481262919899),
(51, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 32, 1481262939699),
(52, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 33, 1481262942309),
(53, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 69, 1481275261284),
(54, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 74, 1481278669688),
(55, 'BackendUser', 1, 'Add', NULL, NULL, 'Advertisement', 1, 1481279937921),
(56, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"35345","URL":"3453","Image":"1481279937_banner-img.jpg","DisplayPriority":345345,"CreatedTime":"1481279937921","LastUpdatedTime":"1481279937921"}', '{"ID":"1","Name":"35345","URL":"3453","DisplayPriority":"345345","img":"undefined","img_prev":"1481279937_banner-img.jpg"}', 'Advertisement', 1, 1481279955976),
(57, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"35345","URL":"3453","Image":"1481279937_banner-img.jpg","DisplayPriority":345345,"CreatedTime":"1481279937921","LastUpdatedTime":"1481279937921"}', '{"ID":"1","Name":"35345","URL":"3453","DisplayPriority":"345345","img":"undefined","img_prev":"1481279937_banner-img.jpg"}', 'Advertisement', 1, 1481279967532),
(58, 'BackendUser', 1, 'Update', '{"ID":1,"Name":"35345  bkesh","URL":"3453","Image":"1481279937_banner-img.jpg","DisplayPriority":345345,"CreatedTime":"1481279937921","LastUpdatedTime":"1481279937921"}', '{"ID":"1","Name":"35345  bkesh","URL":"3453","DisplayPriority":"345345","img":"undefined","img_prev":"1481279937_banner-img.jpg"}', 'Advertisement', 1, 1481279989548),
(59, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"xfgv","CompanyNameChinese":"cbvn","Address":"vbn","Phone":"234","Fax":"34","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280246244}', '{"ID":"74","CompanyNameEnglish":"xfgv","CompanyNameChinese":"cbvn","Address":"vbn","Phone":"234","Fax":"34","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"ID\\":9,\\"CompanyID\\":\\"74\\",\\"NameEnglish\\":\\"asads\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"213\\",\\"DisplayPriority\\":0}]","Associates":"[{\\"ID\\":3,\\"CompanyID\\":\\"74\\",\\"AssociateCompanyNameEnglish\\":\\"asd\\",\\"AssociateCompanyNameChinese\\":\\"cc\\",\\"DisplayPriority\\":1}]"}', 'Company', 74, 1481280246244),
(60, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"234","Fax":"34","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280262647}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"234","Fax":"34","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"ID\\":9,\\"CompanyID\\":\\"74\\",\\"NameEnglish\\":\\"asads\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"213\\",\\"DisplayPriority\\":0}]","Associates":"[{\\"ID\\":3,\\"CompanyID\\":\\"74\\",\\"AssociateCompanyNameEnglish\\":\\"asd\\",\\"AssociateCompanyNameChinese\\":\\"cc\\",\\"DisplayPriority\\":1}]"}', 'Company', 74, 1481280262647),
(61, 'BackendUser', 1, 'Update', '{"ID":1,"CompanyNameEnglish":"gfdg","CompanyNameChinese":"dfg","Address":"d","Phone":"h","Fax":"hh","Email":"h","Website":"h","Registration":"h","ProductsServicesEnglish":"h","ProductsServicesChinese":"h","DisplayPriority":1,"CreatedTime":"1","LastUpdatedTime":1481280346222}', '{"ID":"1","CompanyNameEnglish":"gfdg","CompanyNameChinese":"dfg","Address":"d","Phone":"h","Fax":"hh","Email":"h","Website":"h","Registration":"h","ProductsServicesEnglish":"h","ProductsServicesChinese":"h","DisplayPriority":"1","Representatives":"[]","Associates":"[{\\"ID\\":1,\\"CompanyID\\":\\"1\\",\\"AssociateCompanyNameEnglish\\":\\"Test\\",\\"AssociateCompanyNameChinese\\":\\"Test\\",\\"DisplayPriority\\":1},{\\"AssociateCompanyNameEnglish\\":\\"sdf\\",\\"DisplayPriority\\":\\"5\\"}]"}', 'Company', 1, 1481280346222),
(62, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280424296}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280424296),
(63, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280444287}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280444287),
(64, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280510543}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280510543),
(65, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280522885}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280522885),
(66, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280634280}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280634280),
(67, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280712789}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280712789),
(68, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280760765}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280760765),
(69, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280886535}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[]","Associates":"[]"}', 'Company', 74, 1481280886535),
(70, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481280949785}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"this is new\\",\\"Phone\\":\\"97987\\",\\"DisplayPriority\\":\\"9879\\"}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"this is again new\\",\\"DisplayPriority\\":\\"8787\\"}]"}', 'Company', 74, 1481280949785),
(71, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481281087432}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"this is new\\",\\"Phone\\":\\"97987\\",\\"DisplayPriority\\":\\"9879\\"}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"this is again new\\",\\"DisplayPriority\\":\\"8787\\"}]"}', 'Company', 74, 1481281087432),
(72, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481281130150}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"this is new\\",\\"Phone\\":\\"97987\\",\\"DisplayPriority\\":\\"9879\\"},{\\"NameEnglish\\":\\"bkesh is adding editn\\",\\"Phone\\":\\"9098\\",\\"DisplayPriority\\":\\"09809\\"}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"this is again new\\",\\"DisplayPriority\\":\\"8787\\"},{\\"AssociateCompanyNameEnglish\\":\\"bkesh is addinge ditei\\",\\"DisplayPriority\\":\\"09809\\"}]"}', 'Company', 74, 1481281130150),
(73, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481281160471}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"this is new\\",\\"Phone\\":\\"97987\\",\\"DisplayPriority\\":\\"9879\\"},{\\"NameEnglish\\":\\"bkesh is adding editn\\",\\"Phone\\":\\"9098\\",\\"DisplayPriority\\":\\"09809\\"}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"this is again new\\",\\"DisplayPriority\\":\\"8787\\"},{\\"AssociateCompanyNameEnglish\\":\\"bkesh is addinge ditei\\",\\"DisplayPriority\\":\\"09809\\"}]"}', 'Company', 74, 1481281160471),
(74, 'BackendUser', 1, 'Update', '{"ID":74,"CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":1,"CreatedTime":"1481278669688","LastUpdatedTime":1481281178920}', '{"ID":"74","CompanyNameEnglish":"bkesh is here","CompanyNameChinese":"so is chin","Address":"thecho","Phone":"9841721181","Fax":"dfgdsfg","Email":"sdf","Website":"sdf","Registration":"sdf","ProductsServicesEnglish":"fsd","ProductsServicesChinese":"sd","DisplayPriority":"1","Representatives":"[{\\"NameEnglish\\":\\"this is new\\",\\"Phone\\":\\"97987\\",\\"DisplayPriority\\":\\"9879\\"},{\\"NameEnglish\\":\\"bkesh is adding editn\\",\\"Phone\\":\\"9098\\",\\"DisplayPriority\\":\\"09809\\"}]","Associates":"[{\\"AssociateCompanyNameEnglish\\":\\"this is again new\\",\\"DisplayPriority\\":\\"8787\\"},{\\"AssociateCompanyNameEnglish\\":\\"bkesh is addinge ditei\\",\\"DisplayPriority\\":\\"09809\\"},{\\"AssociateCompanyNameEnglish\\":\\"j\\",\\"DisplayPriority\\":\\"123\\"}]"}', 'Company', 74, 1481281178920),
(75, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 75, 1481281322640),
(76, 'BackendUser', 1, 'Update', '{"ID":75,"CompanyNameEnglish":"Add test","CompanyNameChinese":"Add test","Address":"Test","Phone":"123","Fax":"123","Email":"test@test.com","Website":"www.test.com","Registration":"Test","ProductsServicesEnglish":"Test","ProductsServicesChinese":"Test","DisplayPriority":1,"CreatedTime":"1481281322640","LastUpdatedTime":1481281374687}', '{"ID":"75","CompanyNameEnglish":"Add test","CompanyNameChinese":"Add test","Address":"Test","Phone":"123","Fax":"123","Email":"test@test.com","Website":"www.test.com","Registration":"Test","ProductsServicesEnglish":"Test","ProductsServicesChinese":"Test","DisplayPriority":"1","Representatives":"[{\\"ID\\":23,\\"CompanyID\\":\\"75\\",\\"NameEnglish\\":\\"add\\",\\"NameChinese\\":\\"cc\\",\\"Phone\\":\\"123\\",\\"DisplayPriority\\":0},{\\"NameEnglish\\":\\"update\\",\\"Phone\\":\\"123\\",\\"DisplayPriority\\":\\"2\\"}]","Associates":"[{\\"ID\\":17,\\"CompanyID\\":\\"75\\",\\"AssociateCompanyNameEnglish\\":\\"add add\\",\\"AssociateCompanyNameChinese\\":\\"cc\\",\\"DisplayPriority\\":1}]"}', 'Company', 75, 1481281374687),
(77, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 3, 1481282486854),
(78, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 4, 1481282522933),
(79, 'BackendUser', 1, 'Update', '{"ID":2,"Name":"Test update","Designation":"Test","Company":"Test","Image":"1481282936_banner-img.jpg","DisplayPriority":0,"CreatedTime":"1481185565646","LastUpdatedTime":1481282936997}', '{"ID":"2","Name":"Test update","Designation":"Test","Company":"Test","img_prev":"1481185565_room-1.jpg"}', 'Boc', 2, 1481282936997),
(80, 'BackendUser', 1, 'Add', NULL, NULL, 'Boc', 5, 1481283073071),
(81, 'BackendUser', 1, 'Update', '{"ID":5,"Name":"Test","Designation":"Designation","Company":"Company","Image":"1481283091_black-separator.png","DisplayPriority":0,"CreatedTime":"1481283073071","LastUpdatedTime":1481283091336}', '{"ID":"5","Name":"Test","Designation":"Designation","Company":"Company","img_prev":"1481283073_banner-img.jpg"}', 'Boc', 5, 1481283091336),
(82, 'BackendUser', 1, 'Update', '{"ID":5,"Name":"Update Test","Designation":"Designation","Company":"Company","Image":"1481283195_banner-img.jpg","DisplayPriority":0,"CreatedTime":"1481283073071","LastUpdatedTime":1481283195423}', '{"ID":"5","Name":"Update Test","Designation":"Designation","Company":"Company","DisplayPriority":"0","img_prev":"1481283091_black-separator.png"}', 'Boc', 5, 1481283195423),
(83, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 76, 1481283262227),
(84, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 77, 1481283293180),
(85, 'BackendUser', 1, 'Add', NULL, NULL, 'Company', 78, 1481283324259);

-- --------------------------------------------------------

--
-- Table structure for table `sta_advertisement`
--

CREATE TABLE `sta_advertisement` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL DEFAULT '1',
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_advertisement`
--

INSERT INTO `sta_advertisement` (`ID`, `Name`, `URL`, `Title`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'Private Outdoor', 'http://google.com', 'Pool Decking Specialist ', '1.jpg', 10, 1481279937921, 1481279937921),
(2, 'Commercial Outdoor', 'http://google.com', 'Timber Decor Specialist ', '2.jpg', 20, 1481279937921, 1481279937921);

-- --------------------------------------------------------

--
-- Table structure for table `sta_associatecompany`
--

CREATE TABLE `sta_associatecompany` (
  `ID` int(11) NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `AssociateCompanyNameEnglish` varchar(255) NOT NULL,
  `AssociateCompanyNameChinese` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_associatecompany`
--

INSERT INTO `sta_associatecompany` (`ID`, `CompanyID`, `AssociateCompanyNameEnglish`, `AssociateCompanyNameChinese`, `DisplayPriority`) VALUES
(2, 69, 'sdf', 'cc', 1),
(6, 1, 'Test', 'cc', 1),
(7, 1, 'sdf', 'cc', 5),
(14, 74, 'this is again new', 'cc', 8787),
(15, 74, 'bkesh is addinge ditei', 'cc', 9809),
(16, 74, 'j', 'cc', 123),
(18, 75, 'add add', 'cc', 1),
(19, 76, 'asdf', 'cc', 4),
(20, 77, 'asdf', 'cc', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sta_backenduser`
--

CREATE TABLE `sta_backenduser` (
  `ID` int(11) NOT NULL,
  `FirstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PasswordHash` text COLLATE utf8_unicode_ci NOT NULL,
  `PersistentLoginHashes` text COLLATE utf8_unicode_ci,
  `RoleID` int(11) NOT NULL,
  `LastUpdatedDateTime` bigint(20) UNSIGNED NOT NULL,
  `Status` enum('Active','Disabled','Delete') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sta_backenduser`
--

INSERT INTO `sta_backenduser` (`ID`, `FirstName`, `LastName`, `Email`, `PasswordHash`, `PersistentLoginHashes`, `RoleID`, `LastUpdatedDateTime`, `Status`) VALUES
(1, 'bkesh', 'maharjan', 'justin@sta.com', 'sha256:1000:XDrDbZCKZrS0wZCtzGwdnQuluZwCuaeg:uyYHY7DeuzSYmr8iCqPkm1hkPIghz7dZ', '', 1, 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `sta_boc`
--

CREATE TABLE `sta_boc` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Company` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_boc`
--

INSERT INTO `sta_boc` (`ID`, `Name`, `Designation`, `Company`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'Mr. Chua Seng Chong PBM', 'TRUSTEE', 'at Hin Timber Pte Ltd', 'committee-member-1.jpg', 10, 1481281322640, 1481281374687),
(2, 'Mr. Chong Ha Lee', 'TRUSTEE', 'Hong Huat Timber Trading Pte Ltd', 'committee-member-2.jpg', 20, 1481281322640, 1481281374687),
(3, 'Mr. Neo Koon Boo', 'TRUSTEE', 'LHT Holdings Ltdtd', 'committee-member-3.jpg', 30, 1481281322640, 1481281374687),
(4, 'Mr. Chua Seng Chong PBM', 'PRESIDENT', 'Tat Hin Timber Pte Ltd', 'committee-member-4.jpg', 30, 1481281322640, 1481281374687),
(5, 'Mr. Paul Tay Chee Hian', 'VICE PRESIDENT', 'Kim San Trading (Kilning) Pte Ltd', 'committee-member-5.jpg', 50, 1481281322640, 1481281374687),
(6, 'Mr. Freddie Ng Heok Kwee', 'VICE PRESIDENT', 'Ng Guan Seng Woodworking Ind. P/L', 'committee-member-6.jpg', 60, 1481281322640, 1481281374687),
(7, 'Mr. Low Boh Kee', 'HON. SECRETARY', 'Prime Timber Industries Pte Ltd', 'committee-member-7.jpg', 70, 1481281322640, 1481281374687),
(8, 'Mr. Teo Boon Chong', 'ASST. HON. SECRETARY', 'Goodhill Enterprises (S) Pte Ltd', 'committee-member-8.jpg', 80, 1481281322640, 1481281374687);

-- --------------------------------------------------------

--
-- Table structure for table `sta_company`
--

CREATE TABLE `sta_company` (
  `ID` int(11) NOT NULL,
  `CompanyNameEnglish` varchar(255) NOT NULL,
  `CompanyNameChinese` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Website` varchar(255) NOT NULL,
  `Registration` varchar(255) NOT NULL,
  `ProductsServicesEnglish` varchar(255) NOT NULL,
  `ProductsServicesChinese` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_company`
--

INSERT INTO `sta_company` (`ID`, `CompanyNameEnglish`, `CompanyNameChinese`, `Address`, `Phone`, `Fax`, `Email`, `Website`, `Registration`, `ProductsServicesEnglish`, `ProductsServicesChinese`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(1, 'AEONIC INTERNATIONAL TRADE PTE LTD				\n', 'dfg', '20 Maxwell Road #03-08,		\nMaxwell House, Singapore 069113			\n', ' 6222 7518 ', '6224 2817', 'aeonic@singnet.com.sg', 'www.aeonic.com.sg', 'h', 'h', 'h', 1, 1, 1481280346222),
(69, 'google', 'sadf', 'sadf', '234', '234', 'asdf@sadf.com', 'www.google.com', 'sadf', 'asdf', 'asdf', 2, 1481275261284, 1481275261284),
(74, 'webcrafts', 'so is chin', 'thecho', '9841721181', 'dfgdsfg', 'sdf', 'sdf', 'sdf', 'fsd', 'sd', 1, 1481278669688, 1481281178920),
(75, 'yahoo', 'Add test', 'Test', '123', '123', 'test@test.com', 'www.test.com', 'Test', 'Test', 'Test', 1, 1481281322640, 1481281374687),
(76, 'hooli', 'asdf', 'sadf', '234', '234', 'asdf', 'asdf', 'asf', 'belson crafts', 'asdf', 10, 1481283262227, 1481283262227),
(77, 'pied piper', 'data', 'data', 'tadas', 'asdf', 'adsf', 'adsf', 'asdf', 'asdf', 'afd', 14, 1481283293180, 1481283293180),
(78, 'malta enc.', 'kj', 'gjkh', 'hkasjgg', 'gjhk', 'hjkg', 'gkjh', 'hjk', 'gjhk', 'ghjk', 2, 1481283324259, 1481283324259);

-- --------------------------------------------------------

--
-- Table structure for table `sta_event`
--

CREATE TABLE `sta_event` (
  `ID` int(11) NOT NULL,
  `EventName` varchar(255) NOT NULL,
  `EventDate` varchar(200) NOT NULL,
  `EventFrom` varchar(200) NOT NULL,
  `EventTo` varchar(200) NOT NULL,
  `Description` text NOT NULL,
  `Venue` varchar(255) NOT NULL,
  `Latitude` varchar(200) NOT NULL,
  `Longitude` varchar(200) NOT NULL,
  `CourseFee` varchar(50) NOT NULL,
  `Remarks` text NOT NULL,
  `Pdf` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL,
  `CreatedTime` bigint(20) NOT NULL,
  `LastUpdatedTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_event`
--

INSERT INTO `sta_event` (`ID`, `EventName`, `EventDate`, `EventFrom`, `EventTo`, `Description`, `Venue`, `Latitude`, `Longitude`, `CourseFee`, `Remarks`, `Pdf`, `Image`, `DisplayPriority`, `CreatedTime`, `LastUpdatedTime`) VALUES
(4, 'full moon', '2017-12-13', '4:00', '5:00', 'Description', 'Venue', '58', '95', '564', 'Remarks', '1481185565_WAPT_Report_TimeExtender_8-30-2016.pdf', '1481185565_room-1.jpg', 2, 1481185565646, 1481185565646),
(5, 'day one  tpoc', '2017-12-14', '8:00', '9:00', 'Description', 'Venue', '85', '58', '547', 'Remarks', '1481185893_WAPT_Report_TimeExtender_8-30-2016.pdf', '1481185968_avatar-2.png', 3, 0, 1481185968680),
(6, 'day two lattitude attitiude', '2017-12-15', '9', '5', '5646', '84', '5646', '5', '56465', '65', '6', '4', 564, 1481185893774, 1481185893774),
(7, 'day three brown building', '2017-12-16', '9', '', '', '', '', '', '', '', '', '', 875, 1481185893774, 1481185893774);

-- --------------------------------------------------------

--
-- Table structure for table `sta_module`
--

CREATE TABLE `sta_module` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL COMMENT 'BackenUser, Application User, Booking, Bus Type',
  `Icon` varchar(100) DEFAULT NULL,
  `Caption` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_module`
--

INSERT INTO `sta_module` (`ID`, `Name`, `Icon`, `Caption`) VALUES
(1, 'staff-bus-type', 'icon_bus_type.svg', 'Bus Type'),
(2, 'language', 'icon_language.svg', 'Language'),
(3, 'language-value', 'icon_language_value.svg', 'Language Value'),
(4, 'staff-backend-user', 'icon_backend_user.svg', 'Backend User'),
(6, 'role', 'icon_role.svg', 'Role'),
(8, 'service-provider', 'icon_service_provider.svg', 'Service Provider'),
(10, 'customer', 'icon_customer.svg', 'Customer'),
(11, 'notification-log', 'icon_notification_log.svg', 'Notification Log'),
(12, 'verification-code-log', 'icon_verification_code_log.svg', 'Verification Code Log'),
(13, 'enquiry', 'icon_enquiry.svg', 'Enquiry'),
(14, 'booking', 'icon_booking.svg', 'Booking');

-- --------------------------------------------------------

--
-- Table structure for table `sta_representative`
--

CREATE TABLE `sta_representative` (
  `ID` int(11) NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `NameEnglish` varchar(255) NOT NULL,
  `NameChinese` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `DisplayPriority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_representative`
--

INSERT INTO `sta_representative` (`ID`, `CompanyID`, `NameEnglish`, `NameChinese`, `Phone`, `DisplayPriority`) VALUES
(7, 69, 'asdf', 'cc', '123', 1),
(21, 74, 'this is new', 'cc', '97987', 9879),
(22, 74, 'bkesh is adding editn', 'cc', '9098', 9809),
(24, 75, 'add', 'cc', '123', 0),
(25, 75, 'update', 'cc', '123', 2),
(26, 76, 'asdf', 'cc', '45', 4),
(27, 77, 'asd', 'cc', '1', 43);

-- --------------------------------------------------------

--
-- Table structure for table `sta_role`
--

CREATE TABLE `sta_role` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Status` enum('Active','Delete','Disabled') NOT NULL,
  `LastUpdatedDateTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_role`
--

INSERT INTO `sta_role` (`ID`, `Name`, `Status`, `LastUpdatedDateTime`) VALUES
(1, 'superadmin', 'Active', 0),
(2, 'normalAdmin', 'Active', 1478498280425);

-- --------------------------------------------------------

--
-- Table structure for table `sta_roledetails`
--

CREATE TABLE `sta_roledetails` (
  `ID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_roledetails`
--

INSERT INTO `sta_roledetails` (`ID`, `RoleID`, `ModuleID`) VALUES
(72, 2, 1),
(73, 2, 2),
(74, 2, 3),
(75, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sta_settings`
--

CREATE TABLE `sta_settings` (
  `ID` int(11) NOT NULL,
  `SettingName` varchar(200) NOT NULL,
  `SettingValue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sta_settings`
--

INSERT INTO `sta_settings` (`ID`, `SettingName`, `SettingValue`) VALUES
(2, 'CodeExpixyInMinutes', '5'),
(3, 'TripCompletionInMinutes', '30'),
(4, '< 24 Hours (CancellationFee)', '50'),
(5, '< 72 Hours (CancellationFee)', '25'),
(6, '> 72 Hours (CancellationFee)', '0'),
(7, '< 24 Hours (Surcharge)', '50'),
(8, '< 72 Hours (Surcharge)', '25'),
(9, '> 72 Hours (Surcharge)', '0'),
(10, 'Report An Issue Email', 'rojeena@amnniltech.com,alisha@amniltech.com'),
(11, 'BookABusValidateInMinutes', '360'),
(12, 'AllowCustomerCancellationInMinutes', '30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sta_activitylog`
--
ALTER TABLE `sta_activitylog`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_advertisement`
--
ALTER TABLE `sta_advertisement`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_associatecompany`
--
ALTER TABLE `sta_associatecompany`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_backenduser`
--
ALTER TABLE `sta_backenduser`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_boc`
--
ALTER TABLE `sta_boc`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_company`
--
ALTER TABLE `sta_company`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_event`
--
ALTER TABLE `sta_event`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_module`
--
ALTER TABLE `sta_module`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_representative`
--
ALTER TABLE `sta_representative`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_role`
--
ALTER TABLE `sta_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_roledetails`
--
ALTER TABLE `sta_roledetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sta_settings`
--
ALTER TABLE `sta_settings`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sta_activitylog`
--
ALTER TABLE `sta_activitylog`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `sta_advertisement`
--
ALTER TABLE `sta_advertisement`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sta_associatecompany`
--
ALTER TABLE `sta_associatecompany`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `sta_backenduser`
--
ALTER TABLE `sta_backenduser`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sta_boc`
--
ALTER TABLE `sta_boc`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sta_company`
--
ALTER TABLE `sta_company`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `sta_event`
--
ALTER TABLE `sta_event`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sta_module`
--
ALTER TABLE `sta_module`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sta_representative`
--
ALTER TABLE `sta_representative`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `sta_role`
--
ALTER TABLE `sta_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sta_roledetails`
--
ALTER TABLE `sta_roledetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `sta_settings`
--
ALTER TABLE `sta_settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
